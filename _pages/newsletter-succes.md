---
layout: presentation
title: Vous êtes inscrit. Merci.
description: On vous recontacte bientôt.
permalink: /newsletter-succes/
ref: newsletter-succes
lang: fr
---

## Merci

Nous utilisons les [listes de diffusion](https://riseup.net/fr/lists) de [Riseup.net](https://riseup.net/).

[Riseup.net](https://riseup.net/) fournit des outils de communication en ligne pour les personnes et les groupes qui militent en faveur d'un changement social libérateur. Nous sommes un projet pour créer des alternatives démocratiques et pour pratiquer l'auto-détermination en contrôlant nos propres moyens de communication sécurisés. 

Les listes de [Riseup.net](https://riseup.net/) n’utilise que des logiciels libres, notamment le programme de gestion de listes courriel sympa, le serveur web apache, le système d’exploitation Debian GNU/Linux et le langage de programmation Perl.

Voici la liste de diffusion d'interhop : [https://lists.riseup.net/www/info/interhop](https://lists.riseup.net/www/info/interhop)

Vous pouvez vous en nous envoyant un courriel ou [ici](https://lists.riseup.net/www/sigrequest/interhop).


## Thank you

We use the [mailing lists](https://riseup.net/fr/lists) of [Riseup.net](https://riseup.net/).

[Riseup.net](https://riseup.net/) provides online communication tools for individuals and groups advocating for liberating social change. We are a project to create democratic alternatives and to practice self-determination by controlling our own secure means of communication.

The lists at [Riseup.net](https://riseup.net/) use only free software, including the cool email list management program, the apache web server, the Debian GNU/Linux operating system and the Perl programming language.

Here is the interhop mailing list: [https://lists.riseup.net/www/info/interhop](https://lists.riseup.net/www/info/interhop)

You can do this by sending us an email or [here](https://lists.riseup.net/www/sigrequest/interhop).
