---
layout: presentation
title: "Politique de confidentialité"
permalink: politique-confidentialite/
ref: pages-politique-confidentialite
lang: fr
---

# Introduction

L’association **InterHop** s’engage à ce que les traitements de vos données personnelles effectués sur son site, ainsi que sur **ses différents services HDS** :  
  - [goupile.hds.interhop.org](https://goupile.hds.interhop.org/){:target="_blank"}; 
  - [cryptpad.hds.interhop.org](https://cryptpad.hds.interhop.org/){:target="_blank"}. 
 
ou **ses services non HDS :**
  - [goupile.fr](https://goupile.fr/){:target="_blank"}; 
  - [goupile.interhop.org](https://goupile.interhop.org/){:target="_blank"}; 
  - [app.linkr.interhop.org](https://app.linkr.interhop.org/login/){:target="_blank"}
  - [linkr.interhop.org](https://linkr.interhop.org/){:target="_blank"}; 
  - [pad.interhop.org](https://pad.interhop.org/){:target="_blank"};
  - [cpad.interhop.org](https://cpad.interhop.org/){:target="_blank"}; 
  - [password.interhop.org](https://password.interhop.org/){:target="_blank"};
  - matrix.interhop.org;
  - [element.interhop.org](https://element.interhop.org/){:target="_blank"}.

soient conformes au Règlement Général sur la Protection des Données (ci-après « RGPD »), à la loi Informatique et Libertés (ci-après « LIL ») et aux autres lois applicables.

Pour ce faire, **InterHop** s’efforce de mettre en place toutes les précautions nécessaires à la préservation de la confidentialité et à la sécurité des données à caractère personnel (ci-après « données personnelles ») communiquées et traitées afin de faire obstacle notamment à leur destruction, leur perte, leur altération ou encore à une divulgation non autorisée.

La politique de confidentialité d’**InterHop** a donc pour objet de partager ses engagements en matière de collecte, de traitement et de transfert des données personnelles et, plus largement, en matière de protection des données personnelles.

Il est rappelé que la notion de donnée personnelle désigne toute information relative à une personne permettant de l’identifier directement ou indirectement, comme par exemple le nom, le numéro de téléphone, l’adresse IP ou encore l’adresse électronique.

# À qui s’adresse la politique d’InterHop ?

La politique de confidentialité de l'Association **InterHop** s’adresse aux visiteurs du site Internet **[InterHop.org](https://interhop.org)**, aux utilisateurs  de ses services HDS ou non HDS, à ses adhérents, donateurs, prospects, clients, fournisseurs ou  sous-traitants, employés, ou candidats pour une offre d’emploi au sein d’**InterHop** et, plus largement à toute personne dont les données personnelles seraient susceptibles d’être ou sont collectées, reçues, conservées, traitées, transférées et utilisées, quel que soit le format.
En utilisant le site internet d’**InterHop** et en communiquant vos données personnelles à **InterHop**, vous reconnaissez avoir lu et compris la présente politique de confidentialité et consentez à ce que vos données personnelles soient utilisées et traitées selon les termes de la présente politique, dans le cadre de la législation en vigueur.

# Les données recueillies

### Les informations que vous transmettez à InterHop

En utilisant notre site Internet et nos services, vous êtes amenés à nous transmettre des données personnelles permettant de vous identifier et/ou d’identifier votre organisation (société, association, établissement public…). Parmi ces informations figurent notamment votre identité, votre email, votre numéro de téléphone, votre identifiant, …
De plus, dans le cadre d’une relation contractuelle avec **InterHop**, divers documents sont nécessaires pour la bonne exécution de la relation : adresse, lieu d’exécution de la prestation, nom de l’organisation, nom et prénom du responsable de traitement, nom et prénom de l’interlocuteur privilégié, email, numéro de téléphone, numéro de SIREN, Numéro IC …

### Les données relatives aux réseaux sociaux

Dans le cas où vous vous connectez à nos services en utilisant les fonctionnalités de réseaux sociaux mises à votre disposition, **InterHop** aura accès à certaines de vos données telles que votre prénom, nom de famille, adresse email, de votre compte sur ledit réseau social conformément aux conditions générales d’utilisation du réseau social concerné. 

**NB :** En utilisant les réseaux sociaux, vos données personnelles sont susceptibles d’être diffusées, utilisées par tous et partout, sans votre consentement préalable.

### Les données relatives aux Cookies

Les sites web [interHop.org](https://interhop.org), [goupile.fr](https://goupile.fr/){:target="_blank"}, [goupile.interhop.org](https://goupile.interhop.org/){:target="_blank"}, [goupile.hds.interhop.org](https://goupile.hds.interhop.org/){:target="_blank"}, [app.linkr.interhop.org](https://app.linkr.interhop.org/login){:target="_blank"}, [linkr.interhop.org](https://linkr.interhop.org/){:target="_blank"},  [pad.interhop.org](https://pad.interhop.org/){:target="_blank"}, [cpad.interhop.org](https://cpad.interhop.org/){:target="_blank"}, [password.interhop.org](https://password.interhop.org/){:target="_blank"}, [element.interhop.org](https://element.interhop.org/){:target="_blank"}) et [cryptpad.hds.interhop.org](https://cryptpad.hds.interhop.org/){:target="_blank"} présentent les services administrés par l'association InterHop et n’utilisent pas de cookies.

### Pour les dons 
Vos dons sont essentiels pour garantir notre indépendance, pour développer des outils éthiques en santé, pour pérenniser les projets d'InterHop, pour changer la loi et appliquer le droit.
Pour les [dons](https://interhop.org/dons/), **InterHop** utilise les widgets du site [HelloAsso.com](https://www.helloasso.com/){:target="_blank"} qui contient des traceurs “Google Tag Manager”. 

### Les données de santé dites “sensibles”

Lorsque le responsable de traitement collecte des données de santé avec des logiciels fournis par l’association **InterHop** ces derniers sont hébergées chez [GPLExpert](https://gplexpert.com/){:target="_blank"}, certifié Hébergeur de données de Santé (ci-après HDS) sur les périmètres « Hébergeur d’infrastructure physique » et « Hébergeur infogéreur » (Art. R. 1111-11. – I ; 1 CSP).

Aujourd’hui, les données de santé traitées en utilisant les services via [goupile.hds.interhop.org](https://goupile.hds.interhop.org/){:target="_blank"} et [cryptpad.hds.interhop.org](https://cryptpad.hds.interhop.org/){:target="_blank"} bénéficient d’un HDS.

Le Datacenter de GPLExpert est situé exclusivement en France.

Les données personnelles traitées en utilisant les services non HDS sont également hébergées en France. Pour plus de détails, [voir les mentions légales](https://interhop.org/mentions-legales/).

### Les données chiffrées de bout-en-bout

En utilisant les services d'**InterHop** comme Crytpad (disponible sur [cryptpad.hds.interhop.org](https://cryptpad.hds.interhop.org/){:target="_blank"}  et [cpad.interhop.org](https://cpad.interhop.org/){:target="_blank"} ), Bitwarden (disponible sur [password.interhop.org](https://password.interhop.org/){:target="_blank"}) ou Matrix/Element (disponible sur [element.interhop.org](https://element.interhop.org/){:target="_blank"}) vos données personnelles sont chiffrées de bout en bout.  **Seul l'utilisateur final** est en mesure d'exercer ses droits RGPD tels que définis dans le paragraphe "Vos droits sur vos données personnelles" de ce document.

InterHop ne sera pas en mesure de supprimer les données personnelles en lien avec ces services puisque seul l'utilisateur final du service peut accéder à ses données.


# L’utilisation des données personnelles

Les données personnelles récoltées par **InterHop** sont toujours hébergées en France chez une entité uniquement soumise au droit européen.

Les données personnelles que vous transmettez à **InterHop** ou qu’**InterHop** collectent sont traitées notamment pour les finalités suivantes :
* L’exécution de nos obligations contractuelles respectives ;
* Création, gestion et maintien de sa relation d’affaires avec ses clients, prospects, fournisseurs et sous-traitants ;
* Fourniture de produits et de services, ainsi que leur gestion notamment administrative ;
* Traitement de la facturation et des paiements ;
* Prospection commerciale ;
* Études, analyses et statistiques marketing.

Selon la mission confiée à InterHop, au moins une des **conditions** suivantes, **base légale**, est remplie [(article 6 du RGPD)](https://www.cnil.fr/fr/reglement-europeen-protection-donnees/chapitre2#Article6):
a) la personne concernée a consenti au traitement de ses données à caractère personnel pour une ou plusieurs finalités spécifiques;
b) le traitement est nécessaire à l'exécution d'un contrat auquel la personne concernée est partie ou à l'exécution de mesures précontractuelles prises à la demande de celle-ci;
c) le traitement est nécessaire au respect d'une obligation légale à laquelle le responsable du traitement est soumis;
d) le traitement est nécessaire à la sauvegarde des intérêts vitaux de la personne concernée ou d'une autre personne physique;
e) le traitement est nécessaire à l'exécution d'une mission d'intérêt public ou relevant de l'exercice de l'autorité publique dont est investi le responsable du traitement;
f) le traitement est nécessaire aux fins des intérêts légitimes poursuivis par le responsable du traitement ou par un tiers, à moins que ne prévalent les intérêts ou les libertés et droits fondamentaux de la personne concernée qui exigent une protection des données à caractère personnel, notamment lorsque la personne concernée est un enfant.

# Les destinataires des données personnelles

**InterHop** s’engage à ne pas transmettre de données personnelles à une tierce partie sauf si la personne concernée a donné son accord préalable pour le partage de ses données avec des tiers, ou que le partage de ses données personnelles avec ladite tierce partie est nécessaire à la fourniture de produits ou de services, ou qu’une autorité judiciaire ou administrative compétente exige d’’**InterHop** qu’elle lui communique lesdites données personnelles.

# Durée de conservation

Les données personnelles sont conservées pendant toute la durée nécessaire aux finalités pour lesquelles elles ont été communiquées à **InterHop**, dans le respect de la réglementation applicable et sont, en tout état de cause, détruites à l’issue de celle-ci. 
À l’expiration de ces périodes, les données sont détruites de manière sécurisée ou archivées conformément aux dispositions de la délibération de la CNIL portant adoption d’une recommandation concernant les modalités d’archivage électronique de données à caractère personnel pour les organismes relevant du secteur privé, d’autre part (Art. L. 232-21-2 ; art. R. 232-46 ; art. R. 262-116-4 ; art. 241-19-3 du CASF et art. R. 1112-7du CSP).

# Transfert de vos données

**InterHop** conserve vos données personnelles au sein de l’Union Européenne (en France). Aucun transfert de données hors Union Européenne n’est effectué.

Dans l’hypothèse où **InterHop** devrait le faire, vous en seriez immédiatement informée. Nous vous indiquerions les mesures prises afin de contrôler ce transfert et de s’assurer du respect de la confidentialité des données. 

# Vos droits sur vos données personnelles

À tout moment, vous avez la faculté d’exercer l’un de vos droits en ce qui concerne vos données personnelles, à savoir. Conformément au RGPD et à la LIL, la personne concernée  dispose des droits suivants :
* **Droit à l’information :** **InterHop** organisme qui collecte des informations sur la personne concernée, propose une information claire sur l’utilisation des données de la personne concernée et sur ses droits.

* **Droit d’accès :** les personnes concernées peuvent soumettre une demande d’accès, obligeant **InterHop** à leur fournir une copie de toutes les données qu’elle détient à leur sujet. **InterHop** a un mois pour fournir ces informations, bien qu’il y ait des exceptions pour les demandes étant manifestement infondées, répétitives ou excessives. 

* **Droit de rectification :** si la personne  concernée découvre que les informations détenues à son sujet par **InterHop** sont inexactes ou incomplètes, elle peut demander à ce  qu’elles soient mises à jour. Comme pour le droit d’accès, **InterHop** a  un mois pour s’y conformer, et les mêmes exceptions s’appliquent.
* **Droit d’effacement :** dans certains cas, la personne  concernée peut demander à ce qu’**InterHop** supprime ses données. Par exemple lorsque les données ne sont plus nécessaires, lorsque les données sont traitées de manière illégitimes ou lorsqu’elles ne sont plus nécessaires aux fins pour lesquelles elles ont été collectées. Cela comprend les cas où la personne concernée retire son consentement. Le droit à l’effacement est également connu sous le nom de droit à l’oubli.  
* **Droit à la limitation du traitement :** la personne  concernée peut demander aux organisations de limiter l’utilisation de ses données personnelles. Il s’agit d’une alternative au droit d’effacement et peut être utile lorsqu’une personne  concernée conteste l’exactitude de ses  données personnelles ou lorsque les informations ne sont plus utiles mais qu’**InterHop** en a besoin pour établir, exercer ou défendre une revendication légale.
* **Droit d’opposition au traitement de ses données :** la personne  concernée peut s’opposer au traitement des données personnelles collectées sur la base de l’intérêt légitime ou de l’exécution d’une tâche d’intérêt public ou relevant de l’exercice d’une autorité publique.
**InterHop** doit arrêter de traiter des informations à moins de pouvoir avoir des raisons légitimes sérieuses au traitement, surpassant les intérêts, droits et libertés de la personne concernée ou si le traitement a pour but la mise en place ou l’exercice de la défense en cas de revendications juridiques.  
* **Droit à la portabilité :** la personne  concernée peut obtenir et réutiliser ses  données  personnelles à leurs propres fins et pour différents services. Ce droit ne s’applique qu’aux données personnelles que la personne concernée a fournies aux responsables du traitement via un contrat ou son consentement.
* **Droits liés à la prise de décision automatisée y compris le profilage :** Le RGPD comprend les dispositions concernant les décisions prises sans participation humaine, tel que le profilage, utilisant les données personnelles afin de faire des hypothèses calculées concernant les individus. Il y a des règles strictes concernant le type de traitement, et les individus peuvent contester et demander une révision du traitement s’ils croient que les règles ne sont pas suivies.
* **En cas de décès** et à défaut d’instructions de la part de la personne concernée, **InterHop** s’engage à détruire les données de la personne concernée, sauf si leur conservation s’avère nécessaire à des fins probatoires ou pour répondre à une obligation légale. 

* **Réclamation auprès de l'autorité de contrôle**
Enfin, vous pouvez également introduire une réclamation auprès des autorités de contrôle et notamment de la [CNIL](https://www.cnil.fr/fr/plaintes){:target="_blank"}.

### Pour exercer vos droits 

Pour **toute information ou exercice de vos droits Informatique et Libertés** sur les traitements de données personnelles gérés par **InterHop**, vous pouvez contacter le Délégué à la Protection des Données (DPO) d'**InterHop** :
* En envoyant un courrier à l’adresse suivante : **InterHop**,- A l’attention du Responsable DPD - Chez Me Alibert 37 Boulevard Magenta 35000 Rennes
* En envoyant un courriel à l’adresse suivante : <a href="mailto:dpo-interhop@riseup.net">dpo-interhop@riseup.net</a>

Veuillez cependant noter que si vous avez recours à nos services, les données personnelles relatives au nom de l’entité, l’adresse du siège social, l’adresse de livraison et les coordonnées de la personne s’occupant de la facturation sont obligatoires pour répondre à vos demandes et fournir les produits ou services demandés ; sans ces informations, **InterHop** ne pourra pas être en mesure de répondre à vos demandes et fournir les produits ou services demandés.

# Modification de la politique d’InterHop

**InterHop** peut être amené à modifier sa politique de confidentialité. Nous veillons à ce que vous en soyez informés via notre site internet et/ou [notre lettre d'information](https://interhop.org/nous/).

___________________________________

Vous reconnaissez avoir lu et compris la présente politique et consentez à ce que vos données personnelles soient utilisées et traitées selon les termes de la présente politique de confidentialité. A défaut, vous vous engagez à ne plus :
* Naviguer sur le site internet d'**InterHop**;
* Utiliser les services en ligne d'**InterHop**;
* Communiquer vos données personnelles à **InterHop** par tous moyens ; A contrario, **InterHop** ne sera pas en mesure de répondre à vos demandes et fournir les produits ou services demandés.
