---
layout: presentation
title: "Mentions légales"
permalink: mentions-legales/
ref: pages-mentions-legales
lang: fr
---

# Code entreprises

* SIREN : 885 179 655
* SIRET : 885 179 655 00018
* NAF (Nomenclature d’Activités Française) : 6311Z
* RNA (Répertoire National des Association) : W751256942
* Numéro TVA IC : FR47885179655

# Nous retrouver

* Siège social : Chez Adrien PARROT, 3 Lieu Dit le Val Jourdan, 35120 Saint-Marcan
* Site Web : [interhop.org/nous](https://interhop.org/nous) (ou [goupile.fr](https://goupile.fr/){:target="_blank"}, ou [linkr.interhop.org](https://linkr.interhop.org){:target="_blank"})
* Courriel : <a href="mailto:interhop@riseup.net">interhop@riseup.net</a> (ou <a href="mailto:hello@goupile.fr">hello@goupile.fr</a> ou <a href="mailto:linkr-app@pm.me">linkr-app@pm.me</a>)

# L'équipe

* Responsable de traitement et directeur de la publication : Adrien PARROT, Président de l’Association InterHop
* Co-directeur de la publication : Nicolas PARIS, Ingénieur BigData, Secrétaire
* Déléguée à la protection des données (DPD): Chantal CHARLOT  
@: <a href="mailto:dpo-interhop@riseup.net">dpo-interhop@riseup.net</a>

# Cookies

L'association InterHop édite [InterHop.org](https://interhop.org), [goupile.fr](https://goupile.fr/){:target="_blank"} et [linkr.interhop.org](https://linkr.interhop.org){:target="_blank"}. InterHop n’utilise pas de cookies.
En ce qui concerne les dons (page : [interhop.org/dons](https://interhop.org/dons) notamment), InterHop utilise les widgets du site [HelloAsso.com](https://www.helloasso.com/){:target="_blank"} qui contient des traceurs “Google Tag Manager”.

Lorsqu'un service opensource tierce sera délivré par InterHop les cookies non essentiels seront systématiquement désactivés.

# Hébergeur des sites

InterHop administre des services nécessitant un Hébergeur de Données de Santé (ci-après HDS) et des services qui ne le nécessitent pas.

* En ce qui concerne les services HDS, il s’agit de : 
  - [goupile.hds.interhop.org](https://goupile.hds.interhop.org/){:target="_blank"}; 
  - [cryptpad.hds.interhop.org](https://cryptpad.hds.interhop.org/){:target="_blank"}. 

* En ce qui concerne les services qui ne sont pas HDS, il s’agit de :
  - [goupile.fr](https://goupile.fr/){:target="_blank"}; 
  - [goupile.interhop.org](https://goupile.interhop.org/){:target="_blank"}; 
  - [app.linkr.interhop.org](https://app.linkr.interhop.org/login){:target="_blank"};
  - [linkr.interhop.org](https://linkr.interhop.org/){:target="_blank"}; 
  - [pad.interhop.org](https://pad.interhop.org/){:target="_blank"};
  - [cpad.interhop.org](https://cpad.interhop.org/){:target="_blank"}; 
  - [password.interhop.org](https://password.interhop.org/){:target="_blank"};
  - matrix.interhop.org;
  - [element.interhop.org](https://element.interhop.org/){:target="_blank"}.

Les données à caractère personnel (ci-après données personnelles) traitées en utilisant les services HDS sont hébergées en France par l’HDS certifié [GPLExpert](https://gplexpert.com/){:target="_blank"}. 

Les données personnelles traitées en utilisant les services ne nécessitant pas un HDS sont hébergés en France :
- Les sites web [InterHop.org](https://interhop.org) et [linkr.interhop.org](https://linkr.interhop.org){:target="_blank"} sont  hébergés en France chez Framagit ;
- Les services [pad.interhop.org](https://pad.interhop.org/){:target="_blank"} ([HedgeDoc](https://github.com/hedgedoc/hedgedoc){:target="_blank"}) , [cpad.interhop.org](https://cpad.interhop.org/){:target="_blank"} ([Cryptpad](https://github.com/cryptpad/cryptpad){:target="_blank"}), [password.interhop.org](https://password.interhop.org/){:target="_blank"} ([VaultWarden](https://github.com/dani-garcia/vaultwarden){:target="_blank"}), matrix.interhop.org et [element.interhop.org](https://element.interhop.org/){:target="_blank"} ([Matrix/Element](https://github.com/vector-im){:target="_blank"}) sont autohébergés par le secrétaire de l'association InterHop (en France) ;
- Les sites web de [goupile.fr](https://goupile.fr/){:target="_blank"}, [goupile.interhop.org](https://goupile.interhop.org/){:target="_blank"} ([Goupile](https://framagit.org/interhop/goupile){:target="_blank"}) et [app.linkr.interhop.org](https://app.linkr.interhop.org/login){:target="_blank"} ([LinkR](https://framagit.org/interhop/LinkR){:target="_blank"}) sont hébergés en France chez OVH (VPS).

# Informations personnelles

Conformément aux dispositions de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, et au Règlement Général sur la Protection des données, aucune information personnelle n’est collectée à votre insu ou cédée à des tiers. Pour exercer vos droits ou éclaircir une information intégrée à la politique de confidentialité d’InterHop, vous pouvez contacter la Déléguée à la Protection des Données (DPD). Pour cela, il vous suffit d’envoyer un courriel à <a href="mailto:dpo-interhop@riseup.net">dpo-interhop@riseup.net</a> ou un courrier par voie postale à Association InterHop, Chez Adrien PARROT, 3 Lieu Dit le Val Jourdan, 35120 Saint-Marcan, en justifiant de votre identité.

# Utilisation d’images et d’icônes
* Icônes : [flaticon.com](https://www.flaticon.com/){:target="_blank"}, [iconfinder.com](https://www.iconfinder.com/){:target="_blank"}
* Logo InterHop : Merci à Miguel Angel ARMENGOL DE LA HOZ 
* Logo Goupile : Merci à Niels MARTIGNENE
* Logo LinkR : Merci à Boris DELANGE

# Thèmes Jekyll utilisés

Pour [InterHop.org](https://interhop.org), merci à [CloudCannon](https://github.com/CloudCannon/urban-jekyll-template){:target="_blank"}

Vous retrouvez la politique de confidentialité d'InterHop [ici](https://interhop.org/politique-confidentialite/).

Document révisé le 17 novembre 2023

