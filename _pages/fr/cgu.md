---
layout: presentation
title: "Conditions Générales d’Utilisation"
permalink: cgu/
ref: pages-cgu
lang: fr
---

Les présentes conditions générales d’utilisation (dites « CGU ») ont pour objet l’encadrement juridique des modalités de mise à disposition de nos services ***non HDS*** dont voici la liste exhaustive :
- [goupile.fr](https://goupile.fr/){:target="_blank"};
- [goupile.interhop.org](https://goupile.interhop.org/){:target="_blank"};
- [app.linkr.interhop.org](https://app.linkr.interhop.org/login){:target="_blank"};
- [linkr.interhop.org](https://linkr.interhop.org/){:target="_blank"}; 
- [pad.interhop.org](https://pad.interhop.org/){:target="_blank"};
- [cpad.interhop.org](https://cpad.interhop.org/){:target="_blank"};
- [password.interhop.org](https://password.interhop.org/){:target="_blank"};
- matrix.interhop.org;
- [element.interhop.org](https://element.interhop.org/){:target="_blank"}.

Ces CGU **ne concernent pas les services HDS** pour lesquels un contrat est établi.

Toute inscription ou utilisation de nos services ***non HDS*** implique l’acceptation sans aucune réserve ni restriction des présentes CGU par l’utilisateur.trice.

En cas de non-acceptation des CGU stipulées dans le présent "contrat", l’utilisateur.trice se doit de renoncer à l’accès des services proposés par **« InterHop »**.

[InterHop](https://interhop.org/), pour améliorer ses services, se réserve le droit de modifier unilatéralement et à tout moment le contenu des présentes CGU.

Tous nos services reposent sur des logiciels opensources voire [libres](https://www.gnu.org/philosophy/open-source-misses-the-point.html){:target="_blank"} (FLOSS).

## Article 1 : Les mentions légales

Vous trouvez nos mentions légales [ici](https://interhop.org/mentions-legales/).

## Article 2 : Accès aux services

* **Goupile** 

Goupile est un [outil de conception et de publication d'eCRF]({{ site.baseurl }}/projets/goupile) ("case report form" ou cahier d'observation électronique) libre et gratuit. Goupile s'efforce de faciliter la création de formulaires de recueil de données destinées à la recherche en santé.

Pour vos projets, InterHop peut ouvrir des accès à Goupile et développer de nouvelles fonctionnalités adaptées à des besoins spécifiques.

*Pour y accéder :* [goupile.fr](https://goupile.fr/){:target="_blank"} et [goupile.interhop.org](https://goupile.interhop.org/){:target="_blank"} 

* **LinkR**

LinkR est une [plateforme collaborative et opensource de datascience en santé]({{ site.baseurl }}/projets/linkr)
- pour le.la *clinicien.ne*, elle permet d’accéder aux données de santé avec une interface graphique, ne nécessitant pas de connaissances en programmation.
- pour la *datascientist*, elle permet de manipuler les données via une interface de programmation R et Python
- pour l'*étudiant.e*, elle permet d’apprendre la manipulation des données médicales

Pour vos projets, InterHop peut ouvrir des accès à LinkR et développer de nouvelles fonctionnalités adaptées à des besoins spécifiques.

*Pour y accéder :* [app.linkr.interhop.org](https://app.linkr.interhop.org/login){:target="_blank"} et [linkr.interhop.org](https://linkr.interhop.org/){:target="_blank"}

* **Pad** 

Pad est un espace d’écriture collaborative, comme une page blanche, en ligne qui se synchronise au fur et à mesure.

*Pour y accéder :* [pad.interhop.org](https://pad.interhop.org/){:target="_blank"}

* **CPad** 

CryptPad est une alternative aux services cloud propriétaires (Drive). 

Ce logiciel est respectueux de la vie privée.
Tout le contenu stocké dans CryptPad est chiffré de bout-en-bout, i.e avant d’être envoyé, ce qui signifie que personne (même pas nous !) ne peut accéder à vos données à moins que vous ne lui donniez les clés .

*Pour y accéder :*  [cpad.interhop.org](https://cpad.interhop.org/){:target="_blank"}

* **Gestionnaire de mots de passe**

Un gestionnaire de mots de passe est un coffre-fort virtuel pour stocker l'ensemble de ses mots de passe (banque, impôts, plateforme de formation, assurances, sites d’achat…).

*Pour y accéder :* [password.interhop.org](https://password.interhop.org/){:target="_blank"}

* **Matrix** et **Element**

Matrix ou element est une applications de chat sécurisée.

Elle vous permet de garder le contrôle de vos conversations, à l’abri de l’exploitation de données et des publicités.

Les échanges réalisés via Matrix et Element sont chiffrés de bout en bout.

*Pour y accéder :* matrix.interhop.org et [element.interhop.org](https://element.interhop.org/){:target="_blank"}<br>
InterHop a activé la cooptation sur son instance Element. En conséquence celles et ceux qui veulent un compte doivent en faire la demande par courriel. Un jeton d'enregistrement sera retourné pour finaliser l’inscription.

Pour information, tous les frais supportés par l’utilisateur.trice pour accéder au service (matériel informatique, connexion Internet, etc.) sont à sa charge.

InterHop s'efforce de maintenir les services en fonctionnement nominal. Tout événement dû à un cas de force majeure ayant pour conséquence un dysfonctionnement, quel qu’il soit, et sous réserve de toute interruption ou modification en cas de maintenance, n’engage pas la responsabilité d'InterHop.

Vous avez besoin d’un de nos services, vous êtes utilisateur.tice, vous avez la possibilité de nous contacter par messagerie électronique à l’adresse email suivante : <a href="mailto:interhop@riseup.net">interhop@riseup.net</a>


## Article 3 : Collecte des données

Le site assure à l’utilisateur.trice une collecte et un traitement d’informations personnelles dans le respect de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, au RGPD et à [notre politique de confidentialité](https://interhop.org/politique-confidentialite/). 

En vertu de la loi Informatique et Libertés, en date du 6 janvier 1978 et dans le respect du RGPD, l’utilisateur.trice dispose d’un droit d’accès, de rectification, de suppression et d’opposition de ses données personnelles. L’utilisateur.trice exerce ce droit par mail à <a href="mailto:dpo-interhop@riseup.net">dpo-interhop@riseup.net</a> 

**La finalité** de l’usage des données à caractère personnel collectées reste uniquement la finalité prévue au départ.

**La conservation** : les données sont conservées pendant toute la durée nécessaire aux finalités pour lesquelles elles ont été communiquées à InterHop, dans le respect de la réglementation applicable et sont, en tout état de cause, détruites à l’issue de celle-ci.

## Article 4 : Propriété intellectuelle

Les marques, logos, signes ainsi que tous les contenus du site (textes, images, son…) font l’objet d’une protection par le Code de la propriété intellectuelle et plus particulièrement par le droit d’auteur.

L’utilisateur.trice doit solliciter l’autorisation préalable du site pour toute reproduction, publication, copie des différents contenus. Il s’engage à une utilisation des contenus du site dans un cadre strictement privé.

Toute représentation totale ou partielle des sites web [goupile.fr](https://goupile.fr){:target="_blank"} et [LinkR](https://linkr.interhop.org/){:target="_blank"} par quelque procédé que ce soit, sans l’autorisation expresse de l’exploitant du site Internet constituerait une contrefaçon sanctionnée par l’article L 335-2 et suivants du Code de la propriété intellectuelle.
Il est rappelé conformément à l’article L122-5 du Code de propriété intellectuelle que l’utilisateur.trice qui reproduit, copie ou publie le contenu protégé doit citer l’auteur et sa source.

## Article 5 : Responsabilité

Les sources des informations diffusées sur les sites [interhop.org](https://interhop.org), [goupile.fr](https://goupile.fr){:target="_blank"} et [LinkR](https://linkr.interhop.org/){:target="_blank"} sont réputées fiables mais les sites ne garantissent pas qu’il soit exempt de défauts, d’erreurs ou d’omissions.
Les informations communiquées sont présentées à titre indicatif et général sans valeur contractuelle. Malgré des mises à jour régulières, les sites [interhop.org](https://interhop.org), [goupile.fr](https://goupile.fr){:target="_blank"} et [LinkR](https://linkr.interhop.org/){:target="_blank"} ne peuvent être tenus responsables de la modification des dispositions administratives et juridiques survenant après la publication. De même, les sites ne peuvent être tenus responsables de l’utilisation et de l’interprétation de l’information contenue dans ce site.

L’utilisateur.trice s’assure de garder précieusement son identifiant et son mot de passe secret. Toute divulgation de l’identifiant et du mot de passe, quelle que soit la forme, est interdite. 

L’utilisateur.trice assume les risques liés à l’utilisation de son identifiant et mot de passe. Le site décline toute responsabilité.

Les sites web [interhop.org](https://interhop.org), [goupile.fr](https://goupile.fr){:target="_blank"} et [LinkR](https://linkr.interhop.org/){:target="_blank"} ne peuvent être tenus pour responsables d’éventuels virus qui pourraient infecter l’ordinateur ou tout matériel informatique de l’Internaute, suite à une utilisation, à l’accès, ou au téléchargement provenant de ce site.

La responsabilité du site ne peut être engagée en cas de force majeure ou du fait imprévisible et insurmontable d’un tiers.

**InterHop atteste que les outils qu’elle met à la disposition des étudiant.e.s permettent aux études de rester pseudonymisées dans la mesure où l'utilisateur construit son e-CRF sans intégrer de variable directement identifiante** (ex : nom, prénom, email, numéro de téléphone, date de naissance précise, NIR, etc.). La responsabilité d'InterHop ne peut pas être engagée si l'utilisateur.trice détourne un champ de saisie pour y saisir une information directement identifiante.

InterHop s'engage à prévenir la fuite de données par tous les moyens de sécurité à sa disposition.

## Article 6 : Liens hypertextes

Des liens hypertextes peuvent être présents sur le site. L’utilisateur.trice est informé qu’en cliquant sur ces liens, il sortira des sites [interhop.org](https://interhop.org), [goupile.fr](https://goupile.fr){:target="_blank"} et [LinkR](https://linkr.interhop.org/){:target="_blank"}. Ces derniers n’ont pas de contrôle sur les pages web sur lesquelles aboutissent ces liens et ne sauraient, en aucun cas, être responsables de leurs contenus.

## Article 7 : Cookies

Les services listés dans ces CGU n'utilisent pas de cookies.

## Article 8 : Droit applicable et juridiction compétente

La législation française s’applique au présent contrat. En cas d’absence de résolution amiable d’un litige né entre les parties, les tribunaux français seront seuls compétents pour en connaître.

Pour toute question relative à l'utilisation de Goupile et LinkR vous pouvez joindre l’éditeur aux coordonnées inscrites à l’article 1. Par contre InterHOP n'est pas éditeurs des autres services listés dans ces CGU.


***Si votre projet sort du cadre défini par les CGU (projets recherche, services HDS par exemple), vous devez contacter la Déléguée à la Protection des données à <a href="mailto:dpo-interhop@riseup.net">dpo-interhop@riseup.net</a>. Ce type de projet nécessitera l'élaboration d'un contrat.***

Document révisé le 17 novembre 2023
