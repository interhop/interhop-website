---
layout: presentation
title: "Privacy Policy"
ref: pages-politique-confidentialite
lang: en
permalink: en/politique-confidentialite/
---

# Introduction

TThe **InterHop** association undertakes to ensure that the processing of your personal data carried out on its site, as well as on **its various HDS** services :  
  - [goupile.hds.interhop.org](https://goupile.hds.interhop.org/){:target="_blank"}; 
  - [cryptpad.hds.interhop.org](https://cryptpad.hds.interhop.org/){:target="_blank"}. 
 
or **its non-HDS services:**
  - [goupile.fr](https://goupile.fr/){:target="_blank"}; 
  - [goupile.interhop.org](https://goupile.interhop.org/){:target="_blank"}; 
  - [app.linkr.interhop.org](https://app.linkr.interhop.org/login){:target="_blank"}
  - [linkr.interhop.org](https://linkr.interhop.org/){:target="_blank"}; 
  - [pad.interhop.org](https://pad.interhop.org/){:target="_blank"};
  - [cpad.interhop.org](https://cpad.interhop.org/){:target="_blank"}; 
  - [password.interhop.org](https://password.interhop.org/){:target="_blank"};
  - matrix.interhop.org;
  - [element.interhop.org](https://element.interhop.org/){:target="_blank"}.

comply with the General Data Protection Regulation (hereinafter "GDPR"), the French Data Protection Act (hereinafter "LIL") and other applicable laws.

To this end, **InterHop** shall take all necessary precautions to preserve the confidentiality and security of personal data (hereinafter "personal data") communicated and processed in order to prevent their destruction, loss, alteration or unauthorised disclosure.

The purpose of **InterHop**'s privacy policy is therefore to share its commitments with regard to the collection, processing and transfer of personal data and, more broadly, to the protection of personal data.

It is recalled that the notion of personal data refers to any information relating to a person that allows that person to be identified directly or indirectly, such as name, telephone number, IP address or e-mail address.

# To whom is InterHop's policy addressed?

The **InterHop** Association's privacy policy is intended for visitors to the **[InterHop.org](https://interhop.org/en)** website, users of its HDS or non-HDS services, members, donors, prospects, customers, suppliers or subcontractors, employees, or candidates for employment with **InterHop** and, more broadly, any person whose personal data may be or is collected, received, stored, processed, transferred and used, regardless of the format.
By using the **InterHop** website and providing your personal data to **InterHop**, you acknowledge that you have read and understood this privacy policy and consent to your personal data being used and processed in accordance with the terms of this policy, within the framework of the legislation in force.

# Data collected

### The information you provide to InterHop

By using our website and our services, you may be required to provide us with personal data that can identify you and/or your organisation (company, association, public institution, etc.). This information includes in particular your identity, your email, your telephone number, your identifier, etc.
In addition, in the context of a contractual relationship with **InterHop**, various documents are necessary for the proper execution of the relationship: address, place of execution of the service, name of the organisation, name and surname of the person responsible for processing, name and surname of the preferred contact person, email, telephone number, SIREN number, IC number, etc.

### Data relating to social networks

In the event that you connect to our services using the social network features made available to you, **InterHop** will have access to some of your data such as your first name, last name, email address, from your account on the said social network in accordance with the general terms of use of the social network concerned. 

**NB :** By using social networks, your personal data may be disseminated, used by anyone and everywhere, without your prior consent.

### Data relating to Cookies

Les sites web [interHop.org](https://interhop.org), [goupile.fr](https://goupile.fr/){:target="_blank"}, [goupile.interhop.org](https://goupile.interhop.org/){:target="_blank"}, [goupile.hds.interhop.org](https://goupile.hds.interhop.org/){:target="_blank"}, [app.linkr.interhop.org](https://app.linkr.interhop.org/login){:target="_blank"}, [linkr.interhop.org](https://linkr.interhop.org/){:target="_blank"}, [pad.interhop.org](https://pad.interhop.org/){:target="_blank"}, [cpad.interhop.org](https://cpad.interhop.org/){:target="_blank"}, [password.interhop.org](https://password.interhop.org/){:target="_blank"}, [element.interhop.org](https://element.interhop.org/){:target="_blank"}) et [cryptpad.hds.interhop.org](https://cryptpad.hds.interhop.org/){:target="_blank"} présentent les services administrés par l'association InterHop et n'utilisent pas de cookies.

### For donations 
Your donations are essential to guarantee our independence, to develop ethical health tools, to perpetuate InterHop's projects, to change the law and apply the law.
For [donations](https://interhop.org/en/dons/), **InterHop** uses widgets from the [HelloAsso.com](https://www.helloasso.com/){:target="_blank"} website which contains "Google Tag Manager" trackers. 

### "Sensitive" health data

When the data controller collects health data with software provided by the **InterHop** association, this data is hosted by [GPLExpert](https://gplexpert.com/){:target="_blank"}, a certified Health Data Host (hereinafter HDS) in the "Physical Infrastructure Host" and "Hosting Provider" categories (Art. R. 1111-11. - I; 1 CSP)

Today, health data processed using services via [goupile.hds.interhop.org](https://goupile.hds.interhop.org/){:target="_blank"} and [cryptpad.hds.interhop.org](https://cryptpad.hds.interhop.org/){:target="_blank"} benefit from an HDS.

The GPLExpert datacenter is located exclusively in France.

Personal data processed using non-HDS services are also hosted in France. For more details, [see legal notice](https://interhop.org/en/mentions-legales/).

### End-to-end encrypted data

Using **InterHop** services such as Crytpad (available at [cryptpad.hds.interhop.org](https://cryptpad.hds.interhop.org/){:target="_blank"} and [cpad.interhop.org](https://cpad.interhop.org/){:target="_blank"} ), Bitwarden (available at [password. interhop.org](https://password.interhop.org/){:target="_blank"}) or Matrix/Element (available at [element.interhop.org](https://element.interhop.org/){:target="_blank"}) your personal data is encrypted from end to end.  **Only the end user** is able to exercise his or her GDPR rights as set out in the "Your rights to your personal data" section of this document.

InterHop will not be able to delete personal data in relation to these services as only the end user of the service can access their data.


# Use of personal data

Personal data collected by **InterHop** is always hosted in France by an entity that is subject only to European law.

The personal data that you transmit to **InterHop** or that **InterHop** collects are processed in particular for the following purposes:
* The performance of our respective contractual obligations;
* The creation, management and maintenance of its business relationship with its customers, prospects, suppliers and subcontractors;
* Supply of products and services, as well as their management, particularly administrative;
* Processing of invoices and payments;
* Commercial prospecting;
* Studies, analyses and marketing statistics.

Depending on the task entrusted to InterHop, at least one of the following **legal requirements** is met [(Article 6 of the GDPR)](https://www.cnil.fr/fr/reglement-europeen-protection-donnees/chapitre2#Article6):
(a) the data subject has consented to the processing of his/her personal data for one or more specific purposes;
(b) processing is necessary for the performance of a contract to which the data subject is party or for the performance of pre-contractual measures taken at the request of the data subject;
(c) processing is necessary for compliance with a legal obligation to which the controller is subject
(d) processing is necessary to protect the vital interests of the data subject or of another natural person
(e) processing is necessary for the performance of a task carried out in the public interest or in the exercise of official authority vested in the controller
(f) processing is necessary for the purposes of the legitimate interests pursued by the controller or by a third party, unless the interests or fundamental rights and freedoms of the data subject which require the protection of personal data prevail, in particular where the data subject is a child.

# Recipients of personal data

**InterHop** will not pass on personal data to a third party unless the data subject has given prior consent to the sharing of his/her data with third parties, or the sharing of his/her personal data with such third party is necessary for the provision of products or services, or a competent judicial or administrative authority requires InterHop** to provide such personal data.

# Duration of retention

Personal data is kept for as long as is necessary for the purposes for which it was communicated to **InterHop**, in compliance with the applicable regulations and is, in any event, destroyed at the end of this period. 
At the end of these periods, the data is destroyed in a secure manner or archived in accordance with the provisions of the deliberation of the CNIL adopting a recommendation concerning the modalities of electronic archiving of personal data for organisations in the private sector (Art. L. 232-21-2; art. R. 232-46; Art. R. 262-116-4; Art. 241-19-3 of the CASF and Art. R. 1112-7 of the CSP).

# Transfer of your data

**InterHop** stores your personal data within the European Union (in France). No data is transferred outside the European Union.

If **InterHop** were to do so, you would be informed immediately. We will inform you of the measures taken to control this transfer and to ensure that the confidentiality of the data is respected. 

# Your rights over your personal data

At any time, you have the right to exercise any of your rights with respect to your personal data, namely. In accordance with the RGPD and the LIL, the data subject has the following rights:
* **Right to information:** **InterHop**, the organisation that collects information on the data subject, provides clear information on the use of the data subject's data and on his/her rights:

* **Right of access** : Data subjects may submit an access request, requiring **InterHop** to provide them with a copy of all data it holds about them. **InterHop has one month to provide this information, although there are exceptions for requests that are manifestly unfounded, repetitive or excessive. 

* **Right of rectification:** If the data subject discovers that information held about them by **InterHop** is inaccurate or incomplete, they may request that it be updated. As with the right of access, **InterHop** has one month to comply, and the same exceptions apply.
* **Right to erasure :** In some cases, the data subject may request that **InterHop** delete his or her data. For example, when the data is no longer necessary, when the data is processed illegitimately, or when it is no longer necessary for the purposes for which it was collected. This includes cases where the data subject withdraws consent. The right to erasure is also known as the right to be forgotten.  
* **The right to restrict processing :** the data subject can ask organisations to restrict the use of their personal data. This is an alternative to the right to erasure and can be useful where a data subject disputes the accuracy of their personal data or where the information is no longer useful but **InterHop** needs it to establish, exercise or defend a legal claim.
* **Right to object to the processing of your data**: The data subject may object to the processing of personal data collected on the basis of legitimate interest or the performance of a task carried out in the public interest or in the exercise of official authority.
**InterHop** must stop processing information unless it can show serious legitimate grounds for the processing which override the interests, rights and freedoms of the data subject or if the purpose of the processing is the establishment or exercise of the defence in case of legal claims.  
* **Right to portability:** the data subject may obtain and re-use his/her personal data for their own purposes and for different services. This right only applies to personal data that the data subject has provided to the data controllers via a contract or consent.
* **Rights related to automated decision making including profiling** : The GDPR includes provisions for decisions made without human involvement, such as profiling, using personal data to make calculated assumptions about individuals. There are strict rules about the type of processing, and individuals can challenge and request a review of the processing if they believe the rules are not being followed.
* **In the event of death** and in the absence of instructions from the data subject, **InterHop** undertakes to destroy the data subject's data, unless it is required to be retained for evidential purposes or to comply with a legal obligation. 
* **Complaint to the supervisory authority**.
Finally, you can also lodge a complaint with the supervisory authorities and in particular with the [CNIL](https://www.cnil.fr/fr/plaintes){:target="_blank"}.

### To exercise your rights 

For any information or to exercise your rights regarding the processing of personal data managed by **InterHop**, you can contact the Data Protection Officer (DPO) of **InterHop**:
* By sending a letter to the following address : **InterHop**,- A l’attention du Responsable DPD - Chez Me Alibert 37 Boulevard Magenta 35000 Rennes
* By sending an email to the following address : <a href="mailto:dpo-interhop@riseup.net">dpo-interhop@riseup.net</a>
Please note, however, that if you use our services, personal data such as the name of the entity, the address of the registered office, the delivery address and the contact information of the person in charge of billing are mandatory in order to respond to your requests and provide the requested products or services; without this information, **InterHop** will not be able to respond to your requests and provide the requested products or services.

# Changes to InterHop's Policy

**InterHop** may change its privacy policy from time to time. We will ensure that you are informed of such changes via our website and [our newsletter]({{ site.baseurl }}/en/nous/).

___________________________________

You acknowledge that you have read and understood this policy and consent to the use and processing of your personal data in accordance with this privacy policy. Otherwise, you agree not to:
* Browse the **InterHop** website;
* Use the online services of **InterHop**;
* Communicate your personal data to **InterHop** by any means; or **InterHop** will not be able to respond to your requests and provide the requested products or services.
