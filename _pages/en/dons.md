---
layout: presentation
title: Donate
ref: pages-dons
lang: en
permalink: en/dons/
---

<iframe id="haWidget" allowtransparency="true" src="https://www.helloasso.com/associations/interhop/formulaires/1/widget-bouton" style="width:100%;height:70px;border:none;"></iframe>

# Why donate?

To promote and finance alternative, free, decentralised, certified, interoperable and secure tools,

Donating to InterHop means spreading popular education initiatives for health research,

It means contributing to the emergence of ethical and decentralised health data hosts,

It means wanting to act as a watchdog for democracy on health and digital issues,

Giving to InterHop means understanding that health data is a [sensitive and precious matter]({{ site.baseurl }}/en/2019/12/10/donnees-de-sante-au-service-des-patients), which we must retain control of, and working to make this imperative before our public players,

To refuse to see our health data used in disregard of the European Data Protection Regulation, and to assert our rights.

It means refusing the excessive privatisation of data covered by [medical confidentiality]({{ site.baseurl }}/en/2020/05/27/covid-donnees-de-sante-microsoft), which cannot benefit the interests of GAFAM.

Patients, carers, researchers, we are all concerned by the preservation of medical confidentiality.




# What are your donations used for?

Your donations are essential to ensure the long-term viability of the digital tools we make available to everyone as part of an association.

Your donations are essential to guarantee our independence.


#### Funding certified servers
Your donations will be used to fund **certified servers** [‘Health Data Hosts’]({{ site.baseurl }}/en/projets/hds) to install websites and free/open source software for health.

We take our inspiration from the Framasoft association and invite you to have a look at what the [CHATONS.org](https://entraide.chatons.org){:target=‘_blank’} are doing outside healthcare.

#### Developing ethical e-health tools
These donations will then be used to **[install and develop free software]({{ site.baseurl }}/en/projets/logiciels)** in a secure enclave for healthcare.

#### Changing the law and applying the law
InterHop will coordinate a strategy of **[advocacy at national and European level]({{site.baseurl }}/en/projets/justice)** to defend your fundamental freedoms. Actions before the Court of Justice of the European Union and national courts are under consideration and in progress.

# How to give ?

### Hello Asso


<iframe id="haWidget" allowtransparency="true" scrolling="auto" src="https://www.helloasso.com/associations/interhop/formulaires/1/widget/en" style="width:100%;height:750px;border:none;" onload="window.scroll(0, this.offsetTop)"></iframe>

<div id="haWidgetMobile">
	<p>You will be transfered on HelloAsso.com.</p>
	<p><a class="button alt" style="text-align: center;" type="blue" href="https://www.helloasso.com/associations/interhop/formulaires/1/widget/en">Faire un don</a></p>
</div>

### Bank transfer

Transfers from bank to bank are possible free of charge: check with your bank!

Domiciliation : CREDIT COOPERATIF

#### Account identification for use in France

- Banque : 42559
- Guichet : 10000
- Compte : 08024330959
- Clé RIB : 32
- BIC : CCOPFRPPXXX

#### Account identification for international use (IBAN)

FR76 4255 9100 0008 0243 3095 932
