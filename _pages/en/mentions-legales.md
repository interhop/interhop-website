---
layout: presentation
title: "Legal Notices"
ref: pages-mentions-legales
lang: en
permalink: en/mentions-legales/
---

# Company code

* SIREN : 885 179 655
* SIRET : 885 179 655 00018
* NAF (Nomenclature d’Activités Française) : 6311Z
* RNA (Répertoire National des Association) : W751256942
* IC VAT number: FR47885179655

# Find us

- Head office : Chez Adrien PARROT, 3 Lieu Dit le Val Jourdan, 35120 Saint-Marcan
- Website: [interhop.org/en/nous]({{ site.baseurl }}/en/nous) (or [goupile.fr](https://goupile.fr/){:target="_blank"}, or [linkr.interhop.org](https://linkr.interhop.org){:target="_blank"})
- Email : : <a href="mailto:interhop@riseup.net">interhop@riseup.net</a> (or <a href="mailto:hello@goupile.fr">hello@goupile.fr</a> or <a href="mailto:linkr-app@pm.me">linkr-app@pm.me</a>)


# Team

- Responsible for processing and publication director : Adrien PARROT, President of the InterHop Association
- Co-director of the publication: Nicolas PARIS, BigData Engineer, Secretary
- Data Protection Officer (DPO): Chantal CHARLOT  @: <a href="mailto:dpo-interhop@riseup.net">dpo-interhop@riseup.net</a>

# Cookies

The InterHop association publishes [InterHop.org](https://interhop.org), [goupile.fr](https://goupile.fr/){:target="_blank"} and [linkr.interhop.org](https://linkr.interhop.org){:target="_blank"}. InterHop does not use cookies.
For donations (page: [interhop.org/dons]({{ site.baseurl }}/dons) in particular), InterHop uses widgets from the [HelloAsso.com](https://www.helloasso.com/){:target="_blank"} site, which contains "Google Tag Manager" tracers.

When a third-party opensource service is delivered by InterHop, non-essential cookies will be systematically deactivated.


# Hosting of the sites

InterHop administers services that require a Health Data Host (hereinafter HDS) and services that do not.


* HDS services include : 
  - [goupile.hds.interhop.org](https://goupile.hds.interhop.org/){:target="_blank"}; 
  - cryptpad.hds.interhop.org](https://cryptpad.hds.interhop.org/){:target="_blank"}. 


* For services that are not HDS, these are :
  - [goupile.fr](https://goupile.fr/){:target="_blank"}; 
  - goupile.interhop.org](https://goupile.interhop.org/){:target="_blank"}; 
  - [app.linkr.interhop.org](https://app.linkr.interhop.org/login){:target="_blank"};
  - [linkr.interhop.org](https://linkr.interhop.org/){:target="_blank"}; 
  - [pad.interhop.org](https://pad.interhop.org/){:target="_blank"};
  - [cpad.interhop.org](https://cpad.interhop.org/){:target="_blank"}; 
  - [password.interhop.org](https://password.interhop.org/){:target="_blank"};
  - matrix.interhop.org;
  - [element.interhop.org](https://element.interhop.org/){:target="_blank"}.


Personal data (hereinafter referred to as personal data) processed using HDS services are hosted in France by the certified HDS [GPLExpert](https://gplexpert.com/){:target="_blank"}. 

Personal data processed using services that do not require an HDS are hosted in France:
- The [InterHop.org](https://interhop.org) and [linkR](https://linkr.interhop.org/){:target="_blank"} websites are hosted in France by Framagit;
- The services [pad.interhop.org](https://pad.interhop.org/){:target="_blank"} ([HedgeDoc](https://github.com/hedgedoc/hedgedoc){:target="_blank"}) , [cpad.interhop.org](https://cpad.interhop.org/){:target="_blank"} ([Cryptpad](https://github.com/cryptpad/cryptpad){:target="_blank"}), [password.interhop.org](https://password.interhop.org/){:target="_blank"} ([VaultWarden](https://github.com/dani-garcia/vaultwarden){:target="_blank"}), matrix.interhop.org and [element.interhop.org](https://element.interhop.org/){:target="_blank"} ([Matrix/Element](https://github.com/vector-im){:target="_blank"}) are self-hosted by the secretary of the InterHop association (in France);
- The [goupile.fr](https://goupile.fr/){:target="_blank"}, [goupile.interhop.org](https://goupile.interhop.org/){:target="_blank"} ([Goupile](https://framagit.org/interhop/goupile){:target="_blank"}) and [app.linkr.interhop.org](https://app.linkr.interhop.org/login){:target="_blank"} ([LinkR](https://framagit.org/interhop/LinkR){:target="_blank"}) websites are hosted in France by OVH (VPS).

# Personal information

In accordance with the provisions of law n ° 78-17 of January 6, 1978 relating to data processing, files and freedoms, and the General Regulations on Data Protection, no personal information is collected without your knowledge or transferred to some thirds. To exercise your rights or to clarify information included in InterHop's privacy policy, you can contact the Data Protection Officer (DPO). To do this, simply send an e-mail to <a href="mailto:dpo-interhop@riseup.net">dpo-interhop@riseup.net</a> or a postal letter to Association InterHop, Chez Adrien PARROT, 3 Lieu Dit le Val Jourdan, 35120 Saint-Marcan, justifying your identity.

# Use of images and icons

* Icons : [flaticon.com](https://www.flaticon.com/){:target="_blank"}, [iconfinder.com](https://www.iconfinder.com/){:target="_blank"}
* Logo InterHop : Thanks to Miguel Angel ARMENGOL DE LA HOZ 
* Logo Goupile : Thanks to Niels MARTIGNENE
* LinkR logo : Thanks to Boris DELANGE

# Jekyll themes used

For [InterHop.org](https://interhop.org), thanks to [CloudCannon](https://github.com/CloudCannon/urban-jekyll-template){:target="_blank"}

You can find InterHop's privacy policy [here]({{ site.baseurl }}/en/politique-confidentialite/).

Document revised on 17 November 2023
