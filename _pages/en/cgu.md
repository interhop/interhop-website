---
layout: presentation
title: "Terms of Service"
permalink: en/cgu/
ref: pages-cgu
lang: en
---

The purpose of these general terms and conditions of use (hereinafter referred to as the "GCU") is to provide a legal framework for the terms and conditions under which our ***non HDS*** services are made available:
- [goupile.fr](https://goupile.fr/){:target="_blank"};
- [goupile.interhop.org](https://goupile.interhop.org/){:target="_blank"};
- [/app.linkr.interhop.org](https://app.linkr.interhop.org/login){:target="_blank"};
- [linkr.interhop.org](https://linkr.interhop.org/){:target="_blank"}; 
- [pad.interhop.org](https://pad.interhop.org/){:target="_blank"};
- [cpad.interhop.org](https://cpad.interhop.org/){:target="_blank"};
- [password.interhop.org](https://password.interhop.org/){:target="_blank"};
- matrix.interhop.org;
- [element.interhop.org](https://element.interhop.org/){:target="_blank"}.

These GCU **do not apply to HDS** services for which a contract has been drawn up.

Any registration or use of our ***non HDS*** services implies the user's unreserved and unrestricted acceptance of these GCU.

In the event of non-acceptance of the GCU stipulated in the present "contract", the user must renounce access to the services offered by **"InterHop "**.

In order to improve its services, [InterHop](https://interhop.org/) reserves the right to unilaterally modify the content of these GCU at any time.

All our services are based on open source software or [libres](https://www.gnu.org/philosophy/open-source-misses-the-point.html){:target="_blank"} (FLOSS).

## Article 1 : Legal notice

You can find our legal notice [here](https://interhop.org/en/mentions-legales/).

## Article 2 : Access to the services

* **Goupile** 

Goupile is a [tool for designing and publishing eCRF]({{ site.baseurl }}/en/projets/goupile) ("case report form" or electronic case report) free of charge. Goupile strives to facilitate the creation of data collection forms for health research.

For your projects, InterHop can open access to Goupile and develop new functionalities adapted to specific needs.

*To access it:* [goupile.fr](https://goupile.fr/) and [goupile.interhop.org](https://goupile.interhop.org/) 

* **LinkR**

LinkR is a [collaborative and opensource health datascience platform]({{ site.baseurl }}/en/projets/linkr)
- for the *clinician*, it provides access to health data via a graphical interface, requiring no programming knowledge.
- for the *datascientist*, it enables data to be manipulated via an R and Python programming interface.
- for the *student*, it enables you to learn how to manipulate medical data.

For your projects, InterHop can open access to LinkR and develop new functionalities adapted to specific needs.

*To access it:* [app.linkr.interhop.org](https://app.linkr.interhop.org/login){:target="_blank"} and [linkr.interhop.org](https://linkr.interhop.org/){:target="_blank"}

* **Pad** 

Pad is a collaborative writing space, like a blank page, online that synchronizes as it goes along.

*To access it:* [pad.interhop.org](https://pad.interhop.org/)

* **CPad** 

CryptPad is an alternative to proprietary cloud services (Drive). 

This software is privacy friendly.
All content stored in CryptPad is encrypted end-to-end, i.e. before being sent, which means that nobody (not even us!) can access your data unless you give them the keys.

*To access it:* [cpad.interhop.org](https://cpad.interhop.org/)

* **Password manager**

A password manager is a virtual safe to store all your passwords (bank, taxes, training platform, insurance, shopping sites...).

*To access it:* [password.interhop.org](https://password.interhop.org/)

* **Matrix** and **Element**

Matrix or element is a secure chat application.

It allows you to keep control of your conversations, free from data mining and advertising.

Exchanges via Matrix and Element are encrypted from end to end.

*To access it:* matrix.interhop.org and [element.interhop.org](https://element.interhop.org/)<br>
InterHop has activated co-optation on its Element instance. Consequently, those wishing to create an account must request one by e-mail. A registration token will be returned to finalize the registration.

For your information, all costs incurred by the user to access the service (computer equipment, Internet connection, etc.) are at the user's expense.

InterHop will make every effort to keep the service running smoothly. Any event due to a case of absolute necessity having as a consequence a dysfunction, whatever it is, and subject to any interruption or modification in case of maintenance, does not engage the responsibility of InterHop.

You need one of our services, you are a user, you can contact us by email at the following address: <a href="mailto:interhop@riseup.net">interhop@riseup.net</a>


## Article 3 : Data collection

The site ensures the user a collection and processing of personal information in compliance with the law n°78-17 of January 6, 1978 relating to data processing, files and freedoms, the GDPR and [our privacy policy]({{ site.baseurl }}/en/politique-confidentialite/). 

Under the Data Protection Act of January 6, 1978 and in compliance with the GDPR, the user has a right to access, rectify, delete and oppose his personal data. The user can exercise this right by sending an e-mail to <a href="mailto:dpo-interhop@riseup.net">dpo-interhop@riseup.net</a> 

**The purpose** of the use of the personal data collected remains only the purpose initially planned.

**The conservation**: the data are kept for the duration necessary for the purposes for which they were communicated to InterHop, in compliance with the applicable regulations and are, in any case, destroyed at the end of it.

## Article 4 : Intellectual property

Trademarks, logos, signs and all site content (text, images, sound, etc.) are protected by the French Intellectual Property Code, and more specifically by copyright.

The user must request prior authorization from the site for any reproduction, publication or copy of the various contents. The user undertakes to use the contents of the site in a strictly private context.

Any total or partial representation of the [goupile.fr](https://goupile.fr){:target="_blank"} and [LinkR](https://linkr.interhop.org/){:target="_blank"} websites by any process whatsoever, without the express authorization of the website operator, constitutes an infringement punishable under article L 335-2 et seq. of the French Intellectual Property Code.
In accordance with article L122-5 of the French Intellectual Property Code, users reproducing, copying or publishing protected content must cite the author and source.

## Article 5 : Responsibility

The sources of the information published on the [interhop.org](https://interhop.org), [goupile.fr](https://goupile.fr){:target="_blank"} and [LinkR](https://linkr.interhop.org/){:target="_blank"} sites are deemed reliable, but the sites do not guarantee that they are free from defects, errors or omissions.
The information provided is for general guidance only and is not contractually binding. Despite regular updates, the [interhop.org](https://interhop.org), [goupile.fr](https://goupile.fr){:target="_blank"} and [LinkR](https://linkr.interhop.org/){:target="_blank"} sites cannot be held responsible for any changes in administrative or legal provisions occurring after publication. Likewise, the sites cannot be held responsible for the use and interpretation of the information contained in this site.


Users are responsible for keeping their login and password secret. Any disclosure of the login and password, in whatever form, is prohibited. 


The user assumes all risks associated with the use of his/her login and password. The site declines all responsibility.



The websites [interhop.org](https://interhop.org), [goupile.fr](https://goupile.fr){:target="_blank"} and [LinkR](https://linkr.interhop.org/){:target="_blank"} cannot be held responsible for any viruses that may infect the Internet user's computer or hardware following use, access or downloading from this site.

The site cannot be held liable in the event of force majeure or the unforeseeable and insurmountable act of a third party.

**InterHop certifies that the tools it makes available to students allow studies to remain pseudonymized insofar as the user builds his or her e-CRF without including any directly identifying variable** (e.g. surname, first name, email address, telephone number, precise date of birth, NIR, etc.). InterHop cannot be held responsible if the user hijacks an input field to enter directly identifying information.

InterHop undertakes to prevent data leakage using all available security measures.

## Article 6 : Hypertext links

Hypertext links may be present on the site. Users are informed that by clicking on these links, they will leave the [interhop.org](https://interhop.org), [goupile.fr](https://goupile.fr){:target="_blank"} and [LinkR](https://linkr.interhop.org/){:target="_blank"} sites. The latter have no control over the web pages to which these links lead, and are in no way responsible for their content.

## Article 7: Cookies

The services listed in these GCU do not use cookies.

## Article 8: Applicable law and jurisdiction

French law applies to this contract. In the event of failure to resolve a dispute between the parties amicably, the French courts shall have sole jurisdiction.

For any question relating to the use of Goupile and LinkR you can contact the publisher using the contact details given in article 1. However, InterHOP is not the publisher of the other services listed in these GCU.

***If your project falls outside the scope defined by the GCU (research projects, HDS services for example), you must contact the Data Protection Officer at <a href="mailto:dpo-interhop@riseup.net">dpo-interhop@riseup.net</a>. This type of project will require a contract to be drawn up.***

Document revised on 17 November 2023
