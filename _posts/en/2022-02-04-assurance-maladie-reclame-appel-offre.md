---
layout: post
title: "Health data: the French National Health Insurance calls for a call for tenders for the Health Data Hub"
categories:
  - HealthDataHub
  - Microsoft
  - CNAM
  - AFP
ref: assurance-maladie-reclame-appel-offre
lang: en
---

## AFP press release

Paris, Feb 3, 2022 (AFP) - The Health Data Hub, a megafile of French health data, should be the subject of a "call for tenders with the establishment of an independent commission" to choose its new host to replace Microsoft, says the board of directors of the National Health Insurance (Cnam) in an opinion published Thursday.

"Depreciated" by the controversy, the Health Data Hub needs "some safeguards to regain trust," says the board of the National Health Insurance (Cnam).

The body, which reiterates its "full support" for the project, has never digested the choice of the American giant Microsoft to host this public platform that is supposed to bring together all the health data of the French, in order to facilitate medical research.

This choice was made in the summer of 2019 "without a call for tender" and "in a relative emergency", despite the "risk of unauthorized access to data" from the United States, she stresses.

In the meantime, the Council of State and the Commission informatique et libertés (Cnil) have called to order the government, which had committed in February 2021 to find "a technical solution" that complies with national and European law within a period "not exceeding two years."

In order to repatriate the Health Data Hub on "a sovereign technological platform", the CNAM Council is asking for a transparent selection.

The public authorities should therefore "make public" the list of "nine eligible hosting solutions" identified, and "commit to a precise migration schedule".

Above all, it will have to "proceed with a call for tenders following an independent audit" and "publish the exhaustive specifications".

These proposals come at a time when the project has been on hold since the end of December, when the government withdrew its request for authorization from the CNIL, which is essential for the full implementation of the platform.

A temporary withdrawal justified by technical reasons, but also allowing to avoid a disavowal on this sensitive subject in the middle of the presidential campaign.

