---
layout: post
title: "Referral to the CNIL concerning the use of Google Analytics by many e-health actors"
categories:
  - RGPD
  - CNIL
  - GAFAM
ref: saisine-cnil-google-analytics
lang: en
---

Madam President of the CNIL,

Following the Schrems II ruling, and the opinion rendered by the Commission Nationale de l'Informatique et des Libertés in October 2020 before the Council of State, under number 444937, we remind you that health data are sensitive data.

Following the decision of the Austrian data protection authority having ruled on the use of Google Analytics, deemed illegal and contrary to the RGPD. 


<!-- more -->

That is why we ask the CNIL :
- To analyze the consequences of the Schrems II jurisprudence on the use of the Google Analytics service concerning all e-health actors and more specifically on those under-mentioned
- ASKS the regulator to stop the processing that would be illegal.

We have decided to make this letter public. 

This publicity contributes to the objective of transparency defended by your Commission. 

Thanking you for your attention to our request, please accept, Madam President, our most respectful regards.

Done in Paris, January 28, 2021

InterHop


# Long letter

Title: Referral to the CNIL concerning the use of Google Analytics by many e-health actors

## Regarding the C-311/18 ruling (Schrems II)

"The EU General Data Protection Regulation was adopted with a dual purpose : 
- To facilitate the free flow of personal data within the European Union, 
- while safeguarding the fundamental rights and freedoms of individuals, including their right to the protection of personal data."[^edpb_schrems]

"In its recent ruling C-311/18[^curia_cjue] (Schrems II) the Court of Justice of the European Union (CJEU) recalls that the protection afforded to personal data in the European Economic Area (EEA) must apply to data wherever they are located."[^edpb_schrems].  The transfer of personal data to third countries cannot be a way to weaken the protection that is afforded to European citizens under the GDPR. 
"The Court also states that the level of protection in third countries must be equivalent to that guaranteed in the EEA" [^edpb_schrems].

The Court had also held that "the requirements of US law [...] entail limitations on the protection of personal data which are not circumscribed in such a way as to satisfy requirements substantially equivalent to those required by EU law" [^curia_cjue]. 

## Regarding the CNIL opinion[^cnil_conseil_etat_2]
 
In its opinion rendered on the occasion of a dispute opened in October 2020 before the Conseil d'Etat [^conseil_etat_2], the CNIL questions at length the consequences of two US laws. These texts govern the powers of the intelligence services.

The first is the Foreign Intelligence Surveillance Act (FISA). It concerns the targeting of "persons reasonably believed to be outside the United States" and applies "to providers of electronic communications services." This opaque text applies to Microsoft.

The second is called the Executive Order. This text is a presidential decree that legalizes the interception techniques of signals "from" or "to" the United States.

According to the CNIL, and on the basis of these two texts, Microsoft remains subject to the injunctions of the American intelligence services, which can force it at any time to transfer all the data hosted.

On November 10, 2020, the European Data Protection Committee therefore recalled that, in light of recent European case law, the supervisory authorities ("European CNILs") "will suspend or prohibit data transfers in cases where, following an investigation or a complaint, they find that a substantially equivalent level of protection cannot be ensured" [^edpb_schrems].

## Regarding the recent decision of the Austrian Data Protection Authority (Datenschutzbehörde)

During the proceedings Google admitted [^google_analytics_admission] that :
> all data collected by Analytics [...] is hosted (i.e. stored and further processed) in the United States. 

[^google_analytics_admission]: [Using Google Analytics would violate GDPR, says Austria](https://www.blogdumoderateur.com/utiliser-google-analytics-violation-rgpd-autriche/)

The Austrian Data Protection Authority has ruled that this behavior is a violation of EU law[^decision_GA].

[^decision_GA]: [https://noyb.eu/sites/default/files/2022-01/E-DSB%20-%20Google%20Analytics_DE_bk.pdf](https://noyb.eu/sites/default/files/2022-01/E-DSB%20-%20Google%20Analytics_DE_bk.pdf)

Currently, many companies in the EU still use Google Analytics. By sending their collected data to Google in the United States they are illegally doing so.


According to Max Schrems, data protection activist and president of noyb.eu[^source_1]
> Instead of actually adapting their services to comply with the GDPR, American companies have tried to simply add text to their privacy policies and ignore the Court's ruling. Many European companies have followed suit instead of turning to legal options. Companies can no longer use U.S. cloud services in Europe. It's now been a year and a half since the Court of Justice confirmed this a second time, so it's long past time that the law was enforced as well.

[^source_1]: [Google Analytics use violates RGPD according to Austrian CNIL](https://www.lemondeinformatique.fr/actualites/lire-l-usage-de-google-analytics-viole-le-rgpd-selon-la-cnil-autrichienne-maj-85426.html)

## Concerning French e-health actors using Google Analytics

What about digital health companies? Many use this service provided by Google and named Google Analytics. Here is a non-exhaustive list: Recare, Qare[^qare], HelloCare[^hellocare], Alan[^alan], Therapixel[^therapixel], Implicity[^implicity], Medaviz[^medaviz], Medadom[^medadom], KelDoc[^keldoc], Maiia[^maiia] ...

[^qare]: [https://www.qare.fr/cookies](https://www.qare.fr/cookies)
[^hellocare]: [https://hellocare.com/confidentialite](https://hellocare.com/confidentialite)
[^alan]: [https://alan.com/privacy](https://alan.com/privacy)
[^therapixel]: [https://www.therapixel.com/privacy-policy/](https://www.therapixel.com/privacy-policy/)
[^implicity]: [https://www.implicity.com/privacy-policy/](https://www.implicity.com/privacy-policy/)
[^medaviz]: [https://www.medaviz.com/politique-de-confidentialite-3/#cookies](https://www.medaviz.com/politique-de-confidentialite-3/#cookies)
[^medadom]: [https://www.medadom.com/cookies](https://www.medadom.com/cookies)
[^keldoc]: [https://www.keldoc.com/a-propos/cookies](https://www.keldoc.com/a-propos/cookies)
[^maiia]: [https://www.maiia.com/donnees-personnelles](https://www.maiia.com/donnees-personnelles)
On its website Recare even mentions that "[personal] data may be processed outside the EEA, including in the United States of America. We have concluded EU standard contractual clauses with the service provider to ensure an adequate level of data protection"[^recare].

E-health actors must ensure that they are not subject, in whole or in part, to injunctions from third-party courts or administrative authorities requiring them to transfer data to them.

## Regarding the recommendations of the InterHop association

The InterHop Association
- RECALLS that health data are sensitive data as defined by the CNIL [^cnil_donnee_nodate]. 
- ASKS the CNIL to analyze the consequences of the Schrems II case law on the use of the Google Analytics service for all e-health actors and more specifically for those mentioned above
- ASKS the regulator to stop the processing that would be illegal.

We have decided to make this letter public. This publicity contributes to the objective of transparency defended by your Commission [^transparency].

Yours sincerely

Done in Paris, January 28, 2021




[^doctolib_encryption]: [Doctolib: "When you do the data encryption, the host doesn't matter"](https://www.frenchweb.fr/doctolib-lorsque-vous-faites-le-chiffrement-des-donnees-lhebergeur-a-peu-dimportance/402057)

[^cnil_donnee_nodate]: [https://www.cnil.fr/fr/definition/donnee-sensible](https://www.cnil.fr/fr/definition/donnee-sensible)

[^edpb_schrems]: [Recommendations 01/2020 on measures that supplement transfer tools to ensure compliance with the EU level of protection of personal data](https://edpb.europa.eu/sites/edpb/files/consultation/edpb_recommendations_202001_supplementarymeasurestransferstools_en.pdf)

[^curia_cjue]: [The Court invalidates Decision 2016/1250 on the adequacy of the protection provided by the EU-US Data Protection Shield](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

[^homomorph]: [Why the Health Data Hub is misrepresenting the reality of health data encryption on Microsoft Azure](https://interhop.org/2020/06/15/healthdatahub-travestit-le-chiffrement-des-donnees)

[^cnil_conseil_etat_2]: [CNIL Opinion, Conseil d'Etat, Memoire en Observation](https://www.documentcloud.org/documents/7224049-Me-moireCnilHDH.html)

[^conseil_etat_2]: [Health Data Hub and personal data protection: precautions must be taken while waiting for a permanent solution](https://www.conseil-etat.fr/actualites/actualites/health-data-hub-et-protection-de-donnees-personnelles-des-precautions-doivent-etre-prises-dans-l-attente-d-une-solution-perenne)

[^alan]: [Where we host our members' data and why](https://blog.alan.com/tech-et-produit/pourquoi-nous-hebergeons-sur-aws)

[^article]: [Cancellation of Privacy Shield must apply immediately, says European CNIL](https://www.usine-digitale.fr/article/l-annulation-du-privacy-shield-doit-s-appliquer-immediatement-tranche-la-cnil-europeenne.N989069)

[^transparency]: [StopCovid application: the CNIL draws the consequences of its controls](https://www.cnil.fr/fr/application-stopcovid-la-cnil-tire-les-consequences-de-ses-controles)

[^recare]: [Recare: Personal data protection policy](https://recaresolutions.fr/protection-des-donnees-personnelles/)

[^medadom]: [https://info.medadom.com/cgu](https://info.medadom.com/cgu)

[^doctolib_cgu]: [https://www.doctolib.fr/terms/agreement](https://www.doctolib.fr/terms/agreement)

[^lifen_cgu]: [https://www.lifen.fr/confidentialite](https://www.lifen.fr/confidentialite)

