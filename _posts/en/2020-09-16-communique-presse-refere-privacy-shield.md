---
layout: post
title: "Press release from the SantéNathon collective: Is France illegally transferring our health data to the United States?"
ref: communique-presse-refere-privacy-shield
lang: en
---

**Since April 2020, French health data has been centralized within Microsoft as part of the "state of health emergency". Some data is being transferred to the United States even though the European Court of Justice invalidated the "Privacy Shield" between Europe and the United States on July 16, 2020, on the grounds of an inadequate protection on American soil.
On September 16, 2020, an unprecedented collective of 18 organizations and personalities filed a complaint in front of the Council of State to denounce this illegal transfer of personal and sensitive data. This lawsuit follows a first significant action initiated on May 28th.**

<!-- more -->

Following the decree of April 21, 2020[^arrete_21], the data of french people going to the emergency room, but also their pharmacy data, laboratory results, responses to surveys on their experiences in connection with the COVID-19 epidemic, are uploaded on the Health Data Hub platform hosted by Microsoft without any call for tenders having been issued. Although the data is hosted on servers in the Netherlands, the data is transferred outside the European Union, particularly to the United States, as soon as it is processed or maintained, as attested by the CNIL in its decision of 20/04/2020[^CNIL_del].

However, on July 16, 2020, the Court of Justice of the European Union invalidated the "Privacy Shield"[^CJUE_PrivacyShield] ("data protection shield") between Europe and the United States. This European agreement adopted in 2016 allowed companies to legally transfer personal data (identity, online behavior, geolocation...) of European citizens to the United States. The Court of Justice of the European Union concluded that the protection of European personal data was insufficient with regard to the General Data Protection Regulation (GDPR). It denounces an omnipotent American surveillance system, or a lack of legal recourse open to European citizens.

To alert on this current transfer of health data covered by medical secrecy and carried out without any guarantee, a group of organizations and personalities from the medical and health sector, associations representing patients and users, open source software, unions of engineers and technicians, journalists and citizens filed an emergency appeal to the Council of State on September 16, 2020.

This appeal follows a previous action taken on May 28, 2020, which reported several irregularities in the processing of data on the Health Data Hub platform and major risks to fundamental rights and freedoms. In its response[^conseil_etat], the Council of State had considered that, at the time of the judgment, Microsoft was included in the list of organizations that had joined the "Privacy Shield".

This is no longer the case today. The plaintiffs are therefore asking the Council of State to suspend the processing and centralization of data within the Health Data Hub and, in doing so, to bring itself into line with the latest European case law. They also claim that the contractual commitments concluded between Microsoft and the Health Data Hub are insufficients.

It should be noted that the issue of digital sovereignty is currently at the heart of several parliamentary debates and that other actions are underway in Europe[^wsj] following the invalidation of the Privacy Shield. A new agreement is not expected for several months as the European Commissioner for Justice, Didier Reynders, pointed out on September 4, 2020: "Don't expect new EU-US data transfer deal anytime soon[^euractiv]".

List of 18 claimants :
- **The association Le Conseil National du Logiciel Libre (CNLL)** : "*So that the discourse on digital sovereignty does not remain empty words, strategic projects on the economic level and sensitive in terms of personal freedoms should not be entrusted to operators subject to jurisdictions incompatible with these principles, but to European actors who present serious guarantees on these subjects, including through the use of open and transparent technologies.*"
- **The Ploss Auvergne-Rhône-Alpes association**.
- **The SoLibre association**
- **The company NEXEDI**: "*It is wrong to say that there was no European solution. On the other hand, it is true that the Health Data Hub never responded to the providers of these solutions.*»
- **The National Union of Journalists (SNJ)** : " *For the National Union of Journalists (SNJ), the leading organization of the profession, these actions must make it possible to maintain the secrecy of health data of French citizens and protect the secrecy of journalists' sources, the main guarantee of independent information.*"
- **The Observatory for Transparency in Medicines Policies**
- **The InterHop association**: "*The cancellation of the Privacy Shield sounds the end of European digital naivety. However, a power struggle is taking place between the United States and the European Union concerning the transfer of personal data outside of our legal space.*
*In order to ensure the sustainability of our mutual healthcare system and in view of the sensitivity of the data involved, the hosting and services of the Health Data Hub must fall under the exclusive jurisdiction of the European Union.*"
- **The Federal Union of Doctors, Engineers, Executives, Technicians (UFMICT-CGT)**
- **L'Union Générale des Ingénieurs, Cadres et Techniciens (UGICT-CGT)**: "*For the CGT des cadres et professions intermédiaires (UGICT-CGT), this recourse is essential to preserve the confidentiality of data which has now become, in all fields, a market. As designers and users of technology, we refuse to allow ourselves to be dispossessed of the debate on digital technology on the pretext that it is technical. Only democratic debate will make it possible to place technological progress at the service of human progress!*"
- **The association Constances** : "*Volunteers of Constances, the largest health cohort in France, we are particularly aware of health data and their interest for research and public health. How can it be accepted that data from French citizens is now being transferred to the United States? How can we accept that, in the long term, all the health data of the 67 million French citizens will be hosted by Microsoft and therefore fall under American laws and surveillance programs?*"
- **The French Association of Hemophiliacs (AFH)**
- **The association "Actupiennes"**.
- **The National Union of Young General Practitioners (SNJMG)** : "*Data from care must not be used for any other purpose than to improve care. Guaranteeing the security of health data and their use for public health purposes alone is a priority for all health care providers."
- **The General Medicine Union (SMG)**: "*The security of health data is a major public health issue because it allows medical confidentiality. The Health Data Hub has so far shown no guarantee that the French people's health data will be truly secure, in particular because it has chosen to host this data at Microsoft, thus jeopardizing the medical secrecy that is so necessary for a healthy and efficient therapeutic relationship."*
- **The French Union for Free Medicine (UFML)** : "*Let's avoid the control of monopolistic systems potentially harmful to the health system and to citizens".*
- **Ms. Marie Citrini, in her capacity as User Representative on the Paris Hospitals Supervisory Board**"
- **Mr Bernard Fallery, Professor Emeritus in Information Systems**: *"The 'emergency' management claimed for the Healh Data Hub is a veritable textbook case of all the risks linked to the governance of massive data: digital sovereignty and storage without a specific purpose, but also risky technical centralization, a stranglehold on a digital commons, the oligopoly of the GAFAMs, the dangers of medical secrecy, a grid of traces and adjustments to behavior.*"
- **Mr Didier Sicard, doctor and professor of medicine at the University of Paris Descartes**: *"Offering Microsoft French health data that is among the best in the world, even if it is insufficiently exploited, is a fourfold mistake: enriching Microsoft for free, betraying Europe and French citizens, preventing French companies from participating in the data analysis".*




[^wsj]: [Ireland to Order Facebook to Stop Sending User Data to U.S.](https://www.wsj.com/articles/ireland-to-order-facebook-to-stop-sending-user-data-to-u-s-11599671980)

[^euractiv]: [Don’t expect new EU-US data transfer deal anytime soon, Reynders says](https://www.euractiv.com/section/data-protection/news/dont-expect-new-eu-us-data-transfer-deal-anytime-soon-reynders-says/)

[^arrete_21]: [Arrêté du 21 avril 2020 complétant l'arrêté du 23 mars 2020 prescrivant les mesures d'organisation et de fonctionnement du système de santé nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000041812657/)

[^CNIL_del]: [Mesures d’organisation et de fonctionnement du système de santé nécessaires pour faire face à l’épidémie de covid-19 dans le cadre de l’état d’urgence sanitaire ](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf)

[^CJUE_PrivacyShield]: [La Cour invalide la décision 2016/1250 relative à l'adéquation de la protection assurée par le bouclier de protection des données UE-États-Unis](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

[^conseil_etat]: [Conseil d'État, 19 juin 2020, Plateforme Health Data Hub](https://www.conseil-etat.fr/ressources/decisions-contentieuses/dernieres-decisions-importantes/conseil-d-etat-19-juin-2020-plateforme-health-data-hub)
