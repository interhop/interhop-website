---
layout: post
title: "Free software and open source software: what are the differences ?"
categories:
  - définition
ref: opensource-libre-difference
lang: en
---


It's fairly easy to understand what proprietary software is. The term ‘privateur’ is sometimes used; it helps us to better understand the deprivation of freedom associated with this type of software [^privateur].
On the other hand, we often mix up the notions of **free software** and **open source software**.

<!-- more -->

First of all, the idea of **"free “** is older than that of **”open source ’**.

In fact, the difference between free software and open source software is not necessarily easy to establish. It lies in the values and vision of the world that each person holds.

At the end of the 90s, some members of the free software community began to use **the term ‘open source’ instead of ‘free ’**, which led to the formation of two movements which today can work together while being quite separate.

The libre movement is social, while the open source movement focuses on the methodology for developing and distributing software.

The two movements do not want to be confused.

**Free software is a movement that supports philosophical and political values[^logiciel_libre]**.


According to Richard STALLMAN [^stallman], programmer, free software activist and initiator of the free software movement, the fundamental difference between the two concepts lies in their philosophy: *"Open source is a development methodology; free software is a societal movement ’*[^opensource-libre].

The definition of free software defended by Richard STALLMAN as early as 1980 is made up of 4 freedoms[^4_lib] :

1. Freedom to run the program, for any purpose

2. Freedom to study how the program works and adapt it to one's needs

3. Freedom to redistribute copies. **Underlying principle or philosophy: to help others**.

4. Freedom to improve the program and publish improvements. **Underlying philosophy: to benefit the whole community**.


## To sum up

**Free software is necessarily open source ** It is not necessarily free. The confusion arises from the fact that the word ‘free’ is translated as ‘libre’ as well as ‘gratuit’.

On the other hand **software can be open source without being free** as defined by the Free Software Foundation (FSF), the American non-profit organisation founded by Richard STALLMAN.

At [Interhop.org](https://interhop.org/) we use the term free software and the acronym FLOSS for Free/Libre/Open-source software (FLOSS). Free software is the opposite of proprietary software.
[Interhop.org](https://interhop.org/) installs and makes available free software for healthcare.

[^free_software]: [www.gnu.org : What is free software?](https://www.gnu.org/philosophy/free-sw.fr.html)

[^4_lib]: [https://vive-gnulinux.fr : 4 freedoms](https://vive-gnulinux.fr.cr/logiciel-libre/4-libertes/)

[^privateur]: [https://www.april.org/logiciel-privateur](https://www.april.org/logiciel-privateur)

[^stallman]: [https://fr.wikipedia.org/wiki/Richard_Stallman](https://fr.wikipedia.org/wiki/Richard_Stallman)

[^opensource-free]: [https://www.gnu.org/philosophy/open-source-misses-the-point.fr.html](https://www.gnu.org/philosophy/open-source-misses-the-point.fr.html)
