---
layout: post
title: "Associative Electronic Health Record"
categories:
  - Toobib
  - Dokos
  - Tiers-Lieux
ref: toobib-annonce
lang: en
---

We are delighted to be invited by the Fabrique des santés on Wednesday June 7 at 12:30 pm.

<!-- more -->

> La Fabrique des santés is a collective that facilitates open cooperation and the emergence of common ground in the field of health and care[^1].

[^1]: [https://www.fabsan.cc/](https://www.fabsan.cc/)

On this occasion, we will discuss free and opensource software to improve care, and in particular the computerized patient record ``ToobibPro`` backed by the prologiciel (ERP[^2]) [Dokos](https://maia-by-dokos.fr/){:target="_blank"}.

[^2]: [https://fr.wikipedia.org/wiki/Progiciel_de_gestion_int%C3%A9gr%C3%A9](https://fr.wikipedia.org/wiki/Progiciel_de_gestion_int%C3%A9gr%C3%A9)

Initially developed for the needs of midwives, it is intended to become a multi-professional healthcare software package, to be used in particular by health centers. Being opensource, the values of mutual aid, ethics, eco-responsibility, security and fair costs are at the heart of this project.

Recently, the Dokos project joined forces with [InterHop.org](https://interhop.org), a non-profit association that promotes, develops and makes available free and open-source software for healthcare. 

Dokos for midwives is now hosted on Interhop's HDS-certified servers[^3]. This allows costs to be shared with other services already offered by Interhop: [Goupile](https://goupile.hds.interhop.org/){:target="_blank"}, [Cryptpad](https://cryptpad.hds.interhop.org/){:target="_blank"}.


[^3]: [https://gplexpert.com/hebergement-donnees-sante-hds/](https://gplexpert.com/hebergement-donnees-sante-hds/)

Join Céline DECULTOT and Adrien PARROT on June 07 to create together the care software of the future, opensource and ethical by nature.

➡️ The presentation is available online [here](https://pad.interhop.org/p/zkxbnNSbs){:target="_blank"}.

➡️ The replay is here: [https://peertube.interhop.org/w/1xpBeqYRqt82zbZU2fG1jb](https://peertube.interhop.org/w/1xpBeqYRqt82zbZU2fG1jb){:target="_blank"}

➡️  If you'd like to chat with us about the Toobib project, you can do so on the Element instant messenger here: [https://matrix.to/#/#toobib:matrix.interhop.org](https://matrix.to/#/#toobib:matrix.interhop.org){:target="_blank"}

➡️ And reports on previous meetings, here: [https://pad.fabmob.io/s/Ns_ScQ7G2#](https://pad.fabmob.io/s/Ns_ScQ7G2#){:target="_blank"}

 <div class="responsive-video-container">
<iframe width="100%" height="500" src="https://pad.interhop.org/p/zkxbnNSbs#/" frameborder="0"></iframe>
</div>

