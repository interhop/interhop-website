---
layout: post
title: "InterCHU Days: January 21 report"
ref: journee-interchu-21-janvier 
lang: en
---

The InterCHU OMOP - France day on January 21 brought together about 60 participants from hospitals, public institutions and private companies.

<!-- more -->

The InterCHU days are mainly addressed to French-speaking people (engineers from the public sector, ...) using the OMOP model or wanting to use it in the coming months.

You can find [here](https://interhop.org/en/2021/01/08/reunion-interchu) the principles of exchange and mutual aid that we want to promote during these days.

This day was an opportunity to resume exchanges on interoperability in health. The participants presented the progress of their work on the following themes:
- ETL and creation of health data warehouses: Easter-Eggs, Marseille, Bordeaux, Toulouse, Lille, etc.
- development of tools: visualization of the patient journey, generation of data sets, extraction of characteristics, decentralized calculation, semantic mapping tool

You can find the presentations as well as the complete program at this link: [https://framagit.org/interhop/omop/journees_omop_france_interchu](https://framagit.org/interhop/omop/journees_omop_france_interchu){:target="_blank"}.

## Future actions

We plan to do a next thematic meeting on semantic alignment at the end of February. We will propose a date to interested people very soon. 

The next InterCHU meeting will take place in April.

An inventory of the software used in the CHUs for ETL sharing is also in progress.

## Online

We use an alternative to the digital giants.

We used the video conferencing solution BigBlueButton, opensource and protective of personal data as it is hosted by Octopuce, a company under French law (with servers in France, [https://www.octopuce.fr/mentions-legales/](https://www.octopuce.fr/mentions-legales/){:target="_blank"}). 

Thanks to them.
