---
layout: post
title: "Presidential decree: transatlantic exchange of personal data will remain at high risk for Europeans"
categories:
  - "Executive Order"
  - "Privacy Shield"
  - Schrems
ref: new-executive-order
lang: en
---

On March 25, 2022, President von der Leyen and President Biden announced that they had reached an agreement in principle on a new transatlantic data protection framework between the European Union and the United States. 
On October 7, President Biden therefore signed an executive order on "strengthening safeguards for U.S. signals intelligence activities"[^decret_prez_en_en_francais].

<!-- more -->

[^EO_press]: [FACT SHEET: President Biden Signs Executive Order to Implement the European Union-U.S. Data Privacy Framework](https://www.whitehouse.gov/briefing-room/statements-releases/2022/10/07/fact-sheet-president-biden-signs-executive-order-to-implement-the-european-union-u-s-data-privacy-framework/)


This agreement is crucial for the United States since, according to the US president, "transatlantic data flows are essential to the $7.1 trillion economic relationship between the EU and the United States"[^EO_press].
**The agreement should allow for the secure exchange of data on both sides of the Atlantic Ocean. This will not happen.**

This agreement is also crucial because it aims to put an end to the uncertainty in which thousands of companies were plunged when the European Union Court of Justice rejected two other transatlantic agreements, the Safe Harbor in 2015 and the Privacy Shield in 2020[^cjue_ps], considering that the United States does not guarantee "a level of protection substantially equivalent to that guaranteed within the Union"[^cjue_ps].


[^cjue_ps]: [The Court invalidates the 2016/1250 decision on the adequacy of the protection provided by the EU-US data protection shield](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

Even if it results in a new adequacy decision under [Article 45](https://gdpr-text.com/read/article-45/) of the GDPR, it will still be unlawful under the level of protection afforded by the GDPR. The Court of Justice of the European Union will do as it did with Safe Harbor in 2015 and Privacy Shield in 2020, it will invalidate it.

Like others[^klaba], we are aware that this decree comes in return for gas deliveries made by the USA to Europeans[^garnier]. The problem is that the fundamentals do not change and the US doctrine remains the same, based on mass surveillance, secrecy and no independent judicial remedy.

We will fight against this.

[^garnier]: ["Our personal data is under threat of another Atlantic crossing"](https://www.lemonde.fr/idees/article/2022/04/07/nos-donnees-personnelles-se-retrouvent-sous-la-menace-d-une-nouvelle-traversee-de-l-atlantique_6121031_3232.html)

[^klaba]: [https://twitter.com/olesovhcom/status/1507387995032330283](https://twitter.com/olesovhcom/status/1507387995032330283)

## Description of the new framework

The decree describes a framework for personal data of Europeans transferred to the United States. It describes how U.S. intelligence agencies can access personal data and the right of European citizens to hold those agencies accountable.

This text would put in place safeguards to guarantee the respect of European personal data.

A European citizen will be able to:

1/ contact the Civil Liberties Protection Officer of the Office of the Director of National Intelligence if he/she believes that the US intelligence agencies have accessed his/her personal data in an unjustified way.
This person is therefore responsible for ensuring that privacy and fundamental rights are respected by American intelligence agencies.

Note that the Director of National Intelligence is "under the direct authority and control of the President of the United States to serve as the coordinator of the Intelligence Community, a collection of the 17 principal intelligence agencies of the United States"[^wikipedia_director].

Two remarks:
- we quickly understand the dependency link since this Civil Liberties Protection Officer is under the supervision of the American intelligence. He is appointed and can be removed by the Director of National Intelligence in case of "misconduct, malfeasance, breach of security, dereliction of duty, or incapacity"[^decret_prez_en_francais].
- we note the absence of description as to the information of the persons as well as the right of access to their data. This is contrary to Articles 13 and 15 of the GDPR[^rgpd_13_15]



[^wikipedia_director]: [Director of National Intelligence](https://fr.wikipedia.org/wiki/Directeur_du_renseignement_national)
[^rgpd_13_15]: [CHAPTER III - Rights of the Data Subject](https://www.cnil.fr/fr/reglement-europeen-protection-donnees/chapitre3#Section2)


2/ Apply to the Data Protection Review Court. It consists of a jury appointed by the Attorney General of the United States. This court may review the compliance of the Civil Liberties Protection Officer's decisions.
The Court of Data Protection Review will have the power to investigate complaints from EU citizens.

The dependence on the President of the United States is also glaring: the Attorney General "shall be appointed by the President of the United States after ratification by the Senate"[^attorney]. 

The judges of this Court can be removed by the Attorney General in "cases of misconduct, malfeasance, breach of duty, or incapacity"[^decret_prez_en_en_francais].

[^attorney]: [U.S. Attorney General](https://fr.wikipedia.org/wiki/Procureur_g%C3%A9n%C3%A9ral_des_%C3%89tats-Unis)

## A protective framework?

The CJEU had invalidated the Privacy Shield on two grounds: lack of proportionality within the meaning of Article 52 of the Charter of Fundamental Rights (CFR) and lack of judicial remedy within Article 47 CFR[^chartre_union] [^noyb].

[^chartre_union]: [Charter of Fundamental Rights of the European Union](https://www.europarl.europa.eu/charter/pdf/text_fr.pdf)

[^noyb]: [U.S. surveillance order unlikely to satisfy EU law](https://noyb.eu/fr/le-nouveau-decret-americain-peu-de-chances-de-satisfaire-la-legislation-europeenne)

First, we all know that it is and will remain very difficult for a European citizen to prove that his "European" rights have been violated, let alone to obtain any redress.

Second, this Presidential Order "does not limit any signals intelligence collection technique authorized under the National Security Act of 1947, the Foreign Intelligence Surveillance Act of 1978, as amended (50 U.S.C. 1801 et seq.) (FISA), Executive Order 12333, or any other applicable law or Presidential directive"[^decret_prez_en_en_francais]. Secrecy remains in place and is expressed in these terms as:
- the reports of the Civil Liberties Protection Officer and the Data Protection Review Court are in fact "classified"[^decret_prez_en_en_francais].
- The list of authorized data processing purposes is public "unless the President determines that it would constitute a risk to the national security of the United States"[^decret_prez_en_en_francais].

[^decret_prez_en_en_francais]: [Executive Order on Strengthening Safeguards for U.S. Signals Intelligence Activities](https://interhop.org/2022/10/14/executive-order-enhancing)

Also we have noticed that neither the Civil Liberties Protection Officer nor the Data Protection Review Court are independent from the US executive branch.

![](https://noyb.eu/sites/default/files/styles/media_xlarge/public/2022-10/clpo-grafik_ohne-logo_2700x900px_0.png?itok=cp2s5yr8)

The two-step procedure, agent reporting to the Director of National Intelligence and "Data Protection Review Court" reporting to the Attorney General, therefore does not meet the definition of "Court" in the normal legal sense of Article 47 of the Charter of the Law of the Union[^chartre_union] or even of the US Constitution.


Finally, the pillar texts of American intelligence such as FISA and Executive Order 12333 remain relevant. Thus, bulk collection remains relevant, even though it is the main reason for the invalidity of the last adequacy treaty: the Privacy Shield.
> "Bulk collection of signals intelligence (...) is authorized if (...) an element of the intelligence community (...) determines that the information necessary to advance a validated intelligence priority cannot reasonably be obtained through targeted collection."[^decret_prez_en_en_francais]

# Conclusion

This presidential order must now get "an opinion from the European Data Protection Board (EDPB) and the green light from a committee of EU member state representatives. In addition, the European Parliament has a right of scrutiny over adequacy decisions"[^edbp]. 




InterHop and its partners will remain alert to this issue. We will act to try to keep the protection of personal data as far away as possible from strategic interests outside Europe. Especially when it comes to health data, which are sensitive by nature.

The new adequacy decision could be adopted by the end of the year. 
If adopted, it will be challenged in the European Court of Justice.

A coordinated reaction from the public and private sectors, at national and European level, must occur quickly. 

The risk is that companies will use "this decree as a legal basis for the transfer of data between the United States and the European Union, even before it is approved by the European Commission"[^politico] and while this text does not protect European citizens.

[^politico]: [Biden executive order gives US power to determine if EU surveillance goes too far](https://www.politico.eu/article/us-president-joe-biden-executive-order-eu-privacy-framework-surveillance-programs/)


> It is clearly written in the text that the Court of Data Protection Review cannot confirm with the complainant whether there has been a request from the US intelligence services. This is not the transparency or impartiality that one would expect from an independent tribunal. One can also wonder about the accessibility of a long, expensive procedure, with an American judge rather than a European one, and a law based on the primacy of national security over the rights of citizens, whereas it is the opposite in Europe."[^tribune_clairement]

[^tribune_clairement]: [Transfert de données : un parcours semé d'embûches pour le nouveau cadre légal entre l'UE et les Etats-Unis](https://www.latribune.fr/technos-medias/transferts-de-donnees-un-parcours-seme-d-embuches-pour-le-nouveau-cadre-legal-entre-l-ue-et-les-etats-unis-936306.html)


We need to act fast especially since "winter is coming" and we need gas to heat us.

 

[^edbp]: [Questions & Answers: EU-U.S. Data Privacy Framework ](https://ec.europa.eu/commission/presscorner/detail/fr/qanda_22_6045)

