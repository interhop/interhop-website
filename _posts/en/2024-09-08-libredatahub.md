---
layout: post
title: "LibreDataHub.org : Open source data science platform"
categories:
  - DataThon2024
  - LibreDataHub
ref: libredatahub-blog
lang: en
---


> InterHop is proud to announce the launch of [LibreDataHub.org](https://libredatahub.org){:target=“_blank”} <br>
> LibreDataHub is a ubiquitous, opensource and secure data analysis platform.

<!-- more -->


LibreDataHub is a meta-project that integrates several open-source projects into a convenient, secure, turnkey solution.

LibreDataHub is easy to install on a Linux server.


# Presentation

With the organization of the [datathon](https://interhop.org/projets/datathon), InterHop is proud to announce the launch of its newest project: [LibreDataHub.org](https://libredatahub.org){:target=“_blank”}

LibreDataHub provides free tools for data warehousing, decentralized AI, statistics, machine learning (ML) and Deep Learning.

LibreDataHub is a scalable, modular, opensource and collaborative IT meta-project. LibreDataHub is assembled by healthcare data processing experts from the InterHop association.

LibreDataHub can be freely installed on a Linux machine (Debian, for example).

LibreDataHub is a project developed and maintained by the InterHop association.



# Technologies used

### Architecture diagram



![](https://pad.interhop.org/uploads/e4b99716-5528-4555-9821-278d232936a7.png)



### Shiny Proxy


![](https://pad.interhop.org/uploads/e7a1b1e3-d2dd-4cfc-8c69-485a68c8f145.png)


[Shiny Proxy](https://www.shinyproxy.io/){:target=“_blank”} is a java application for deploying applications. Resources (CPU and RAM) are shared between users.

The principle: the user connects to Shinyproxy, clicks on the application he wishes to run, a docker container of the application is launched and the application is displayed in the user's browser.


The following applications are installed within LibreDataHub's Shiny Proxy:
- Metabase
- LinkR
- Jupyter Notebook
- RStudio
- CloudBeaver
- SchemaSpy

Our special thanks go to the contributors to the Shiny Proxy project (including the [openanalytics](https://www.openanalytics.eu/){:target=“_blank”} team), who made the creation of LibreDataHub possible.


### Metabase

[Metabase](https://www.metabase.com/){:target=“_blank”} is a versatile Business Intelligence (BI) tool for data visualization, suitable for a wide range of users, for data exploration, analysis and decision support.

![](https://pad.interhop.org/uploads/fbbeb470-6c85-4ebf-9a2a-53148093a70a.png)


### LinkR

At the heart of LibreDataHub is [LinkR](https://linkr.interhop.org){:target=“_blank”}, an open-source web application developed by InterHop.

LinkR enables users to access, manipulate and analyze healthcare data with low-code tools, i.e. without the need for in-depth programming knowledge. LinkR uses the common data model [OMOP](https://ohdsi.github.io/TheBookOfOhdsi/){:target=“_blank”} to facilitate code exchange between multiple centers.

It provides both a graphical interface for clinicians and a programming environment for data scientists, making it ideal for collaborative healthcare projects.

![](https://pad.interhop.org/uploads/64b64fb7-556c-4f6e-9d02-b42cedd2c788.png)

### Jupyter


[Jupyter](https://jupyter.org/){:target=“_blank”} is a web application used for programming in over 40 languages, including Python, Julia, Ruby, R and Scala. Jupyter lets you create notebooks, i.e. programs containing both Markdown text and code in Julia, Python, R...
These code notebooks are used in data science to explore and analyze data.

![](https://pad.interhop.org/uploads/0db2ec40-7ebb-4c53-ab97-0b32ce3ec91a.png)


### CloudBeaver

[CloudBeaver](https://dbeaver.com/){:target=“_blank”} is a lightweight web application for working with different types of databases, all through a single, secure cloud solution accessible via a browser.

![](https://pad.interhop.org/uploads/a1e16eed-729b-4238-9fb1-c1b48c677fec.png)


### Schema Spy

[SchemaSpy](https://schemaspy.org/){:target=“_blank”} is designed to simplify the understanding and documentation of database schemas. It generates detailed reports and interactive diagrams of database structure, helping analysts and researchers to easily navigate complex databases. This tool is particularly useful for understanding the relationships between different data tables and ensuring data integrity.

![](https://pad.interhop.org/uploads/a5c4dd38-c22c-4032-987a-4225edb63fa7.png)

### Grafana

For data visualization and real-time monitoring, LibreDataHub integrates Grafana, an open-source web application that lets users create dynamic, customizable dashboards to visualize data.
LibreDataHub uses Graphana to monitor its technical infrastructure.

Here's the result of a dashboard collected during the datathon.
![](https://pad.interhop.org/uploads/0d971430-8f05-472d-a29a-1b07dc1b5fa7.png)

### DuckDB

LibreDataHub also integrates [DuckDB](https://duckdb.org/), an opensource database management system designed for data analysis.

Its support for columnar storage formats such as Parquet allows seamless integration with other LibreDataHub components, such as Jupyter Notebooks or LinkR, enabling high-performance data queries directly in the search environment.

# Development

[LibreDataHub.org](https://libredatahub.org){:target=“_blank”} is a project run by the InterHop association.

The source code for the LibreDataHub platform is available online: [https://framagit.org/interhop/libre-data-hub](https://framagit.org/interhop/libre-data-hub/datathon)

We welcome any suggestions for improvement or reports of problems: [https://framagit.org/groups/interhop/libre-data-hub/-/issues](https://framagit.org/groups/interhop/libre-data-hub/-/issues)

The InterHop team is on hand to host LibreDataHub on HDS servers, for user feedback within the association's network and for help with RGPD.
