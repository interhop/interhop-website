---
layout: post
title: "Le DMP devient Mon Espace Santé"
categories:
  - Mon Espace Santé
  - centralisation
  - opt-out
ref: mon-espace-sante
lang: fr
---

Le Dossier Médical Personnel devient le dossier médical partagé, lequel est intégré à l'Espace Numérique de Santé, dont l'ouverture est automatique depuis début 2022. Ce nouveau service s’appelle aussi **‘’Mon espace santé’’** MES. 


Le site dmp.fr disparaît et le DMP est totalement intégré au site monespacesante.fr. Les données seront automatiquement transférées dans **"Mon espace santé"**.
MES donne accès au DMP, à une messagerie sécurisée, à un agenda santé et à un catalogue d’applications référencées par l’État. 
<!-- more -->

# Officiellement, tes droits

**Mon Espace santé est en quelque sorte un carnet de santé informatisé accessible sur internet**. Tout bénéficiaire de l’assurance maladie peut donc l'avoir. 

Tu [crées](https://www.monespacesante.fr/enrolement-accueil) ton Espace Santé si tu le souhaites (sauf si tu le refuses plutôt) et tu décides qui y a accès (enfin, pas forcément). Ton Espace Santé contiendra tous les documents relatifs à ta santé : antécédents, comptes-rendus hospitaliers et radiologiques, résultats d'analyses de biologie, allergies, actes importants réalisés, don d’organes, médicaments prescrits et délivrés, directives anticipées.

Tu peux, quand tu le souhaites,  supprimer certains documents ou masquer certaines informations (presque à tout le monde).
Toi, les professionnels de santé autorisés par tes soins, et le régulateur du SAMU Centre 15, en cas d’urgence, avez accès à ton Espace Santé. Par contre, la médecine du travail n’y a pas accès.

Tu as le droit d’indiquer ton [opposition](https://www.monespacesante.fr/enrolement-accueil) à l’accès et de le modifier quand tu veux à partir des paramètres de ton compte sur le site.
En ligne, tu peux fermer ton Espace Santé à tout moment directement en te connectant.
Tu peux aussi fermer ton Espace Santé à tout moment auprès d’un établissement de santé ou lors d’une consultation médicale. 
Les données de ton Espace Santé sont conservées 10 ans après sa fermeture, puis supprimées. Pendant cette période, tu peux demander la réactivation de ton Espace.


L'hébergement centralisé est assuré par l'hébergeur Santeos[^santeos], hébergeur situé sur le territoire français et certifié. Santeos bénéficie d'une certification pour héberger des données de santé à caractère personnel.

[^santeos]: [Conditions générales d'utilisation - Mon Espace Santé](https://www.monespacesante.fr/cgu)

# Officieusement nos craintes

En tant que vigies de la démocratie, association de patient.e.s et militants de la société civile nous nous posons plusieurs questions.



## Information

Les premiers courriels d'information vont commencer à être envoyés cette semaine. Ils seront envoyés par l'Assurance maladie  et les premières ouvertures automatiques interviendront dans un délai de six semaines, soit au début du mois de mars 2022. Au total il est prévu d'envoyer plus de 40 millions de courriels ainsi que 28 millions de courriers postaux. Les courriels partiront à des dates différentes en fonction des départements[^dossier_presse_cnam].

[^dossier_presse_cnam]: [Décollage imminent pour découvrir un nouvel espace](https://solidarites-sante.gouv.fr/IMG/pdf/dossier_de_presse_lancement_de_mon_espace_sante_-_03.02.2022.pdf)

<img src="https://i.ibb.co/LZqhzbm/Screenshot-2022-02-07-at-08-50-04.png" alt="Screenshot-2022-02-07-at-08-50-04" border="0">


[^contexte_1]: [L’espace numérique de santé manque son départ](https://www.contexte.com/article/e-sante/lespace-numerique-de-sante-manque-son-depart_143718.html)


D'après le retour de la phase pilote dans 3 départements on apprend que : 
- sur 3 391 505 demande d'ouverture seulement 5.5 % des personnes répondent.
- parmis les répondants (186 989) 12 % des usagers s'opposent à la création de MES.

<img src="https://i.ibb.co/4th2fZ8/Screenshot-2022-01-14-at-15-58-18.png" alt="Screenshot-2022-01-14-at-15-58-18" border="0">

Moins de 5 % des assurés sollicités activent leur espace en ligne !
Mais grâce à  l'« opt-out », le taux de création du compte MES est très élevé et supérieur à 99 %.
Ces chiffres posent question. Surtout, peut-on parler d'une information claire, loyale et appropriée avec un taux de non réponse aussi élevé ?

> "Une information loyale est une information honnête, une information claire est une information intelligible, facile à comprendre, une information appropriée est une information adaptée à la situation propre à la personne soignée"[^info_patient]

[^info_patient]: [Information des patients : recommandations destinées aux médecins](https://www.has-sante.fr/upload/docs/application/pdf/infopat.pdf)

## Gestion des accès

La politique de gestion des accès doit être clarifiée.

D'emblée nous nous demandons si les ingénieurs informatiques assurant la maintenance auront accès aux données de santé ? Si oui en quelle proportion ?

Ensuite dans les décrets il est mentionné que le médecin traitant dispose de tous les accès aux données de ses patients, il est impossible de lui refuser l’accès ou de masquer des données. Ceci n'est pas acceptable.

## Evaluation de l'efficacité des outils

Des indices d'efficacité doivent être publiés régulièrement.

Concernant les outils numériques anti-covid la CNIL a plusieurs fois demandé au gouvernement des études d'efficacité de ces outils. Qu'en est-il de MES ? Le projet pilote montre t-il des parcours de soins plus courts ? Plus longs ? Diminue-t-il le nombre de prescriptions redondantes ? Qu'en est-il de l'illétrisme numérique ? Des régions mal dotées en accès numérique ?

La campagne de communication des prochains mois doit pleinement avoir lieu. Des marqueurs de son efficacité doivent être fournis. En fonction de ces derniers des mesures correctrices doivent être prises.


## Centralisation

Ces schémas montrent les différences entre un système centralisé et décentralisé.

Un système centralisé concentre les données en un point : un serveur.

![](https://www.i-manuel.fr/REBTS_MG2/REBTS_MG2part3dos1CO2doc3img1.jpg)

Au contraire un système décentralisé stocke moins de données en un seul point. Nous pensons qu'il est plus protecteur ; même si le risque zéro n'existe pas.

![](https://www.i-manuel.fr/REBTS_MG2/REBTS_MG2part3dos1CO2doc3img2.jpg)

Ce carnet de santé vise à termes à centraliser énormément de données de santé directement nominatives. 
Sachant qu'aucun acteur du numérique (y compris Microsoft[^microsoft]) ne peut se glorifier de n'avoir aucune faille et que la plupart des fuites sont d'origine interne aux organisations[^interne_orga], la question est la suivante : que se passera-t-il lors d'un piratage de données sensibles et qui valent de l'or ?
L'informatique n'est qu'une fuite en puissance de données à une échelle plus ou moins grande. Nous voulons minimiser ce risque.

[^interne_orga]: [The Biggest Cybersecurity Threats Are Inside Your Company](https://hbr.org/2016/09/the-biggest-cybersecurity-threats-are-inside-your-company) 

[^microsoft]: [Les hackers de SolarWinds ont accédé au code source de Microsoft Azure, Intune et Exchange](https://hbr.org/2016/09/the-biggest-cybersecurity-threats-are-inside-your-company)

Nous défendons une vision plus résiliente du stockage des données de santé : la décentralisation. Cette vision est mécaniquement protectrice car au sein d'un serveur le volume de données est minimisé. 


## Opt-out

> L'opt-in, c'est obtenir l'accord du destinataire de la publicité : s'il n'a pas dit "oui", c'est "non". 
>  L'opt-out, c'est lorsque le destinataire de la publicité ne s'est pas opposé : s'il n'a pas dit "non", c'est "oui"[^cnil_opt]. 

[^cnil_opt]: [Opt-in, opt-out, ça veut dire quoi ?](https://www.cnil.fr/fr/cnil-direct/question/opt-opt-out-ca-veut-dire-quoi)

Le législateur a prévu que l'accord de création de Mon Espace Santé soit réalisé en opt-out. Cela signifie qu'en l'absence de réponse, l'espace numérique sera créé.

Que penser d’une système de santé où « quand on ne dit pas non, c’est que c’est oui » ? Peut-on parler de consentement lorsque les possibilités de refuser ne sont pas les mêmes pour chacun·es ?

L'opt-out nous semble dangereux. En effet l'information de création de l'espace n'est faite que par courriel. Comment vérifier que les courriels ne passent pas dans les spams ? Comment vérifier que les usagers les ouvrent bien et les lisent en entier ?

<img src="https://i.ibb.co/7K5GDNL/Screenshot-2022-01-14-at-15-50-08.png" alt="Screenshot-2022-01-14-at-15-50-08" border="0">





## Catalogues de services

Quatre arrêtés fixant les modalités de fonctionnement de MES manquent encore :
- celui fixant le processus et les critères de référencement des services et outils numériques qui pourront être inscrits dans le catalogue de l’espace numérique de santé ;
- celui déterminant la composition de la commission de référencement qui statuera sur les demandes ;
- celui précisant les critères de qualité que devront respecter ces contenus numériques, et que la Haute Autorité de santé a commencé à définir ;
- celui qui publiera la matrice d’habilitation des accès des professionnels de santé au dossier médical partagé (DMP). Voici une première version non officielle : [https://www.monespacesante.fr/pdf/matrice-habilitations.pdf](https://www.monespacesante.fr/pdf/matrice-habilitations.pdf)

En effet "Mon Espace Santé" permettra dans le futur à d'autres services de s'interconnecter avec lui ; par exemple une prise de rendez-vous. Ces services seront opérationnels d’ici juin 2022, au fur et à mesure.

Le 16 décembre 2021 la DNS avait illustré la façon dont allait pouvoir s’organiser le catalogue d’applications mobiles et de services numériques de santé labellisés par l’État. 
> Elle répartit les outils en quatre segments : « accès aux soins et prise de RDV » (Doctolib, Qare…), « suivi de pathologies, traitements et vaccins » (Withings, Vidal, Abbott…), « suivi pré et post hospitalier » (Elsan, AP-HP…) et « suivi généraliste, prévention et bien-être » (VYV, FeelVeryBien qui commercialise l’appli Petit Bambou…)[^contexte_1].

Nous nous questionnons sur le modèle économique de ses acteurs. Qui paiera pour leur utilisation ? Le.la professionnel.le de santé, le.la patient.e, la CNAM, les contribuables ?

Nous nous questionnons sur la façon dont MES enverra des données à ses application.
> Notre perspective à plus long terme est de devenir producteur et/ou consommateur de données, assume quant à lui Vincent Vercamer, responsable accès au marché et affaires publiques de Withings[^contexte_2]
Nous nous interrogeons sur le recueil des consentements. Consentement fin entreprise par entreprise ou grossier ?

[^contexte_2]: [Les cinq ambiguïtés du catalogue de l’espace numérique de santé](https://www.contexte.com/article/e-sante/les-cinq-ambiguites-du-catalogue-de-lespace-numerique-de-sante_135864.html)

## Envoi des données vers Microsoft ?

Une crainte majeure est l'interfacage de MES avec le Health Data Hub HDH. Le HDH doit regrouper, à terme et entre autres, les données de la médecine de ville, des pharmacies, du système hospitalier, des laboratoires de biologie médicale, du dossier médical partagé, de la médecine du travail, des EHPAD ou encore les données des programmes de séquençage de l’ADN. Il est hébergé chez Microsoft Azure.
Actuellement aucun échange de données entre les deux structures n'est prévu. Mais que ce passera t-il si de façon unilatérale le Ministère de la Santé décidait qu'il est licite d'envoyer les données de MES vers le HDH ? Ceci s'est d'ailleurs déjà passé dans d'autres pays notamment au Royaume-Uni[^nhs_aws].


[^nhs_aws]: [Le Royaume-Uni ouvre l’accès complet aux données du NHS à Amazon](https://www.lebigdata.fr/royaume-uni-nhs-amazon)


