---
layout: post
title: "Téléconsultations et hébergement américain"
categories:
  - Téléconsultations
  - RGPD
  - USA
ref: teleconsultations-amendement
lang: fr
---

La commission des affaires sociales de l’Assemblée nationale a récemment adopté une proposition d’amendement[^amendement] visant à modifier la définition de la téléconsultation dans la loi.

<!-- more -->

Voici son contenu :
> "Les actes de téléconsultation doivent être réalisés par le biais d’une maison de santé pluridisciplinaire, d’une officine ou d’une collectivité afin de garantir un meilleur encadrement de cette pratique."

[^amendement]: [Amendement LFSS pour 2023 - (N°274)](https://www.assemblee-nationale.fr/dyn/16/amendements/0274/CION-SOC/AS902.pdf)


France Asso Santé[^fas] pointe fort justement une problématique importante :
> En l’état, ce texte signe donc la fin du remboursement des téléconsultations à domicile ! Il s’agit donc d’un retour de plus de 5 ans en arrière qui efface l’entrée de la téléconsultation dans les parcours de santé des Français, accélérée par la crise sanitaire du Covid-19.

Des associations de médecins mettent en avant que la téléconsultation se substitue, dans certains cas, parfaitement à la consultation physique[^sfar].

Mais le problème est-il là ?

# Hébergement des données de santé pour la téléconsultation



Nous allons regarder en détail l'hébergement choisi par les solutions de téléconsultations les plus utilisées[^qare_tele].

[^qare_tele]: [Téléconsultation : comparatif des principales plateformes](https://www.qare.fr/sante/teleconsultation/comparatif/)

<!-- https://emojipedia.org/flag-european-union/ -->
Voici la liste des principales solutions de téléconsultation médicale disponibles en 2021 avec en lien leurs différents hébergeurs:

- [Qare](https://cdn.qare.fr/mentionsLegales.pdf) : OVH 🇪🇺
- [Doctolib](https://info.doctolib.fr/politique-de-protection-des-donnees-personnelles/) : Vonage 🇺🇸 
- [Livi](https://elsan-livi.fr/mentions-legales/) : Amazon Web Services 🇺🇸
- [Maiia](https://www.maiia.com/mentions-legales) : CEGEDIM 🇪🇺
- [Hellocare](https://hellocare.com/mentions-legales) : Amazon Web Services 🇺🇸
- [Médecindirect](https://www.medecindirect.fr/mentions-legales) : Teladoc Health France SAS 🇪🇺
- [MesDocteurs](https://patient-plateforme.preprod.mesdocteurs.com/mentions-legales) : OLAS Hebergement 🇪🇺
- [Medadom](https://www.medadom.com/politique-de-confidentialite) : Amazon Web Services USA 🇺🇸
- [Medaviz](https://www.medaviz.com/mentions-legales/) : OVH 🇪🇺
- [Consulib](https://consulib-media.s3.eu-west-3.amazonaws.com/Mentions%2BLe%CC%81gales.pdf) : OVH 🇪🇺
- [Leah](https://legal.leah.care/conditions-generales-utilisation-service-leah.php) : Claranet 🇪🇺

Concernant les 3 principaux acteurs Qare🇪🇺, Doctolib🇺🇸 et Livi🇺🇸, un seul est hébergé par un acteur relevant "uniquement du droit européen".

# Conséquences réglementaires européennes

Dézommons maintenant et inscrivons nous dans une échelle européenne.

## Invalidation du Privacy Shield[^cnil_cpu]

Le 16 juillet 2020, la CJUE a jugé que la surveillance exercée par les services de renseignements américains sur les données personnelles des citoyens européens était excessive, insuffisamment encadrée et sans réelle possibilité de recours. Elle en a déduit que les transferts de données personnelles depuis l’Union européenne vers les États-Unis sont contraires au RGPD et à la Charte des droits fondamentaux de l’Union européenne, sauf si des mesures supplémentaires sont mises en place ou si les transferts sont justifiés au regard de l’article 49 du RGPD (qui prévoit des dérogations dans des situations particulières).

[^cnil_cpu]: [La CNIL appelle à des évolutions dans l’utilisation des outils collaboratifs états-uniens pour l’enseignement supérieur et la recherche](https://www.cnil.fr/fr/la-cnil-appelle-evolutions-dans-utilisation-outils-collaboratifs-etatsuniens-enseignement-superieur-recherche)

## Avis CNIL pour la Conférence des grandes écoles (CGE) et la Conférence des présidents d’université (CPU)[^cnil_cpu]

La Conférence des grandes écoles (CGE) et la Conférence des présidents d’université (CPU) ont ensuite interrogé la CNIL sur la conformité au RGPD de l’utilisation, dans l’enseignement supérieur et la recherche, d’outils collaboratifs proposés par certaines sociétés dont les sièges sont situés aux États-Unis. Les outils collaboratifs comme Microsoft Teams incorpore des solutions des visio-conférences[^teams].

Cette demande est donc analogue à la téléconsultation. Cette demande de conseil s’inscrit dans le prolongement de  l’invalidation du Privacy Shield par la Cour de justice de l’Union européenne (CJUE).

[^teams]: [ Microsoft Teams - À la maison, au travail ou à l’école, vos plus belles réalisations s’appuient sur le collectif](https://www.microsoft.com/fr-fr/microsoft-teams/group-chat-software)

Les documents transmis par la CPU et la CGE à la CNIL font apparaître, dans certains cas, des transferts de données personnelles vers les États-Unis dans le cadre de l’utilisation des « suites collaboratives pour l’éducation ». Dans les établissements qui emploient ces outils, les données traitées concernent potentiellement un nombre important d’utilisateurs (étudiants, chercheurs, enseignants, personnel administratif), et ces outils peuvent conduire au traitement d’une quantité considérable de données dont certaines sont sensibles (par exemple des données de santé dans certains cas) ou ont des caractéristiques particulières (données de la recherche ou relatives à des mineurs).

La CNIL se proposait même d'apporter "toute l’aide nécessaire à ces organismes afin de leur permettre d’identifier des alternatives possibles".

## Avis de la CNIL

Marie-Laure Denis l'a encore dit encore le 12 octobre lors d'une audition à l'assemblée nationale[^assemblee_nat] :
> Le collège de la CNIL est extrêmement attentif à ce que les données soient hébergées par des structures exclusivement soumises aux droits français et européen pour limiter l'accès aux données par des acteurs étrangers.

[^assemblee_nat]: [Commission des lois : Mme Marie-Laure Denis, présidente de la CNIL](https://videos.assemblee-nationale.fr/video.12274033_63467ff24a462.commission-des-lois--mme-marie-laure-denis-presidente-de-la-cnil-12-octobre-2022)

Cette doctrine constante était d'ailleurs énoncée dans le dernier référentiel[^conformite_ref] sur les entrepôts de données de santé. Ainsi :
> Seuls les entrepôts ayant recours à un sous-traitant relevant exclusivement des juridictions de l’Union européenne ou d’un pays considéré comme adéquat au sens de l’article 45 du RGPD sont conformes au présent référentiel.

[^conformite_ref]: [Référentiel relatif aux traitements des données à caractère personnel mis en oeuvre à des fins de création d'un entrepôts de données dans le domaine de la santé](https://www.cnil.fr/sites/default/files/atoms/files/referentiel_entrepot.pdf)


# Conclusion

Face aux GAFAM et notamment Amazon Web Service, Google et Microsoft nous traitons de problèmatiques juridique et de compliance entre l'Europe et les Etats-Unis.
Ceci est largement indépendant des aspects techniques, de sécurité des systèmes d'information et même de pertinence médicale de ces téléconsulations.

La plupart des patient.e.s sont en insécurité juridique avec les solutions de téléconsultation largement utilisées. **C'est même le secret médical qui n'est pas garanti.**

Alors que de nombreuses solutions garantes de la confidentialité et de la sécurité juridique existent nous appelons le législateur à garantir la confiance des patient.e.s dans le système de soin.

[^sfar]: [Télémédecine : une avancée sociétale stoppée par un (en)jeu politique !](https://sfar.org/telemedecine-une-avancee-societale-stoppee-par-un-enjeu-politique/)

[^fas]: [Téléconsultations : ne sanctionnons pas les usagers !](https://www.france-assos-sante.org/actualite/teleconsultations-ne-sanctionnons-pas-les-usagers/)
