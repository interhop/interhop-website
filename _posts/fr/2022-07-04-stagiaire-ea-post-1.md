---
layout: post
title: "L'arrivée des stagiaires à InterHop !"
categories:
  - Développement
  - EasyAppointments
  - Stagiaires
ref: stagiaire-ea-post-1
lang: fr
---



L'association InterHop est ravie de recevoir ses premiers stagiaires en développement web. Merci pour leur confiance.

Ils viennent de l'école d'informatique [```ENI Ecole informatique``` de Rennes](https://www.eni-ecole.fr/ecole/campus-physiques/){:target="_blank"}. 

<!-- more -->

Nous leur avons demandé de tenir un journal de bord hebdomadaire pour documenter leurs parcours au sein de notre association. Le voici.

Première semaine chez Interhop, nos missions consistent à adapter l'application open source [EasyAppointments.org](https://easyappointments.org/) à une utilisation médicale, pour une future intégration dans les outils [Toobib.org](https://toobib.org){:target="_blank"}.
Pour ce faire, nous devrons appliquer certaines modifications, tant sur la nomenclature que sur l'ajout de certaines fonctionnalités ou encore le respect des normes RGPD.

La première étape fut donc de s'approprier cette application, un tuto clair sur l'installation est fourni dans le [GitHub officiel d'EasyAppointments](https://github.com/alextselegidis/easyappointments){:target="_blank"}. Nous découvrons à ce moment que le framework utilisé, [CodeIgniter](https://www.codeigniter.com/){:target="_blank"} (CI), est bien trop loin de framework PHP que nous avions l'habitude d'utiliser avec notre équipe, à savoir, Symfony. 
En effet, il nous est compliqué de s'y retrouver, quel fichier gère quelle page, où se fait l'accès à la base de données, tant de questions ne nous permettant pas d'avancer efficacement et sans faire de graves erreurs. Par conséquent, nous avons décidé de suivre une [formation accélérée de ce framework](https://www.codeigniter.com/user_guide/index.html){:target="_blank"}.

C'est ainsi que la première semaine de stage fut une semaine de formation en totale autonomie sur un framework inconnu. Nous y avons appris l'arborescence d'un Projet CI, les classes et librairies mises à disposition pour nous aider dans notre travail de développeur, comment en créer ou en importer d'autres, comment respecter avec CI le modèle de développement le plus courant, le modèle MVC, Modèle-View-Controller.

Nous nous lançons donc dans la création de notre premier petit site. Notre site sera un blog. Il nous permettra d'affûter nos compétences afin de gérer l'affichage d'articles, l'accès à la base de données, la gestion des utilisateurs et de leurs droits, le tout en respectant les conventions web qui assurent une certaine sécurité et facilité de maintenance. 

Nous terminons en cette fin de première semaine notre entrainement, nous sommes ainsi fins prêts à plonger au cœur de notre mission, EasyAppointments !
