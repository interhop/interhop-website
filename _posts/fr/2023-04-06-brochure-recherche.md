---
layout: post
title: "Brochure recherche"
categories:
  - Recherche
  - "2023"
pdf_local: "/assets/pdf/brochure_v1_03042023_compressed.pdf"
ref: plaquette-recherche-2023
lang: fr
---

L'équipe d'InterHop est heureuse de vous présenter le guide de ses prestations dédiées à la recherche en santé.

<!-- more -->


N'hésitez pas à <a href="mailto:interhop@riseup.net"> nous contacter par courriel</a> !

{% pdf {{ page.pdf_local }} no_link width=100% height=1000px %}

Votre navigateur ne supporte pas les PDF. Vous pouvez <a href="{{ page.pdf_local }}">télécharger le fichier PDF</a>.


