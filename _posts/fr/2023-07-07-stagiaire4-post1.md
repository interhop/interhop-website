---
layout: post
title: "EasyAppointments - gestion des outils externes (entre autres)"
categories:
  - Développement
  - EasyAppointments
  - Stagiaires
ref: stagiaire4-ea
lang: fr
---

L’association InterHop est ravie d’avoir reçu sa quatrième promotion de stagiaires en développement web. Merci pour leur confiance.

De nouveau, ils viennent de l’école d’informatique [ENI Ecole informatique](https://www.eni-ecole.fr). Il s’agit de Tom Willem et Arthur Lepley.

<!-- more -->

Début d’année 2023 (troisième promotion) nous avions travaillé notamment sur la ”[suppression automatique des informations ainsi que le mode imprévu"](https://interhop.org/2022/12/14/stagiaire2-ea-post-1).

Voici la liste des fonctionnalités mises en place par cette dernière promotion de stagiaires.

# Affichage de la page des CGU

https://easyappointments-test.interhop.org/index.php/CGU

Les conditions générales d’utilisation ainsi que la politique de confidentialité ont été ajoutées sur une page externe, accessible depuis un lien dans le footer de l’application.

Il est disponible pour les patients dans le formulaire, et dans le back office.

La page est ainsi accessible depuis n’importe quel endroit de l’application.

![](https://pad.interhop.org/uploads/0727bdfa-d645-4d98-833e-b323277d24fd.png)

![](https://pad.interhop.org/uploads/2a03f439-3425-43d5-91c8-5d46c8b92cd5.png)

Le bouton "Retour" renvoie à la page consultée précédemment.


# Option basculer l’affichage côté patient

Cette fonctionnalité ajoute un paramètre dans le backend, accessible uniquement par l’administrateur. Un sélecteur permet de basculer entre deux modes sur la première page du formulaire de prise de rendez-vous :

- Choisir un soignant d’abord, puis choisir entre tous les types de  consultations qu'ils proposent.
- Choisir un type de consultation d’abord, puis choisir parmi les soignantes qui peuvent s’en occuper.

![](https://pad.interhop.org/uploads/3e2f3bec-5b8b-4a84-ba8e-d330cca5d634.png)


## L’option ‘Alterner l’affichage’.

Pour ajouter ce paramètre, il a fallu insérer une nouvelle entrée dans la table ```settings``` en base de données.

Voici les deux vues

### Affichage "Soignant d'abord"

![](https://pad.interhop.org/uploads/d182484b-ae2f-4a59-88ac-115fe52239b7.png)

### Affichage "Consultation d'abord"
![](https://pad.interhop.org/uploads/979e9824-3f02-4a8e-9be8-21ee3194b7f9.png)

Les consultations sont triées par catégories. En mode ‘consultation d’abord’, une option ‘premier soignant disponible est ajoutée à la fin pour la consultation choisie.

# Gestion des outils externes

La génération automatique de liens de visio et partage de documents étant en développement, une fonctionnalité pour gérer les outils informatiques externes et les associer aux consultations a été implémentée.

Cette gestion se passe dans la page ‘consultations’ du backend, accessible aux soignants et aux administrateurs. Un nouvel onglet ‘outils externes’ à été créé, avec possibilité d’ajouter, éditer et supprimer des outils.

![](https://pad.interhop.org/uploads/ae15a937-17e2-42dc-b4d8-b7486bd47318.png)

Sur l’onglet ‘consultations’, une liste de checkboxes permet de choisir les outils à associer à la consultation sélectionnée.

![](https://pad.interhop.org/uploads/93be7f3b-0587-4071-9d4c-56a8596f144d.png)


Pour faciliter la gestion, les outils sont triés par type : un champ dans l’onglet ‘outils externes’ permet de donner un type à l’outil sélectionné, et de créer et supprimer de nouveaux types à l’aide de prompts.

![](https://pad.interhop.org/uploads/71719220-bc4a-429f-96f8-637e3198a464.png)

![](https://pad.interhop.org/uploads/84e7dfb0-bf51-4bc5-98da-b0e84ebe8b1b.png)

![](https://pad.interhop.org/uploads/556d0b6f-a0f9-4aba-b9ec-da51b7e18670.png)


## La gestion des types d’outils

Une migration de la base de données a été nécessaire pour cette fonctionnalité : la table existante ```external_tools``` a été reliée à la table ```services``` par une table intermédiaire, et une nouvelle table ```types``` a été créée, reliée à ```external_tools``` par l’ID du type.

![](https://pad.interhop.org/uploads/bb015dc7-bbbf-4b49-bc63-e0060868fb18.png)


# Message ‘vérifiez dans vos spams’

Le message d’invitation à regarder dans le dossier des spams de sa boîte e-mail a été rajouté à la page d’envoi du code de vérification au moment de la prise de rendez-vous.


# Réinitialisation forcée du mot de passe de tous les utilisateurs

Ajout d’une fonctionnalité permettant la réinitialisation de tous les mots de passe par l’administrateur, dans la page "paramètres". Les mots de passe réinitialisés sont envoyés par email.

![](https://pad.interhop.org/uploads/b6aa73e2-e147-4dd9-b79b-7760fef1d302.png)

Cette fonctionnalité est en alpha !
Elle nécessite encore **des améliorations, comme une demande de validation** (par un prompt qui demande le mot de passe de l’admin, par exemple) avant de réinitialiser tous les mots de passe.
Il faut aussi faire en sorte que **le mot de passe de l’admin à l’origine de la réinitialisation soit exclu du changement**. :-)
