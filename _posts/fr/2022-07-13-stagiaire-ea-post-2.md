---
layout: post
title: "EasyAppointments - Le travail a bien commencé"
categories:
  - Développement
  - EasyAppointments
  - Stagiaires
ref: stagiaire-ea-post-2
lang: fr
---


L'association InterHop est ravie de recevoir ses premiers stagiaires en développement web. 

Après [leur arrivée](https://interhop.org/2022/07/04/stagiaire-ea-post-1), voici le résumé leur billet de blog résumant les deux dernières semaines.

<!-- more -->


La deuxième semaine a débuté par une exploration poussée du code source d'[EasyAppointments.org](https://easyappointments.org/) . 
Son architecture suit celle que nous avions étudié lors de notre découverte de [CodeIgniter](https://www.codeigniter.com/). 
A ceci près, les vues et les actions sur celles-ci, sont gérées par des fichiers JavaScript. 
Un challenge de plus à relever pour notre équipe ...

Afin de travailler de manière efficace, nous avons revu l'ensemble de la feuille de route avec Camille. Elle est médecin généraliste du [Syndicat de Médecine Générale (SMG)](https://syndicat-smg.fr/) et nous a fait l'honneur de partager avec nous son expérience du terrain. Le but étant de nous aider à saisir les subtilités propres à son métier. Il résulte de cet entretien une hiérarchisation des besoins d'Interhop, ce qui nous à permis de cibler précisement les fonctionnalités devant être présente au plus vite sur les premières itérations de la version Interhop d'EasyAppointments. 

En voici le programme !

1. Changer la traduction de certains termes pour qu'ils soient plus en adéquation avec le métier des professionnels de la santé. *semaine 2*
2. Dans l’interface du soignant, supprimer la flèche inutile qui indiquerait un changement de soignant et centrer l’agenda directement à partir de 8h ou d’un autre horaire. *semaine 2*
3. Garder par défault l'affichage des heures françaises et commencer la semaine par le lundi. *semaine 2*
4. Permettre aux soignants et aux secrétaires de changer les plages horairse. Actuellement, seul un administrateur peut le faire. *semaine 3*
5. Intégrer la gestion des types de prestation téléconsultation, au cabinet ou domicile avec des horaires dédiés pour chaque catégorie de consultation. *semaine 3-4*
6. Donner la possibilité aux soignants d'ajouter des rendez-vous en dehors des plages habituelles. *semaine 4*
7. Permettre aux soignants de bloquer des plages horaires réservées pour les consultations en urgences, puis de les libérer. *semaine 4*
8. Donner la possibilité à un soignant de voir soit la liste de ses patients, soit tous les patients enregistrés dans la base de données, via l'interface d'administration. *semaine ?*
9. Ajouter la confirmation du rendez-vous par email ou texto. Actuellement, seule l'adresse email sert d'identifiant pour les patients enregistrés dans la base de données. *semaine ?*
10. Interfaçage avec jitsi et possibilité de générer automatiquement un lien jitsi sur une URL dédiée si la prestation se fait en téléconsultation. *semaine ?*
11. Durant une prise de rendez-vous, ajouter l’obligation de remplir la case numéro de téléphone et adresse si l’acte est une visite à domicile. *semaine ?*
12. Ajouter un mode remplaçant avec par défaut le même planning que le soignant qu’il remplace. Notifier au patient, avant la confirmation du rendez-vous, que c’est le remplaçant qui va le recevoir. *semaine ?*
13. Vérifier que le mot de passe est bien chiffré coté client. *semaine ?*

Aprés un long moment d'étude du fonctionnement des scripts JavaScript (JS), nous nous attelons... à la traduction... Et oui, la tâche la plus simple. Ces scripts font d'EasyAppointments une véritable usine à gaz. La moindre modification du site nécessite des travaux d'une complexité souvent déraisonnable par rapport à l'ampleur du boulot. 
C'est pour cela qu'après avoir parcouru ces scripts, nous remettons au lendemain les fonctionnalités lourdes pour terminer calmement la journée avec ces simples traductions.

Après une bonne nuit de sommeil, il est enfin temps de rentrer dans le vif du sujet. Nous commençons par des fonctionnalités qui ne sont pas necessairement les mieux placées dans la liste, mais qui permettent de nous familiariser avec la logique du créateur et développeur principal d'EasyAppointment, Alex Tselegidis. 
Parmis les fonctionnalités énoncées ci-dessus nous réalisons d'abord des modifications concernant principalement le front-end, pour manipuler ces fameux scripts. Cette logique nous emmène à réaliser les tâches 2 et 3 de la liste. Bien que simples d'apparence, ces fonctionnalités demandent des modifications dans la base de données, la configuration et les vues.

La troisième semaine a été consacrée à rendre accessible le planning de travail aux soignants et à leurs secrétaires (fonctionnalité 4).
Nous avons pour cela modifié la base de données afin de créer un droit d'accès spécifique au planning et créé les pages correspondantes.
Nous avons aussi entamé la création de nouvelles catégories - téléconsultations/visites à domicile - avec la mise en place d'horaires dédiés, gérés par le soignant et/ou les secrétaires et proposés aux patients selon la catégorie de consultation choisie.

<img src="https://i.ibb.co/hgMRP4Y/image.png" alt="image" border="0">

Etant dans la création de plages horaires spécifiques, nous avons également commencé les horaires pour les urgences (fonctionnalité 7). Celles-ci ont la particularité d'être privées (non proposées à la prise de rendez-vous par le patient), car seul le soignant est à même de décider du caractère d'urgence du rendez-vous.

Afin de différencier ces nouvelles catégories de consultations sur le calendrier et d'améliorer la lisibilité, nous avons mis en place la possibilité de lui attribuer une couleur d'affichage de fond dans cette plage horaire.

La semaine prochaine nous permettra de finaliser l'affichage de ces différentes plages horaires sur le calendrier.

A bientôt
