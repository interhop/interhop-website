---
layout: post
title: "Saisine de la CNIL concernant l'utilisation par de nombreux acteurs de la e-santé des Google Analytics"
categories:
  - RGPD
  - CNIL
  - GAFAM
ref: saisine-cnil-google-analytics
lang: fr
---

Madame La Présidente de la CNIL,

Suite à l'arrêt Schrems II, et à l’avis rendu par la Commission Nationale de l'Informatique et des Libertés en octobre 2020 devant le Conseil d’État, sous le numéro 444937, nous rappelons que les données de santé sont des données sensibles.

Suite à la décision de l'autorité autrichienne chargée de la protection des données ayant statué sur l'utilisation de Google Analytics, jugés illégaux et contraire au RGPD. 

<!-- more -->

C’est pourquoi nous demandons à la CNIL :
- d'analyser les conséquences de la jurisprudence Schrems II sur l'utilisation du service Google Analytics concernant l'ensemble des acteurs de la e-santé et plus spécifiquement sur ceux sous-mentionnés
- DEMANDE au régulateur de stopper les traitements qui s'avèreraient illégaux.

Nous avons décidé de rendre public ce courrier. 

Cette publicité contribue à l’objectif de transparence défendu par votre Commission. 

En vous remerciant de l’attention que vous porterez à notre demande, nous vous prions d’agréer, Madame la Présidente,  nos salutations les plus respectueuses.

Fait à Paris, le 28 janvier 2021

InterHop


# Courrier long

Titre: Saisine de la CNIL concernant l'utilisation par de nombreux acteurs de la e-santé des Google Analytics

## Concernant l'arrêt C-311/18 (Schrems II)

"Le règlement général sur la protection des données de l'UE a été adopté dans un double but : 
- faciliter la libre circulation des données à caractère personnel au sein de l'Union européenne, 
- tout en préservant les libertés et droits fondamentaux des personnes, notamment leur droit à la protection des données à caractère personnel."[^edpb_schrems]

"Dans son récent arrêt C-311/18[^curia_cjue] (Schrems II) la Cour de Justice de l'Union Européenne (CJUE) rappelle que la protection accordée aux données à caractère personnel dans l'Espace économique européen (EEE) doit s'appliquer sur les données où qu'elles se trouvent"[^edpb_schrems].  Le transfert de données à caractère personnel vers des pays tiers ne peut être un moyen d'affaiblir la protection qui est accordée aux citoyen.ne.s européens dans le cadre du RGPD. 
"La Cour affirme également que le niveau de protection dans les pays tiers se doit d'être équivalent à celui garanti dans l'EEE"[^edpb_schrems].

La Cour avait aussi considéré que “les exigences du droit américain […] entraînent des limitations de la protection des données personnelles qui ne sont pas circonscrites de manière à satisfaire à des exigences essentiellement équivalentes à celles requises par le droit de l’UE”[^curia_cjue]. 

## Concernant l'avis de la CNIL[^cnil_conseil_etat_2]
 
Dans son avis rendu à l’occasion d'un contentieux ouvert  en octobre 2020 devant le Conseil d’État[^conseil_etat_2], la CNIL se questionne longuement sur les conséquences de deux textes de lois américains. Ces textes régissent les pouvoirs des services de renseignements.

Le premier est le Foreign Intelligence Surveillance Act (FISA). Il concerne le ciblage “des personnes dont on peut raisonnablement penser qu’elles se trouvent en dehors des États-Unis” et s’applique “aux fournisseurs de services de communications électroniques.” Ce texte opaque s’applique à Microsoft.

Le deuxième se nomme l’Executive Order. Ce texte est un décret présidentiel qui légalise les techniques d’interception des signaux “en provenance” ou “vers” les États-Unis.

Selon la CNIL, et sur la base de ces deux textes, Microsoft reste soumis aux injonctions des services de renseignements américains qui peuvent l’obliger à tout moment à transférer l’ensemble des données hébergées.

Le 10 novembre 2020, le Comité Européen de Protection des Données a donc rappelé qu'au regard de la toute récente jurisprudence européenne, les autorités de contrôle ("CNILs européennes") "suspendront ou interdiront les transferts de données dans les cas où, à la suite d'une enquête ou d'une plainte, elles constateront qu'un niveau de protection essentiellement équivalent ne peut être assuré"[^edpb_schrems].

## Concernant la récente décision de l'autorité autrichienne de la protection des données (la Datenschutzbehörde)

Au cours de la procédure Google a avoué[^google_analytics_avouement] que :
> toutes les données collectées par Analytics [...] sont hébergées (c'est-à-dire stockées et traitées ultérieurement) aux États-Unis. 

[^google_analytics_avouement]: [Utiliser Google Analytics serait une violation du RGPD, selon l’Autriche](https://www.blogdumoderateur.com/utiliser-google-analytics-violation-rgpd-autriche/)

L'autorité autrichienne de protection des données a statué que ce comportement constitue une violation du droit européen[^decision_GA].

[^decision_GA]: [https://noyb.eu/sites/default/files/2022-01/E-DSB%20-%20Google%20Analytics_DE_bk.pdf](https://noyb.eu/sites/default/files/2022-01/E-DSB%20-%20Google%20Analytics_DE_bk.pdf)

Actuellement, de nombreuses entreprises au sein de l’UE utilisent toujours Google Analytics. En transmettant leurs données récoltées à Google aux États-Unis elles sont dans l'illégalité.


D'après Max Schrems, militant de la protection des donnée et président de noyb.eu[^source_1]
> Au lieu d'adapter réellement leurs services pour être conformes au RGPD, les entreprises américaines ont essayé de simplement ajouter un texte à leurs politiques de confidentialité et d'ignorer la décision de la Cour de justice. De nombreuses entreprises européennes ont suivi le mouvement au lieu de se tourner vers des options légales. Les entreprises ne peuvent plus utiliser les services cloud américains en Europe. Cela fait maintenant un an et demi que la Cour de justice l'a confirmé une deuxième fois, il est donc plus que temps que la loi soit également appliquée.

[^source_1]: [L'usage de Google Analytics viole le RGPD selon la Cnil autrichienne](https://www.lemondeinformatique.fr/actualites/lire-l-usage-de-google-analytics-viole-le-rgpd-selon-la-cnil-autrichienne-maj-85426.html)

## Concernant les acteurs français de la e-santé utilisant les Google Analytics

Qu’en est-il des entreprises digitales en santé ? Plusieurs utilisent ce service fournit par Google et nommé Google Analytics. En voici une liste non exhaustive : Recare, Qare[^qare], HelloCare[^hellocare], Alan[^alan], Therapixel[^therapixel], Implicity[^implicity], Medaviz[^medaviz], Medadom[^medadom], KelDoc[^keldoc], Maiia[^maiia] ...

[^qare]: [https://www.qare.fr/cookies](https://www.qare.fr/cookies)
[^hellocare]: [https://hellocare.com/confidentialite](https://hellocare.com/confidentialite)
[^alan]: [https://alan.com/privacy](https://alan.com/privacy)
[^therapixel]: [https://www.therapixel.com/privacy-policy/](https://www.therapixel.com/privacy-policy/)
[^implicity]: [https://www.implicity.com/privacy-policy/](https://www.implicity.com/privacy-policy/)
[^medaviz]: [https://www.medaviz.com/politique-de-confidentialite-3/#cookies](https://www.medaviz.com/politique-de-confidentialite-3/#cookies)
[^medadom]: [https://www.medadom.com/cookies](https://www.medadom.com/cookies)
[^keldoc]: [https://www.keldoc.com/a-propos/cookies](https://www.keldoc.com/a-propos/cookies)
[^maiia]: [https://www.maiia.com/donnees-personnelles](https://www.maiia.com/donnees-personnelles)
Sur son site Internet Recare mentionne même que des "données [personnelles] peuvent être traitées en dehors de l'EEE, notamment aux États-Unis d'Amérique. Nous avons conclu des clauses contractuelles types de l'UE avec le fournisseur de services afin de garantir un niveau adéquat de protection des données"[^recare].

Les acteurs de la e-santé doivent s’assurer de leur absence de soumission, totale ou partielle, à des injonctions de juridictions ou autorités administratives tierces les obligeant à leur transférer des données.

## Concernant les recommandations de l'association InterHop

L'association InterHop
- RAPPELLE que les données de santé sont des données sensibles comme définies par la CNIL[^cnil_donnee_nodate]. 
- DEMANDE à la CNIL d'analyser les conséquences de la jurisprudence Schrems II sur l'utilisation du service Google Analytics concernant l'ensemble des acteurs de la e-santé et plus spécifiquement sur ceux sus-mentionnés
- DEMANDE au régulateur de stopper les traitements qui s'avèreraient illégaux.

Nous avons décidé de rendre publique ce courrier. Cette publicité contribue à l’objectif de transparence défendu par votre Commission[^transparence].

Nous vous prions d’agréer, Madame La Présidente, l’expression de nos salutations distinguées.

Fait à Paris, le 28 janvier 2021


[^doctolib_chiffrement]: [Doctolib: « Lorsque vous faites le chiffrement des données, l’hébergeur a peu d’importance »](https://www.frenchweb.fr/doctolib-lorsque-vous-faites-le-chiffrement-des-donnees-lhebergeur-a-peu-dimportance/402057)

[^cnil_donnee_nodate]: [https://www.cnil.fr/fr/definition/donnee-sensible](https://www.cnil.fr/fr/definition/donnee-sensible)

[^edpb_schrems]: [Recommendations 01/2020 on measures that supplement transfer tools to ensure compliance with the EU level of protection of personal data](https://edpb.europa.eu/sites/edpb/files/consultation/edpb_recommendations_202001_supplementarymeasurestransferstools_en.pdf)

[^curia_cjue]: [La Cour invalide la décision 2016/1250 relative à l'adéquation de la protection assurée par le bouclier de protection des données UE-États-Unis](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

[^homomorphe]: [Pourquoi le Health Data Hub travestit la réalité sur le chiffrement des données de santé sur Microsoft Azure](https://interhop.org/2020/06/15/healthdatahub-travestit-le-chiffrement-des-donnees)

[^cnil_conseil_etat_2]: [Avis CNIL, Conseil d'Etat,  Mémoire en Observation](https://www.documentcloud.org/documents/7224049-Me-moireCnilHDH.html)

[^conseil_etat_2]: [Health Data Hub et protection de données personnelles : des précautions doivent être prises dans l’attente d’une solution pérenne](https://www.conseil-etat.fr/actualites/actualites/health-data-hub-et-protection-de-donnees-personnelles-des-precautions-doivent-etre-prises-dans-l-attente-d-une-solution-perenne)

[^alan]: [Où nous hébergeons les données de nos membres et pourquoi](https://blog.alan.com/tech-et-produit/pourquoi-nous-hebergeons-sur-aws)

[^article]: [L'annulation du Privacy Shield doit s'appliquer immédiatement, tranche la Cnil européenne](https://www.usine-digitale.fr/article/l-annulation-du-privacy-shield-doit-s-appliquer-immediatement-tranche-la-cnil-europeenne.N989069)

[^transparence]: [Application « StopCovid » : la CNIL tire les conséquences de ses contrôles ](https://www.cnil.fr/fr/application-stopcovid-la-cnil-tire-les-consequences-de-ses-controles)

[^recare]: [Recare : Politique de protection des données personnelles](https://recaresolutions.fr/protection-des-donnees-personnelles/)

[^medadom]: [https://info.medadom.com/cgu](https://info.medadom.com/cgu)

[^doctolib_cgu]: [https://www.doctolib.fr/terms/agreement](https://www.doctolib.fr/terms/agreement)

[^lifen_cgu]: [https://www.lifen.fr/confidentialite](https://www.lifen.fr/confidentialite)

