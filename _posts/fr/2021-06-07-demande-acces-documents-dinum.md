---
layout: post
title: "Demande de documents administratifs : audit de sécurité et de réversibilité du Health Data Hub"
categories:
  - Courrier
  - Audit
  - DiNum
  - Health Data Hub
ref: demande-acces-doc-adm-audit-secu-rev-dinum
lang: fr
---

<p style="text-align:right;">
Nadi BOU HANNA <br/>
Directeur interministériel du numérique <br/>
20 Avenue de Ségur, 75007 PARIS
</p>

<!-- more -->

Monsieur le Directeur Interministériel,

Le 10 mai 2021 le Groupement Intérêt Public (GIP) "Health Data Hub" a publié sur son site la "feuille de route 2021"[^1]. Il est mentionné la réalisation d'un audit de sécurité "au titre de l'article 3 du décret n° 2019-1088 relatif au système d'information et de communication de l'État" par la Direction interministériel du numérique (DINUM) en date du 11 novembre 2020. Par ailleurs dans "le rapport annuel 2020"[^2], il est mentionné qu'en juin 2020 le rapport de réversibilité a été actualisé (1er rapport en date du 7 novembre 2019[^3]) . 

Le Health Data Hub vise à développer l’intelligence artificielle appliquée à la santé et veut devenir un guichet unique d’accès à l’ensemble des données de santé.
Les données concernées sont celles des centres hospitaliers, des pharmacies, du dossier médical partagé et les données de recherche issues de divers registres. La quantité des données hébergées est amenée à exploser, notamment avec l’émergence de la génomique, de l’imagerie et des objets connectés. Ces données sont stockées chez Microsoft Azure, cloud public du géant américain Microsoft.

Afin d’éclairer nos positions et de pouvoir jouer son rôle d’expertise et de vigie de la démocratie,  l'association InterHop demande la communication des documents administratifs suivants, en application des articles L311-1 à R311-15 relatifs du Code des Relations entre le Public et l'Administration à la communication des documents administratifs [^4]  :
- avis public de la DINUM concernant l'audit de sécurité du Health Data Hub,
- avis public de la DINUM concernant la réversibilité du Health Data Hub (celui de juin 2020).

Nous souhaitons recevoir ces documents :
- dans un format numérique, ouvert et réutilisable,
- dans un format papier.

Nous souhaitons également, dans l'intérêt des citoyen.ne.s à avoir accès aux informations administratives, que ce document soit publié comme le mentionnait la Directrice du Health Data sur le site de la DINUM, notamment dans la rubrique dédiée aux publications : [https://www.numerique.gouv.fr/publications/](https://www.numerique.gouv.fr/publications/){:target="_blank"}

Nous vous remercions de nous indiquer, après publication, leur adresse de téléchargement et de nous les envoyer en pièces jointes.

Comme le livre III du code des relations entre le public et l’administration le prévoit lorsque le demandeur a mal identifié celui qui est susceptible de répondre à sa requête, je vous prie de bien vouloir transmettre cette demande au service qui détient les documents demandés si tel est le cas.

Veuillez agréer, Monsieur le Directeur Intermisnistériel, l'expression de nos sentiments distingués.

Fait à Paris, le 12 mai 2021

Adrien PARROT
Président InterHop

[^1]: https://www.health-data-hub.fr/sites/default/files/2021-05/Feuille_route_HDH_2021.pdf

[^2]: https://www.health-data-hub.fr/sites/default/files/2021-05/HDH-RA-2020.pdf

[^3]: https://www.health-data-hub.fr/sites/default/files/2021-05/Etude%20de%20r%C3%A9versibilit%C3%A9%20de%20la%20plateforme%20technologique%20%E2%80%93%20Novembre%202019.pdf

[^4]: [https://www.cada.fr/particulier/mes-droits](https://www.cada.fr/particulier/mes-droits){:target="_blank"}
