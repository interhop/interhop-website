---
layout: post
title: "Demande de documents concernant la “Plateforme des données de santé"
categories:
  - Courrier
  - Ministre de la Santé
  - Contrats
  - Microsoft
redirect_from:
  - /communique-presse-contrat-microsoft
ref: communique-presse-contrat-microsoft
lang: fr
---

Envoi au ministre de la santé d’un courrier de : Demande de documents concernant la “Plateforme des données de santé”

<!-- more -->

# Documents demandés

- **contrat commercial entre le groupement d´intérêt public “Plateforme des données de santé” et l´hébergeur américain Microsoft Azure**

- analyse des risques et des impacts sur la vie privée du Health Data Hub
- analyse actualisée de conformité au référentiel de sécurité du SNDS de la “Plateforme des données de santé”

# Contenu de la lettre

Monsieur le Ministre,

Le déploiement d’une “Plateforme des données de santé” par la création d’un groupement d’intérêt public, 
a été proclamée par la loi n° 2019-774 du 24 juillet 2019 relative à l’organisation et à la transformation du système de santé.[^loisante]<br>
Elle vise à développer l’intelligence artificielle dans le secteur de la santé et à devenir le guichet unique d’accès à l’ensemble des données de santé du territoire national.<br>
Les données concernées sont celles des centres hospitaliers, des pharmacies, du dossier médical partagé et les données de recherche 
issues de divers registres. Cette quantité des données hébergées est amenée à exploser avec l’émergence de la génomique, de l’imagerie et des objets connectés.

Selon les déclarations de Madame Stéphanie COMBES[^declaration_combes], il est prévu que Microsoft Azure, cloud public du géant américain Microsoft, assure la sous-traitance de la solution technique de la plateforme. Cette  information a  été  confirmée par  réponse écrite  du ministère des solidarités et de la santé, publiée au journal officiel du Sénat le 13 février 2020[^senat_microsoft].

La CNIL dans sa délibération n° 2020-044 du 20 avril 2020 [^CNILdel] a révélé “que la Plateforme des données de santé a réalisé une analyse d’impact relative à la protection des données” ainsi “qu’une analyse actualisée de conformité au référentiel de sécurité du SNDS”.<br>
Cette dernière a notamment fait état des « contrats fournis » (p.6 avis CNIL du 20 avril 2020) pour statuer sur les risques de transferts de données vers des pays tiers et les divulgations non autorisées par le droit de l’Union dans le cadre du contrat de sous-traitance de la solution technique de la plateforme à Microsoft Azure.

Dans ce contexte, et pour faire suite à nos précédents courriers des 4 mars et 30 avril 2020, nous vous prions de bien vouloir nous communiquer les documents suivants :
- l´analyse globale des risques et des impacts sur la vie privée du Health Data Hub
- le contrat commercial entre le groupement d´intérêt public “Plateforme des données de santé” et l´hébergeur américain Microsoft Azure
- l´analyse actualisée de conformité au référentiel de sécurité du SNDS de la “Plateforme des données de santé”

Dans cette attente, nous vous prions de croire, Monsieur le Ministre en l’expression de notre très haute considération. 

Le collectif interhop.org<br>
Un courrier similaire a été envoyé par Jean-Baptiste Soufron pour [Santénathon](https://santenathon.org/)

[^declaration_combes]: [« L’exploitation de données de santé sur une plate-forme de Microsoft expose à des risques multiples »](https://www.lemonde.fr/idees/article/2019/12/10/l-exploitation-de-donnees-de-sante-sur-une-plate-forme-de-microsoft-expose-a-des-risques-multiples_6022274_3232.html)

[^senat_microsoft]: [Modalités de stockage du « health data hub »](https://www.senat.fr/basile/visio.do?id=qSEQ200114130&idtable=q371194%7Cq370883%7Cq370105%7Cq369641%7Cq368627%7Cq371617%7Cq371538%7Cq371754%7Cq371099%7Cq371232&_c=recherche&rch=qs&de=20170205&au=20200205&dp=3+ans&radio=dp&aff=sep&tri=dd&off=0&afd=ppr&afd=ppl&afd=pjl&afd=cvn)

[^CNILdel]: [Mesures d’organisation et de fonctionnement du système de santé nécessaires pour faire face à l’épidémie de covid-19 dans le cadre de l’état d’urgence sanitaire ](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf)

[^loisante]: [LOI n° 2019-774 du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038821260&categorieLien=id)
