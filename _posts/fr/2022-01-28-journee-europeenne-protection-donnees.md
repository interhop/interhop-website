---
layout: post
title: "2 ans après... Les transferts de données aux Etats-Unis sont toujours illégaux"
categories:
  - RGPD
  - CNIL
  - GAFAM
ref: journee-europeenne-protection-donnees-2022
lang: fr
---


La Journée Européenne de Protection des Données est l'occasion de rappeler les apports de l'affaire “Schrems II”.  Les entreprises du numérique et plus particulièrement de la e-santé doivent urgemment tirer les conséquences de cette jurisprudence.

<!-- more -->

# Arrêt Schrems II

"Le règlement général sur la protection des données de l'UE a été adopté dans un double but : 
- faciliter la libre circulation des données à caractère personnel au sein de l'Union européenne, 
- tout en préservant les libertés et droits fondamentaux des personnes, notamment leur droit à la protection des données à caractère personnel."[^edpb_schrems]

Le 16 juillet 2020, "dans son arrêt C-311/18[^curia_cjue] (Schrems II) la Cour de Justice de l'Union Européenne (CJUE) rappelle que la protection accordée aux données à caractère personnel dans l'Espace économique européen (EEE) doit s'appliquer sur les données où qu'elles se trouvent"[^edpb_schrems].  Le transfert de données à caractère personnel vers des pays tiers ne peut être un moyen d'affaiblir la protection qui est accordée aux citoyen.ne.s européens dans le cadre du RGPD. 
"La Cour affirme également que le niveau de protection dans les pays tiers se doit d'être équivalent à celui garanti dans l'EEE"[^edpb_schrems].

La CJUE avait aussi considéré que “les exigences du droit américain […] entraînent des limitations de la protection des données personnelles qui ne sont pas circonscrites de manière à satisfaire à des exigences essentiellement équivalentes à celles requises par le droit de l’UE”[^curia_cjue]. En clair “tout transfert de données vers les États-Unis présente un risque”[^article].

[^edpb_schrems]: [Recommendations 01/2020 on measures that supplement transfer tools to ensure compliance with the EU level of protection of personal data](https://edpb.europa.eu/sites/edpb/files/consultation/edpb_recommendations_202001_supplementarymeasurestransferstools_en.pdf)

[^curia_cjue]: [La Cour invalide la décision 2016/1250 relative à l'adéquation de la protection assurée par le bouclier de protection des données UE-États-Unis](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

[^article]: [L'annulation du Privacy Shield doit s'appliquer immédiatement, tranche la Cnil européenne](https://www.usine-digitale.fr/article/l-annulation-du-privacy-shield-doit-s-appliquer-immediatement-tranche-la-cnil-europeenne.N989069)


# FISA et Executive Order 12333

Dans un avis postérieur et recueilli lors de notre action au Conseil d'Etat[^conseil_etat_2], la CNIL[^cnil_conseil_etat_2] se questionne  sur les conséquences de deux textes de lois américains. Ces textes régissent les pouvoirs des services de renseignements.

Le premier est le Foreign Intelligence Surveillance Act (FISA). Il concerne le ciblage “des personnes dont on peut raisonnablement penser qu’elles se trouvent en dehors des États-Unis” et s’applique “aux fournisseurs de services de communications électroniques.” Ce texte opaque s’applique à Microsoft.

Le deuxième se nomme l’Executive Order. Ce texte est un décret présidentiel qui légalise les techniques d’interception des signaux “en provenance” ou “vers” les États-Unis.

Selon la CNIL, et sur la base de ces deux textes, les GAFAM (notamment) sont soumis aux injonctions des services de renseignements américains qui peuvent l’obliger à tout moment à transférer l’ensemble des données hébergées.

[^cnil_conseil_etat_2]: [Avis CNIL, Conseil d'Etat,  Mémoire en Observation](https://www.documentcloud.org/documents/7224049-Me-moireCnilHDH.html)

[^conseil_etat_2]: [Health Data Hub et protection de données personnelles : des précautions doivent être prises dans l’attente d’une solution pérenne](https://www.conseil-etat.fr/actualites/actualites/health-data-hub-et-protection-de-donnees-personnelles-des-precautions-doivent-etre-prises-dans-l-attente-d-une-solution-perenne)

# Cloud de confiance

Pour rappel avec le cloud confiance, deux nouvelles règles apparaissent : “les entreprises qui utiliseront ce cloud de confiance”[^ref0] devront souscrire à “deux conditions : les serveurs doivent être opérés en France et les entreprises qui utilisent et vendent ce cloud doivent être européennes et appartenir à des Européens. Ce sont les deux garanties juridiques que nous donnons en matière d’indépendance par rapport aux lois extraterritoriales américaines.”[^ref0]

Depuis l'annonce du cloud de confiance par l'ensemble du gouvernement, le risque d’extraterritorialité est maintenant clairement établi par la justice et par l’Etat. **Irresponsable est donc celui qui permettra l’envoi de données vers une plateforme assujettie au droit américain ne rentrant pas dans le cadre du “cloud confiance”**.

Concernant la santé et le Health Data Hub ce risque avait été reconnu avant par le Ministre de la santé qui en novembre 2020 promettait une sortie de Microsoft sous 2 ans[^courrier_veran]. Récemment la presse s'est faite l'écho de l'avancée des travaux sur cette réversibilité[^ref0]. Cependant lors de ces voeux à la presse[^lemonde_hdh], le secrétaire d’Etat au Numérique Cédric O affirme que le transfert des données de santé stockées chez Microsoft est un sujet trop sensible politiquement pour être remis en cause "avant la présidentielle".
Depuis l'annonce du Ministre de la santé en novembre 2020 la sortie la sortie de Microsoft reste théorique. Les travaux concernant la protection des données de santé ne peuvent être rythmés par des considérations électoralistes.

[^courrier_veran]: [Courrier Ministre de la Santé à la CNIL](https://www.documentcloud.org/documents/7331959-Courrier-Veran.html#document/p2)

[^ref0]: [https://www.economie.gouv.fr/cloud-souverain-17-mai](https://www.economie.gouv.fr/cloud-souverain-17-mai)

[^ref1]: [Données de santé: le Health Data Hub se prépare à quitter Microsoft Azure](https://www.ticpharma.com/story/1845/donnees-de-sante-le-health-data-hub-se-prepare-a-quitter-microsoft-azure.html)

[^lemonde_hdh]: [Health Data Hub : l’hébergement par Microsoft ne sera pas remis en jeu « avant la présidentielle »](https://www.lemonde.fr/economie/article/2022/01/20/health-data-hub-l-hebergement-par-microsoft-ne-sera-pas-remis-en-jeu-avant-la-presidentielle_6110275_3234.html)

# Transfert de données et Google Analytics

Ce mois-ci la CNIL autrichienne (la Datenschutzbehörde) a jugé que l'utilisation de Google Analytics violait le RGPD[^usine_digital_GA]. Ce service, permettant de connaître les habitudes de consommation, contrevenait à l'arrêt "Schrems II".
En cause : les transferts de données personnelles des Européens vers les Etats-Unis.

Pour Max Schrems, militant pour la protection des données, la conclusion est simple :
> Les entreprises ne peuvent plus utiliser les services américains de cloud en Europe. Cela fait maintenant un an et demi que la Cour de justice l'a confirmé, il est donc temps que la loi soit appliquée.

Cette décision a même fait réagir le Président des affaires mondiales et directeur juridique d’Alphabet qui dans un billet de blog[^google_blog] énonce :
> Les enjeux sont trop élevés - et le commerce international entre l'Europe et les États-Unis trop important pour les moyens de subsistance de millions de personnes - pour échouer à trouver une solution rapide à ce problème imminent.

[^usine_digital_GA]: [Est-ce la fin de Google Analytics en Europe ?](https://www.usine-digitale.fr/article/est-ce-la-fin-de-google-analytics-en-europe.N1773702)

[^google_blog]: [It’s time for a new EU-US data transfer framework](https://blog.google/around-the-globe/google-europe/its-time-for-a-new-eu-us-data-transfer-framework/)

# Conséquences

Les risques du transfert des données (Google Analytics) ainsi ceux liés au traitement de données sur des plateformes extra-européennes (Cloud de Confiance) sont maintenant établis.
Seuls les hébergeurs "ayant recours à un sous-traitant relevant exclusivement des juridictions de l’Union européenne"[^arrete_2020] sont protecteurs des données de santé.

**Ainsi, nous appelons  les acteurs de la e-santé à s’assurer de leur absence de soumission, totale ou partielle, à des injonctions de juridictions ou autorités administratives tierces les obligeant à leur transférer des données.** La seule garanties de localisation européenne des données n'est pas suffisante. 

Le Health Data Hub ainsi que l'ensemble du secteur de la e-santé doivent donc arrêter d'utiliser les hébergeurs et les services informatiques ne remplissant pas ces conditions. Ceci relève des conséquences que fut le big bang "Schrems II". 

[^arrete_2020]: [Délibération n° 2020-044 du 20 avril 2020 portant avis sur un projet d'arrêté complétant l’arrêté du 23 mars 2020 prescrivant les mesures d’organisation et de fonctionnement du système de santé nécessaires pour faire face à l’épidémie de covid-19 dans le cadre de l’état d’urgence sanitaire](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf)

