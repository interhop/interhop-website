---
layout: post
title: "Communiqué de presse : L’assurance maladie va-t-elle devoir verser 11,5 millions d’euros au Health Data Hub en 2022 ?"
categories:
  - CNAM
  - Financement
  - HDH
  - Microsoft
ref: cnam-doit-verser-millions
lang: fr
---

Dans un courrier daté du 12 janvier 2022, l'Assurance maladie s'inquiète du projet d’arrêté qui prévoit le versement de 11,5 millions d’euros du régime général au Health Data Hub, alors que le Ministère des Solidarités et de la Santé a retiré sa demande de versement des données de santé des 67 millions de Françaises et Françaises chez Microsoft. **Nous — citoyens, soignants, patients — demandons à connaître l'objectif précis de cette dotation et le bilan précis des réalisations de ces dernières années et de leurs coûts.**

<!-- more -->

Pour rappel, l'ambition du Health Data Hub, dans sa version actuelle, doit centraliser, à terme et entre autres, les données de la médecine de ville, des pharmacies, du système hospitalier, des laboratoires de biologie médicale, de la médecine du travail, des EHPAD ou encore les données des programmes de séquençage de l’ADN.


# Les doutes de l'assurance maladie 

Le 11 janvier 2022, le Ministre chargé des Comptes publics, délégué auprès du Ministre de l'Economie, des Finances et de la Relance, a sollicité l'avis du Conseil de la Caisse Nationale de l’Assurance Maladie (CNAM) sur un futur arrêté définissant le budget alloué au Health Data Hub (HDH) pour l'année 2022. Dans sa réponse du 12 janvier 2022, l'assurance maladie s'interoge sur "l'impact de la mise en pause du HDH sur le projet d'arrêté qui prévoit le versement de 11,5 millions d'euros du régime général à la plateforme".


Voici sa déclaration : 
> En ce qui concerne le financement de la plateforme des données de santé, le président de la Commission des Systèmes d’Information et de la Transition Numérique (CSI-TN) alerte sur les conséquences du retrait de la demande d’autorisation déposée par la plateforme auprès de la CNIL, dont la presse s’est fait l’écho : **que signifie la mise sur pause du projet et quel est son impact sur le projet d’arrêté qui prévoit le versement de 11,5 millions d’euros du régime général à la plateforme ?** Le Ministre des Solidarités et de la Santé a pris l’engagement de se retirer de l’hébergeur américain Microsoft Azure dans les 24 mois.

> Il est à cet égard rappelé que la mission « Données de santé » de la CNAM, coprésidée par les présidents de la CSI-TN et de la COR, considère que si la plateforme de données de santé présente un large intérêt à des fins de recherche médicale pour l’aide au diagnostic et à la qualité de la prise en charge des patients, elle dénonce en revanche **le manque total de confiance et de transparence vis-à-vis d’un acteur américain pour héberger de telles données, soulignant que les lois fédérales américaines ne pourraient empêcher un transfert de ces données**.


Les doutes réitérés de la CNAM, dont **la commission de la réglementation doit se réunir exceptionnellement le 18/01/2022** [^APMnews_2022], doivent être entendus. Un débat sur le financement du Health Data Hub doit avoir lieu. Surtout, des garanties concernant la fin rapide de l'hébergement par Microsoft  doivent être fournies avant la promulgation de cet arrêté. Pour l'année 2021, le montant de la dotation versée à la Plateforme des données de santé était de 11,6 millions € [^budget_2021].



# Des doutes déjà exprimés, il y a un an... 

En février 2021[^cnam_2021], la CNAM dans une délibération adoptée à l’unanimité des membres précisait déjà que :
> “Les conditions juridiques nécessaires à la protection de ces données ne semblent pas réunies pour que l’ensemble de la base principale soit mise à disposition d’une entreprise non soumise exclusivement au droit européen (…) indépendamment de garanties contractuelles qui auraient pu être apportées”, écrit cette instance dans une délibération adoptée à l’unanimité des membres qui ont pris position.

Comme Martin Hirsch, président de l'APHP avant elle, le conseil d'administration de la CNAM rappelait que :
> “Seul un dispositif souverain et uniquement soumis au RGPD (le règlement européen qui garantit aux usagers certains droits sur leurs données, ndlr) permettra de gagner la confiance des assurés”.

[^cnam_2021]: [Les Conditions ne sont pas réunies pour que Microsoft gère les données de santé, selon l'Assurance maladie](https://interhop.org/2021/02/19/depeche-hdh-cnam)


# Nous lançons l'alerte

Nous sommes pour la recherche en santé. Nous sommes contre l'hypercentralisation des données de santé chez un acteur américain soumis à des lois extra-territoriales. Ces dernières ne peuvent pas garantir la confiance des usagers.

**Nous demandons à connaître l'objectif précis du financement 2022 du Health Data Hub notamment concernant la réversibilité de la plateforme Microsoft. Nous appelons à la publication du bilan précis des réalisations de ces dernières années et de leurs coûts.**


[^budget_2021]: [Arrêté du 26 janvier 2021 fixant le montant pour l'exercice 2021 du financement de la Plateforme des données de santé](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000043081443)

[^APMnews_2022]: [Article APM News : Health Data Hub: un "manque total de confiance et de transparence" vis-à-vis de Microsoft Azure](https://www.apmnews.com/nostory.php?uid=149851&objet=377664)


## Liste des signataires :
- **L'association Constances**
- **L'association InterHop**
- **Le Syndicat de la Médecine Générale**
- **La Fédération SUD Santé Sociaux**
- **La Ligue des Droits de l’Homme**
- **L'association AIDES**
