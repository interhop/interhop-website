---
layout: post
title: "Datathon 2024 - Sujets"
categories:
  - DataThon2024
  - Sujets
  - Linkr
ref: datathon-2024
lang: fr
---


Ce billet de blog vise à renseigner une liste prélimitaire des sujets qui seront traités lors du hackathon sur données (Datathon) organisé par l'association InterHop en septembre 2024. 

<!-- more -->


Cette liste de proposition est indicative et évoluera en fonction de l'envie des participants.

Pour plus d'informations voici en lien le billet de blog présentant les modalités pratiques de réalisation de ce [Datathon](https://interhop.org/projets/datathon).

Voici [le replay de la réunion d'information](https://visio.octoconf.com/playback/presentation/2.3/8d7742fb121806737a8c2bc5bd9dbade20963e96-1719486059418){:target="_blank"} sur les modalités pratiques de réalisation du datathon.


La principale source de données utilisée est MIMIC au format OMOP. Les participant.es doivent anticiper la [création de leur compte sur physionet avec notamment la signature de l'accord sur l'utilisation des données (DUA)](https://mimic.mit.edu/docs/gettingstarted/){:target="_blank"}.

Nous vous donnons rendez-vous le jeudi 8 août à 13h00 dans le cadre d'une [réunion du groupe interCHU](https://interhop.org/2023/10/23/reunion-interchu-anesth-rea) pour la constitution des équipes.

# FINESS+

### Thème principal
- Data Engineer
- Data Scientists

### Synopsis

La santé prend le virage des parcours coordonnés de soin.
Cette ambition nécessite d’avoir des données concernant l’offre de soin qui soient à jour.

Il s’agit en particulier de tenir à jour les données géographiques élémentaires (géolocalisation des 100 000 établissements de soins recensés au FINESS) dans des systèmes ouverts comme OpenStreetMap (OSM) de sorte que ces informations restent exactes et interopérables (tags et iconographies documentés et à jour).

Ce maintien à jour pourra être fait (au moins en partie) au sein de l'association Toobib.org.

### En savoir plus
Voici le lien vers le [billet de blog]({{ site.baseurl }}/2024/08/30/finess-plus) expliquant en détail ce sujet.

# Indicateur Maternité

### Thème principal
- Data visualization
- Data Engineer
- Data Scientists

### Synopsis

L'ensemble des maternités de France fournissent annuellement des indicateurs d'activité : nombre de sièges, de césariennes, transferts, nombre de péridurales...

Ce sujet concerne la visualisation des données puisqu'il s'agit de présenter ces indicateurs sous forme de graphiques et de façon dynamique.

Pour ce projet nous aimerions utiliser l'outil de data science opensource LinkR.

### En savoir plus
Voici le lien vers le [billet de blog]({{ site.baseurl }}/2024/08/31/maternite) expliquant en détail ce sujet.

# Qualité des données

### Thème principal
- Data cleaning / Pre-processing

### Synopsis

Ce sujet concerne la production d'indicateurs de qualité pouvant être partagés et réutilisės.

Pour ce projet nous aimerions utiliser l'outil de data science opensource LinkR.
Il sera basé sur le modèle de donné open source OMOP.

### En savoir plus
Voici le lien vers le [billet de blog]({{ site.baseurl }}/2024/09/02/data-quality) expliquant en détail ce sujet.

# Survie / prédiction de mortalité
 
### Thème principal
- Stats / data sciences

### Synopsis

Ce sujet concerne la réalisation d'un **sujet de data science facile à mettre en oeuvre d'un point de vue statistique**.

Avec ce sujet les participant⸱es ambitionnent d’implémenter un module de statistiques prêt à l’emploi au sein de l’outil opensource logiciel LinkR.

### En savoir plus
Voici le lien vers le [billet de blog]({{ site.baseurl }}/2024/09/03/broken-bone) expliquant en détail ce sujet.

# Aide au codage CIM-10

### Thème principal

- Data science / Large language models

### Synopsis

L'apport des Large Language Models (tels que ChatGPT pour le plus connu) permet d'exploiter les données de façon plus optimale, notamment avec les ChatBot.

La base de données CIM-10 est complexe, et il est parfois difficile de trouver les diagnostics que nous recherchons. Les requêteurs "classiques" que nous trouvons sur internet utilisent des outils de recherche classique (à base de regex) non optimaux.

Les LLM, grâce au RAG (retrieval augmentated generation) permettent d'utiliser les LLM en les "nourrissant" de fichiers, tels qu'un fichier CSV contenant l'ensemble des diagnostics.

Ainsi, il est possible d'utiliser un LLM déjà entraîné, de lui fournir la base de données CIM-10 et de l'interroger directement pour nous donner les codes demandés.

L'approche par RAG a l'avantage de diminuer le risque d'hallucinations.

L'**avantage de ce projet est une application immédiate** ! Si vous êtes médecins, vous pourrez utiliser cel algorithme depuis votre PC perso pour aider au codage de vos patients.

### Source de données
- Base de données CIM-10 au format OMOP

### En savoir plus
Voici le lien vers le [billet de blog]({{ site.baseurl }}/2024/09/04/cim-10) expliquant en détail ce sujet.
