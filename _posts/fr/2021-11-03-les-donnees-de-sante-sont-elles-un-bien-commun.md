---
layout: post
title: "Les données de santé sont-elles un bien commun ?"
categories:
  - Données de santé
  - Commun
  - France Culture
ref: les-donnees-de-sante-sont-elles-un-bien-commun
lang: fr
---


Mercredi dernier dans l'émission [Le temps du débat](https://www.franceculture.fr/emissions/le-temps-du-debat/les-donnees-de-sante-sont-elles-un-bien-commun), sur France Culture la question suivante fut abordée : "Les données de santé sont-elles un bien commun ?".

<!-- more -->

Trois intervenants étaient invités : 
- Alain Livartowski : Docteur en médecine, spécialiste en pneumologie et en cancérologie, conseiller médical à la Direction des données de l'Institut Curie en charge des projets DATA de l’Ensemble hospitalier, président du conseil scientifique consultatif du Health Data Hub

- Adrien Parrot : Médecin anesthésiste-réanimateur et informaticien, président de l'association InterHop

- Coralie Lemke : Journaliste pour Sciences et Avenir, autrice de "Ma santé, mes données" (Premier Parallèle)

Suite à ce débat, InterHop souhaite revenir sur certains points.

Tout d’abord, les bénéfices de la recherche ne font **aucun doute** auprès des membres d’InterHop, qu’ils soient médecins, informaticiens ou juristes. 

Contrairement à ce qui a été dit, le Health Data Hub est bien chargé du stockage et de la mise à disposition de l'ensemble des données de santé (Système National des Données de Santé ou SNDS) produites sur le territoire français  par les acteurs publics. Ceci est d'ailleurs énoncé dans le 1 de l'article L1461-1 : "Le système national des données de santé rassemble et met à disposition[^1] (...) Les données destinées aux professionnels et organismes de santé recueillies à l'occasion des activités (...)[^1] de prévention, de diagnostic, de soins ou de suivi social et médico-social, pour le compte de personnes physiques ou morales à l'origine de la production ou du recueil de ces données ou pour le compte du patient lui-même"[^2].

[^1]: [https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038886868/2021-11-01](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038886868/2021-11-01)

[^2]: [https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000033862549/](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000033862549/)

Aujourd’hui, les données de santé collectées par le Health Data Hub sont toujours hébergées par Microsoft, entreprise soumise au droit américain, dont le « Le Foreign Intelligence Surveillance Act » (FISA), et le  Clarifying Lawful Overseas Use of Data Act (Cloud Act).
Le Cloud Act s'applique lorsqu'une structure française permet à un prestataire d'origine américaine - ou localisé aux Etats-Unis de traiter ses données. 
Le FISA définit les procédures de surveillance physique et électronique, ainsi que la collecte d'information sur des puissances étrangères directement, ou indirectement.

Pour minimiser les risques, InterHop pense qu’il faut travailler avec des acteurs strictement de droit européens. 
Bien sûr ce point n'annule pas tous les risques de hacking. En effet le risque zéro n'existe pas et nous avons vu que les hôpitaux[^hopitaux], les entreprises[^dedalus] ou même les géants du numériques comme Microsoft[^micro1][^micro2] sont soumis à des attaques informatiques dévastatrices.

[^hopitaux]: [Après la cyberattaque au CHU de Rouen, l’enquête s’oriente vers la piste crapuleuse](https://www.lemonde.fr/pixels/article/2019/11/26/apres-la-cyberattaque-au-chu-de-rouen-l-enquete-s-oriente-vers-la-piste-crapuleuse_6020609_4408996.html)

[^dedalus]: [Un « leader européen » des données de santé licencie un lanceur d'alerte pour « faute grave »](https://www.nextinpact.com/article/43405/un-leader-europeen-donnees-sante-licencie-lanceur-dalerte-pour-faute-grave)

[^micro1]: [Les hackers de SolarWinds ont accédé au code source de Microsoft Azure, Intune et Exchange](https://www.usine-digitale.fr/article/les-hackers-de-solarwinds-ont-accede-au-code-source-de-microsoft-azure-intune-et-exchange.N1062819)

[^micro2]: [Microsoft Cosmos DB : des milliers de bases de données étaient accessibles](https://www.nextinpact.com/lebrief/47878/microsoft-cosmos-db-milliers-bases-donnees-etaient-accessibles)

S'adresser à des acteurs européens permet simplement de s'affranchir d'entreprises d'abord soumises aux lois extraterritoriales de leur pays (FISA et CloudAct par exemple). Tout litige avec une entreprise américaine sera bien plus difficile, voire impossible à régler.

Il faut se souvenir que la CNIL est également opposée au choix de la solution Microsoft[^cnil_ce]. 

[^cnil_ce]: [Observations de la CNIL au Conseil d'Etat](https://cdn2.nextinpact.com/medias/observations-de-la-cnil-8-octobre-2020-1---1-.pdf)

Par ailleurs, les GAFAM s’intéressent à l'assurance santé. Le risque est de casser nos systèmes de santé mutualisés dans lesquelles petits risques et gros risques sont partagés.  Par exemple un patient diabétique dialysé trois fois par semaine coûte beaucoup d'argent. Mais est aidé par des patients de plus petits risques qui sont, eux, en bonne santé. 
Si les géants du numérique se tournent vers l'assurance santé, il est probable que l’objectif sera de maximiser les profits. A ce moment-là, la prime d'assurance sera adaptée en fonction de nouveaux critères qui remettront forcément en cause notre système de sécurité sociale. 

Enfin InterHop rappelle qu'il est aujourd'hui impossible pour un ordinateur (même un supercalculateur) de procéder à des calculs sur des données chiffrées. La technologie permettant de faire cela s'appelle le chiffrement homomorphe[^homomorphe] ; celle-ci est à l’étape de recherche. Actuellement on est donc dans l’obligation de déchiffrer les données pour faire fonctionner n'importe quel algorithme ou programme informatique. 
A notre sens, il est dangereux de déchiffrer des données de santé sur une plateforme américaine (Microsoft ainsi que les autres GAFA) ou chinoise (BATX).

[^homomorphe]: [Pourquoi le Health Data Hub travestit la réalité sur le chiffrement des données de santé sur Microsoft Azure](https://interhop.org/2020/06/15/healthdatahub-travestit-le-chiffrement-des-donnees)
