---
layout: post
title: "Communiqué de presse du collectif SanteNathon : La France transfère-t-elle illégalement nos données de santé aux États-Unis ?"
categories:
  - HDH
  - Microsoft
  - Conseil d'Etat
  - Europe
  - Privacy Shield
  - RGPD
redirect_from:
  - /communique-presse-refere-privacy-shield
ref: communique-presse-refere-privacy-shield
lang: fr
---


**Depuis avril 2020, les données de santé des Françaises et des Français sont centralisées chez Microsoft dans le cadre de « l’état d’urgence sanitaire ». Certaines données sont transférées aux États-Unis alors que la Cour de Justice de l’Union Européenne a invalidé, le 16 juillet 2020, le « bouclier de protection des données » entre l’Europe et les États-Unis au motif d’une protection inadéquate sur le sol américain. 
Le 16 septembre 2020, un collectif inédit de 18 organisations et personnalités a saisi le Conseil d’État pour dénoncer ce transfert illégal de données personnelles et sensibles. Ce recours fait suite à une première action significative initiée le 28 mai dernier.**

<!-- more -->

Suite à l’arrêté du 21 avril 2020[^arrete_21], les données des Français et des Françaises passant aux urgences, mais aussi leurs données de pharmacie, résultats de laboratoires, réponses à des enquêtes sur leur vécu en lien avec l’épidémie de COVID-19, sont versées sur la plateforme du Health Data Hub hébergée chez la société Microsoft et ce, sans qu’aucun appel d’offre n’ait été lancé. Si l’hébergement des données se réalise sur des serveurs aux Pays-Bas, les données sont transférées en dehors de l’Union Européenne et notamment aux Etats-Unis dès lors qu’intervient un traitement, une opération de maintenance, comme en atteste la CNIL dans sa délibération du 20/04/2020[^CNIL_del]

Or, le 16 juillet 2020, la Cour de Justice de l’Union Européenne a invalidé le “Privacy Shield”[^CJUE_PrivacyShield] (ou « bouclier de protection des données ») entre l’Europe et les États-Unis. Cet accord européen adopté en 2016 permettait aux entreprises de transférer légalement les données personnelles (identité, comportement en ligne, géolocalisation…) de citoyen.ne.s européen.ne.s vers les États-Unis. La Cour de Justice de l’Union Européenne a conclu que la protection des données personnelles européennes était insuffisante au regard du Règlement Général sur la Protection des Données (RGPD). Elle dénonce un système de surveillance américain omnipotent, ou encore une absence de recours juridique ouvert pour les citoyens européens.

Pour alerter sur ce transfert actuel de données de santé couvertes par le secret médical et réalisé sans aucune garantie, un collectif d’organisations et de personnalités issues du secteur médical et de la santé, d'associations représentant les malades et les usagers, du logiciel libre, de syndicats d’ingénieurs et techniciens, de journalistes et de citoyens a déposé, le 16 septembre 2020, un recours en urgence au Conseil d’État.

Ce recours fait suite à une précédente action menée le 28 mai 2020 qui signalait plusieurs irrégularités dans le traitement des données sur la plateforme du Health Data Hub et des risques majeurs pour les droits et libertés fondamentales. Dans sa réponse[^conseil_etat], le Conseil d’État avait considéré, qu’au moment du jugement, la société Microsoft intégrait la liste des organisations ayant adhéré au « Bouclier de protection des données ». 

Ce n’est plus cas aujourd’hui. Les requérant.e.s demandent par conséquent au Conseil d’Etat de suspendre le traitement et la centralisation des données au sein du Health Data Hub et, ce faisant, de s'aligner sur la toute récente jurisprudence européenne. Ils font également valoir que les engagements contractuels conclus entre la société Microsoft et le Health data Hub sont insuffisants.

A noter que la question de la souveraineté numérique est actuellement au coeur de plusieurs débats parlementaires et que d’autres actions sont en cours en Europe[^wsj] suite à l’invalidation du Privacy Shield. Un nouvel accord n’est pas prévu avant plusieurs mois comme l’a souligné le commissaire européen à la justice, Didier Reynders, le 4 septembre 2020 : « Don’t expect new EU-US data transfer deal anytime soon[^euractiv] ».

Liste des 18 réquérant.e.s :
- **L’association Le Conseil National du Logiciel Libre (CNLL)** : « *Pour que les discours sur la souveraineté numérique ne restent pas des paroles en l'air, les projets stratégiques au plan économique et sensibles au plan des libertés personnelles ne doivent pas être confiés à des opérateurs soumis à des juridictions incompatibles avec ces principes, mais aux acteurs européens qui présentent des garanties sérieuses sur ces sujets, notamment par l'utilisation de technologies ouvertes et transparentes.* »
- **L’association Ploss Auvergne-Rhône-Alpes**
- **L’association SoLibre**
- **La société NEXEDI** : « Il est faux de dire qu'il n'y avait pas de solution européenne. Il est exact en revanche que le Health Data Hub n'a jamais répondu aux offreurs de ces solutions. »
- **Le Syndicat National des Journalistes (SNJ)** : « *Pour le Syndicat national des journalistes (SNJ), première organisation de la profession, ces actions doivent permettre de conserver le secret sur les données de santé des citoyennes et citoyens de France ainsi que protéger le secret des sources des journalistes, principale garantie d’une information indépendante.* »
- **L'Observatoire de la transparence dans les politiques du médicament** 
- **L'association InterHop**  : « *L'annulation du Privacy Shield sonne la fin de la naiveté numérique européenne. Cependant des rapports de force se mettent en place entre les Etats-Unis et l'Union Européenne concernant le transfert  des données personnelles en dehors de notre espace juridique.*
*Pour pérenniser notre système de santé mutualiste et eu égard à la sensibilité des données en cause, l'hébergement et les services du Health Data Hub doivent relever exclusivement des juridictions de l’Union européenne.* »
- **L'Union Fédérale Médecins, Ingénieurs, Cadres, Techniciens (UFMICT-CGT)**
- **L'Union Générale des Ingénieurs, Cadres et Techniciens (UGICT-CGT)**: « *Pour la CGT des cadres et professions intermédiaires (UGICT-CGT), ce recours est indispensable pour préserver la confidentialité des données qui sont désormais devenues, dans tous les domaines, un marché. Concepteurs et utilisateurs des technologies, nous refusons de nous laisser déposséder du débat sur le numérique au prétexte qu'il serait technique. Seul le débat démocratique permettra de placer le progrès technologique au service du progrès humain!* »
- **L'association Constances** : « *Volontaires de Constances, la plus grande cohorte de santé en France, nous sommes particulièrement sensibilisés aux données de santé et leurs intérêts pour la recherche et la santé publique. Comment admettre que des données de citoyens français soient aujourd'hui transférées aux Etats-Unis ? Comment accepter qu'à terme, toutes les données de santé des 67 millions de Français soient hébergées chez Microsoft et donc tombent sous les lois et les programmes de surveillance américains ?* »
- **L'association Française des Hémophiles (AFH)**
- **L'association les "Actupiennes"**
- **Le Syndicat National des Jeunes Médecins Généralistes (SNJMG)** : « *Les données issues des soins ne doivent pas servir d'autre finalité que l'amélioration des soins. Garantir la sécurité des données de santé et leur exploitation à des seules fins de santé publique est une priorité pour toustes les soignant.es.* »
- **Le Syndicat de la Médecine Générale (SMG)**: « *La sécurisation des données de santé est un enjeu majeur de santé publique puisqu'elle permet le secret médical. Le Health Data Hub n'a jusqu'ici montré aucune garantie sur une véritable sécurisation des données de santé des Français.es, notamment par son choix d'héberger celles-ci chez Microsoft, et met ainsi en danger le secret médical pourtant nécessaire à une relation thérapeutique saine et efficiente*. »
- **L’Union Française pour une Médecine Libre (UFML)** : « *Évitons le contrôle de systèmes monopolistiques potentiellement nuisibles pour le système de santé et les citoyens.* »
- **Madame Marie Citrini, en son mandat de représentante des usagers du Conseil de surveillance de l'AP-HP**
- **Monsieur Bernard Fallery, professeur émérite en systèmes d’information** : « La gestion "par l’urgence" revendiquée pour le Healh Data Hub est un véritable cas d’école de tous les risques liés à la gouvernance des données massives : souveraineté numérique et stockage sans finalité précisée, mais aussi centralisation technique risquée, mainmise sur un commun numérique, oligopole des GAFAM, dangers sur le secret médical, quadrillage des traces et ajustement des comportements » 
- **Monsieur Didier Sicard, médecin et professeur de médecine à l’Université Paris Descartes** : « Offrir à Microsoft les données de santé françaises qui sont parmi les meilleures du monde, même si elles sont insuffisamment exploitées, est une quadruple faute : enrichir gratuitement Microsoft, trahir l'Europe et les citoyens français, empêcher les entreprises françaises de participer à l'analyse des données » 

[^wsj]: [Ireland to Order Facebook to Stop Sending User Data to U.S.](https://www.wsj.com/articles/ireland-to-order-facebook-to-stop-sending-user-data-to-u-s-11599671980)

[^euractiv]: [Don’t expect new EU-US data transfer deal anytime soon, Reynders says](https://www.euractiv.com/section/data-protection/news/dont-expect-new-eu-us-data-transfer-deal-anytime-soon-reynders-says/) 

[^arrete_21]: [Arrêté du 21 avril 2020 complétant l'arrêté du 23 mars 2020 prescrivant les mesures d'organisation et de fonctionnement du système de santé nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000041812657/)

[^CNIL_del]: [Mesures d’organisation et de fonctionnement du système de santé nécessaires pour faire face à l’épidémie de covid-19 dans le cadre de l’état d’urgence sanitaire ](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf)

[^CJUE_PrivacyShield]: [La Cour invalide la décision 2016/1250 relative à l'adéquation de la protection assurée par le bouclier de protection des données UE-États-Unis](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

[^conseil_etat]: [Conseil d'État, 19 juin 2020, Plateforme Health Data Hub](https://www.conseil-etat.fr/ressources/decisions-contentieuses/dernieres-decisions-importantes/conseil-d-etat-19-juin-2020-plateforme-health-data-hub)


