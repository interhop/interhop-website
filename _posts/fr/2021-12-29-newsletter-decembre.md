---
layout: post
title: "Bilan de l'année 2021"
categories:
  - Newsletter
  - Décembre
  - "2021"
ref: newsletter-decembre-2021
lang: fr
---

L'année 2021 a été riche en actions menées par les membres de la petite association InterHop.

Voici avec cette dernière newsletter de l'année, l'occasion de faire le bilan des projets de 2021 !

<!-- more -->

Nous aimerions aussi vous rappeler qu'il est possible de nous aider grâce à des dons défiscalisés sur [interhop.org/dons](https://interhop.org/dons).

## Serveurs certifiés "Hébergeur de données de Santé HDS"

[InterHop](https://interhop.org/projets/) installe des logiciels libres pour la santé. Ces logiciels sont hébergés sur des machines particulières que l’on appelle serveurs. Pour la santé, le niveau de sécurité et de disponibilité est renforcée : la certification des Hébergeurs de Données de Santé (HDS) est obligatoire.

En 2021 nous avons pérennisé nos serveurs HDS. Nous les utilisons maintenant au quotidien sur plusieurs projets liés à Goupile.


## Goupile ou la conception de formulaire clinique en ligne

Traditionnellement, le Formulaire de rapport de cas (CRF) est un document papier destiné à recueillir les données des personnes incluses dans un projet de recherche.
Ces documents sont de plus en plus remplacés ou complétés par des portails numériques, on parle alors d’eCRF (‘‘e’’ pour electronical).

[Goupile](https://goupile.fr/) est un outil de conception d’eCRF libre et opensource, qui s’efforce de rendre la création de formulaires et la saisie de données à la fois puissantes et faciles. Son accès est nominatif et protégé.

Nous accompagnons les utilisateurs.trices de [Goupile](https://goupile.fr/) pour leur ouvrir des accès au logiciel et les informer sur les problématiques légales (RGPD notamment). Notre Déléguée à la Protection des Données est aussi là pour vous aider.

## Actions juridiques

2021 a été riche en actions juridiques portées par notre avocate.

Récemment, nous avons demandé la [production des évaluations](https://interhop.org/2021/12/10/demande-efficacite-covid) relatives aux outils numériques « SI-DEP », « CONTACT COVID », « TOUSANTICOVID », « VACCIN COVID » auprès du ministère de la santé. Cette demande fait suite aux avis de la CNIL, qui a regretté l’absence de production d’une quelconque évaluation sur l’efficacité des dispositifs numériques de lutte contre la covid19.

A la suite du Cash Investigation, InterHop s'est associée à plusieurs associations pour porter [une plainte CNIL](https://interhop.org/2021/05/27/signalement-cnil-iqvia) concernant l'entrepôt de donnée  de la société IQVIA. Une des revendications était la production d'une nouvelle analyse de risques liée à l'exploitation des données de santé par une société de droit américain.

Enfin, [InterHop est allé au Conseil d'Etat](https://interhop.org/2021/03/10/reponse-franceinter-doctolib). Avec d'autres organisations, nous avons fait  valoir que le choix d’avoir recours au prestataire Doctolib pour organiser la gestion de la prise de rendez-vous dans le cadre de la politique vaccinale contre la Covid19 n’est pas conforme au RGPD. En effet, la société Doctolib a choisi de faire appel au géant américain Amazon Web Services pour héberger les données de santé. Même si le juge n'a pas souhaité vérifier de façon indépendante notre argumentaire technique, nous continuons nos actions sur ce sujet d'ampleur.

## Recherche

Historiquement, les membres d'InterHop sont proches de la recherche hospitalière. InterHop est donc maintenant associées à deux [publications](https://medinform.jmir.org/2021/12/e30970) [scientifiques](https://www.jmir.org/2021/10/e29259) :-) Nous en sommes très fiers.

Nous poursuivons nos travaux sur l'interopérabilité des systèmes d'information, et en particulier l'implémentation du modèle de données commun OMOP.

Nos réunions interCHU, rassemblant les centres hospitaliers français et promouvant l'interopérabilité en leur sein, ont vocation à se poursuivre. Notre prochain cheval de bataille étant sûrement le domaine du Programme de médicalisation des systèmes d'information (PMSI) et l'anesthésie-réanimation.

Notre secrétaire a par ailleurs finalisé le développement de la première version de Susana, un outil permettant de lier plusieurs dictionnaires médicaux (interopérabilité sémantique).

## Sensibilisation à nos actions

Nous avons participé à de nombreuses tables rondes concernant la cybersécurité et la refondation du service public hospitalier.

Notre avocate s'est rendue à la fête de l'humanité pour parler des données de santé.

Nous avons été entendus par [France Télévision](https://www.france.tv/france-2/cash-investigation/2450927-nos-donnees-personnelles-valent-de-l-or.html) (Cash Investigation) ou encore par [France Radio](https://interhop.org/2021/11/03/les-donnees-de-sante-sont-elles-un-bien-commun) (France Culture).

Nous avons participé à plusieurs congrès des professionnels de santé (Syndicat de la Médecine Générale, Association des Jeunes Anesthésistes Réanimateurs)...
 
## Notre feuille de route pour 2022

Mon Espace Santé ouvre l'année prochaine. Nous allons être vigilants. Nous militerons pour une information large, loyale pour tous et pour toutes. Nous nous battrons pour un code source ouvert.
Un seul chiffre : actuellement 3 pilotes sont lancés en France. Plus de 90% des patients n'ont pas souhaité créer leur espace numérique de façon active. Seulement en l'absence de réponse, celui-ci sera automatiquement créé dans les prochaines semaines ; les personnes ont-elles été suffisamment informées des enjeux ?

Nous allons redoubler d'efforts sur Toobib, notre logiciel de prise de rendez-vous en ligne. Nous sommes en train de rédiger ses spécifications. Nous vous mettrons bientôt à contribution.

Cette année, nous avons lancé plusieurs Webinaires ; nous prévoyons de les continuer, notamment sur les aspects reglémentaires et recherche. Plusieurs actions juridiques sont actuellement en cours :-)

Enfin, en 2022 nous espérons que l'utilisation de [Goupile](https://goupile.fr/) s'envolera.

## Dons

Notre financement est quasi-intégralement lié à vos dons. Si vous croyez en nos actions, merci de faire un don sur notre page dédiée : [interhop.org/dons](https://interhop.org/dons).
