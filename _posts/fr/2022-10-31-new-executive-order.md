---
layout: post
title: "Décret présidentiel : l'échange transatlantique de données personnelles restera à haut risque pour les européens"
categories:
  - "Executive Order"
  - "Privacy Shield"
  - Schrems
ref: new-executive-order
lang: fr
---



[^EO_press]: [FACT SHEET: President Biden Signs Executive Order to Implement the European Union-U.S. Data Privacy Framework](https://www.whitehouse.gov/briefing-room/statements-releases/2022/10/07/fact-sheet-president-biden-signs-executive-order-to-implement-the-european-union-u-s-data-privacy-framework/)

Le 25 mars 2022, la présidente von der Leyen et le président Biden avaient annoncé qu'ils étaient parvenus à un accord de principe sur un nouveau cadre transatlantique de  protection des données entre Union Européenne et États-Unis. 
Le 7 octobre, le président Biden a signé un décret concernant "le renforcement des garanties pour les activités de renseignement électromagnétique des États-Unis"[^decret_prez_en_francais].

<!-- more -->

Vous trouverez dans un autre billet de blog, la traduction de ce décret[^decret_prez_en_francais].

Cet accord est capital pour les Etats-Unis puisque d'après le président des Etats-Unis "les flux de données transatlantiques sont essentiels à la relation économique entre l'UE et les États-Unis, qui représente 7 100 milliards de dollars"[^EO_press].
**Celui-ci devrait permettre un échange sécurisé de données de part et d'autre de l'océan atlantique. Il n'en sera rien.**

Cet accord est capital aussi puisqu'il vise à mettre fin à l'incertitude dans laquelle des milliers d'entreprises ont été plongées quand la Cour de Justice de l'Union Européenne a rejeté deux autres accords transatlantiques, le Safe Harbor en 2015 et le Privacy Shield en 2020[^cjue_ps], estimant que les Etats-Unis ne garantissent pas "un niveau de protection substantiellement équivalent à celui garanti au sein de l’Union"[^cjue_ps].


[^cjue_ps]: [La Cour invalide la décision 2016/1250 relative à l'adéquation de la protection assurée par le bouclier de protection des données UE-États-Unis](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

Même s'il a pour conséquence l'adoption d'une nouvelle décision d'adéquation en vertu de l'[article 45](https://gdpr-text.com/fr/read/article-45/) du RGPD, celle-ci restera illégale au regard du niveau de protection accordé par le RGPD. La Cour de Justice de l'Union Européen fera comme pour le Safe Harbor en 2015 et le Privacy Shield en 2020, elle l'invalidera.

Comme d'autres[^klaba], nous sommes conscients que ce décret intervient en contrepartie de livraisons de gaz faites par les USA aux européens[^garnier]. Problème, les fondamentaux ne changent pas et la doctrine Etats-Unienne reste identique, fondée sur une surveillance de masse, secrète et sans recours judiciaire indépendant.

**Nous nous battrons contre cela.**

[^garnier]: [« Nos données personnelles se retrouvent sous la menace d’une nouvelle traversée de l’Atlantique »](https://www.lemonde.fr/idees/article/2022/04/07/nos-donnees-personnelles-se-retrouvent-sous-la-menace-d-une-nouvelle-traversee-de-l-atlantique_6121031_3232.html)

[^klaba]: [https://twitter.com/olesovhcom/status/1507387995032330283](https://twitter.com/olesovhcom/status/1507387995032330283)

## Description du nouveau canevas

Le décret décrit un canevas relatif au données personnelles des européens transférées aux États-Unis. Il décrit les modalités d'accès aux données personnelles par les agences de renseignements américaines et le droit des citoyens européens à demander des comptes à ces mêmes agences.

Ce texte mettrait en place des garde-fous pour garantir le respect des données personnelles européennes.

Un citoyen européen pourra :

1/  s'adresser au *Responsable de la Protection des Libertés Civiles du Bureau* ("Civil Liberties Protection Officer") du Directeur du Renseignement National s'il pense que le renseignement Etat-Unien a accédé à ses données personnelles de façon non justifiée.
Cette personne est donc chargée de veiller au respect de la vie privée et des droits fondamentaux par les agences de renseignement américaines.

Notons que le Directeur du Renseignement National est "sous l'autorité et le contrôle directs du président des États-Unis pour faire fonction de coordinateur de la communauté du renseignement, un ensemble des 17 principales agences de renseignements des États-Unis"[^wikipedia_director].

Deux remarques :
- nous comprenons rapidement le lien de dépendance puisque ce *Responsable à la Protection des Libertés Civiles* est sous la tutelle des renseignements américains. Il est nommé puis peut être révoqué par le directeur du renseignement national en "cas de faute professionnelle, de malversation, d’atteinte à la sécurité, de manquement au devoir ou d’incapacité".[^decret_prez_en_francais].
- nous remarquons l'absence de description quant à l'information des personnes ainsi qu'au droit d'accès à leurs données. Ceci est contraire aux articles 13 et 15 du RGPD[^rgpd_13_15].

[^wikipedia_director]: [Directeur du renseignement national](https://fr.wikipedia.org/wiki/Directeur_du_renseignement_national)
[^rgpd_13_15]: [CHAPITRE III - Droits de la personne concernée](https://www.cnil.fr/fr/reglement-europeen-protection-donnees/chapitre3#Section2)


2/ S'adresser à la *Cour de Révision de la Protection des Données* ("Data Protection Review Court"). Elle est constitué d'un jury nommé par le Procureur Général des Etats-Unis (Attorney General). Cette cour pourra controler la conformité des décisions du *Responsable de la Protection des Libertés Civiles*.
La *Cour de Révision de la Protection des Données* sera habilitée à enquêter sur les plaintes déposées par des personnes de l'UE.

Le lien de dépendance avec le président des Etats-Unis est important puisque le procureur général "est nommé par le président des États-Unis après ratification par le Sénat"[^attorney]. 

Les juges de cette Cour peuvent être révoqués par le procureur général en "cas de faute professionnelle, de malversation, d’atteinte à la sécurité, de manquement au devoir ou d’incapacité"[^decret_prez_en_francais].

[^attorney]: [Procureur général des États-Unis](https://fr.wikipedia.org/wiki/Procureur_g%C3%A9n%C3%A9ral_des_%C3%89tats-Unis)

## Un canevas protecteur ?

La CJUE avait invalidé le Privacy Shield pour deux raisons : le manque de proportionalité au sens de l'article 52 de la Charte des droits fondamentaux (CFR)  et l'absence de recours judiciaire au sein de  l'article 47 CFR[^chartre_union][^noyb].

[^chartre_union]: [Charte des droits fondamentaux de l'union européenne](https://www.europarl.europa.eu/charter/pdf/text_fr.pdf)

[^noyb]: [Le décret sur la surveillance américaine a peu de chances de satisfaire au droit européen](https://noyb.eu/fr/le-nouveau-decret-americain-peu-de-chances-de-satisfaire-la-legislation-europeenne)

D'abord nous savons tous qu'il est et restera très difficile pour un citoyen européen de prouver que ses droits "européens" ont été violés et encore plus d'obtenir une quelconque réparation.

Ensuite cet ordre présidentiel "ne limite aucune technique de collecte de renseignements d’origine électromagnétique autorisée en vertu de la loi sur la sécurité nationale de 1947, de la loi sur la surveillance des renseignements étrangers de 1978, telle que modifiée (50 U.S.C. 1801 et seq.) (FISA), de l’ordre exécutif 12333, ou de toute autre loi ou directive présidentielle applicable"[^decret_prez_en_francais]. Le secret reste de rigueur et est exprimé en ces termes puisque :
- les rapports du *Responsable de la Protection des Libertés Civiles* et de la *Cour de Révision de la Protection des Données*  sont en fait "classifiés"[^decret_prez_en_francais].
- la liste des objectifs autorisée de traitement des données est publique "à moins que le président ne détermine que cela constituerait un risque pour la sécurité nationale des États-Unis"[^decret_prez_en_francais].

[^decret_prez_en_francais]: [Décret exécutif sur le renforcement des garanties pour les activités de renseignement électromagnétique des États-Unis](https://interhop.org/2022/10/14/executive-order-enhancing)

Aussi nous l'avons remarqué, ni le *Responsable de la Protection des Libertés Civiles* ni la *Cour de Révision de la Protection des Données* ne sont indépendants de l'exécutif américain.

![](https://noyb.eu/sites/default/files/styles/media_xlarge/public/2022-10/clpo-grafik_ohne-logo_2700x900px_0.png?itok=cp2s5yr8)

La procédure en deux étapes, agent relevant du directeur du renseignement national et "Cour de révision de la protection des données" relevant du procureur général, ne remplit donc pas la définition du terme "Cour" au sens juridique normal de l'article 47 de la Charte du droit de l'Union[^chartre_union] ou même de la Constitution américaine.


Enfin les textes piliers du renseignement américain comme le FISA et l'Executive Order 12333 restent d'actualité. Ainsi la collecte en vrac reste d'actualité, alors même que c'est la raison principale d'invalidité du dernier traité d'adéquation : le Privacy Shield.
> "La collecte en vrac de renseignements d’origine électromagnétique (...) est autorisé si (...) un élément de la communauté du renseignement (...) détermine que les informations nécessaires pour faire progresser une priorité validée en matière de renseignement ne peuvent être raisonnablement obtenues par une collecte ciblée."[^decret_prez_en_francais]

# Conclusion

Cet ordre présidentiel doit maintenant obtenir "l'obtention d'un avis du Comité Européen de la Protection des Données (EDPB) et le feu vert d'un comité composé de représentants des États membres de l'UE. En outre, le Parlement européen dispose d'un droit de regard sur les décisions d'adéquation"[^edbp]. 


InterHop et ses partenaires resteront en alerte sur ce sujet. Nous agirons pour tenter de préserver la protection des données personnelles aussi loin que possible d'intérêts stratégiques extra-européens. Surtout s'ils s'agit de données de santé, sensibles par nature.

La nouvelle décision d'adéquation pourrait intervenir d'ici la fin de l'année. 
Si elle était adoptée elle sera attaquée à la Cour de Justice de l'Union Européenne.

Une réaction coordonnée des secteurs public et privé, à l'échelle nationale et européenne doit intervenir rapidement. 

Le risque est que les entreprises utilise "ce décret comme base juridique pour le transfert de données entre les États-Unis et l’Union européenne, avant même qu’il ne soit approuvé par la Commission européenne" [^politico] et alors que ce texte ne protège en rien les citoyen.ne.s européen.ne.s.

[^politico]: [Biden executive order gives US power to determine if EU surveillance goes too far](https://www.politico.eu/article/us-president-joe-biden-executive-order-eu-privacy-framework-surveillance-programs/)


> « Il est clairement écrit dans le texte que la Cour de révision de la protection des données ne peut pas confirmer auprès du plaignant s'il y a bien eu une demande de la part des services de renseignement américain. Ce n'est donc pas la transparence ni l'impartialité qu'on peut attendre d'un tribunal indépendant. On peut aussi s'interroger sur l'accessibilité d'une procédure longue, coûteuse, avec un juge américain plutôt qu'européen, et une loi basée sur la primauté de la sécurité nationale sur les droits des citoyens, alors que c'est l'inverse en Europe »[^tribune_clairement]

[^tribune_clairement]: [Transfert de données : un parcours semé d'embûches pour le nouveau cadre légal entre l'UE et les Etats-Unis](https://www.latribune.fr/technos-medias/transferts-de-donnees-un-parcours-seme-d-embuches-pour-le-nouveau-cadre-legal-entre-l-ue-et-les-etats-unis-936306.html)


Il faut agir vite surtout que "l'hiver approche" et que nous avons besoin de gaz pour nous chauffer.

 

[^edbp]: [Questions & Answers: EU-U.S. Data Privacy Framework ](https://ec.europa.eu/commission/presscorner/detail/fr/qanda_22_6045)


