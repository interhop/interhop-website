---
layout: post
title: "Vers un Cloud de Confiance ?"
categories:
  - Cloud
  - HDH
ref: analyse-cloud-confiance 
lang: fr
---

Le 17 mai dernier, **Bruno Le Maire**, ministre de l’Économie, des Finances et de la Relance, **Amélie de Montchalin**, ministre de la Transformation et de la Fonction publiques, et **Cédric O**, secrétaire d'État chargé de la Transition numérique et des Communications électroniques ont présenté la nouvelle stratégie nationale pour le cloud[^ref0].

Après l’échec du "cloud souverain" il y a plusieurs années, la principale nouvelle est l'annonce d'un "cloud de confiance". Fini aussi les trois niveaux de cloud promus par l'ancien secrétaire d’État au numérique, Mounir Mahjoubi en 2018[^ref1]. Il n'en restera finalement que deux : le cloud interne pour les Ministères de l'Intérieur et des Finances Publiques et le cloud externe dit "de confiance".

<!-- more -->

Pour rappel, l'association InterHop, réunit plusieurs fois en collectif, a alerté depuis presque deux ans des risques liés à l'extraterritorialité du droit américain qui permet au service de renseignements américains de collecter de façon massive l'ensemble des données des européennes. Ces textes de portée mondiale s'appellent le CLOUDAct mais surtout le FISA (section 702) et l'Executive Order 12333.
Nous avons attaqué plusieurs fois l'État au Conseil d'État : concernant le Health Data Hub hébergé chez Microsoft et concernant la campagne de vaccination orchestrée  par Doctolib hébergée chez Amazon. Microsoft et Amazon sont des sociétés américaines et ont donc l'obligation légale de se plier à des injonctions américaines, même si, des contrats, des arrêtés ou des décrets français et européens l'interdisent.

Nous sommes donc heureux que ce soit le Health Data Hub qui ait "justifié, d'une  certaine  manière,  que  nous  formalisions  cette  mise  à  jour  de  la  doctrine  du  cloud,  notamment quand il s'agit de données qui sont maniées par les administrations ou par des opérateurs publics".

Contredisant cette fois-ci le Conseil d'État dans l'affaire Doctolib[^docto] (!), le gouvernement reconnaît pleinement ce risque : "nous  refusons  ces  lois  extraterritoriales  donc  nous  nous  donnons  les  moyens  de  nous  protéger juridiquement par rapport à des lois américaines qui permettraient de récupérer les données". 

# Qu'est-ce le cloud de confiance ?

Avec le cloud confiance, deux nouvelles règles apparaissent : "les entreprises  qui  utiliseront  ce  cloud  de  confiance" devront souscrire à  "deux  conditions :  les serveurs doivent être opérés en France et les entreprises qui utilisent et vendent ce cloud doivent être européennes  et appartenir  à  des  Européens. Ce  sont  les  deux  garanties  juridiques  que  nous donnons  en  matière  d'indépendance  par  rapport  aux  lois  extraterritoriales  américaines."

En effet nous avons alerté que les conditions d'hébergement (localisation des serveurs en France par exemple) n'étaient pas suffisantes et que les entités hébergeant les données et propriétaires des serveurs devaient être exclusivement soumises au droit européen[^cnil] !


Concernant les modalités techniques d'hébergement, le gouvernement poursuit l'existant avec l'application large de la certification SecNumCloud qui est délivrée par l’Agence nationale de la sécurité des systèmes d’information (ANSSI) 

Sur le terrain du risque lié aux effets extraterritoriaux du droit américain nous sommes donc en grande partie rassurés. InterHop est même fière d'avoir participé aux discussions et d'avoir été entendue.



# Le mécanisme des licences

Cependant, partant du principe que "les meilleures entreprises de services mondiaux  aujourd'hui sont  américaines" le gouvernement a donc décidé que "Microsoft  ou  Google  pourrait licencier  tout  ou partie  de  leur  technologie  à  des  entreprises  françaises.". En résumé donc, nous revenons au bon vieu principe des licences. Un logiciel développé par Microsoft installé sur des serveurs OVH ou Orange.

**Le mécanisme de licence suffit-il à lui seul à contrer les lois extraterritoriales comme le Cloud Act ?**


Pour Monsieur O "La réponse est très clairement, oui." 

Cependant dans une audition de Guillaume POUPARD - directeur de l'ANSSI (Agence Nationale de Sécurité des Systèmes d'Information) sur la cybersécurité et le HDH, la réponse est plus nuancée. Le "risque est possible mais résiduel". Est-ce possible de courir un risque si mince soit-il sur des données de santé alors même qu'il existe de multiples alternatives? 
Arrêtons de centrer le débat sur le texte le moins liberticide qu'est le CLOUD Act. Les vraies problématiques se situent du coté du FISA et de l'EE 12333.

InterHop est donc certaine que le risque extraterritorial est diminué, mais que cela ne l'annule pas.

# Les autres acteurs

Concernant le Health Data Hub l'État a finalement deux choix :
- Choisir des acteurs purement français comme OVH, Outscale, le CASD.eu, les hôpitaux; et les aider à monter en compétence. Nous signalons aussi que par exemple l'APHP ou le [CASD.eu](https://casd.eu) traitent des millions de données de patients. L'expertise française permet déjà de faire des traitements de données à l'état de l'art (intelligence artificielle) avec un niveau de sécurité technique de haut niveau. Rappelons aussi que personne n'est à l'abri d'attaques y compris Microsoft[^microsoftattack].
- Prendre les opérateurs américains en faisant porter sur la licence les solutions soumises au droit européen est le choix de la facilité.

Nous soulignons d'une part le risque de dépendance en cas de volonté de retrait de la solution. Le cheval de Troie est alors encore plus solidement ancré. D'autre part les problématiques d'interopérabilité et de portabilité des données n'ont par ailleurs jamais été évoquées. 

Nous soulignons aussi les risques d’augmentation des prix[^augmentation_prix] lorsque les produits Microsoft sous licence migreront du cloud Azure vers celui d’OVH. Nous signalons que certains acteurs d'intérêt public pratiquent des prix beaucoup moins chers pour des services comparables et souverains[^casd_tarif].


# Conclusion d'InterHop

InterHop demande un moratoire sur le Health Data Hub. Le risque d'extraterritorialité est maintenant clairement établi par la justice et par l'Etat. Irresponsable est donc celui qui permettra l'envoi de données vers une plateforme assujettie au droit américain ne rentrant pas dans le cadre du "cloud confiance" même si, comme le soutient Interhop, cette solution est loin d'être parfaite.

Par ailleurs qu'en est-il des sociétes privées (hors administrations) européennes hébergeant leurs données sur un cloud soumis au droit américain ? Nous évoquons ici par exemple les sociétés Doctolib ou Lifen hébergées chez le cloud américain Amazon Web Service. Combien de temps ont-elles pour se conformer ?

Pour  annuler  tout  risque  de  transfert  de  données  en  dehors  du  territoire  de  l'Union européenne" les administrations - et HDH est concerné - auront "12  mois pour migrer  à  partir  du  moment  où  des  offres  de  cloud  de  confiance existeront." : c'est le « Cloud au centre » énoncé par Madame Amélie De Montchalin. Ce délai de 12 mois s'appliquera-t-il au secteur privé ?

Par ailleurs et surtout qu'en est-il des sociétés américaines ayant des filiales françaises  traitant directement des données de santé européennes et françaises ? Un exemple mis en lumière dans le dernier Cash Investigation[^cash] : la société IQVIA qui selon sa politique de confidentialité permet que des données de santé soient "transférées par IQVIA aux États-Unis dans le cadre de ces activités liées à la recherche"[^reflol].

[^ref0]: [https://www.economie.gouv.fr/cloud-souverain-17-mai](https://www.economie.gouv.fr/cloud-souverain-17-mai)

[^ref1]: [https://www.banquedesterritoires.fr/letat-definit-trois-niveaux-de-cloud-pour-concilier-securite-des-donnees-et-acceleration-des-usages](https://www.banquedesterritoires.fr/letat-definit-trois-niveaux-de-cloud-pour-concilier-securite-des-donnees-et-acceleration-des-usages)

[^docto]: [Décision du Conseil d’Etat du 12 mars 2021](https://interhop.org/2021/03/12/communique-presse-decision-ce-rendezvous)

[^microsoftattack]: [Les hackers de SolarWinds ont accédé au code source de Microsoft Azure, Intune et Exchange ](https://www.usine-digitale.fr/article/les-hackers-de-solarwinds-ont-accede-au-code-source-de-microsoft-azure-intune-et-exchange.N1062819)

[^casd_tarif]: [CASD - Tarifs](https://www.casd.eu/tarifs-2/)

[^reflol]: [Politique de protection des données personnelles - Iqvia](https://pharmastat.iqvia.com/donnees-personnelles)

[^cash]: [Cash investigation - Nos données personnelles valent de l'or !](https://peertube.interhop.org/w/efFi5JvzWh5jMsk7jttGbD)

[^cnil]: [https://cnll.fr/documents/35/OBSERVATIONS_DE_LA_CNIL_8_OCTOBRE_2020.pdf](https://cnll.fr/documents/35/OBSERVATIONS_DE_LA_CNIL_8_OCTOBRE_2020.pdf)

[^augmentation_prix]: [Le gouvernement français précise sa stratégie Cloud et veut imposer encore plus de souveraineté au stockage des données…](https://www.itforbusiness.fr/le-gouvernement-francais-precise-sa-strategie-cloud-et-veut-imposer-encore-plus-de-souverainete-au-stockage-des-donnees-43338)
