---
layout: post
title: "Bilan de l'année 2024 : Ensemble, créons un numérique libre en santé"
categories:
  - Newsletter
  - Décembre
  - "2024"
ref: newsletter-decembre-2024
lang: fr
---

L'année 2024 a été une année de **grands projets** et de **défis** pour l'association InterHop. Merci à nos équipes bénévoles pour le temps passé à développer des outils numériques au **service** de la **santé publique** et de la **recherche**. 
Nous sommes heureux de vous présenter un aperçu de nos réalisations de l'année et de vous inviter à **soutenir nos actions** par un don.

<!-- more -->

# LinkR : un outil low-code de Data Sciences en santé, renforcée par la saison 12 de DataForGood

Cette année, InterHop a participé à la 12e saison d'accélération de l'[association DataForGood](https://interhop.org/2024/02/04/dataforgood) avec notre projet LinkR. Ce logiciel libre dédié à l'analyse de données de santé a bénéficié de nombreuses améliorations, notamment le développement de plugins permettant de faciliter la visualisation et l'analyse des données médicales. Ces avancées offrent aux cliniciens et chercheurs des outils plus efficaces pour manipuler de grandes quantités de données, avec des solutions low-code adaptées.


👉 [Découvrez LinkR et son impact](https://interhop.org/projets/linkr)

# Goupile : L'outil libre de recueil de données pour la recherche

Le projet Goupile a connu une année exceptionnelle, avec notamment une présentation lors de l'atelier [BlueHats](https://interhop.org/2024/05/24/bluehats) en juin 2024. Goupile est un outil libre de conception d'eCRF (formulaires électroniques de collecte de données), utilisé dans de nombreuses institutions et projets de recherche. Cette année, Goupile a également reçu un financement de la [NLnet Foundation et de la Commission Européenne](https://interhop.org/2024/11/11/goupile-nlnet), soulignant l'importance de ce projet pour la recherche en santé.

Grâce à Goupile, les chercheurs peuvent concevoir facilement des formulaires pour la collecte de données, accessibles à la fois sur ordinateur et sur mobile. Nous continuons à améliorer cet outil pour le rendre encore plus accessible aux établissements et chercheurs du monde entier.

👉 [En savoir plus sur les fonctionnalités à venir en 2025](https://interhop.org/2024/11/11/goupile-nlnet)


# LibreDataHub : Une plateforme libre pour l'analyse des données

Le lancement de **LibreDataHub** en 2024 a constitué un tournant majeur pour notre association. Cette plateforme libre d'analyse de données regroupe plusieurs outils open source pour le stockage, l'analyse, et l'apprentissage automatique des données. LibreDataHub inclut des applications comme LinkR, Airflow, Jupyter, Metabase et bien d'autres, permettant une analyse de données collaborative et sécurisée, idéale pour les chercheurs et cliniciens.

Cette année, LibreDataHub est installé sur nos [serveurs HDS](https://interhop.org/projets/hds), au sein d'institutions de santé, et nous avons accompagné des étudiants du Master Data Science pour la Santé à Lille ou à Saint-Denis, en leur offrant une plateforme pratique pour apprendre Python, R et SQL, sans avoir à installer des logiciels complexes.

En septembre, LibreDataHub a également permis d'organiser le [Datathon](https://interhop.org/projets/datathon) réunissant bénévoles et chercheurs. 

![](https://pad.interhop.org/uploads/0fd02638-d17c-4945-bd3d-748fc56590b6.jpg)

👉 [Découvrez LibreDataHub](https://libredatahub.org){:target="_blank"}

# Collaboration avec la communauté et nos partenaires

En 2024, nous avons renforcé nos liens avec la communauté open source, les chercheurs et les institutions publiques. De nombreux bénévoles ont rejoint nos projets, et nous avons multiplié les partenariats pour développer des solutions innovantes. Nous remercions chaleureusement toutes celles et ceux qui ont contribué à nos projets, que ce soit en tant que développeur·euse, formateur·rice, ou en partageant leur expertise.

Par exemple InterHop est fière d'animer le [réseau InterCHU](https://interhop.org/projets/interchu) qui promeut l'interopérabilité et le modèle OMOP  au sein des centres hospitaliers français.

Nous nous sommes également rapprochés de la communauté OpenStreetMap ainsi que de la DINUM dans le cadre du [Référentiel National du Batiment](https://rnb.beta.gouv.fr/){:target="_blank"}.

# Cartographie de l'offre de soin et Open Data

InterHop travaille dans le cadre d'un projet soutenu conjointement avec les associations **[Latitudes](https://www.latitudes.cc/){:target="_blank"}** et **[Toobib](https://toobib.org){:target="_blank"}**. Ce projet vise à créer une cartographie de l'offre de soins à jour, en croisant les données publiques et les contributions des citoyens, des soignants et des institutions de santé. Ce travail collaboratif permettra de répondre à plusieurs défis cruciaux pour le système de santé, en particulier la gestion de l'offre de soins.

Le projet s'appuie sur des données publiques et des outils open-source pour améliorer l'accès aux soins sur le territoire national. L'objectif est de maintenir à jour des informations géolocalisées concernant les établissements de soins, en les enrichissant par des données sur les conditions d'accès, les horaires d'ouverture, les périodes de congés, etc.


Cette année, nous avons renforcé notre engagement pour la transparence et l'ouverture des données en travaillant sur des **alignements de données** que nous avons publiées sur la plateforme **[Data.gouv.fr](https://www.data.gouv.fr/fr/organizations/interhop/){:target="_blank"}**. En mettant nos jeux de données à disposition du public, nous permettons à d'autres organisations, chercheurs, et acteurs de la santé publique de les exploiter, de les enrichir et de les utiliser pour des analyses ou des projets collaboratifs.

Ces alignements visent à corriger les incohérences entre les différentes bases de données existantes et à offrir des jeux de données plus précis et à jour. En particulier, nous avons travaillé sur :

- **La géolocalisation des établissements de santé** : en alignant les données de santé avec le [référentiel national des adresses](https://adresse.data.gouv.fr/){:target="_blank"} et des [bâtiments](https://rnb.beta.gouv.fr/){:target="_blank"}.
- **L'intégration et la mise à jour régulière des référentiels** : les données mises à disposition sur **[Data.gouv.fr](https://www.data.gouv.fr/fr/organizations/interhop/){:target="_blank"}** sont mises à jour régulièrement et peuvent être consultées par les acteurs du secteur de la santé pour améliorer la qualité de l'offre de soins et la planification des ressources.

👉 [Découvrez nos jeux de données sur Data.gouv.fr](https://www.data.gouv.fr/fr/organizations/interhop/#/datasets){:target="_blank"}


# Accès plus facile à nos applications

InterHop propose depuis peu un système de Single Sign-On (SSO) permettant aux utilisateurs d'accéder facilement à l'ensemble de ses applications avec un seul identifiant. Ce système facilite l'authentification et la gestion des accès, tout en garantissant la sécurité des données.

La liste des applications accessibles est disponible en ligne : [interhop.org/apps](https://interhop.org/apps)

![](https://pad.interhop.org/uploads/686c5e0a-e4b5-4820-a375-3ec58af53767.png)

👉 [L'inscription est ouverte à tous et à toutes librement](https://keycloak.interhop.org/realms/Interhop/account/){:target="_blank"}

# Pourquoi nous avons besoin de vous

Les projets d'InterHop sont tous portés par des bénévoles passionnés, mais pour continuer à offrir des solutions accessibles et libres, nous avons besoin de ressources. Nous faisons appel à votre générosité pour soutenir nos projets, leur permettre de se développer et de toucher un public encore plus large.

**Faire un don à InterHop, c’est soutenir l’innovation en santé publique, l’open data, et la recherche médicale.**

### Comment faire un don ?

Votre soutien est essentiel pour continuer à porter des projets ambitieux en 2025. Vous pouvez faire un don directement sur notre site [ici](https://interhop.org/dons). Chaque contribution, même modeste, nous aide à aller plus loin et à rendre nos projets accessibles à tous.

### Merci à vous !

En 2024, grâce à l'engagement de chacun, nous avons pu accomplir de grandes choses. Mais ce n'est que le début. En 2025, nous comptons sur vous pour faire avancer nos projets et continuer à promouvoir l'innovation ouverte au service de la santé.

### Restez connectés

N’hésitez pas à suivre nos actualités sur [Mastodon](https://mstdn.io/@interhop){:target="_blank"}, [Matrix](https://matrix.to/#/#public:matrix.interhop.org){:target="_blank"} et [LinkedIn](https://www.linkedin.com/company/interhop/){:target="_blank"}, et rejoignez-nous pour promouvoir un numérique éthique  en santé.

Librement, InterHop
