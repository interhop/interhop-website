---
layout: post
title: "EasyAppointments - Validation des rendez-vous avec le courriel"
categories:
  - Développement
  - EasyAppointments
  - Stagiaires
ref: stagiaire-ea-post-4
lang: fr
---

L'association InterHop est ravie de recevoir ses premiers stagiaires en développement web. 

Voici leur quatrième billet de blog.

<!-- more -->


## Finalisation des plages horaires spécialisées et leur traitement

Après avoir mis en place la sélection des horaires proposés en fonction de la catégorie choisie par le patient, nous avons remarqué quelques dysfonctionnements. Il est désormais nécessaire de catégoriser toutes les plages horaires du planning de travail selon le type de consultations proposées par le soignant. En effet, les catégories sont désormais indispensables dans la gestion des horaires disponibles et ouverts à la prise de rendez-vous. On ne peut donc pas laisser des plages horaires non-catégorisées.  

![](https://pad.interhop.org/uploads/626d9d28-5c3c-4783-9559-cce46391cbc3.png)

![](https://pad.interhop.org/uploads/626d9d28-5c3c-4783-9559-cce46391cbc3.png)

## Retouches nécessaires

Suite à ces petites corrections, nous avons fait beaucoup de tests sur l'application.
Nous avons remarqué quelques petites failles
* un petit bug survenant après l’ajout d’un patient.
Si on ajoute un nouveau patient et que la première lettre de son prénom ne rentre pas l'intervalle de ceux apparaisssant dans la liste des 20 affichés à gauche (rangée par ordre alphabétique), la page ne se recharge pas et bloque, même si l'enregistrement du patient est fait. 
Nous avons contourné ce problème en rechargeant la page avec le formulaire vide (sans les données du patient nouvellement ajouté).
* la saisie de n'importe quel caractère dans le formulaire de détails 
Nous avons donc mis en place des expressions régulières (Regex) pour limiter les caractères admissibles : 
- Pour les numéros de téléphone : seuls des chiffres acceptant le +33 (indicatif France).
- Pour les noms et prénoms : seulement des lettres et "-" pour les noms composés
- Pour les nom d'utilisateurs : seulement les lettres et les chiffres et "-".
- Pour les mots de passe, on a voulu renforcer la sécurité du mot de passe en ajoutant des conditions : 
  - minimum de 7 caractères
  - maximum de 30 caractères
  - au moins 1 majuscule
  - au moins 1 minuscule
  - au moins 1 chiffre
  - au moins 1 caractère spécial -+!*$@%_&
   
## Vérification de l'adresse e-mail du patient

Un patient est identifié par l’adresse e-mail qu’il saisit lors de sa prise de rendez-vous. Chaque e-mail doit donc être unique dans la base de données.
Un même patient peut prendre plusieurs rendez-vous, il va donc à chaque fois saisir son adresse e-mail, ce qui mettra à jour ses informations personnelles. Par contre, si une autre personne utilise la même adresse mais saisit d’autres informations personnelles (nom, prénom, numéro de téléphone, …), elle prendra sa place dans la base de données. Les rendez-vous passés et futurs seront attribués à l’usurpateur.

Nous avons donc rajouté la validation du rendez-vous par l'e-mail. La prise de rendez-vous se fait en quatre étapes. La dernière étant la confirmation par l’envoi d’un courriel récapitulatif et l’enregistrement en base de donnée du patient et du rendez-vous. 
La vérification de l’adresse e-mail du patient doit se faire après la troisième étape, la saisie des informations personnelles. 
En effet une fois l’e-mail renseigné, en cliquant sur “suivant“ un code généré aléatoirement est envoyé à l’adresse fournie. Le patient devra ouvrir sa boîte mail pour récupérer le code et le saisir correctement afin de valider son rendez-vous.
Ce qui permet de s’assurer que le patient a bien accès à cette adresse e-mail.
Nous avons ajouté une page pour la saisie du code lors de la prise de rendez-vous, généré l'envoi d'un mail avec le code et le mail en lui-même.

![](https://pad.interhop.org/uploads/93fd144c-2be3-49f0-b239-98c45e820999.png)

![](https://pad.interhop.org/uploads/df85f2f2-9de6-42e3-bc5f-d703d3b55efa.png)

Maintenant que ça marche en local nous essayons de le mettre en place sur le serveur de l'hébergeur.

Nous prévoyons en dernière semaine de rédiger un petit tutoriel illustré pour les usagers et de faire un chiffrement du mot de passe côté client...
