---
layout: post
title: "EasyAppointments - le mode pédiatrique (entre autres)"
categories:
  - Développement
  - EasyAppointments
  - Stagiaires
ref: stagiaire2-ea
lang: fr
---

L'association InterHop est ravie d'avoir reçu sa deuxième promotion de stagiaires en développement web. Merci pour leur confiance.

Cette fois encore, ils viennent de l'école d'informatique [```ENI Ecole informatique```](https://www.eni-ecole.fr/ecole/campus-physiques/). Il s'agit de Jordan Azran et Aurore Budzik. Nous sommes ravis de leur travail et leur souhaitons, pour la suite, une belle vie professionnelle.

<!-- more -->

Cet été nous avions travaillé sur plusieurs fonctionnalités. La plus importante concernait la sécurité avec l'ajout [d'un code de validation envoyé  par courriel](https://interhop.org/2022/08/09/stagiaire-ea-post-4).

Voici la liste des fonctionnalités mises en place.

# Mode pédiatrie (tierce personne)

Il permet à un parent d’inscrire son enfant avec le courriel parental. 
La fonctionnalité rajoutée permet donc de créer plusieurs noms pour un seul email !
Lors de la prise de rendez-vous, un utilisateur (parent) pourra cocher la case ""rendez vous pour une tierce personne" et indiquer les nom et prénom de son enfant.

Ce mode peut aussi être utilisé pour une personne sous tutelle.

![](https://pad.interhop.org/uploads/d96e848c-9dec-481f-807a-07526381650c.png)

On retrouve bien les informations saisies lors de la confirmation du rendez-vous.

![](https://pad.interhop.org/uploads/c6469c7f-3251-48d0-bf83-9ef96216793b.png)


Dans l’interface d’un.e provider ou d’un.e secrétaire, on crée un nouveau customer auquel on ajoute le nom et le prénom d’une tierce personne.

![](https://pad.interhop.org/uploads/a8e53370-cb51-486a-b693-048ceaf3e6c1.png)

Lors d’une mise à jour des coordonnées, on retrouve bien l’affichage des informations enregistrées pour le rendez-vous d’une tierce personne.

![](https://pad.interhop.org/uploads/328c7e01-cae9-48d1-994d-2077b30308d8.png)

Dans l’interface de prise de rendez-vous pour un provider ou une secrétaire, on retrouve les champs “Prénom” et “Nom” pour le rendez-vous d’une tierce personne : 

![](https://pad.interhop.org/uploads/988b5504-99af-43be-a2d8-b8fc54ada265.png)



Ces informations sont bien répétées sur la vue du calendrier hebdomadaire et quotidien.

![](https://pad.interhop.org/uploads/8f5f0d6e-9d9c-4844-ba7f-d8975f283de9.png)

![](https://pad.interhop.org/uploads/5742e5ee-3053-4f3d-928c-840ee7d79b1a.png)


## Filtre appliqué sur la recherche du nom et prénom d’une tierce personne

A l’ajout d’un customer, on renseigne le prénom et le nom d’une tierce personne.

Ci dessous le prénom est Maya et le nom est Solveig.

![](https://pad.interhop.org/uploads/fb3f33cd-4eff-4aef-868e-d0febc12583e.png)

Le filtre prend bien en compte les champs recherchés :

![](https://pad.interhop.org/uploads/a8a206f2-087e-4e30-a1d5-42bca533b1c8.png)



## Permettre de voir uniquement les clients d’un provider connecté (ou de sa secrétaire)

Sur la version de base de EasyAppointments, les providers voient l'ensemble des clients de l'instance. Nous avons souhaité limiter cela.
Un provider ne doit pouvoir voir que ses clients (patients en l'occurrence)

Dans l’interface du provider on retrouve la liste de ses customers:

![](https://pad.interhop.org/uploads/ddcb1541-21fa-48c4-a688-92cfd31bf88a.png)

![](https://pad.interhop.org/uploads/47e94081-85af-4943-9738-1d94564800dd.png)

Le provider connecté à l’id 5, ses customers ont les id 28,29,31,et 32, en base de données.

Dans la table ”ea_users”:

![](https://pad.interhop.org/uploads/998e9be8-9f85-4009-a51f-14e859931536.png)

Dans la table ”ea_appointments”:

![](https://pad.interhop.org/uploads/dee7f2ea-4667-4418-b9de-fe87cda23dee.png)


## Limiter le nombre de patient pour un provider dans l’interface admin / du provider

Dans l’interface admin, on attribue une limite de customers au provider “Soignant B”

![](https://pad.interhop.org/uploads/b9a0c94a-50bd-44f6-a760-a664a5f7e42c.png)

Dans la table ”ea_users”, la limite de customers est prise en compte dans la colonne “customers_count”.

![](https://pad.interhop.org/uploads/4bcbb37a-0966-447c-8a84-549ddb49cfaf.png)

Dans l’interface du provider, on retrouve la liste de ses customers:

![](https://pad.interhop.org/uploads/d6eeb8b9-a0e6-4171-a264-0a35297e6559.png)

On retrouve les id des customers relié au provider en base de données :

![](https://pad.interhop.org/uploads/eec98f67-66d0-483d-b9a4-b04ef4d73644.png)

A l’ajout d’un nouveau client, le message “You can no longer add patients” s’affiche :

![](https://pad.interhop.org/uploads/b577b414-d197-430f-9609-859dbacb5a9c.png)

![](https://lh4.googleusercontent.com/0jeNFrVMRJ1W5xC2gn2ZLkymeV3WA_AWxYYDooCps9AxB59UQbvKnWKOa5sJGdBVoZG1igCE0kqE6lFLOR1xSKLdGH1ihd5_ewCoKe0dzChRs2BlEpAcvVhEG2wLhN7NnTq9wBfU8uEM8eEm85GN2-TbIgskOp7QYAPeA3fPS5Ydj0gCHQQ3KJLt2BKtGQ)

Ainsi que dans l’interface de prise de rendez vous:

![](https://pad.interhop.org/uploads/c23e0871-bab0-4e8f-98d5-0bdd93f45fe4.png)


# Supprimer le message “enregistrer votre rendez-vous sur l’agenda google” à la validation du rendez-vous côté utilisateur

![](https://pad.interhop.org/uploads/694bd8c4-8188-4840-8b42-5ece9a81af63.png)

![](https://pad.interhop.org/uploads/87fc2b5e-2da1-4cda-8170-cd72b0a8cc79.png)


