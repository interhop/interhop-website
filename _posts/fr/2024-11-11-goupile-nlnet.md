---
layout: post
title: "Goupile : Roadmap 2025"
categories:
  - Goupile
  - NLNet
ref: goupile-nlnet
lang: fr
---

**[Goupile](https://interhop.org/projets/goupile)** continue d'évoluer, avec l'objectif de rendre la collecte de données de santé plus ouverte, transparente et accessible. Ce projet libre, développé par Niels MARTIGNENE au sein de l'association **InterHop**, permet aux chercheur·euses et professionnel·les de santé de concevoir des **formulaires électroniques de collecte de données** (eCRF). 

<!-- more -->

Aujourd'hui, Goupile se déploie localement dans plusieurs institutions de recherche en santé. Ce billet présente les avancées de Goupile et les fonctionnalités à venir.

# Soutien de la NLnet Foundation

Nous sommes fière d'annoncer que Goupile est maintenant financé par la [foundation NLnet et de la Commission Européenne](https://nlnet.nl/project/Goupile/){:target="_blank"}. NLNet est une organisation qui finance des initiatives open-source à impact positif sur la société.

![](https://pad.interhop.org/uploads/85dc1020-435d-473a-b418-5fb6ea7c5e2a.png)

# Fonctionnalités à venir

Voici un aperçu des **fonctionnalités à venir**, telles qu'elles sont définies dans notre feuille de route sur **Framagit**

### Sécurité et Accessibilité
Le premier axe de travail concerne la réalisation d'un **audit de sécurité et d'accessibilité** pour garantir que les données collectées dans le cadre de projets de santé sont protégées et que l'outil est accessible à tous.  Ceci inclut la révision du modèle de données, des mécanismes de gestion des utilisateurs·trices et des permissions, et la mise en œuvre des recommandations issues des tests.

### Gestion des Utilisateurs et Intégration SSO (Single Sign-On)
Nous prévoyons également une **amélioration de la gestion des utilisateurs·trices**, notamment en intégrant des protocoles **SSO (Single Sign-On)** comme OIDC et SAML. Cela permettra de centraliser l'authentification et la gestion des droits des utilisateurs·trices. Goupile pourra ainsi s’intégrer à des systèmes d'authentification existants, facilitant l'accès au système pour les utilisateurs·trices et réduisant les efforts nécessaires pour gérer les permissions. Des outils seront également développés pour importer et exporter des utilisateurs et gérer leurs droits de manière simplifiée (via CSV, par exemple).

### Collecte de Données Hors Ligne
Un autre aspect important est l’amélioration de la **collecte de données hors ligne**. Actuellement, les utilisateurs·trices rencontrent des difficultés pour savoir quand Goupile fonctionne en mode hors ligne et pour gérer la synchronisation des données. Il est nécessaire de clarifier les fonctionnalités disponibles en mode hors ligne, en fournissant des indications visuelles et textuelles claires. Des rappels réguliers seront aussi ajoutés pour informer les utilisateurs·trices de l'état de synchronisation des données.

### Internationalisation de l'Interface Utilisateur
Nous  prévoyons de rendre Goupile accessible à un public plus large en **internationalisant** l’interface. Actuellement en français, Goupile sera traduit en anglais en priorité, mais cette approche pourrait s'étendre à d’autres langues à l'avenir. Cela inclut la traduction des textes de l’interface utilisateur·trice, des messages d’erreur serveur et de la documentation complète, afin de rendre l'outil utilisable à l'international, notamment dans des contextes multiculturels ou multilingues.

### Amélioration des Outils d'Administration
Un autre objectif majeur est d'améliorer les **outils d’administration** pour simplifier la gestion des paramètres de Goupile. Cela inclut des paramètres comme la configuration des courriels avec SMTP, l'intégration de l'API SMS, la gestion des clés de chiffrement et l'ajout de nouveaux paramètres spécifiques aux projets (ex. : dates de début et de fin de collecte de données, politique de conservation des données).

### Assistants visuels et Documentation
Une autre priorité est d’améliorer la **conception des formulaires** et l’**expérience utilisateur·trice**. Goupile souhaite simplifier la création et la gestion des formulaires à l’aide d’assistants virtuels pour ajouter des questions de manière intuitive. Ces outils permettront de générer des questions de type texte, choix unique, échelle, etc. 

De plus, une documentation complète sera mise à disposition pour guider les utilisateurs·trices tout au long du processus de création de projet, incluant des sections sur l’administration des projets, des utilisateurs·trices, la gestion des archives, ainsi qu’un tutoriel interactif pour la création d'un formulaire complet de collecte de données.


### Gestion des Ontologies et Terminologies
Pour faciliter la gestion des données et assurer une cohérence dans les termes utilisés, Goupile prévoit d'**intégrer des terminologies standardisées** au travers de FHIR et d'OMOP. Cela permettra une meilleure structuration des données médicales. Il sera aussi possible de gérer ces terminologies en mode hors ligne, tout en intégrant des widgets avec auto-complétion pour simplifier la saisie des données.

### Reconnaissance Vocale
Une autre fonctionnalité en développement est le support de la **reconnaissance vocale** pour la saisie de texte dans Goupile. Cette fonctionnalité sera particulièrement utile dans des environnements où l’accès à Internet est limité ou pour les professionnel·les de santé sur le terrain. L'objectif est d'utiliser un moteur de reconnaissance vocale fonctionnant en mode hors ligne (par exemple, [Vosklet](https://github.com/msqr1/Vosklet){:target="_blank"}), et de garantir que cette fonctionnalité fonctionne de manière fiable, même sans connexion.

### Randomisation

Nous voulons améliorer la prise en charge de la randomisation, essentielle pour de nombreuses études cliniques. Goupile offrira plusieurs types de randomisation, y compris la randomisation simple (améliorée pour être plus facile à utiliser), ainsi que des fonctionnalités plus avancées comme la randomisation stratifiée et en blocs. 

### Import / export

L’export et l’import des données seront également simplifiées, avec la création de web API pour l’import en masse et une interface utilisateur·trice simplifiée pour l'import de données au format CSV, XLSX, etc. Cela facilitera l'intégration de données provenant d’autres systèmes ou sources.

De plus nous prévoyions une **intégration à la plateforme [LibreDataHub.org](https://libredatahub.org){:target="_blank"}** pour permettre l’export automatique des données collectées vers LibreDataHub. Les utilisateurs.trices pourront ensuite analyser les données préalablement recueillies.

### Etudes longitudinales

Pour les études longitudinales, Goupile permettra de suivre les participant·es à travers des fonctionnalités telles que la création de comptes invités via des formulaires d’inclusion, l'envoi de liens magiques pour la reconnexion par e-mail ou SMS, et un authentification forte (SMS, 2FA) pour garantir la sécurité des données collectées. Il sera également possible de planifier des tâches pour rappeler aux participant·es de compléter des données supplémentaires à des dates précises, facilitant ainsi la collecte de données à long terme et améliorant la participation continue à l'étude.


👉 [Consulter la feuille de route et les issues sur Framagit](https://framagit.org/interhop/goupile/-/issues/?label_name%5B%5D=NLNet)
