---
layout: post
title: "La fin des Google Analytics pour la e-santé ?"
categories:
  - RGPD
  - CNIL
  - GAFAM
ref: saisine-cnil-google-analytics-suite-1
lang: fr
---

En janvier dernier[^Google_analytics] nous avons saisi la CNIL concernant l'utilisation de l'outil Google Analytics par plusieurs acteurs de la santé.

<!-- more -->

> Qu’en est-il des entreprises digitales en santé ? 
> Plusieurs d'entre elles utilisent ce service fourni par Google, nommé Google Analytics. En voici une liste non exhaustive : Recare, Qare[^qare], HelloCare[^hellocare], Alan[^alan], Therapixel[^therapixel], Implicity[^implicity], Medaviz[^medaviz], Medadom[^medadom], KelDoc[^keldoc], Maiia[^maiia] ...

Notre action faisait suite à la décision de la CNIL autrichienne qui avait jugé l’utilisation de Google Analytics comme illégale et contraire au RGPD[^decision_GA].

**Suite à ce signalement, à l'action conjointe de notre avocate Juliette ALIBERT et de la CNIL plusieurs de ces entreprises du digitale en santé ont arrêté l'utilisation du service de Google.** Merci à elles.

Nous remercions particulièrement la CNIL pour son action en faveur de la protection des libertés fondamentales numériques.

Cette décision pose cependant plusieurs questions. 

La première est de comprendre pourquoi toutes les entreprises de la e-santé n'ont pas stoppé l'utilisation des Google Analytics. Nous rappelons qu'au cours de la procédure auprès de la CNIL autrichienne, Google avait avoué[^google_analytics_avouement] que 
> toutes les données collectées par Analytics [...] sont hébergées (c'est-à-dire stockées et traitées ultérieurement) aux États-Unis.

[^google_analytics_avouement]: [Utiliser Google Analytics serait une violation du RGPD, selon l’Autriche](https://www.blogdumoderateur.com/utiliser-google-analytics-violation-rgpd-autriche/)

La seconde est plus large et concerne l'utilisation de services informatiques cloud  américains pour des données de santé. Qu'en est-il de la légalité de CloudFlare[^Docto_cloudflare] ? Du Cloud Microsoft Azure[^StopHealthDataHub][^synapse] ? De l'outils Teams [^teams] ? d'Amazon Web Service [^lifen]

Ces services ne sont pas plus légaux que Google Analytics. Ni un accord cadre transatlantique, ni des mesures supplémentaires pour renforcer la protection des données de santé ne permettent de garantir un niveau de protection équivalent au droit de l'Union Européenne[^ga_cnil].
> Par conséquent, les mesures supplémentaires adoptées, telle qu’elles ont été présentées par Google, ne sont pas efficaces dans la mesure où aucune d’entre elles ne résout les problèmesspécifiques au cas d’espèce. En effet, aucune d’entre elles n’empêche les services de renseignement américains d’accéder aux données en cause ou ne rendent cet accès ineffectif.

[^ga_cnil]: [Mise en demeure anonymisée CNIL - Google analytics](https://www.cnil.fr/sites/default/files/atoms/files/med_google_analytics_anonymisee.pdf)

Ensemble et avec nos partenaires, nous poursuivrons donc nos contentieux stratégiques pour garantir la confiance des patient.e.s dans le système de soin.


[^synapse]: [Mentions légales - Synapse](https://synapse-medicine.com/fr/legal)

[^lifen]: [Mentions légales - Lifen](https://www.lifen.fr/mentions-legales)

[^teams]: [L’Institut Curie déploie Microsoft Teams pour assurer la continuité des soins et innover](https://www.youtube.com/watch?v=3ykaHCDzg9w)

[^Docto_cloudflare]: [Politique de protection des Données à caractère personnel - Professionnels de santé](https://info.doctolib.fr/politique-de-protection-des-donnees-personnelles/)

[^StopHealthDataHub]: [#StopHealthDataHub : les données de santé en otage chez Microsoft](https://interhop.org/2020/12/14/stophealthdatahub-donnees-de-sante-en-otage-chez-microsoft)

[^decision_GA]: [https://noyb.eu/sites/default/files/2022-01/E-DSB%20-%20Google%20Analytics_DE_bk.pdf](https://noyb.eu/sites/default/files/2022-01/E-DSB%20-%20Google%20Analytics_DE_bk.pdf)

[^Google_analytics]: [Saisine de la CNIL concernant l'utilisation par de nombreux acteurs de la e-santé des Google Analytics](https://interhop.org/2022/01/29/saisine-cnil-google-analytics)

[^doctolib_chiffrement]: [Doctolib: « Lorsque vous faites le chiffrement des données, l’hébergeur a peu d’importance »](https://www.frenchweb.fr/doctolib-lorsque-vous-faites-le-chiffrement-des-donnees-lhebergeur-a-peu-dimportance/402057)

[^cnil_donnee_nodate]: [https://www.cnil.fr/fr/definition/donnee-sensible](https://www.cnil.fr/fr/definition/donnee-sensible)

[^edpb_schrems]: [Recommendations 01/2020 on measures that supplement transfer tools to ensure compliance with the EU level of protection of personal data](https://edpb.europa.eu/sites/edpb/files/consultation/edpb_recommendations_202001_supplementarymeasurestransferstools_en.pdf)

[^curia_cjue]: [La Cour invalide la décision 2016/1250 relative à l'adéquation de la protection assurée par le bouclier de protection des données UE-États-Unis](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

[^homomorphe]: [Pourquoi le Health Data Hub travestit la réalité sur le chiffrement des données de santé sur Microsoft Azure](https://interhop.org/2020/06/15/healthdatahub-travestit-le-chiffrement-des-donnees)

[^cnil_conseil_etat_2]: [Avis CNIL, Conseil d'Etat,  Mémoire en Observation](https://www.documentcloud.org/documents/7224049-Me-moireCnilHDH.html)

[^conseil_etat_2]: [Health Data Hub et protection de données personnelles : des précautions doivent être prises dans l’attente d’une solution pérenne](https://www.conseil-etat.fr/actualites/actualites/health-data-hub-et-protection-de-donnees-personnelles-des-precautions-doivent-etre-prises-dans-l-attente-d-une-solution-perenne)

[^alan]: [Où nous hébergeons les données de nos membres et pourquoi](https://blog.alan.com/tech-et-produit/pourquoi-nous-hebergeons-sur-aws)

[^article]: [L'annulation du Privacy Shield doit s'appliquer immédiatement, tranche la Cnil européenne](https://www.usine-digitale.fr/article/l-annulation-du-privacy-shield-doit-s-appliquer-immediatement-tranche-la-cnil-europeenne.N989069)

[^transparence]: [Application « StopCovid » : la CNIL tire les conséquences de ses contrôles ](https://www.cnil.fr/fr/application-stopcovid-la-cnil-tire-les-consequences-de-ses-controles)

[^recare]: [Recare : Politique de protection des données personnelles](https://www.recaresolutions.fr/protection-des-donnes-personnelles)

[^medadom]: [https://www.medadom.com/cgu](https://www.medadom.com/cgu)

[^doctolib_cgu]: [https://www.doctolib.fr/terms/agreement](https://www.doctolib.fr/terms/agreement)

[^lifen_cgu]: [https://www.lifen.fr/confidentialite](https://www.lifen.fr/confidentialite)

[^qare]: [https://www.qare.fr/cookies](https://www.qare.fr/cookies)
[^hellocare]: [https://hellocare.com/confidentialite](https://hellocare.com/confidentialite)
[^alan]: [https://alan.com/privacy](https://alan.com/privacy)
[^therapixel]: [https://www.therapixel.com/privacy-policy/](https://www.therapixel.com/privacy-policy/)
[^implicity]: [https://www.implicity.com/privacy-policy/](https://www.implicity.com/privacy-policy/)
[^medaviz]: [https://www.medaviz.com/politique-de-confidentialite-3/#cookies](https://www.medaviz.com/politique-de-confidentialite-3/#cookies)
[^medadom]: [https://www.medadom.com/cookies](https://www.medadom.com/cookies)
[^keldoc]: [https://www.keldoc.com/a-propos/cookies](https://www.keldoc.com/a-propos/cookies)
[^maiia]: [https://www.maiia.com/donnees-personnelles](https://www.maiia.com/donnees-personnelles)
