---
layout: post
title: "Encryption Europe - Santé"
categories:
  - Chiffrement
  - Extra-terriorial
  - Loi
ref: encryption-europe-sante
lang: fr
---

Par l’intermédiaire de son avocate Juliette Alibert, ce lundi 31 mai, InterHop était présente au webinaire [Encryption Europe](https://www.encryptioneurope.eu/2021/05/17/Encryption-Europe-Webinar-Series-May-31,-2021.html) pour débattre de la protection des données et des solutions de chiffrement.

<img src="https://i.ibb.co/4pjVxC7/Screenshot-2021-06-03-at-18-32-32.png" alt="Screenshot-2021-06-03-at-18-32-32" border="0">

<!-- more -->

Le webinar a commencé par un état des lieux de la protection des systèmes d’information et des risques dans le secteur de la santé présenté par Bruno Halopeau responsable technique auprès du CyberPeace Institute. Selon ce dernier, le secteur hospitalier est victime de trois attaques « types » : les rançongiciels, les fuites de données, les campagnes de désinformation. Il a rappelé un « manque systémique de ressources dans le secteur » et des infrastructures parfois vulnérables, obsolètes, qui empêchent une bonne protection.

Thierry Le Blond co-fondateur de Parsec a ensuite présenté les solutions dites « Zero Trust » reconnues par l’ANSSI. Elles consistent à implémenter strictement le « Principe du moindre privilège ». Ce principe suppose que chaque terminal et chaque utilisateur doivent être identifiés. En parallèle, une politique de reconsidération en permanence des accès sécurisés est déployée, et le chiffrement de bout-en-bout, lorsqu’il est possible, est privilégié.

Pour le compte d’InterHop, Me Juliette Alibert a posé les problématiques rencontrées par l’association dans le cadre des actions juridiques qu’elle a portées : contre le Health Data Hub, contre le partenariat entre l’État et Doctolib et récemment contre IQVIA (suite au Cash Investigation). Les épineuses questions de : « Qui détient les clefs de chiffrement ? Est-on sur du chiffrement de bout en bout ? Les données sont-elles accessibles à Microsoft et Amazon qui hébergent les données pour le HealthDataHub et Doctolib ? » ont été posées par notre intervenante avec présentation des enjeux pour ces données particulièrement sensibles. Elle a rappelé les principes « chers » à Interhop : l’ouverture du code source (logiciel libre) pour une transparence des algorithmes mais un chiffrement maximal des données de santé pour une protection optimale de l’information de santé.

Le webinar s’est poursuivi par un riche débat autour des problématiques de chiffrement et de gouvernance.

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="ISOC Live - Santé &amp; Zéro Trust #EncryptEurope @EncryptEurope @ParsecScille #GlobalEncryption #CyberSecurity […]" src="https://peertube.interhop.org/videos/embed/d5089944-5ae3-4121-8699-6f73d349b54d" frameborder="0" allowfullscreen></iframe>

