---
layout: post
title: "Datathon 2024 - Qualité de données"
categories:
  - DataThon2024
  - Sujets
ref: datathon-2024
lang: fr
---



> Avec ce sujet les participant⸱es ambitionnent de créer des indicateurs de qualité robustes et réutilisables. Ils pourront être intégrés au sein d'un module [LinkR]({{ site.baseurl }}/projets/linkr) dédié.<br>
> Plus d'informations et inscription : [interhop.org/projets/datathon](https://interhop.org/projets/datathon)

<!-- more -->


# Qualité des données : incidence des logiciels ?

![](https://pad.interhop.org/uploads/da2dbbb0-2b1b-4a2c-9abd-b63a33f930f2.png)

## Sommaire
La qualité des données produites dans le cadre du soin dépend-elle du logiciel qui les a générées ? Les logiciels ont-ils des biais semblables ? Certains logiciels protègent-ils mieux certaines données ? Quelle serait l'origine des erreurs ? Quelles corrections seraient applicables ?

Nous aurons 48h pour pour construire des outils qui nous permettraient de répondre à ces questions.

## Matériel et méthode

Pour faciliter ce parcours et éliminer d'emblée l'un des biais, nous travaillerons sur des données au format OMOP-CDM de la base MIMIC. Et pour faciliter le partage des outils et des observations, le développement sera proposé sur la plate-forme LinkR.

Les différences de comportement des logiciels seront observées selon les valeurs étranges que peuvent prendre les variables telles que :

* poids, taille, age, sexe
* durée de séjour dans le service
* durée d'anesthésie (selon disponibilité des données)
* doses de drogues
* proportion d'artefacts de la pression artérielle

Des propositions de tests et de représentation sont donnés à titre de rampe de lancement.

Cette liste pourra être étendue par d'autres données, susceptibles de défaut de qualité dépendant du logiciel et si possible ayant un impact sur la prise en charge. 

Les critères de vraissemblance des données pourront être simples (bornes min-max), multivariés ou utilisant des similarités. Les critères de qualités déjà définis par OMOP CDM seront utilisables.

### Ressources

- LinkR : [https://interhop.org/projets/linkr]({{site.baseurl}}/projets/linkr)
- DataSet MIMIC au format OMOP: [https://github.com/MIT-LCP/mimic-omop](https://github.com/MIT-LCP/mimic-omop){:target="_blank"}
- Librairies DataQualityDashboard : [https://github.com/OHDSI/DataQualityDashboard](https://github.com/OHDSI/DataQualityDashboard){:target="_blank"}, [https://www.ohdsi.org/software-tools/](https://www.ohdsi.org/software-tools/){:target="_blank"}, [https://data.ohdsi.org/DataQualityDashboard/](https://data.ohdsi.org/DataQualityDashboard/){:target="_blank"}
- [The Book of OHDSI - Data Quality Dashboard in Practice](https://ohdsi.github.io/TheBookOfOhdsi/DataQuality.html#dqdInPractice){:target="_blank"}
- Algorithme de Du & Al de détermination des artefacts de pression artérielle : [https://github.com/jasmine-jk/ICU-MAP-Cleaning/tree/main](https://github.com/jasmine-jk/ICU-MAP-Cleaning/tree/main){:target="_blank"}
- Package rtifact : [https://gitlab.com/beralef/rtifacts](https://gitlab.com/beralef/rtifacts){:target="_blank"}
-  Incidence of Artifacts and Deviating Values in Research Data Obtained from an Anesthesia Information Management System in Children. Anesthesiology 2018;128:293-304. 
  -  Hoorweg AJ, Pasma W, van Wolfswinkel L, de Graaff JC.
  -  [https://doi.org/10.1097/aln.0000000000001895](https://doi.org/10.1097/aln.0000000000001895){:target="_blank"}
- Annexe : Sélection de variables proposées à titre indicatif


## Résultats

Les développements réalisés sur la base MIMIC seront utilisés pour générer des éléments de comparaison entre les données produites dans différents établissements et contextes.

## Annexe

Sélection de variables OMOP utilisables 

|measurement_source_value|measurement_concept_id|measurement_type_concept_id|
|------------------------|------------------------|-----------------------------|
|poids|1003901|32817|
|taille|45876161|32817|
|Temp|4302666|32817|
|SpO2|45770406|32817|
|PASd|44782659|32817|
|PASm|4239021|32817|
|PASs|4152194|32817|
|PNId|4068414|32817|
|PNIm|4108289|32817|
|PNIs|4354252|32817|
|B.I.S|4134573|32817|
|BIS signal quality index|4134573|32817|
|BIS SR|21490690|32817|
|FC|4239408|32817|
|FR|4313591|32817|

