---
layout: post
title: "Le collectif Santénathon continue le combat contre le transfert illégal de nos données de santé aux USA"
categories:
  - HDH
  - Microsoft
  - Conseil d'Etat
  - Europe
  - Privacy Shield
  - RGPD
redirect_from:
  - /communique-presse-refere-privacy-shield-le-collectif-santenathon-continue
ref: communique-presse-refere-privacy-shield-le-collectif-santenathon-continue
lang: fr
---

Un collectif comprenant le CNLL, l'association InterHop, l'association Constances et plusieurs syndicats de médecins et de patients - soit 18 requérants - [avaient demandé au Conseil d’Etat](https://interhop.org/2020/09/16/communique-presse-refere-privacy-shield) de suspendre le traitement et la centralisation de nos données au sein du Health Data Hub hébergé par la société Microsoft. Ce faisant, les requérants demandaient au Conseil d'Etat de s’aligner sur la toute récente jurisprudence européenne.

Cette saisine faisait en effet suite à la décision (arrêt "Schrems II") de la Cour de Justice de l'Union Européenne (CJUE) qui avait décidé d'annuler le *Privacy Shield*, accord qui permettait aux entreprises de transférer légalement les données personnelles des européens aux Etats-Unis.

La CJUE avait également fait valoir que les engagements contractuels (CCT) n'étaient pas suffisants. En effet, les programmes de surveillance américain ne présentent aucune limitation quant à l’habilitation et l'utilisation des données de personnes non américaines. Ceci n'est purement et simplement pas conforme au droit européen et à notre Règlement protecteur, le Règlement Général de Protection des Données (RGPD).

**Sur cette base, tout traitement de données personnelles de citoyens européens aux Etats-Unis doit aujourd'hui être considéré comme illégal sans délai**.

<!-- more -->

Mais par une décision en référé du 21 septembre 2020, le Conseil d’Etat a estimé que la requête du collectif ne présentait pas de caractère urgent et qu’il leur fallait agir par le biais d’une procédure normale. 

Tout en regrettant que le Conseil d’Etat refuse ainsi de jouer son rôle de gardien des libertés chères à notre République, les requérants prennent acte de cette décision et vont désormais déposer la même requête, mais au fond. 

Dans l’attente de cette décision qui peut prendre plusieurs années, **les requérants demandent la mise en place immédiate d’un moratoire sur le Health Data Hub** tant qu’il ne peut pas être assuré qu’aucune donnée de santé ne sera transférée aux Etats-Unis, en dehors de toute protection ou garantie adéquate pour les citoyens français.

En parallèle, et au vu des réserves du Conseil d’Etat, **les requérants saisissent la CNIL quant au transfert illégal de nos données de santé hébergés sur le Health Data Hub qui intégrera à terme les données de tous et toutes, soit de plus de  67 millions de personnes**.

Contacts parmi les requérant.e.s :
- Association InterHop : interhop@riseup.net / Adrien Parrot : 06.31.47.86.99
- Association Constances : contact@assoconstances.fr
- Dominique Pradalié, journaliste, secrétaire générale SNJ : dpradalie@snj.fr
- Conseil National Logiciel Libre (CNLL) : contact@cnll.fr
- Sophie Binet, co secrétaire générale UGICT-CGT, 06.86.87.68.45

Contacts juridiques :
- Jean-Baptiste Soufron : jbsoufron@fwpa-avocats.com, 0142.966.000
- Juliette Alibert : jalibert@fwpa-avocats.com, 0142.966.000

Liste des 18 requérant.e.s :
- **L’association Le Conseil National du Logiciel Libre (CNLL)** : « *Pour que les discours sur la souveraineté numérique ne restent pas des paroles en l'air, les projets stratégiques au plan économique et sensibles au plan des libertés personnelles ne doivent pas être confiés à des opérateurs soumis à des juridictions incompatibles avec ces principes, mais aux acteurs européens qui présentent des garanties sérieuses sur ces sujets, notamment par l'utilisation de technologies ouvertes et transparentes.* »
- **L’association Ploss Auvergne-Rhône-Alpes**
- **L’association SoLibre**
- **La société NEXEDI** : « Il est faux de dire qu'il n'y avait pas de solution européenne. Il est exact en revanche que le Health Data Hub n'a jamais répondu aux offreurs de ces solutions. »
- **Le Syndicat National des Journalistes (SNJ)** : « *Pour le Syndicat national des journalistes (SNJ), première organisation de la profession, ces actions doivent permettre de conserver le secret sur les données de santé des citoyennes et citoyens de France ainsi que protéger le secret des sources des journalistes, principale garantie d’une information indépendante.* »
- **L'Observatoire de la transparence dans les politiques du médicament** 
- **L'association InterHop**  : « *L'annulation du Privacy Shield sonne la fin de la naiveté numérique européenne. Cependant des rapports de force se mettent en place entre les Etats-Unis et l'Union Européenne concernant le transfert  des données personnelles en dehors de notre espace juridique.*
*Pour pérenniser notre système de santé mutualiste et eu égard à la sensibilité des données en cause, l'hébergement et les services du Health Data Hub doivent relever exclusivement des juridictions de l’Union européenne.* »
- **L'Union Fédérale Médecins, Ingénieurs, Cadres, Techniciens (UFMICT-CGT)**
- **L'Union Générale des Ingénieurs, Cadres et Techniciens (UGICT-CGT)**: « *Pour la CGT des cadres et professions intermédiaires (UGICT-CGT), ce recours est indispensable pour préserver la confidentialité des données qui sont désormais devenues, dans tous les domaines, un marché. Concepteurs et utilisateurs des technologies, nous refusons de nous laisser déposséder du débat sur le numérique au prétexte qu'il serait technique. Seul le débat démocratique permettra de placer le progrès technologique au service du progrès humain!* »
- **L'association Constances** : « *Volontaires de Constances, la plus grande cohorte de santé en France, nous sommes particulièrement sensibilisés aux données de santé et leurs intérêts pour la recherche et la santé publique. Comment admettre que des données de citoyens français soient aujourd'hui transférées aux Etats-Unis ? Comment accepter qu'à terme, toutes les données de santé des 67 millions de Français soient hébergées chez Microsoft et donc tombent sous les lois et les programmes de surveillance américains ?* »
- **L'association Française des Hémophiles (AFH)**
- **L'association les "Actupiennes"**
- **Le Syndicat National des Jeunes Médecins Généralistes (SNJMG)** : « *Les données issues des soins ne doivent pas servir d'autre finalité que l'amélioration des soins. Garantir la sécurité des données de santé et leur exploitation à des seules fins de santé publique est une priorité pour toustes les soignant.es.* »
- **Le Syndicat de la Médecine Générale (SMG)**: « *La sécurisation des données de santé est un enjeu majeur de santé publique puisqu'elle permet le secret médical. Le Health Data Hub n'a jusqu'ici montré aucune garantie sur une véritable sécurisation des données de santé des Français.es, notamment par son choix d'héberger celles-ci chez Microsoft, et met ainsi en danger le secret médical pourtant nécessaire à une relation thérapeutique saine et efficiente*. »
- **L’Union Française pour une Médecine Libre (UFML)** : « *Évitons le contrôle de systèmes monopolistiques potentiellement nuisibles pour le système de santé et les citoyens.* »
- **Madame Marie Citrini, en son mandat de représentante des usagers du Conseil de surveillance de l'AP-HP**
- **Monsieur Bernard Fallery, professeur émérite en systèmes d’information** : « La gestion "par l’urgence" revendiquée pour le Healh Data Hub est un véritable cas d’école de tous les risques liés à la gouvernance des données massives : souveraineté numérique et stockage sans finalité précisée, mais aussi centralisation technique risquée, mainmise sur un commun numérique, oligopole des GAFAM, dangers sur le secret médical, quadrillage des traces et ajustement des comportements » 
- **Monsieur Didier Sicard, médecin et professeur de médecine à l’Université Paris Descartes** : « Offrir à Microsoft les données de santé françaises qui sont parmi les meilleures du monde, même si elles sont insuffisamment exploitées, est une quadruple faute : enrichir gratuitement Microsoft, trahir l'Europe et les citoyens français, empêcher les entreprises françaises de participer à l'anlyse des données » 
