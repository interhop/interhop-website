---
layout: post
title: "LinkR - les développements avec DataForGood continuent"
categories:
  - Linkr
  - DataForGood
ref: dataforgood-goon
lang: fr
---

Data For Good et InterHop ça continue !

<!-- more -->


🎬 InterHop a participé à la 12e saison d'accélération de projets numériques d'intérêt général de Data For Good !
Voici le [billet de blog](https://interhop.org/2024/02/04/dataforgood) expliquant le projet.

> [Data For Good](https://dataforgood.fr/docs/dataforgood/){:target="_blank"} est une association loi 1901 (100% bénévole et non mercantile) créée en 2014 qui rassemble une communauté de 3200+ volontaires tech (Data, Dev, Designers) souhaitant mettre leurs compétences à profit d'associations et d'ONG et de s'engager pour l'intérêt général.

> [LinkR](https://interhop.org/2023/11/15/linkr-logiciel-datasciences) est une plateforme web de data science en santé, permettant de simplifier la visualisation et l’analyse des données de santé à l’aide d’outils low-code. Le développeur principal est Boris DELANGE, membre de l'association InterHop


Pendant les 3 prochains mois, nous allons travailler sur les axes suivants :
- Site web : créer un site web avec le template docsy d'Hugo
- LLM : réfléchir à comment intégrer les LLM dans LinkR / créer un chatbot d'assistance sur l'application
- Plugin machine learning : nous allons continuer à travailler sur le plugin de machine learning

Nous sommes donc toujours à la recherche de profils web pour nous aider sur la création du site, de data scientists / analysts de façon générale et également avec des connaissances en LLM.

Nous avons toujours pour objectif la préparation du [datathon en septembre prochain](https://interhop.org/2024/02/20/datathon), qui permettra de tester en "conditions réelles" LinkR !
D'ailleurs il n'est pas trop tard pour s'inscrire !

La prochaine réunion aura lieu le lundi 17 juin à 19 h.

Vous pouvez aussi nous contacter via la messagerie instantanée et opensource [Element](https://matrix.to/#/#linkr:matrix.interhop.org){:target="_blank"}. Un tutoriel d'aide à la connexion est disponible [ici](https://pad.interhop.org/p/rynEJduy_){:target="_blank"}.
