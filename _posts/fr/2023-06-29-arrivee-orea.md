---
layout: post
title: "Arrivée d'Orea pour la transmission de patients"
categories:
  - Orea
  - Développement
ref: orea-annonce
lang: fr
---

Une nouvelle application Interhop voit le jour ! Orea, pour la transmission de patients en unité de soins intensifs.

En voici la présentation :

<!-- more -->

C'est en 2020, pendant le premier confinement, qu'Alexandre Martin développe une première version d'Orea, appelée alors Transmed.

Cette version permet déjà d'ajouter et observer un résumé des patients d'un service de réanimation, et de remplir leurs fiches détaillées. Il y est possible de générer et imprimer un compte-rendu synthétique des patients désirés.

Entre deux contrats professionnels, à cheval sur 2022 et 2023, Alexandre s'est replongé dans Transmed pour refondre entièrement l'application, sur les aspects frontend, backend et opérationnels, afin de refondre entièrement l'application avec l'expérience acquise.

La version de démo est déjà accessible [ici](https://orea-demo.interhop.org/).

### Une base simple

Les fonctionnalités désirées ont été définies avec deux réanimateurs, Adrien et Jonas.

#### Vue complète

![](https://pad.interhop.org/uploads/30bfc9ce-474f-495c-90f4-c5cf2ccd1b5a.png)

Nous retrouvons les patients répartis en Unité de soin, sur la page du Service de soin voulu.

Pour chaque patient nous disposons :
- des informations démographiques de base avec code couleur pour la criticité du séjour 

![](https://pad.interhop.org/uploads/890130a7-e595-4521-a12b-b639bb93eb28.png)

- de l'affichage des différentes défaillances 

![](https://pad.interhop.org/uploads/9229d410-2aec-4abf-a871-6653085ab201.png)

- un affichage rapide des antécédents et des actions à réaliser

![](https://pad.interhop.org/uploads/a7b5b008-0d16-4920-bbc3-67e9df1c0357.png)


En cliquant sur un patient, la fiche détaillée s'ouvre :

![](https://pad.interhop.org/uploads/cd549882-0f02-4ab2-8bca-acfe027fede6.png)


En plus des infos précédentes que nous pouvons éditer ici, nous trouvons :
- les limitations de traitements 

![](https://pad.interhop.org/uploads/06e8c937-932b-4ef1-a56e-5dab4ee95e0b.png)

- le suivi et l'ajout des derniers dépistages

![](https://pad.interhop.org/uploads/3b90ec72-61f2-4e27-8e6b-269bca0f71f9.png)

- l'ajout de prises de notes personnelles et la lecture de celles des confrères 

![](https://pad.interhop.org/uploads/b718735e-74e9-47ed-a405-79d2ac6fc5c0.png)

- l'édition des différentes prises de notes 

![](https://pad.interhop.org/uploads/616a73bd-a8df-4b57-a8f5-3bae26ff82b8.png)


#### Vue de garde

Pour le tour de garde, on peut activer une vue focalisée sur la Todo list liée à chaque patient.

![](https://pad.interhop.org/uploads/afda3d95-3878-4896-9548-2671ff33e7f0.png)


#### Compte-rendu imprimable

En quelque clics, vous définissez de quelles informations et quel.les patient.e.s vous avez besoin.

![](https://pad.interhop.org/uploads/8c245578-46ff-4fab-aec8-f750b4d4a979.png)


Le pdf généré est imprimable de suite.

![](https://pad.interhop.org/uploads/99235f42-0aad-44a3-9a52-ee2094eb1ca2.png)


### Installable en quelques instants

La partie Opérationnelle a également été travaillée pour permettre, via *docker et *docker-compose*, une installation sur un serveur en quelques minutes.

Pour les détails d'installation, un kit est mis à disposition sur un [répertoire framagit](https://framagit.org/interhop/orea/setup).

### La suite du projet

La version en ligne, dont les images et les répertoires sont ouverts au téléchargement, attend ses premiers retours utilisateurs !

Attention, actuellement pour une version déployée en production, seule l'authentification via un serveur externe dédié (propre à l'hôpital par exemple) est possible. L'ajout des formulaires pour une gestion de compte complètement interne à Orea est à venir.

De nouveaux modes d'authentification, une amélioration de l'expérience utilisateur et le peaufinage des tests techniques sont prévus.

Suivez le développement des futurs tickets sur [framagit](https://framagit.org/groups/interhop/orea/-/boards).
