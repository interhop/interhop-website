---
layout: post
title: "Linkr x DataForGood"
categories:
  - Linkr
  - DataForGood
  - "Saison 12"
ref: dataforgood-2024
lang: fr
---

🎬 InterHop participe à la 12e saison d'accélération de projets numériques d'intérêt général de Data For Good !

LinkR est l'un des projets numériques accompagnés.

<!-- more -->


> [Data For Good](https://dataforgood.fr/docs/dataforgood/){:target="_blank"} est une association loi 1901 (100% bénévole et non mercantile) créée en 2014 qui rassemble une communauté de 3200+ volontaires tech (Data, Dev, Designers) souhaitant mettre leurs compétences à profit d'associations et d'ONG et de s'engager pour l'intérêt général.

Pour la saison 12 une dizaine d'associations et ONGs  présentent un projet numérique sur lequel ils ont besoin d'un coup de main. Des équipes citoyennes se forment pour travailler ensemble pendant 3 mois et résoudre à leur échelle un grand défi social ou environnemental : lutte contre la pêche frauduleuse, évasion fiscale, transition énergétique, ...

> [LinkR](https://interhop.org/2023/11/15/linkr-logiciel-datasciences) est une plateforme web de data science en santé, permettant de simplifier la visualisation et l’analyse des données de santé à l’aide d’outils low-code. Le développeur principal est Boris DELANGE, membre de l'association InterHop

Pour InterHop le projet de la saison 12 est d’améliorer LinkR en développant des plugins pour ajouter de nouvelles fonctionnalités low-code à l’application, aidant ainsi les cliniciens et data scientists à analyser les données de santé.
L’ensemble est codé en SQL, en R et en Python.

Vous pouvez voir la [documentation en ligne](https://linkr.interhop.org/docs/){:target="_blank"}.

## Les objectifs

Vous pouvez également voir [nos objectifs](https://framagit.org/interhop/linkr/linkr/-/issues/){:target="_blank"} présents sur notre dépôt git.

Nous avons amélioré le logiciel en y ajoutant des [plugins](https://linkr.interhop.org/docs/components/plugins/){:target="_blank"}.

Les plugins peuvent être codés en Python, R et SQL.
Ils sont ensuite intégrés à LinkR par un travail de conversion en R/Shiny pour la partie frontend. Voir [cette issue](https://framagit.org/interhop/linkr/linkr/-/issues/6){:target="_blank"}.

Nous améliorons également les scripts de data cleaning afin de faciliter les études réalisées sur les entrepôts de données de santé.

Ces scripts sont codés en SQL, en manipulant des données respectant le format de données [OMOP](https://ohdsi.github.io/CommonDataModel/cdm60.html){:target="_blank"}, un modèle de données standard, international.

L’ensemble de ce travail est partagé avec la communauté scientifique médicale, permettant d’accélérer la recherche portant sur les entrepôts de données de santé.

## L'équipe projet

Ce projet est encadré par Boris DELANGE coté InterHop et  Nassima KHELDOUNI coté DataForGood.

Nous accueillerons tous.te.s les bénévoles avec plaisir. Les profils particulièrement recherchés sont les suivants :

![](https://pad.interhop.org/uploads/973f3d4f-1762-4b70-93cc-8e8038884584.png)

## Le programme

Les dates clés de la saison 12 ont été : 
- 3 février 2024 : début de la saison 12,  création des équipes.

<div class="responsive-video-container">
<iframe title="[D4G]  LinkR - Réunion de lancement de la saison 12 DataForGood (version longue)" width="560" height="315" src="https://peertube.interhop.org/videos/embed/cf3db6e2-559d-4448-84e8-543fb67f836c" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
</div>


- 20 mars 2024 : point de mi-saison 
- 25 avril à 19h : Démo Day, permettra de célébrer la fin de la saison et de montrer les développements réalisés.

<div class="responsive-video-container">
<iframe title="[D4G] Demo day - Saison 12 - LinkR" width="560" height="315" src="https://peertube.interhop.org/videos/embed/1096eb6c-1dbd-4024-a133-6df1591eae88" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
</div>


Le Demo Day a eu lieu au [Wagon, à Paris](https://www.openstreetmap.org/search?query=16+Vla+Gaudelet+75011+Paris){:target="_blank"}.

[En septembre 2024, l’équipe d’InterHop organisera un **datathon**](https://interhop.org/2024/02/20/datathon) afin d’utiliser les plugins et les scripts développés par les bénévoles. Les bénévoles de l'association DataForGood sont bien sûr conviés. Vous pouvez vous y [inscrire](https://interhop.org/2024/02/20/datathon).

Rejoignez InterHop sur nos réseaux sociaux.


## Références
- Framagit : [framagit.org/interhop/linkr](https://framagit.org/interhop/linkr)
- Issues : [framagit.org/interhop/linkr/linkr/-/issues](https://framagit.org/interhop/linkr/linkr/-/issues)
- Doc : [linkr.interhop.org](https://linkr.interhop.org)
- Tutoriels
  - Importer des données : [linkr.interhop.org/docs/import_data/](https://linkr.interhop.org/docs/import_data/)
  - Créer des plugins : [linkr.interhop.org/docs/components/plugins/](https://linkr.interhop.org/docs/components/plugins/)
- Vidéos : [peertube.interhop.org/c/linkr/videos](https://peertube.interhop.org/c/linkr/videos){:target="_blank"}
- Modèle de données OMOP : [ohdsi.github.io/CommonDataModel/cdm60.html](https://ohdsi.github.io/CommonDataModel/cdm60.html){:target="_blank"}
- Base de données MIMIC-III : [physionet.org/content/mimiciii/1.4/](https://physionet.org/content/mimiciii/1.4/){:target="_blank"}
