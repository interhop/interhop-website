---
layout: post
title: "Communiqué de presse :  Vaccination - le partenariat avec Doctolib contesté devant le Conseil d’Etat"
categories:
  - Conseil Etat
  - Ministère
  - Doctolib
  - AWS
ref: conseil-etat-vaccination
lang: fr
---

Un collectif d'associations de patients et de syndicats de médecins conteste le partenariat noué entre Doctolib et le Ministère de la Santé devant le Conseil d'Etat.

<!-- more -->

Voici les 12 requérants:
- InterHop
- Syndicat National Jeunes Médecins Généralistes (SNJMG)
- Syndicat de la Médecine Générale (SMG)
- Union française pour une médecine libre (UFML)
- Fédération des Médecins de France (FMF)
- Didier Sicard
- Association Constances
- Les Actupiennes
- Marie Citrini, personne qualifiée au Conseil de l'APHP, représentante des usagers
- Actions Traitements
- Act-Up Sud Ouest
- Fédération SUD Santé Sociaux


Ils font valoir que le choix d’avoir recours au prestataire Doctolib pour organiser la gestion de la prise de rendez-vous dans le cadre de la politique vaccinale contre la Covid19 n’est pas conforme au RGPD. En effet, la société Doctolib a choisi de faire appel au géant américain Amazon Web Services pour héberger les données de santé. 

Or, conformément à une série de décisions intervenue devant la Cour de Justice de l'Union Européenne mais également en ce qui concerne le Health Data Hub hébergé chez Microsoft, le droit américain n'assure pas un niveau de protection adéquat avec le Règlement Général de Protection des Données (RGPD).

Ce choix soumet les patient.e.s à un risque inutile surtout qu'il existe de nombreuses alternatives, notamment les deux autres prestataires choisis par le gouvernement qui assurent un hébergement des données de santé auprès de sociétés  françaises, non soumises au droit américain.

> En ayant recours à Doctolib, le Ministère s’inscrit donc en violation du RGPD.

Il s’agit dès lors de demander au juge qu’il soit mis fin à l’utilisation de cette plateforme de prise de rendez-vous en ligne dans le cadre de la politique de vaccination car elle constitue une atteinte grave au droit au respect de la vie privée. 

Cette action est relayée dans Médiapart : ["Vaccination : le partenariat avec Doctolib contesté devant le Conseil d’Etat"](https://www.mediapart.fr/journal/france/260221/vaccination-le-partenariat-avec-doctolib-conteste-devant-le-conseil-d-etat)

Contact :
- Maitre ALIBERT j.alibert@alibert-avocat.fr
