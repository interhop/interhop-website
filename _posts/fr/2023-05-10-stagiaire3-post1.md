---
layout: post
title: "EasyAppointments - le mode imprévu (entre autres)"
categories:
  - Développement
  - EasyAppointments
  - Stagiaires
ref: stagiaire3-ea
lang: fr
---


L’association InterHop est ravie d’avoir reçu sa troisième promotion de stagiaires en développement web. Merci pour leur confiance.

De nouveau, ils viennent de l’école d’informatique [ENI Ecole informatique](https://www.eni-ecole.fr). Il s’agit de Thibaut Gentric, Nicolas Michel et Mathieu Leroy.

<!-- more -->

En fin d’année 2022 (troisième promotion) nous avions travaillé notamment sur le ”[Mode Pédiatrie" (tierce personne)](https://interhop.org/2022/12/14/stagiaire2-ea-post-1).

Voici la liste des fonctionnalités mises en place par cette dernière promotion de stagiaires.

# Modifications mineures sur les pages de prises de rendez-vous

Les premières améliorations apportées à l’outil [Easy!Appointments](https://easyappointments.org/) se passent côté 'front', dans le formulaire de prise de RDV.

La logique initiale de EA est de proposer dans un premier temps la sélection d’un type de consultation puis de sélectionner  les soignant.e.s le réalisant. Nous avons inversé cette logique afin de pouvoir partir d’abord du soignant, pour restreindre ensuite le choix aux types d’actes que celui-ci peut effectuer :

![](https://pad.interhop.org/uploads/9be49db8-852f-4d91-9ccb-7a3a7a85878a.png)

À l'origine (gauche), le choix d'une consultation mettait à jour la liste des soignants disponibles. C'est désormais la sélection d'un soignant qui met à jour la liste des consultations possibles.

De plus l’affichage des informations d’une consultation (son prix, sa durée, sa localisation etc..) a été rendu plus lisible, la duplication des cases à cocher concernant la politique de confidentialité et les conditions générales a été retirée et enfin, une traduction en français lors de la confirmation d’un rendez-vous a été effectuée.

![](https://pad.interhop.org/uploads/dff305ae-9295-49dc-b83e-70864e839255.png)

Cette première tâche relativement rapide à réaliser nous a permis de nous faire la main sur la logique de l’outil et voir comment s’articulent et s’imbriquent les différentes couches.

# Suppression automatique des rendez-vous passés

Dans l’interface administrateur, au sein de l’onglet paramètres > général, une nouvelle rubrique intitulée « Délai de suppression automatique des rendez-vous passés » fait son apparition en fin de page. Une liste déroulante propose les 3 modalités possibles  : 
- « pas de suppression automatique »
- «  imposer un même délai de suppression à tous les soignants »
- « définir le délai de suppression dans chaque profil soignant »

![](https://pad.interhop.org/uploads/e8864d97-5066-4293-81e1-9525d92ed91b.png)


Concernant chaque cas de figure : 
- « pas de suppression automatique » : L’administrateur décide que les rendez-vous passés de tous les soignants sont gardés. 
- «  imposer un même délai de suppression à tous les soignants » : En cliquant sur cette option, l’administrateur fait apparaître un champ de saisie dans lequel il définit le délai de suppression qui s’applique à  tous les soignants.

![](https://pad.interhop.org/uploads/63309c94-1b7d-4f07-89bf-cdac81e537a1.png)

- « définir le délai de suppression dans chaque profil soignant » : Cette modalité permet d’appliquer un traitement spécifique par soignant.  Le délai de suppression est saisi par l’administrateur ou par le soignant concerné, chacun dans leur interface respective.  En cliquant sur cette option, l’administrateur fait apparaître comment accéder au profil soignant. Si la suppression des rendez-vous est déjà définie dans certains profils soignants, un message complémentaire récapitule le(s) nom(s) du(des) soignant(s) et la valeur du délai de suppression choisie.

![](https://pad.interhop.org/uploads/8484199d-0149-44c8-ae5a-9fcdd45a6467.png)


Si cette dernière option est choisie, le champ de saisie du délai de suppression apparaît entre « Calendrier » et « Timezone » dans le profil soignant (Interface administrateur/onglet Utilisateurs/Soignants et interface Soignant/onglet paramètres). A noter que l’option « Pas de suppression automatique » est disponible en entrant la valeur 0. 

![](https://pad.interhop.org/uploads/bf5aa9fd-febb-4261-898e-e6774a338c9c.png)


# Développement d’une fonctionnalité “imprévue”

Celle-ci s’est faite en plusieurs temps. D’abord, nous avons bloqué la prise de rendez-vous sur une période marquée comme indisponible, notamment pour les périodes supérieures à une journée (par exemple commençant le lundi à 12h30 et se terminant le jeudi à 14h).


Ensuite, nous avons pu nous attaquer à la gestion des rendez-vous qui pourraient se trouver sur une période d’indisponibilité, afin de pouvoir envoyer un courriel aux patients concernés. L’objectif était de pouvoir automatiquement, à la saisie de cette période de non-disponibilité, récupérer l’ensemble des rendez-vous affectés et envoyer un courriel à chacun des patients concernés leur informant de l’annulation de leur rendez-vous, avec la possibilité d’ajouter un message (global), afin de personnaliser le courriel d’annulation, que voici :

![](https://pad.interhop.org/uploads/3ecb0154-d2ee-4433-9ce4-1c1d4b216e7e.png)



Concernant les soignants et secrétaires, le choix a été fait de n’envoyer qu’un seul courriel récapitulant les rendez-vous annulés, afin de limiter le nombre d’envoi et concentrer toutes les informations dans un unique courriel listant l’ensemble des rendez-vous annulés.

À la suite de cette implémentation, il a également été ajouté une information sur le nombre de rendez-vous possiblement annulés par la période de non-disponibilité dans la partie 'back-office', à la saisie de celle-ci :

![](https://pad.interhop.org/uploads/7e5a8dbb-057c-4222-af8f-ebb6f27f74e2.png)

# Génération automatique d’un lien de visio et partage de documents

Dans le cas d’une téléconsultation, il est nécessaire de pouvoir fournir un lien unique de visioconférence au soignant et au patient lors de la prise de rendez-vous. Le service de [Jitsi](https://github.com/jitsi), gratuit, libre et opensource a été choisi. Pour cela, une clé est générée automatiquement puis associée à l’adresse jitsi, créant ainsi une salle de visioconférence privée.

Dans le cas du partage de documents, c’est l’utilisation du service [Lufi](https://github.com/ldidry/lufi) qui a été sélectionné, lui aussi libre, gratuit et chiffré. La seule information retenue par le service étant l’adresse IP, obligatoire pour des raisons légales. 
Dans les deux cas, ces informations ont été rajoutées dans le mail récapitulatif du rendez-vous, envoyé à la fois au patient et au soignant.

![](https://pad.interhop.org/uploads/a43485db-f297-4c96-8999-6d3f593fa4cd.png)



Le choix a été fait de pouvoir conserver cette clé unique générée automatiquement et de permettre au personnel soignant de pouvoir la retrouver parmi les informations d’un rendez-vous dans le cas où un mail, côté patient ou soignant, se retrouve malencontreusement supprimé. L’affichage se présente ainsi :

![](https://pad.interhop.org/uploads/4431ad1c-fe9f-4504-9b2b-2f77ee157e88.png)


Suite à cet ajout, il a été décidé d’améliorer encore la fonctionnalité afin de permettre à un soignant de pouvoir paramétrer les différents services qui pourront être utilisés dans le cadre de ses activités. Bien qu’encore en développement, elle nous a amené à modifier puis à schématiser la base de données dans son état actuel.

![](https://pad.interhop.org/uploads/a771228b-3836-4d8d-8770-3a12f09a9677.png)


La table “ea_externals_tools” servira à l’enregistrement des outils et services externes à l’application. La table “ea_appointments_externals_tools” quant à elle liera un rendez-vous à un ou plusieurs outils externes.

# Apparence des rendez-vous

Enfin, la dernière amélioration apportée à l’outil EasyAppointments concerne l’apparence des rendez-vous dans le calendrier du soignant. Il a donc été choisi de rajouter une colonne ‘appointment_status’ dans la table ‘appointments’ de la base de données, afin de suivre le parcours du rendez-vous : 'prévu' ('planned'), 'en cours' ('happening'), 'terminé' ('done') et 'annulé' ('cancelled'). Sachant que ce dernier statut n’est attribué que lorsque l’annulation d’un rendez-vous est faite par l’équipe médicale, puisque lorsqu’un patient annule de son fait, le rendez-vous est simplement supprimé de la base de données, après envoi d’un mail informatif au soignant & secrétaires concernés.

L’ensemble de ces statuts permet ensuite de mettre aisément en place un code couleur pour chacun des rendez-vous dans le calendrier, que l’on soit sur une vue globale, ou 'table' pour les secrétaires :

![](https://pad.interhop.org/uploads/5507c681-2753-44ed-89bd-52e379d1de82.png)

Dans cette capture d’écran, les rendez-vous grisés sont ceux qui ont eu lieu, et ont donc le statut 'terminé', tandis que le rendez-vous jaune est en train de se dérouler 'en cours' et le vert est 'prévu'. On remarque également que le rendez-vous rouge a été annulé pour cause d’indisponibilité.

A la création d’un rendez-vous, que ce soit par le soignant ou le patient, celui-ci obtient automatiquement le statut 'prévu'. Une fois la date du rendez-vous atteinte, il prend, toujours de façon automatique, le statut 'en cours' et lorsque celle-ci s’achève, le rendez-vous est considéré comme 'terminé'. 
