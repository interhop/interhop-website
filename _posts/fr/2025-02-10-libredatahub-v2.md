---
layout: post
title: "LibreDataHub.org V2 : l'évolution"
categories:
  - LibreDataHub
ref: libredatahub-v2
lang: fr
---


LibreDataHub est une plateforme open-source de sciences de la donnée. Elle fournit une série d'outils libres pour le stockage de données, l’IA décentralisée, les statistiques et l’apprentissage machine.<br>
La deuxième version de LibreDataHub LDH V2 permet l'intégration d'outils open-source et propose une gestion efficace des ressources serveurs afin de partager équitablement la capacité entre les utilisateurs. 

<!-- more -->

![](https://pad.interhop.org/uploads/61c4b09d-c6f1-46f4-9e3e-dc9590e2a321.png)


LibreDataHub est une plateforme sécurisée d'analyse de données conçue pour être utilisée par de petites équipes de recherche jusqu'au institutions de plus grande taille, comme des hôpitaux. 

# Des nouveaux outils

Comme dans la première version[^v1] les utilisateurs peuvent accéder à leurs applications (Jupyter, RStudio, LinkR, Grafana...) dans un espace projet collaboratif et sécurisé. 

Cette deuxième version incorpore de nouveau outils par exemple  Airflow  ou encore MyST.

![](https://pad.interhop.org/uploads/0950bcc3-3367-4e5d-b3cc-4055c18d9242.png)

> [Airflow](https://airflow.apache.org/){:target="_blank"} est une plateforme open-source utilisée pour orchestrer, planifier et automatiser des workflows de données. Elle permet de définir, de gérer et de superviser des tâches complexes sous forme de "DAGs" (Directed Acyclic Graphs), où chaque tâche représente une unité de travail.

![](https://elest.io/images/softwares/249/screenshot2.png
)

> [MyST](https://mystmd.org/){:target="_blank"} permet d'inclure des visualisations interactives directement dans les projets en utilisant les Notebooks Jupyter.

![](https://pad.interhop.org/uploads/f1cb0698-f7ee-4fcb-9740-f498fe8d39ac.png)

[^v1]: [https://interhop.org/2024/09/08/libredatahub](https://interhop.org/2024/09/08/libredatahub)


# Installation simplifié

Le processus d'installation de LDH V2 sur GNU-linux est maintenant mieux automatisé et documenté[^doc_frama_ldh] en particulier pour Debian. La sécurité a été renforcée puisque l'outil fonctionne désormais avec la version dite "rootless" de docker.

![](https://pad.interhop.org/uploads/c33cc7ff-6ff1-4368-b655-ed946af81bfe.png)

Par rapport à une autre plateforme de science de la données nommé Onyxia[^onyxia], LibreDataHub V2 est une plateforme plus simple puisqu'elle se déploie sur un unique serveur. A la différence d'Onyxia qui repose sur des technologies plus complexes comme Kubernetes.


[^onyxia]: [https://www.onyxia.sh/](https://www.onyxia.sh/)

[^doc_frama_ldh]: [https://framagit.org/interhop/libre-data-hub/libredatahub](https://framagit.org/interhop/libre-data-hub/libredatahub)

La gestion des utilisateurs et des groupes dans LDH V2 est basée sur des règles spécifiques : un administrateur global peut gérer tous les projets, tandis que des utilisateurs avec des rôles spécifiques peuvent accéder aux données et applications des projets auxquels ils appartiennent. Chaque projet dispose de sa propre base de données PostgreSQL.. L'accès aux applications est cloisonné, avec des permissions réglementant les accès aux ressources.
L'authentification à la plateforme est sécurisé via le SSO (Keycloak) de l'association InterHop.
Keycloak est aussi utilisé pour administrer les équipes projets.


# Déploiement en HDS

Enfin nous sommes fiers d'annoncer que LibreDataHub est maintenant installé sur les serveurs certifiés "[Hébergeur de données de santé](https://interhop.org/projets/hds)" HDS de l'association InterHop permettant de délivrer la plateforme facilement aux chercheurs et chercheuses via une simple URL. Pour les projets traitant des données de santé, nous avons décidé de renforcer la sécurité en mettant systématiquement en place la double authentification (2FA).

# Conclusion

LDH V2 s'affirme comme une plateforme open-source polyvalente et sécurisée, dédiée aux sciences des données. 

Accessible à tous, elle peut être installée par n'importe qui, offrant ainsi une solution flexible pour divers besoins en analyse de données. 
La plateforme a déjà prouvé son efficacité dans plusieurs contextes : elle a été utilisée pour organiser un datathon, a servi de support pour des cours d'informatique dans deux universités (Lille et Saint-Denis), et a été déployée dans le cadre de projets HDS (Hébergeur de Données de Santé) pour des projets de data science et de statistiques. De plus, LibreDataHub a permis à des stagiaires de se familiariser avec des outils avancés de gestion et d'analyse de données.

Avec ses nouvelles fonctionnalités et son processus d'installation simplifié, LibreDataHub V2 se positionne comme un outil incontournable pour les petites équipes de recherche comme pour les grandes institutions.
