---
layout: post
title: "Saisi en référé par le collectif SantéNathon, le Conseil d’Etat reconnait que le gouvernement américain peut accéder sans contrôle aux données de santé des Français hébergées par le Health Data Hub chez Microsoft"
categories:
  - HDH
  - Microsoft
  - Conseil d'Etat
  - Europe
  - Privacy Shield
  - RGPD
redirect_from:
  - /communique-presse-le-conseil-detat-reconnait-acces-aux-donnees-de-sante
ref: communique-presse-le-conseil-detat-reconnait-acces-aux-donnees-de-sante
lang: fr
---

Un collectif comprenant de 18 requérants issus du milieu du logiciel libre, d'associations de patients, de syndicats de médecins et techniciens et du milieu du journalisme, a demandé au Conseil d’Etat de suspendre le traitement et la centralisation des données de santé de plus de 67 millions de personnes au sein du Health Data Hub, hébergées chez Microsoft Azure, le cloud du géant américain

Ce collectif dénonçait le choix de Microsoft essentiellement à cause de l’absence d’appel d’offre et des effets de l’extraterritorialité du droit américain. En effet, la Cour de Justice de l’Union Européenne ("CJUE") a récemment révélé que les renseignements américains (via le FISA et l'*Executive Order* 12 233) n’ont aucune limitation quant à l’utilisation des données des citoyens et citoyennes européens. 

<!-- more -->

A la suite d’un important [mémoire de la CNIL](https://cnll.fr/documents/35/OBSERVATIONS_DE_LA_CNIL_8_OCTOBRE_2020.pdf), et malgré [un arrêté](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000042413782) pris en urgence par le gouvernement le lendemain même de l'audience, le Conseil d’Etat a pris [une décision](https://www.conseil-etat.fr/actualites/actualites/health-data-hub-et-protection-de-donnees-personnelles-des-precautions-doivent-etre-prises-dans-l-attente-d-une-solution-perenne) par laquelle il reconnaît que le Health Data Hub hébergé chez Microsoft ne protège pas les données de santé des français contre les intrusions du gouvernement américain, et ce contrairement à tout ce qui était affirmé depuis des mois par le Ministère de la Santé. 

Preuve de la gravité des infractions constatées, ce refus de suspendre n’est cependant qu’une décision prise « à très court terme », notamment pour éviter d’interrompre brusquement les quelques projets en cours sur le Health Data Hub. 

En revanche, le Conseil d’Etat demande au Health Data Hub et à Microsoft de conclure encore de nouveaux avenants à leurs contrats, et de prendre encore des précautions supplémentaires, sous le contrôle de la CNIL. 

Surtout, au-delà du « très court terme », le Conseil d’Etat indique être dans l’attente d’une solution qui permettra d’éliminer tout risque d’accès aux données personnelles par les autorités américaines, renvoyant notamment à un appel d’offre d’un prestataire français ou européen comme annoncé par le Secrétaire d’Etat au numérique, et comme évoqué par la CNIL dans ses observations au Conseil. 

Autrement dit, le Health Data Hub, tel qu’il existe aujourd’hui, ne devrait pas pouvoir être utilisé en l’état au-delà des quelques projets existants, faute pour les nouveaux de risquer de se mettre en infraction. 

Et comme le réclamait la CNIL dans son mémoire en observation, les données de santé des français ne devraient pas pouvoir être hébergées chez la société Microsoft pour le futur, et devraient se tourner vers l'une des nombreuses alternatives existantes dont l'offre est compatible avec les loies européennes.

**Avec l'arrêté pris en urgence par le gouvernement ainsi que les importantes observations remises par la CNIL, c’est donc une victoire dont se félicitent le collectif SanteNathon et ses membres.**

Mais face à l’absence de suspension effective, et face à la mauvaise foi continuelle du Gouvernement et du Ministère de la Santé dans ce dossier, le combat doit continuer. 

C'est pourquoi, face à l’urgence d’empêcher que les données de santé ne soient transférées aux Etats-Unis de façon irréversible, le collectif souhaite désormais saisir le Conseil d’Etat sur le fond afin de prendre des mesures qui puissent dépasser le « très court terme », ainsi que la CNIL au regard des infractions en cours et passées. 

D'autres actions sont également à l'étude, notamment sur le plan européen. 

Liste des 18 requérants :
- **L’association Le Conseil National du Logiciel Libre ([CNLL](https://cnll.fr/))** : « *Pour que les discours sur la souveraineté numérique ne restent pas des paroles en l'air, les projets stratégiques au plan économique et sensibles au plan des libertés personnelles ne doivent pas être confiés à des opérateurs soumis à des juridictions incompatibles avec ces principes, mais aux acteurs européens qui présentent des garanties sérieuses sur ces sujets, notamment par l'utilisation de technologies ouvertes et transparentes.* »
- **L’association Ploss Auvergne-Rhône-Alpes**, membre du CNLL
- **L’association SoLibre**, membre du CNLL
- **La société NEXEDI** : « *Il est faux de dire qu'il n'y avait pas de solution européenne. Il est exact en revanche que le Health Data Hub n'a jamais répondu aux offreurs de ces solutions.* »
- **Le Syndicat National des Journalistes (SNJ)** : « *Pour le Syndicat national des journalistes (SNJ), première organisation de la profession, ces actions doivent permettre de conserver le secret sur les données de santé des citoyennes et citoyens de France ainsi que protéger le secret des sources des journalistes, principale garantie d’une information indépendante.* »
- **L'Observatoire de la transparence dans les politiques du médicament** 
- **L'association InterHop**  : « *L'annulation du Privacy Shield sonne la fin de la naiveté numérique européenne. Cependant des rapports de force se mettent en place entre les Etats-Unis et l'Union Européenne concernant le transfert  des données personnelles en dehors de notre espace juridique. Pour pérenniser notre système de santé mutualiste et eu égard à la sensibilité des données en cause, l'hébergement et les services du Health Data Hub doivent relever exclusivement des juridictions de l’Union européenne.* »
- **L'Union Fédérale Médecins, Ingénieurs, Cadres, Techniciens (UFMICT-CGT)**
- **L'Union Générale des Ingénieurs, Cadres et Techniciens (UGICT-CGT)**: « *Pour la CGT des cadres et professions intermédiaires (UGICT-CGT), ce recours est indispensable pour préserver la confidentialité des données qui sont désormais devenues, dans tous les domaines, un marché. Concepteurs et utilisateurs des technologies, nous refusons de nous laisser déposséder du débat sur le numérique au prétexte qu'il serait technique. Seul le débat démocratique permettra de placer le progrès technologique au service du progrès humain!* »
- **L'association Constances** : « *Volontaires de Constances, la plus grande cohorte de santé en France, nous sommes particulièrement sensibilisés aux données de santé et leurs intérêts pour la recherche et la santé publique. Comment admettre que des données de citoyens français soient aujourd'hui transférées aux Etats-Unis ? Comment accepter qu'à terme, toutes les données de santé des 67 millions de Français soient hébergées chez Microsoft et donc tombent sous les lois et les programmes de surveillance américains ?* »
- **L'association Française des Hémophiles (AFH)**
- **L'association les "Actupiennes"**
- **Le Syndicat National des Jeunes Médecins Généralistes (SNJMG)** : « *Les données issues des soins ne doivent pas servir d'autre finalité que l'amélioration des soins. Garantir la sécurité des données de santé et leur exploitation à des seules fins de santé publique est une priorité pour toustes les soignant.es.* »
- **Le Syndicat de la Médecine Générale (SMG)**: « *La sécurisation des données de santé est un enjeu majeur de santé publique puisqu'elle permet le secret médical. Le Health Data Hub n'a jusqu'ici montré aucune garantie sur une véritable sécurisation des données de santé des Français.es, notamment par son choix d'héberger celles-ci chez Microsoft, et met ainsi en danger le secret médical pourtant nécessaire à une relation thérapeutique saine et efficiente*. »
- **L’Union Française pour une Médecine Libre (UFML)** : « *Évitons le contrôle de systèmes monopolistiques potentiellement nuisibles pour le système de santé et les citoyens.* »
- **Madame Marie Citrini, en son mandat de représentante des usagers du Conseil de surveillance de l'AP-HP**
- **Monsieur Bernard Fallery, professeur émérite en systèmes d’information** : « *La gestion "par l’urgence" revendiquée pour le Healh Data Hub est un véritable cas d’école de tous les risques liés à la gouvernance des données massives : souveraineté numérique et stockage sans finalité précisée, mais aussi centralisation technique risquée, mainmise sur un commun numérique, oligopole des GAFAM, dangers sur le secret médical, quadrillage des traces et ajustement des comportements* » 
- **Monsieur Didier Sicard, médecin et professeur de médecine à l’Université Paris Descartes** : « *Offrir à Microsoft les données de santé françaises qui sont parmi les meilleures du monde, même si elles sont insuffisamment exploitées, est une quadruple faute : enrichir gratuitement Microsoft, trahir l'Europe et les citoyens français, empêcher les entreprises françaises de participer à l'analyse des données* » 
