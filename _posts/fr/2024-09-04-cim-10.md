---
layout: post
title: "Datathon 2024 - CIM 10"
categories:
  - DataThon2024
  - Sujets
ref: datathon-2024
lang: fr
---

> Avec ce sujet les participant⸱es proposent une intégration de LLM avec la méthode RAG pour la recherche de codes CIM-10 au sein du logiciel opensource LinkR.<br>
> Plus d’informations et inscription : [interhop.org/projets/datathon](https://interhop.org/projets/datathon)
<!-- more -->

## Introduction

A chaque séjour hospitalier d'un patient, des **codes CIM-10** sont **attribués**, correspondant aux **pathologies** présentées durant le séjour.

La base de données CIM-10 est **complexe**, et il est parfois **difficile** de **trouver** les **diagnostics** que nous recherchons. Les requêteurs “classiques” que nous trouvons sur internet utilisent des outils de recherche classique (à base de regex) non optimaux.

Les LLM (Large Language Models, tels que ChatGPT pour le plus connu), grâce au RAG (retrieval augmentated generation) permettent d’utiliser les LLM en les “nourrissant” de fichiers, tels qu’un fichier CSV contenant l’ensemble des diagnostics.

Ainsi, il est possible d’utiliser un LLM déjà entraîné, de lui fournir la base de données CIM-10 et de l’interroger directement pour nous donner les codes demandés.

L’approche par RAG a l’avantage de diminuer le risque d’hallucinations.

## Matériel et méthodes

La base de données CIM-10 est disponible au format OMOP sur le site <a href="https://athena.ohdsi.org" target="_blank">Athena</a>.

Des LLM seront utilisés (soit accessibles via API tels que GPT-4, soit téléchargés localement tels que Llama 7B).
La base de données CIM-10 alimentera un RAG.

Le choix des librairies utilisées est libre. Voici quelques propositions :

- <a href="https://huggingface.co/docs/transformers/index" target="_blank">Transformers</a>, lien vers les <a href="https://huggingface.co/docs/transformers/model_doc/rag" target="_blank">fonctions RAG</a>
- <a href="https://www.langchain.com/" target="_blank">LangChain</a> : lien vers les <a href="https://www.langchain.com/retrieval" target="_blank">fonctions RAG</a>
- <a href="https://docs.llamaindex.ai/en/stable/" target="_blank">LlamaIndex</a> : lien vers les <a href="https://docs.llamaindex.ai/en/stable/optimizing/production_rag/" target="_blank">fonctions RAG</a>

La recherche s'effectuera à l'aide d'une interface graphique, sous la forme d'une **application web**.

En bonus, nous pouvons également imaginer :

- un système de **favoris**, où les diagnostics les plus fréquemment choisis par l'utilisateur seront mis en avant
- un système de **recherche classique** associé : l'utilisateur pourra choisir entre une recherche par regex ou une recherche par LLM (il ne faut probablement pas mettre de côté les systèmes les plus simples qui fonctionnent à moindre coût, et qui malgré tout répondent à la demande dans la majorité des cas)
- une système **speech to text**, pour transformer la requête audio en prompt

## Conclusion

L'intégration de LLM avec la méthode RAG pour la recherche de codes CIM-10 pourrait permettre une recherche plus facile et plus précise qu'avec les systèmes de recherche classique.
