---
layout: post
title: "Communiqué de presse : Signalement CNIL co-porté par plusieurs associations et syndicats"
categories:
  - Iqvia
  - Plainte CNIL
  - EDS
  - CloudAct
ref: signalement-cnil-iqvia
lang: fr
---

**Dans le cadre du reportage Cash Investigation[^cash_reportage] diffusé sur France 2 portant comme intitulé « Nos données personnelles valent de l’or ! », les journalistes se sont intéressés aux pratiques de l’entreprise IQVIA en matière d’utilisation des données de santé récupérées par les « logiciels de gestion de fiches client » mis à disposition par l’entreprise IQVIA aux pharmacies.**

**Plusieurs organisations et associations dénoncent les risques en matière de protection des données de santé collectées et recueillies par IQVIA dans le cadre de Pharmastat.**

**Ils saisissent ce jour la CNIL d'un signalement.**

<!-- more -->

## Traitement des données de santé à des fins commerciales

IQVIA revendique un réseau de plusieurs dizaines de milliers de pharmacies qui lui remonte des données de santé à intervalles réguliers : Pharmastat[^pharmastat]. 

Si IQVIA a une autorisation CNIL pour administrer un entrepôt de données de santé nommé "LRX"[^lrx] et traiter ces données à des fins d'intérêt public (notamment pour des recherches et évaluations), IQVIA utiliserait également les données de santé à des fins commerciales.

En effet, comme révélé par Cash Investigation, le réseau Pharmastat délivrerait aux pharmaciens des logiciels de Gestion d'Officine en intégrant certaines données de santé recueillies à l'occasion du passage des patients en pharmacie. Le problème est que les patients ne seraient pas informés de l'utilisation de leurs données à des fins commerciales, et ne pourraient par conséquent ni y consentir, ni s'y opposer. 

D'après le reportage "Cash Investigation" IQVIA vendrait des études de marché pouvant coûter jusqu'à 500 000 euros.

Voici ce que déclare un commercial de l'entreprise (Extrait Cash Investigation): 
> "Pour information, sur une pathologie, pour avoir un peu d'informations en terme de parcours patients, c'est minimum 20 000 balles. Et encoe là, je suis vraiment dans le bas, bas, bas de la fourchette, ce sera plus élévée. Parce que c'est la seule base qui existe et qui permet de faire ca en France"

Or, les données recueillies sont particulièrement massives et sensibles[^lrx]. Ce sont les données de santé de plus de la moitié des officines françaises[^nombre_total_officines].

<img src="https://i.ibb.co/PrdhpFF/Screenshot-2021-05-26-at-07-35-08.png" alt="Screenshot-2021-05-26-at-07-35-08" border="0">
> Source : Plaquette commerciale IQVIA "Comment faire évoluer votre officine ?"

## Société de droit américain

Par ailleurs, la société IQVIA est une société américaine.

En ce sens, l’entrepôt de données de santé de IQVIA est soumis au droit américain, lequel n’assurerait pas un niveau de protection suffisant au regard du RGPD.

En effet, les conséquences de l’arrêt de la CJUE du 16 juillet 2020[^cjue] « Schrems II » sur l’hébergement de données entreposées sur le sol français ou européen par des sociétés de droit américain serait incompatible avec le RGPD tel qu’il en ressort de l’appréciation de la CNIL[^cnil_ce], du Conseil d’État[^ce] et tout récemment de des déclarations du gouvernement lui-même au travers de la nouvelle politique "cloud de confiance"[^cloud_confiance].

<div class="alert alert-red" markdown="1">
## Demandes des signalants

Le collectif de signalants regroupe des associations des libertés numériques et de défense des libertés fondamentales,des associations de patients, des syndicats de médecin, de salariées des secteurs sanitaires, sociaux et médico-sociaux.

Ils sollicitent la CNIL pour :

- Que soit précisé les cas où IQVIA intervient en tant que « responsable de traitement » ou « sous-traitant » ou encore « co-responsable » avec les pharmacies du réseau Pharmastat ;

- Qu’un contrôle soit réalisé en ce qui concerne les informations délivrées aux patients, et notamment la révision des notices d’information IQVIA mis à disposition sur le site de l’Union des Syndicats de Pharmaciens d’Officine[^information_uspo] ;

- Que le principe du recueil de consentement dans le cadre des finalités commerciales Pharmastat soit rappelé, contrôlé et effectif ;

- Que l’effectivité des droits d’opposition des patients par le biais du code barre à scanner soit contrôlée et vérifiée ;

- Qu’un contrôle soit réalisé pour vérifier que l’entrepôt de données LRX n’est pas utilisé pour alimenter les traitements de données dans le cadre des outils Pharmastat (tableau de bord, etc), les bases de données de Pharmastat devant être étanches vis à vis de celles de LRX ;

- Que le principe de portabilité des données recueillies par les logiciels de gestion d'Officine LGO fournis par IQVIA soit effectif et respecté ;

- Que le traitement des données de santé notamment dans le cadre de l’entrepôt LRX par une société soumise au droit américain donne lieu à une nouvelle analyse des risques en présence ;
</div>


## Liste des signalants :
- **La Fédération SUD Santé Sociaux**
- **L’association AIDES**
- **L’association Nothing2Hide**
- **L’association Actions Traitement**
- **La Ligue des Droits de l’Homme**
- **Le Syndicat de la Médecine Générale**
- **L’Union Française pour une Médecine Libre**
- **L'association InterHop**

## Avocate : Me Juliette ALIBERT 

[^cash_reportage]: [Cash investigation - Nos données personnelles valent de l'or !](https://peertube.interhop.org/w/efFi5JvzWh5jMsk7jttGbD)

[^pharmastat]:[Pharmastat - Politique de protection des données personnelles](https://pharmastat.iqvia.com/donnees-personnelles)

[^lrx]: [Délibération n° 2018-289 du 12 juillet 2018 autorisant la société IQVIA Opérations France à mettre en œuvre un traitement automatisé de données à caractère personnel ayant pour finalité la constitution d’un entrepôt de données à caractère personnel à des fins de recherche, d’étude ou d’évaluation dans le domaine de la santé, dénommé base de données LRX (n° 2120812)](https://www.legifrance.gouv.fr/cnil/id/CNILTEXT000037462044/)

[^nombre_total_officines]: [https://www.ciopf.org/Fiches-des-pays/France](https://www.ciopf.org/Fiches-des-pays/France)


[^cjue]: [La Cour invalide la décision 2016/1250 relative à l'adéquation de la protection assurée par le bouclier de protection des données UE-États-Unis](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

[^cnil_ce]: [https://cnll.fr/documents/35/OBSERVATIONS_DE_LA_CNIL_8_OCTOBRE_2020.pdf](https://cnll.fr/documents/35/OBSERVATIONS_DE_LA_CNIL_8_OCTOBRE_2020.pdf)

[^ce]: [Health Data Hub et protection de données personnelles : des précautions doivent être prises dans l’attente d’une solution pérenne](https://www.conseil-etat.fr/actualites/actualites/health-data-hub-et-protection-de-donnees-personnelles-des-precautions-doivent-etre-prises-dans-l-attente-d-une-solution-perenne)

[^cloud_confiance]: [https://interhop.org/2021/05/20/analyse-cloud-confiance](https://interhop.org/2021/05/20/analyse-cloud-confiance)

[^information_uspo]: [IQVIA – OSPHARM…pensez à informer vos patients (affiches)](https://uspo.fr/iqvia-ospharm-pensez-a-informer-vos-patients-affiches/)
