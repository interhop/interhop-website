---
layout: post
title: "Datathon 2024 - FINESS+"
categories:
  - DataThon2024
  - Sujets
ref: datathon-2024
lang: fr
---

> Ce sujet proposé au datathon vise à créer un référentiel FINESS amélioré aligné sur le [Référentiel National du Batiment](https://rnb.beta.gouv.fr/){:target="_blank"} RNB.<br>
> Plus d'informations et inscription : [interhop.org/projets/datathon](https://interhop.org/projets/datathon)

<!-- more -->



# Introduction
La santé prend le virage des parcours coordonnés de soin. Cette ambition nécessite d’avoir des données concernant l’offre de soin qui soient à jour.

Il s’agit, à termes, de tenir à jour les données géographiques élémentaires (géolocalisation des 100 000 établissements de soins recensés au FINESS) dans des systèmes ouverts comme OpenStreetMap (OSM) de sorte que ces informations restent exactes et interopérables (tags et iconographies documentés et à jour).

Ce maintien à jour pourra être fait (au moins en partie) au sein de l’association [Toobib.org](https://toobib.org){:target="_blank"} et de sa plateforme éponyme.


Le datathon de Rennes est l'occasion de créer un référentiel FINESS amélioré aligné sur le [Référentiel National du Batiment](https://rnb.beta.gouv.fr/){:target="_blank"} RNB. Un tel alignement doit permettre:
- d'[identifier des problèmes de qualité](https://framagit.org/terminos/irene/-/blob/main/finess/doc/README-jsom.md#finess-r%C3%A8gles-de-controles-qualit%C3%A9){:target="_blank"} dans le FINESS
  - sur les champs adresses ou sur les champs de géolocalisations ;
- de proposer des corrections
  - sur les champs adresses ou sur les champs de géolocalisations ;
- d'"entrer" via l'identifiant pivot ``"rnb_id"`` dans des référentiels métiers plus complets dans leurs domaines respectifs (BDTOPO batiment...) en vue d'obtenir des informations utiles supplémentaires au secteur de la santé (performances énergétiques des bâtiments par GHT, données d'accessibilité, présence d'ascenceurs...) et de démontrer l'intérêt des données liées.

La faisabilité d'un tel alignement est ["visualisable"](https://framagit.org/terminos/irene/-/blob/main/finess/doc/README-jsom.md#geofriness-dans-qgis){:target="_blank"} avec QGIS par exemple.

Ce FINESS+ servira de base aux implémentations Toobib mentionnées plus haut ([requêter les pharmacies sur un territoire donné par exemple](https://framagit.org/terminos/irene/-/raw/main/finess/doc/res/saint-malo-1-pharmacies.png){:target="_blank"}) et de base de discussions avec les différents responsables des référentiels source en vue de les améliorer.

# Matériels et méthodes

Le challenge retenu consiste donc à aligner le FINESS et le RNB. Cette section synthétise les informations essentielles à connaitre sur ces référentiels. 

### FINESS

#### Description du FINESS
Voir par exemple:
- [framagit.org/terminos/irene/-/blob/main/finess/doc/README.md](https://framagit.org/terminos/irene/-/blob/main/finess/doc/README.md){:target="_blank"}
- [framagit.org/terminos/irene/-/blob/main/finess/doc/README-jsom.md](https://framagit.org/terminos/irene/-/blob/main/finess/doc/README-jsom.md){:target="_blank"}

#### Données géolocalisées du FINESS
Des données récentes géolocalisées du FINESS sont disponibles dans ce [.zip](https://framagit.org/terminos/irene/-/blob/main/finess/dat/osm/finess.zip?ref_type=heads) au format GeoJSON en WGS84.
Les données FINESS au format [OWL](https://bioportal.lirmm.fr/ontologies/FINESS/?p=classes&conceptid=http%3A%2F%2Fdrees.solidarites-sante.gouv.fr%2Fterminologies%2Ffiness%2F604&jump_to_nav=true){:target="_blank"} et OMOP sont disponibles sur [framagit.org/terminos/irene/-/blob/main/finess](https://framagit.org/terminos/irene/-/blob/main/finess){:target="_blank"}.


#### Traitement du FINESS en python


Le code source du projet [BQSS de la HAS](https://gitlab.has-sante.fr/has-sante/public/bqss) traite le fichier FINESS en Python, avec [notamment](https://gitlab.has-sante.fr/has-sante/public/bqss/-/blob/main/bqss/finess/processing_finess.py?ref_type=heads#L330){:target="_blank"}:
```
def convert_coordinates_to_wgs84(
    finess_df: pd.DataFrame,
) -> pd.DataFrame
```
Sa [documentation](https://has-sante.pages.has-sante.fr/public/bqss/creation/finess/index.html){:target="_blank"} est disponible.

#### Traitement du FINESS en R
Le projet [FINESS etalab](https://github.com/GuillaumePressiat/finess_etalab){:target="_blank"} traite le FINESS avec R.

### Référentiel National des Bâtiments (RNB)

#### Description du RNB
Le [site du RNB](https://rnb.beta.gouv.fr/){:target="_blank"} pointe vers la [documentation technique](https://rnb-fr.gitbook.io/documentation){:target="_blank"} facilitant l'utilisation du RNB et de ses API, sachant que la création du RNB a nécessité un travail de [définition du bâtiment](https://github.com/fab-geocommuns/BatID/blob/eea3555c0de8fb178a85379306fbe85c358ea9ce/docs/CNIG/Annexe-Definition-Batiment.md){:target="_blank"}.

#### Données du RNB
Le RNB est disponible en téléchargement CSV sur le site [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/referentiel-national-des-batiments/){:target="_blank"}.

Les champs sont les suivants:

|Champs|Type et Taille|Description|
|:---|:---|:---|
|rnb_id varchar(12)|L’identifiant RNB du bâtiment
|point|geometry eWKT|La géométrie d’un point sur le bâtiment
|shape|geometry eWKT|La géométrie du bâtiment
|status|varchar()|Le statut du bâtiment
|ext_ids|json|Identifiants du bâtiment dans la BDNB et la BDTOPO
|addresses array Tableau des adresses du bâtiment
|code_dept|varchar(3)|(optionnel, fonction export national versus export par département) Code du département du point

Exemple de géométrie ponctuelle au format eWKT:
```
SRID=4326;POINT(5.057550620559569 45.84776805257559)
```

#### API RNB
- API ["Identification de bâtiment"](https://rnb-fr.gitbook.io/documentation/api-et-outils/api-batiments/identification-de-batiment){:target="_blank"} par "address" et par "point"
- API de [recherche de batiments par "Bounding box"](https://rnb-fr.gitbook.io/documentation/api-et-outils/api-batiments/listing-de-batiments){:target="_blank"}
- API de [récupération des détails d'un bâtiment](https://rnb-fr.gitbook.io/documentation/api-et-outils/api-batiments/consultation-dun-batiment){:target="_blank"}

### Référentiel BDTOPO

#### Description de la BDTOPO
Disponbible en pdf : [https://geoservices.ign.fr/sites/default/files/2021-07/DC_BDTOPO_3-0.pdf)](https://geoservices.ign.fr/sites/default/files/2021-07/DC_BDTOPO_3-0.pdf){:target="_blank"}

et notamment BDTOPO Batiment page 63

#### Données de la BDTOPO

La BDTOPO est alignée sur le RNB (champ "identifiants_rnb").

Ses champs pour le volet "bêtiments" sont:

|Champ|Type|
|:---|:---|
|cleabs|xsd:string|
|nature|xsd:string|
|usage_1|xsd:string|
|usage_2|xsd:string|
|construction_legere|xsd:boolean|
|etat_de_l_objet|xsd:string|
|date_creation|xsd:dateTIme|
|date_modification|xsd:dateTIme|
|date_d_apparition|xsd:date|
|date_de_confirmation|xsd:date|
|sources|xsd:string|
|identifiants_sources|xsd:string|
|methode_d_acquisition_planimetrique|xsd:string|
|methode_d_acquisition_altimetrique|xsd:string|
|precision_planimetrique|xsd:double|
|precision_altimetrique|xsd:double|
|nombre_de_logements|xsd:int|
|nombre_d_etages|xsd:int|
|materiaux_des_murs|xsd:string|
|materiaux_de_la_toiture|xsd:string|
|hauteur|xsd:double|
|altitude_minimale_sol|xsd:double|
|altitude_minimale_toit|xsd:double|
|altitude_maximale_toit|xsd:double|
|altitude_maximale_sol|xsd:double|
|origine_du_batiment|xsd:string|
|appariement_fichiers_fonciers|xsd:string|
|identifiants_rnb|xsd:string|

Ces données sont disponibles par département et par thèmes sur le [site de l'IGN](https://geoservices.ign.fr/bdtopo#telechargementshpdept){:target="_blank"}.

#### API BDTOPO

Voir la doc [irene/finess](https://framagit.org/terminos/irene/-/blob/main/finess/doc/README-jsom.md?ref_type=heads#geofriness-dans-qgis){:target="_blank"} pour l'intégration de BDTOPO dans QGis par exemple.

### Autres données et API d'intérêt
- [https://geo.api.gouv.fr](https://geo.api.gouv.fr){:target="_blank"}
- [https://gateway.api.esante.gouv.fr/fhir](https://gateway.api.esante.gouv.fr/fhir){:target="_blank"}
- [https://api.gouv.fr/les-api/base-adresse-nationale](https://gateway.api.esante.gouv.fr/fhir){:target="_blank"}
- [https://adresse.data.gouv.fr/api-doc/adresse](https://adresse.data.gouv.fr/api-doc/adresse){:target="_blank"}
- [https://smt.esante.gouv.fr/fhir/CodeSystem/](https://smt.esante.gouv.fr/fhir/CodeSystem/){:target="_blank"}

### Outils d'intérêts
[GIS (Geographic information system)](https://en.wikipedia.org/wiki/Geographic_information_system){:target="_blank"}: jOSM et QGIS

# Résultats
L'algorithme d'alignement produit un [CSV FINESS](https://www.data.gouv.fr/fr/datasets/finess-extraction-du-fichier-des-etablissements/){:target="_blank"} auquel est ajouté les 6 colonnes suivantes:
- finesset_latitude
    - latitude de l'ES en WGS84 issue de la conversion depuis les données FINESS, ex. 46.22274483202441
- finesset_longitude
    - longitude  de l'ES en WGS84 issue de la conversion depuis les données FINESS, ex. 5.2085961326419605
- rnb_id
    - id RNB proposé par l'algorithme d'alignement, ex. "QBAAG16VCJWA"
- rnb_score
    - score "interne" assigné à l'alignement RNB proposé, ex. 1.0
- rnb_relation
    - relation d'alignement proposée par l'algorithme, prise dans le jeu de valeur ([https://www.hl7.org/fhir/valueset-concept-map-relationship.html](https://www.hl7.org/fhir/valueset-concept-map-relationship.html){:target="_blank"} union [https://build.fhir.org/valueset-iana-link-relations.html](https://build.fhir.org/valueset-iana-link-relations.html){:target="_blank"}), ex. "[https://hl7.org/fhir/concept-map-relationship](https://hl7.org/fhir/concept-map-relationship){:target="_blank"}"
- rnb_algo
    - GUID identifiant l'algorithme utilisé pour proposer l'alignement RNB, ex. "a174b42a-f2ff-49cc-9765-37b28cf3737a"|

Le nom du fichier de sortie est du type:

etalab-cs1100507-stock-20240717-0337___RNB_datagouvfr_publication_2024-03-07_16-28-06.csv
(concatenation via des ___ des x fichiers sources utilisés).

Le fichier de sortie est un CSV encodé en UTF-8 qui contient donc les colonnes suivantes:

|Colonne|Remarque|
|:---|:---|
|structureet|---|
|nofinesset|---|
|nofinessej|---|
|rs|---|
|rslongue|---|
|complrs|---|
|compldistrib|---|
|numvoie|---|
|typvoie|---|
|voie|---|
|compvoie|---|
|lieuditbp|---|
|commune|---|
|departement|---|
|libdepartement|---|
|ligneacheminement|---|
|telephone|---|
|telecopie|---|
|categetab|---|
|libcategetab|---|
|categagretab|---|
|libcategagretab|---|
|siret|---|
|codeape|---|
|codemft|---|
|libmft|---|
|codesph|---|
|libsph|---|
|dateouv|---|
|dateautor|---|
|datemaj|---|
|numuai|---|
|finesset_latitude|latitude de l'ES en WGS84 issue de la conversion depuis les données FINESS, ex. 46.22274483202441|
|finesset_longitude|longitude  de l'ES en WGS84 issue de la conversion depuis les données FINESS, ex. 5.2085961326419605|
|rnb_id|id RNB proposé par l'algorithme d'alignement, ex. "QBAAG16VCJWA"|
|rnb_score|score "interne" assigné à l'alignement RNB proposé, ex. 1.0|
|rnb_relation|relation d'alignement proposée par l'algorithme, prise dans le jeu de valeur ([https://www.hl7.org/fhir/valueset-concept-map-relationship.html](https://www.hl7.org/fhir/valueset-concept-map-relationship.html){:target="_blank"} union [https://build.fhir.org/valueset-iana-link-relations.html](https://build.fhir.org/valueset-iana-link-relations.html){:target="_blank"}), ex. {"system": "[https://hl7.org/fhir/concept-map-relationship](https://hl7.org/fhir/concept-map-relationship){:target="_blank"}", "code":"equivalent", "display":"Equivalent"}|
|rnb_algo|GUID identifiant l'algorithme utilisé pour proposer l'alignement RNB, ex. "a174b42a-f2ff-49cc-9765-37b28cf3737a"|

Le code source produit est mis à disposition sur framagit LOv2 à l'adresse suivante [https://framagit.org/interhop/challenges/datathon-2024](https://framagit.org/interhop/challenges/datathon-2024){:target="_blank"}.

Les ressources produites alimenteront les pages "réutilisation de données" de [data.gouv.fr](htts://data.gouv.fr){:target="_blank"} pour les référentiels utilisés (FINESS, RNB, BDTOPO...) mentionnant les pages du datathon.

Une mesure de la performance de l'alignement (F-Measure) est proposée.

# Discussion

Le choix de l'implémentation de l'algorithme est libre:
- langage de programmation utilisé;
- "pipe" d'alignement: via l'adresse, puis via les coordonnées WGS83, ou l'inverse, en best effort sur le Bounding box, par [approche lexicale ou structurelle](https://www.cismef.org/cismef/wp/wp-content/uploads/2021/05/Alignements-de-terminologies_2021.pdf){:target="_blank"}, par proximité sémantique, par apprentissage/embedding...
- l'ajout de colonnes supplémentaires est possible (pour signifier la détection d'un problème dans les données FINESS d'origine, pour debugger, pour proposer une correction, pour préciser la source / le JSonPath de l'alignement proposé...).

L'exercice peut être limité lors du datathon à un sous ensemble de données (par exemple à un département ou une région) si l'exercice devait s'avérer être trop long en temps d'exécution. Dans ce cas, l'algorithme conçu doit démontrer sa "scalability" potentielle (mesure du temps d'exécution, règles de 3/anticipation des temps sur les datasets complets).

L'exercice peut être complété par le même type d'exercice d'alignement sur le [RPPS](https://annuaire.sante.fr/){:target="_blank"} afin d'avoir une base de données géolocalisée de "lieux de soins".

Passé le datathon, le travail peut être complété (alignement avec les [ressources européennes de INSPIRE](https://inspire-mif.github.io/uml-models/approved/mapping/){:target="_blank"}, définition de référentiels syntaxiques "communs" - [FHIR address](https://www.hl7.org/fhir/datatypes.html#Address){:target="_blank"}, [schema.org/Place](https://schema.org/Place){:target="_blank"} ou [schema.org/Organization](https://schema.org/Organization){:target="_blank"}...) et faire l'objet d'une publication.
