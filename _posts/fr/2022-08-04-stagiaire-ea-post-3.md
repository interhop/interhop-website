---
layout: post
title: "EasyAppointments - Des couleurs et des consultations"
categories:
  - Développement
  - EasyAppointments
  - Stagiaires
ref: stagiaire-ea-post-3
lang: fr
---

L'association InterHop est ravie de recevoir ses premiers stagiaires en développement web. 

Voici leur troisième billet de blog.

<!-- more -->

Pour finaliser notre fonctionnalité de catégorisation des consultations selon des horaires dédiés, nous avons modifier la base de données pour y ajouter l'enregistrement du choix de la couleur et du mode privé.

Désireux d'une bonne ergonomie de la fonctionnalité, nous nous sommes concentrés sur la lisibilité de l'affichage du planning. Pour cela, nous avons voulu afficher les couleurs des plages horaires dédiées et faire en sorte que la sélection par défaut dépende de la plage horaire sélectionnée.

L'affichage du calendrier est gérée par la librairie FullCalendar. 
Nous avons dû, dans un premier temps, prendre connaissance de cette librairie afin de mieux la comprendre. Il était important de repérer la gestion des événements et leurs affichages selon les différents modes disponibles.
Ceci s'est avéré une tâche ardue.

Après plusieurs jours de recherche, de documentation et de code, nous avons réussi...

![](https://pad.interhop.org/uploads/1c49b0f2-b7ff-491e-80f6-80506660fa2b.png)

Ici, on peut remarquer les plages "roses" représentant les "urgences", le bleu pour les "visites à domicile" et le vert pour les "téléconsultations".
Ces plages sont "dédiées" et non "bloquées" aux autres types de consultations pour le soignant/secrétaire.
Lors d'un clic dans la zone "urgences"(rose), la fenêtre de prise de rendez-vous s'affiche avec la présélection de la catégorie "Urgences".

![](https://pad.interhop.org/uploads/a26d6878-2f9e-4211-b11d-104fd8b7ed4c.png)

Cet affichage est aussi présent en mode journalier.

![](https://pad.interhop.org/uploads/e76b57da-dd4e-4949-93f4-b00c612b52c1.png)

 Ainsi qu'en mode table
 
 ![](https://pad.interhop.org/uploads/a996fde1-cb94-49a4-a944-9a9b9c3453c6.png)
 
Côté de l'affichage patient, nous avons limiter la proposition d'horaires de rendez-vous selon la catégorie de consultation choisie.
Si le patient choisi la téléconsulation, seuls les horaires disponibles sur la plage "verte", lui seront proposés.
