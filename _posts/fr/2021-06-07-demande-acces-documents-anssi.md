---
layout: post
title: "Demande de documents administratifs : audit technique de sécurité du Health Data Hub"
categories:
  - Courrier
  - Audit
  - Anssi
  - Health Data Hub
ref: demande-acces-doc-adm-audit-secu-anssi
lang: fr
---

<p style="text-align:right;">
Guillaume POUPARD
Directeur général
Tour Mercure 31 Quai de Grenelle 75015 Paris
</p>


<!-- more -->
Objet: Demande de documents administratifs

Monsieur le Directeur Général,

Le 10 mai 2021 le Groupement Intérêt Public (GIP) "Health Data Hub" a publié sur son site sa "feuille de route 2021"[^1]. Il est mentionné la réalisation d'un audit de sécurité par l'Agence Nationale de sécurité des Systèmes d'Information (ANSSI) en date de février 2021.

Le Health Data Hub vise à développer l’intelligence artificielle appliquée à la santé et veut devenir un guichet unique d’accès à l’ensemble des données de santé.
Les données concernées sont celles des centres hospitaliers, des pharmacies, du dossier médical partagé et les données de recherche issues de divers registres. La quantité des données hébergées est amenée à exploser, notamment avec l’émergence de la génomique, de l’imagerie et des objets connectés. Ces données sont stockées chez Microsoft Azure, cloud public du géant américain Microsoft.

Afin d’éclairer nos positions et de pouvoir jouer son rôle d’expertise et de vigie de la démocratie,  l'association InterHop demande la communication du document administratif suivant, en application des articles L311-1 à R311-15 relatifs du Code des Relations entre le Public et l'Administration à la communication des documents administratifs [^2]  : **audit technique de sécurité de l'ANSSI datant de février 2021**.

Nous souhaitons recevoir ce document :
- dans un format numérique, ouvert et réutilisable,
- dans un format papier.

Nous vous remercions de nous indiquer, après publication,  l'adresse de téléchargement et de nous l'envoyer en pièce jointe.

Comme le livre III du code des relations entre le public et l’administration le prévoit lorsque le demandeur a mal identifié celui qui est susceptible de répondre à sa requête, je vous prie de bien vouloir transmettre cette demande au service qui détient le document demandé si tel est le cas.

Veuillez agréer, Monsieur le Directeur Général, l'expression de nos sentiments distingués.

Fait à Paris, le 12 mai 2021

Adrien PARROT 
Président InterHop

[^1]: https://www.health-data-hub.fr/sites/default/files/2021-05/Feuille_route_HDH_2021.pdf

[^2]: https://www.cada.fr/particulier/mes-droits
