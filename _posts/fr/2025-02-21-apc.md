---
layout: post
title: "InterHop sur les libertés numériques et les logiciels libres dans les soins de santé"
categories:
  - Tribune
ref: apc
lang: fr
---

[Tribune initialement publiée](https://www.apc.org/fr/node/40517){:target="_blank"} en anglais par l'association APC, dans laquelle est publié un entretien mensuel avec les bénéficiaires de la subvention [NGI Zero](https://www.apc.org/en/project/next-generation-internet-zero){:target=« _blank »} (NGI0). 

> L'Association pour le progrès des communications (APC) est un réseau international d'organisations de la société civile fondé en 1990, qui se consacre à l'autonomisation et au soutien des personnes œuvrant pour la paix, les droits de l'homme, le développement et la protection de l'environnement, grâce à l'utilisation stratégique des technologies de l'information et de la communication (TIC).

<!-- more -->

En 2024, InterHop a reçu une [subvention NGI0 pour Goupile](https://nlnet.nl/project/Goupile/){:target=« _blank »}, un éditeur de formulaires open source conçu pour la collecte de données dans la recherche, en particulier dans le domaine de la santé, remplaçant les formulaires traditionnels de rapport de cas sur papier (Case Report Form CRF) par des versions électroniques (eCRF). Financé par la Commission européenne, le NGI0 soutient des projets de logiciels libres, de données libres, de matériel libre et de normes ouvertes. Il apporte un soutien financier et pratique sous diverses formes, notamment le mentorat, les essais, les tests de sécurité, l'accessibilité, la diffusion...

Découvrez le travail d'InterHop dans cette interview, à laquelle l'association a répondu collectivement.

Cet entretien a été édité pour des raisons de clarté et de longueur.

# Qu'est-ce qui vous a poussé à créer l'association InterHop ?


InterHop est l'initiative d'ingénieurs travaillant dans plusieurs hôpitaux français. Au début, dans les années 2018-2019, nous étions un simple collectif informel. Nous nous réunissions pour discuter de nos pratiques, de l'interopérabilité et, plus largement, des logiciels libres en santé.

En 2019, des membres de l'association ont été auditionnés dans le cadre de la mission de préfiguration du [Health Data Hub](https://fr.wikipedia.org/wiki/Health_Data_Hub){:target=« _blank »} [^1]. Lorsqu'il a fallu faire un choix technique, nous nous sommes rendu compte que le cloud de Microsoft, Azure, risquait d'être choisi. Pour de multiples raisons, nous étions opposés à ce choix et avons décidé de tirer la sonnette d'alarme en interne dans nos hôpitaux, puis d'écrire un article dans [Le Monde](https://interhop.org/2019/12/10/donnees-de-sante-au-service-des-patients).

L'année suivante, nous nous sommes associés à d'autres organisations pour déposer un recours devant le Conseil d'Etat [^2]. Nous avions besoin d'une structure juridique et avons décidé de créer l'association [InterHop.org](https://interhop.org/).

# Quels sont les problèmes fondamentaux posés par ces choix politiques de solutions numériques ?

Notre association défend les libertés numériques dans le domaine de la santé. Nous n'abordons donc pas les problèmes économiques que ces décisions peuvent engendrer.

D'un point de vue juridique, nos inquiétudes sont principalement liées à la nationalité extraterritoriale de ces opérateurs de services en nuage, en l'occurrence basés aux Etats-Unis. Toute une série de lois américaines (le [CLOUD Act](https://en.wikipedia.org/wiki/CLOUD_Act){:target=« _blank »}, le [Foreign Intelligence Surveillance Act](https://en.wikipedia.org/wiki/Foreign_Intelligence_Surveillance_Act){:target=« _blank »}, l'[Executive Order 12333](https://en.wikipedia.org/wiki/Executive_Order_12333){:target=« _blank »}) exposent les données de santé au risque d'être communiquées aux autorités publiques aux États-Unis. Comme la [Commission nationale de l'informatique et des libertés](https://en.wikipedia.org/wiki/Commission_nationale_de_l%27informatique_et_des_libert%C3%A9s){:target=« _blank »} (CNIL), nous estimons que les données de santé « ne doivent pas être soumises au risque d'un accès non autorisé par des autorités de pays tiers »[^3]. Et toujours en accord avec la CNIL, nous demandons à nos décideurs de « faire appel à un prestataire exclusivement soumis au droit européen et offrant un niveau de protection adéquat ». [^3]

D'un point de vue moral, le problème est plus profond et affecte directement la relation de soins. « Concrètement, les patients pourraient être soumis à une violation du secret médical, ce qui constitue un danger aussi personnel que symbolique, l'intégrité du serment d'Hippocrate étant mise en cause. » [^4]

Pour nous, le risque le plus important est la perte de confiance dans le système de santé. La confiance qui sous-tend la relation entre patients et soignants repose sur un certain nombre de facteurs, dont le secret, qui est essentiel.

De plus, ces choix architecturaux sont construits sur des modèles centralisateurs (par opposition à la fédération ou à la décentralisation). En cas de piratage, les conséquences seront plus importantes en termes d'étendue des données potentiellement compromises.


Enfin, les logiciels libres devraient être la norme dans le domaine de la santé en ligne. La [Déclaration de Genève](https://en.wikipedia.org/wiki/Declaration_of_Geneva){:target=« _blank »} souligne la responsabilité des médecins en matière de partage des connaissances et déclare : 
> « Je PARTAGERAI mes connaissances médicales au bénéfice du patient et pour le progrès des soins de santé ».

Il est difficile d'imaginer comment un professionnel de santé impliqué dans l'innovation en santé pourrait s'opposer à l'ouverture de son travail sans être en conflit avec son éthique médicale. Au contraire, nous devons proposer aux cliniciens et à la communauté médicale de prendre des mesures actives pour empêcher la propriété privée des logiciels de la médecine numérique. L'open source doit devenir la norme en médecine. Et ce, dans tous les domaines : gouvernance, infrastructures, ontologies, algorithmes, logiciels, etc.

# Face à ces problèmes et menaces, en plus de vos efforts de plaidoyer, vous avez commencé à développer des logiciels libres pour le secteur de la santé. Quel est votre objectif dans ce travail ?

L'association InterHop édite des logiciels libres pour la recherche en santé. Selon la Free Software Foundation, un « logiciel libre » est un logiciel qui respecte la liberté des utilisateurs. Fondamentalement, cela signifie que « les utilisateurs ont la liberté d'exécuter, de copier, de distribuer, d'étudier, de modifier et d'améliorer le logiciel ». [^6]

Nous publions Goupile [^7], qui est un logiciel permettant de créer des formulaires de collecte de données pour la recherche. Nous publions également LinkR [^8] pour l'analyse low-code, c'est-à-dire sans (ou avec peu) de compétences en codage informatique. Enfin, nous venons de mettre en place LibreDataHub [^9], une plateforme open source qui regroupe un certain nombre de logiciels de science des données (comme Jupyter, RStudio et LinkR) et permet de gérer un espace de projet pour le travail en équipe. Tous ces outils sont disponibles directement en ligne sur la certification des hébergeurs de données de santé (HDS) [^10] sur des serveurs loués par l'association.

Avec ces logiciels, nous souhaitons allier un engagement juridique, c'est-à-dire les contentieux que nous menons, à une application pratique et technique de nos idées. Nous montrons qu'il est possible, avec des moyens très limités - tous nos membres sont par exemple bénévoles - de faire de la recherche en santé en utilisant des outils numériques gratuits hébergés par des entités juridiques strictement soumises au droit européen.

# Technologie numérique, informations personnelles et médicales, droit à la vie privée et accès aux soins dans des conditions de confiance, les problèmes que vous exposez et les solutions que vous développez ne se limitent pas à la France. Pouvez-vous nous en dire plus à ce sujet ?

Les solutions techniques que nous développons peuvent être utilisées dans un contexte large, européen, voire mondial. C'est sans doute la raison pour laquelle nous avons reçu un financement européen de la Fondation NLnet, et donc de la Commission européenne [^11], pour améliorer Goupile, l'outil que nous utilisons pour créer des formulaires à des fins de recherche.

En ce qui concerne les questions juridiques de l'extraterritorialité du droit américain, tous les Européens sont concernés. C'est pourquoi, à l'instar de la CNIL, nous incitons les éditeurs à utiliser des fournisseurs de cloud dont le siège social est strictement situé en Europe.



En effet, la seule spécificité française est la certification « Hébergeur de Données de Santé » HDS. Cette certification a pour objectif de renforcer la protection technique des données de santé à caractère personnel. Elle est aussi potentiellement un frein réglementaire à l'utilisation des logiciels libres dans le domaine de la santé, les serveurs étant plus coûteux. C'est pourquoi nous proposons d'installer des logiciels libres sur ces serveurs HDS, qui sont loués par l'association. Nous délivrons nos propres logiciels tels que [Goupile](https://nlnet.nl/project/Goupile/){:target=« _blank »}, LinkR et LibreDataHub ; nous délivrons également d'autres logiciels libres tels que [Cryptpad](https://www.apc.org/en/news/cryptpad-how-it-balances-accessibility-and-privacy-secure-digital-collaboration){:target=« _blank »}. [^12].

# Comment Interhop et vos projets numériques fonctionnent-ils financièrement ?

InterHop est une association à but non lucratif régie par la loi 1901. Notre financement provient donc en partie des cotisations des membres et des dons (exonérés d'impôts). Dans le cadre de la recherche en santé, lorsque nous mettons à disposition nos outils sur nos serveurs HDS, nous demandons une participation financière [^13]. Enfin, nous répondons régulièrement à des appels de fonds publics. Notre premier succès a été via la Fondation NLnet déjà citée.

# Avez-vous hésité à demander une subvention NGI0 via la Fondation NLnet ?

Pas vraiment. Nous avons trouvé le processus de sélection assez simple et direct, surtout par rapport à d'autres appels à projets.

Cet argent va nous permettre d'améliorer considérablement l'outil. Nos objectifs sont disponibles en ligne [^14]. Nous soutenons également l'appel de l'association petites singularités [pour que l'Union européenne continue à financer les logiciels libres](https://www.apc.org/en/pubs/european-union-must-keep-funding-free-software){:target=« _blank »}. [^15].

# Comment, dans le contexte politique actuel, participer à la défense de nos droits en matière de santé, de vie privée et d'accès aux soins et à la médecine ?

D'aucuns diront que les démocraties semblent en déclin, que les nations se referment sur elles-mêmes. A l'inverse, les logiciels libres et l'association InterHop sont synonymes de transparence et d'ouverture aux autres. Nous favorisons la discussion et l'échange, et nous animons un vaste réseau pour permettre aux chercheurs de travailler ensemble plus efficacement [^16].

Nous nous efforçons de faire en sorte que la santé (numérique) soit accessible au plus grand nombre et au juste prix. Nous promouvons l'entraide comme moteur du développement des sociétés, plutôt que l'appropriation et la concurrence.

**Au quotidien, nous défendons l'autonomie numérique des acteurs de santé [^17]. L'autonomie numérique peut être appréhendée à plusieurs niveaux : individus, institutions (sécurité sociale, régions, départements, villes, hôpitaux, etc.), nations, Europe, États-plateformes ou encore humanité. Elle se définit comme la capacité d'autodétermination dans l'environnement cybernétique.**

Tous nos outils numériques (Goupile, LinkR, LibreDataHub) peuvent donc être utilisés directement sur les serveurs HDS de l'association sans nécessiter d'installation. Cependant, certains hôpitaux ont décidé d'aller plus loin en installant localement nos outils numériques. Ils sont sur la voie d'une plus grande autonomie numérique.

Enfin, l'un des principes fondamentaux du RGPD - le règlement général sur la protection des données de l'UE - est la minimisation de la collecte des données [^18] au regard des finalités de leur traitement [^18]. Loin des plateformes centralisatrices américaines et chinoises, nous essayons, comme d'autres avant nous [^19], d'offrir une vision moins mercantile et plus décentralisatrice. Dans la mesure du possible, nous utilisons des technologies qui peuvent être fédérées, comme Fediverse [^20]. De même, pour garantir le secret des échanges, nous préconisons l'utilisation la plus large possible du chiffrement de bout en bout.

[^1]: [https://sante.gouv.fr/ministere/documentation-et-publications-officielles/rapports/sante/article/rapport-health-data-hub-mission-de-prefiguration](https://sante.gouv.fr/ministere/documentation-et-publications-officielles/rapports/sante/article/rapport-health-data-hub-mission-de-prefiguration){:target="_blank"}

[^2]: [https://interhop.org/2020/10/08/attaque-du-secret-medical](https://interhop.org/2020/10/08/attaque-du-secret-medical)

[^3]: [https://www.cnil.fr/fr/cloud-les-risques-dune-certification-europeenne-permettant-lacces-des-autorites-etrangeres](https://www.cnil.fr/fr/cloud-les-risques-dune-certification-europeenne-permettant-lacces-des-autorites-etrangeres){:target="_blank"}

[^4]: [https://interhop.org/2019/12/10/donnees-de-sante-au-service-des-patients](https://interhop.org/2019/12/10/donnees-de-sante-au-service-des-patients)

[^5]: [https://www.conseil-national.medecin.fr/medecin/devoirs-droits/serment-dhippocrate](https://www.conseil-national.medecin.fr/medecin/devoirs-droits/serment-dhippocrate){:target="_blank"}

[^6]: [https://www.gnu.org/philosophy/free-sw.en.html](https://www.gnu.org/philosophy/free-sw.en.html){:target="_blank"}

[^7]: [https://interhop.org/projets/goupile](https://interhop.org/projets/goupile)

[^8]: [https://interhop.org/projets/linkr](https://interhop.org/projets/linkr)

[^9]: [https://interhop.org/2024/09/08/libredatahub](https://interhop.org/2024/09/08/libredatahub)

[^10]: [https://interhop.org/projets/hds](https://interhop.org/projets/hds)

[^11]: [https://interhop.org/2024/11/11/goupile-nlnet](https://interhop.org/2024/11/11/goupile-nlnet)

[^12]: [https://cryptpad.fr](https://cryptpad.fr){:target="_blank"}

[^13]: [https://interhop.org/2023/04/06/brochure-recherche](https://interhop.org/2023/04/06/brochure-recherche) ︎

[^14]: [https://interhop.org/2024/11/11/goupile-nlnet](https://interhop.org/2024/11/11/goupile-nlnet)

[^15]: [https://interhop.org/2024/09/10/open-letter-to-the-commission](https://interhop.org/2024/09/10/open-letter-to-the-commission)

[^16]: [https://interhop.org/projets/interchu](https://interhop.org/projets/interchu)

[^17]: [https://interhop.org/2020/11/12/coup-data](https://interhop.org/2020/11/12/coup-data)

[^18]: [https://www.cnil.fr/fr/definition/minimisation](https://www.cnil.fr/fr/definition/minimisation)

[^19]: [https://degooglisons-internet.org/fr/](https://degooglisons-internet.org/fr/) ︎

[^20]: [https://www.radiofrance.fr/franceinter/podcasts/veille-sanitaire/veille-sanitaire-du-vendredi-08-septembre-2023-8005700](https://www.radiofrance.fr/franceinter/podcasts/veille-sanitaire/veille-sanitaire-du-vendredi-08-septembre-2023-8005700 ︎) ︎
