---
layout: post
title: "La lutte contre les fraudes de médicaments ne doit pas être un frein à l’accès au traitement !"
categories:
  - CNAM
  - Traitements
  - TRT5
ref: lutte-contre-fraude
lang: fr
---

> Un collectif de plusieurs associations s'inquiète du nouveau disposition de lutte contre la fraude aux médicaments et appellent au retrait du dispositif proposé par la CNAM.

<!-- more -->

Les modalités d’application du dispositif de lutte contre les fraudes, qui impose aux pharmaciens-nes de contrôler l’authenticité de chaque ordonnance où figurent des traitements au prix supérieur à 300 euros, font l’objet de concertations associant syndicats de pharmaciens-nes, de médecins et représentants-es d’usagers-ères.

A un mois de l’entrée en vigueur prévue (l’échéance de septembre a dû être reportée faute de consensus des parties prenantes), la CNAM ne parvient pas à répondre aux diverses craintes formulées par les acteurs-trices de terrain. Pour préserver l’accès à la santé des personnes, notamment celles les plus éloignées du système de santé, nos structures appellent au retrait du dispositif.


## Un mode de concertation peu inclusif et productif

Premier sujet de défiance, le dispositif, qui prévoit de modifier les conditions de dispensation de traitements aussi essentiels que les antirétroviraux, les anticancéreux ou les antiviraux à action directe, a été conçu sans  consultation préalable des syndicats de pharmaciens-nes, de médecins et des représentants-es d’usagers-ères du système de santé.

Des concertations ultérieures à son entérinement offrent, maigre consolation, de participer aux négociations sur les modalités de sa mise en œuvre. Il s’agit de définir les outils sur lesquels reposera le contrôle effectué par les pharmaciens-nes. Mais ces concertations échouent à associer l’intégralité des parties prenantes. En cause, un manque de sollicitation pro-active de la part de la CNAM rendant le processus hautement confidentiel.

Enfin, les difficultés d’application opposables à la liste d’outils de contrôle présentée par la CNAM ne sont pas prises en compte de réunion en réunion. Des demandes simples, comme celle d’accéder à des informations sur une expérimentation visant à faciliter la prise de contact entre prescripteurs-trices et pharmaciens-nes, restent sans réponse.  Il y a de quoi s’interroger sur la marge de manœuvre dont disposent réellement les acteurs-trices de terrain présents-es autour de la table.

## Une mise en œuvre rendant l’accès aux traitements incertain et inégalitaire

Consultation des dossiers médicaux, connaissance préalable du-de la prescripteur-trice, appel du-de la prescripteur-trice comme dernier recours… En termes d’applicabilité, les moyens d’authentification exposés jusqu’ici ne permettent pas de garantir à tous-tes la dispensation complète des traitements figurant sur les ordonnances, dans des délais compatibles avec leur état à de santé.

Il faut par exemple que les personnes disposent de dossiers complets (certains, comme l’Espace numérique de santé, ne sont pas généralisés), ou que les prescripteurs-trices soient disponibles au moment de l’authentification. Les inégalités rencontrées par les personnes aux parcours peu documentés ou moins à même de justifier de la nécessité de leur traitement, notamment les bénéficiaires de l’Aide médicale d’Etat, n’en seront qu’accentuées.

Pour elles, la dispensation d’un conditionnement minimal, prévue en cas d’ordonnances non- authentifiable avec certitude, risque de devenir la norme et d’écarter des options plus avantageuses, comme la dispensation trimestrielle d’antirétroviraux en cours de déploiement. De quoi multiplier les déplacements vers les pharmacies et les coûts, financiers comme organisationnels, qui s’y rattachent. La prise des traitements dans des délais impartis, voire la rétention dans le soin, sont ici en jeu.

Finalement, ce ne sont pas les fraudeurs-euses qui seront les plus impactés-es par ce dispositif, mais bien les patients-es. A elles et eux de démontrer leur « bonne foi », pour citer nos interlocuteurs-trices, et d’ouvrir sans discuter l’accès à leurs données de santé.

## Une seule issue : le retrait du dispositif pour la santé des patients-es

Déployer de tels moyens, alors même que la Caisse affirme que « 100% des fraudes » comportent des erreurs détectables à l’œil nu, est incompréhensible. A l’heure où le trafic de médicaments fait l’objet de politiques publiques et de débats légitimes, il convient de fournir aux parties prenantes le temps et les moyens contributifs indispensables à la conception de mesures  qui n’impacteront pas l’accès à la santé des personnes.

L’authentification des ordonnances par les pharmaciens-nes et la méthode de concertation associée  ne remplissant pas ces exigences, nos structures appellent au retrait du dispositif de lutte contre les fraudes aux ordonnances de dispensation de médicaments onéreux de la convention signée entre la CNAM et les syndicats de pharmaciens-nes.

Signataires : 
Collectif TRT-5 CHV
- Interhop
- Les Séropotes
- Médecins du Monde
- Observatoire du Droit à la Santé des Etrangers
- SOS Hépatites
