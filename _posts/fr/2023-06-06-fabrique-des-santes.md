---
layout: post
title: "Dossier patient associatif"
categories:
  - Toobib
  - Dokos
  - Tiers-Lieux
ref: toobib-annonce
lang: fr
---

Nous sommes heureux d'être invités ce mercredi 7 juin à 12h30 par la ```Fabrique des Santés```.

<!-- more -->

> La Fabrique des santés est un collectif qui facilite les coopérations ouvertes et l'émergence de communs dans le domaine de la santé et du soin[^1].

[^1]: [https://www.fabsan.cc/](https://www.fabsan.cc/)

A cette occasion nous discuterons de logiciels libres et opensources pour améliorer le soin et notamment du dossier patient informatisé ```ToobibPro``` adossé au prologiciel (ERP[^2]) [Dokos](https://maia-by-dokos.fr/){:target="_blank"}.  

[^2]: [https://fr.wikipedia.org/wiki/Progiciel_de_gestion_int%C3%A9gr%C3%A9](https://fr.wikipedia.org/wiki/Progiciel_de_gestion_int%C3%A9gr%C3%A9)

Développé initialement pour les besoins des sages-femmes, il a vocation à devenir un logiciel pluri-professionnel de santé et donc d'être utilisé notamment par les centres de santés. Etant opensource, les valeurs d'entraide, d'éthique, d'éco-responsabilité mais aussi de sécurité et de justes coûts sont au coeur de ce projet.

Récemment le projet Dokos s'est marié avec l’association [InterHop.org](https://interhop.org), association à but non lucratif qui promeut, développe et met à disposition des logiciels libres et open-sources pour la santé. 

Dokos pour sages-femmes est donc maintenant hébergé sur les serveurs certifiés hébergeurs de donnés de santé HDS d’Interhop[^3]. Ceci permet de mutualiser les coûts avec les autres services déjà proposés par Interhop : [Goupile](https://goupile.hds.interhop.org/){:target="_blank"}, [Cryptpad](https://cryptpad.hds.interhop.org/){:target="_blank"}.

[^3]: [https://gplexpert.com/hebergement-donnees-sante-hds/](https://gplexpert.com/hebergement-donnees-sante-hds/)

Rejoignez Céline DECULTOT et Adrien PARROT le 07 juin pour créer ensemble le logiciel de soin du futur, opensource et éthique par nature.

➡️  La présentation est disponible en ligne [ici](https://pad.interhop.org/p/zkxbnNSbs){:target="_blank"}.

➡️  Le replay, là : [https://peertube.interhop.org/w/1xpBeqYRqt82zbZU2fG1jb](https://peertube.interhop.org/w/1xpBeqYRqt82zbZU2fG1jb){:target="_blank"}

➡️  Si vous voulez discuter avec nous du projet Toobib c'est sur la messagerie instantanée Element et c'est ici : [https://matrix.to/#/#toobib:matrix.interhop.org](https://matrix.to/#/#toobib:matrix.interhop.org){:target="_blank"}

➡️  Et les comptes-rendus des rencontres précédentes, là : [https://pad.fabmob.io/s/Ns_ScQ7G2#](https://pad.fabmob.io/s/Ns_ScQ7G2#){:target="_blank"}

 <div class="responsive-video-container">
<iframe width="100%" height="500" src="https://pad.interhop.org/p/zkxbnNSbs#/" frameborder="0"></iframe>
</div>


