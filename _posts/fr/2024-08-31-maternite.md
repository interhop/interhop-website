---
layout: post
title: "Datathon 2024 - Maternité"
categories:
  - DataThon2024
  - Sujets
ref: datathon-2024
lang: fr
---



> Avec ce sujet les participant⸱es ambitionnent non seulement de créer des indicateurs robustes pour les maternités, mais aussi de développer des outils et des méthodologies réutilisables par d'autres acteurs du système de santé.<br>
> Plus d'informations et inscription : [interhop.org/projets/datathon](https://interhop.org/projets/datathon)

<!-- more -->

# Introduction

Les maternités en France fournissent annuellement des indicateurs d'activité essentiels, tels que le nombre de césariennes, de sièges, de transferts, ou encore le nombre de péridurales. 
Voici un exemple de ces indicateurs pour l'année 2023 dans une maternité française.
![](https://pad.interhop.org/uploads/43ce1329-1cc5-40ea-b682-ede3cb3f6cba.png)

Ces indicateurs, cruciaux pour évaluer et améliorer les pratiques obstétricales, sont dérivés des séjours RSS[^hdh_rss] (Résumé de Sortie Standardisé), qui reprennent les informations médicales de chaque patient lors de leur séjour hospitalier produites à travers plusieurs unités de soins

Le challenge de ce datathon réside non seulement dans la **création automatisée de ces indicateurs** à partir des données brutes, mais aussi dans **leur visualisation efficace**. 

[^hdh_rss]: [https://www.documentation-snds.health-data-hub.fr/snds/glossaire/rss.html](https://www.documentation-snds.health-data-hub.fr/snds/glossaire/rss.html){:target="_blank"}

# Matériels et Méthodes

Le datathon organisé par l'association InterHop.org vise à explorer ces aspects en utilisant l'outil open-source [LinkR](https://interhop.org/projets/linkr), une plateforme permettant la manipulation et l'analyse des données de santé, tant pour les cliniciens que pour les data scientists.

Pour atteindre les objectifs de ce datathon, plusieurs outils et librairies R et Python seront employés pour transformer, analyser et visualiser les données RSS[^rss].

[^rss]: [https://www.documentation-snds.health-data-hub.fr/snds/glossaire/rss.html](https://www.documentation-snds.health-data-hub.fr/snds/glossaire/rss.html){:target="_blank"}

1. **Création des indicateurs (à partir du format RSS)** :
    - **R** : La librairie `dplyr` ou `pmeasyr`[^pmeasyr] pourra être utilisée pour manipuler les données, effectuer des filtrages et des agrégations nécessaires pour restructurer les RSS
    - **Python** : Le paquet `pandas`,  `pola.rs` ou `pypmsi`[^pypmsi] sera essentiel pour les opérations de manipulation des DataFrames, permettant de réorganiser les données et de les formater correctement pour l'analytique.

2. **Transformation des données vers modèle OMOP**
    - Objectif subsidiaire
    - **ETL (Extract, Transform, Load)** : les outils comme `Airflow` peuvent être intégrés pour orchestrer les pipelines de transformation des données, en automatisant le processus de conversion des RSS vers OMOP.

3. **Visualisation des données** :
    - **R** : `ggplot2` pourra être utilisé pour créer des graphiques clairs et esthétiques des indicateurs, tels que des histogrammes des taux de césariennes, ou des courbes de survie.
    - **Python** : `matplotlib` et `seaborn` pourront être employés pour des visualisations dynamiques, et `plotly` pour créer des graphiques interactifs permettant aux utilisateurs de naviguer à travers les données.

4. **Création d'un programme de création de données synthétique au format RSS**
   - Objectif subsidiaire
   - Données synthétiques : création d'un programme de génération de données fictives au format "RSS groupé"[^format_rss]. Les fonctions de groupement des GHM[^fonction_groupement] pourront aussi être implémentée à partir des guides officiels
   - Pseudonymisation : création d'un programme permettant de pseudonymiser facilement un fichier au format "RSS groupé"

[^pmeasyr]: [https://guillaumepressiat.github.io/pmeasyr/](https://guillaumepressiat.github.io/pmeasyr/){:target="_blank"}

[^pypmsi]: [https://guillaumepressiat.github.io/pypmsi/](https://guillaumepressiat.github.io/pypmsi/){:target="_blank"}

[^format_rss]: [https://www.atih.sante.fr/formats-pmsi-2024-0](https://www.atih.sante.fr/formats-pmsi-2024-0){:target="_blank"}

[^fonction_groupement]: [https://www.atih.sante.fr/manuel-des-ghm-version-provisoire-2024](https://www.atih.sante.fr/manuel-des-ghm-version-provisoire-2024){:target="_blank"}


### Ressources
Jeux de données fictif et pseudonymisé. Mis a disposition aux participant⸱es

# Résultats

Le datathon vise à implémenter le calcul d'indicateurs clés concernant les activités des maternités françaises, accessibles et interprétables par les cliniciens, tout en fournissant une base solide pour d'autres analyses secondaires.
Cette connaissance métier sera apportée par un médecin responsable d'un Département d'Informatique Médical DIM d'un hôpital français.

Pour exemple l’Agence technique de l’information sur l’hospitalisation (ATIH) met à disposition des indicateurs sur l’activité hospitalière en périnatalité produits à partir des données PMSI (Programme de médicalisation des systèmes d’informations).
Un tableau de bord est disponible sur le site internet [ScanSanté](https://www.scansante.fr/applications/indicateurs-de-sante-perinatale){:target="_blank"}, et permet de sélectionner aussi bien le type d’indicateur que la période ou le périmètre géographique.


Les résultats seront présentés sous forme de tableaux de bord dynamiques, permettant de visualiser et d'explorer les indicateurs sur plusieurs dimensions (temps, localisation, niveau de maternité, etc.). 
Voici un exemple de dashboard réalisé par le NHS[^nhs_dashboard] en Angleterre:

![](https://pad.interhop.org/uploads/e749eebb-e3d3-4f0f-a939-16757eba6ebc.png)

[^nhs_dashboard]: [https://digital.nhs.uk/data-and-information/data-collections-and-data-sets/data-sets/maternity-services-data-set/maternity-services-dashboard#maternity-dashboard](https://digital.nhs.uk/data-and-information/data-collections-and-data-sets/data-sets/maternity-services-data-set/maternity-services-dashboard#maternity-dashboard){:target="_blank"}

Avec ce datathon nous souhaitons proposer
- une amélioration du pilotage de l'obstétrique aux différentes échelles (chu, bassin de soin, région...), notamment à terme avec des visu de données historisées et des tendances, couplée à des alertes et des seuils, 
- faire ressortir des nouvelles clé de compréhension et d'analyse de la MCO notamment pour les parcours MCO complexes

L'utilisation de LinkR devrait faciliter la réutilisation de ces indicateurs au sein des services des  Département d'Informatique Médical DIM améliorant ainsi la qualité des soins obstétricaux. Nous sou  zhaitons aussi démocratiser l'utilisation des données et permettre à une utilisation plus large auprès des équipes soignantes.

Voici un exemple de rendu sur LinkR[^linkr_website] :

![](https://pad.interhop.org/uploads/f77cb8ed-2346-4a4d-a496-07579b66c90f.png)

[^linkr_website]: [https://linkr.interhop.org/blog/2024/08/01/tableau-de-bord-de-r%C3%A9animation/](https://linkr.interhop.org/blog/2024/08/01/tableau-de-bord-de-r%C3%A9animation/){:target="_blank"}

Le code source produits lors de ce datathon sera colligé sur le dépot de code de l'association InterHop. Il sera disponible à cette adresse : [https://framagit.org/interhop/challenges/datathon-2024](https://framagit.org/interhop/challenges/datathon-2024){:target="_blank"}


# Conclusion

Ce projet s'inscrit dans une démarche de science ouverte, où la production de code, les méthodes et les outils développés seront rendus accessibles à tous et toutes.

Avec ce sujet les participant⸱es ambitionnent non seulement de créer des indicateurs robustes pour les maternités, mais aussi de développer des outils et des méthodologies réutilisables par d'autres acteurs du système de santé.
