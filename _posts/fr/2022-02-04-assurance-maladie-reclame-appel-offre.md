---
layout: post
title: "Données de santé: l'Assurance maladie réclame un appel d'offres pour le Health Data Hub"
categories:
  - HealthDataHub
  - Microsoft
  - CNAM
  - AFP
ref: assurance-maladie-reclame-appel-offre
lang: fr
---

## Dépêche AFP

Paris, 3 fév 2022 (AFP) - Le Health Data Hub, mégafichier des données de santé françaises, doit faire l'objet d'un "appel d'offres avec mise en place d'une commission indépendante" pour choisir son nouvel hébergeur en remplacement de Microsoft, estime le conseil d'administration de l'Assurance maladie dans un avis publié jeudi.

<!-- more -->

"Déprécié" par la controverse, le Health Data Hub a besoin de "quelques garde-fous pour regagner la confiance", estime le Conseil de la Caisse nationale d'assurance maladie (Cnam).

L'instance, qui rappelle son "plein soutien" au projet, n'a toutefois jamais digéré le choix du géant américain Microsoft pour héberger cette plateforme publique censée regrouper toutes les données de santé des Français, afin de faciliter la recherche médicale.

Ce choix a été opéré à l'été 2019 "sans appel d'offre" et "dans une relative urgence", en dépit du "risque d'accès non autorisé aux données" depuis les Etats-Unis, souligne-t-elle.

Entre temps, le Conseil d'Etat et la Commission informatique et libertés (Cnil) ont rappelé à l'ordre le gouvernement, qui s'était engagé en février 2021 à trouver "une solution technique" conforme au droit national et européen dans un délai "ne dépassant pas deux ans".

Pour rapatrier le Health Data Hub sur "une plateforme technologique souveraine", le Conseil de la Cnam demande une sélection transparente.

Les pouvoirs publics devraient ainsi "rendre publique" la liste des "neuf solutions d'hébergement éligibles" identifiées, et "s'engager sur un calendrier précis de migration".

Surtout, il faudra "procéder à un appel d'offres à la suite d'un audit indépendant" et en "publier le cahier des charges exhaustif".

Ces propositions interviennent alors que le projet est en suspens depuis fin décembre, le gouvernement ayant alors retiré sa demande d'autorisation à la Cnil, indispensable à la pleine mise en oeuvre de la plateforme.

Un repli temporaire justifié par des motifs techniques, mais permettant aussi d'éviter un désaveu sur ce sujet sensible en pleine campagne présidentielle.

## Communiqué de la présidence de la CNAM

<img src="https://i.ibb.co/L9BQd06/Screenshot-2022-02-04-at-08-22-16.png" alt="Screenshot-2022-02-04-at-08-22-16" border="0">
<img src="https://i.ibb.co/nkhDd3k/Screenshot-2022-02-04-at-08-22-23.png" alt="Screenshot-2022-02-04-at-08-22-23" border="0">
