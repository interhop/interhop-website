---
layout: post
title: "Bilan de l'année 2023"
categories:
  - Newsletter
  - Décembre
  - "2023"
ref: newsletter-decembre-2023
lang: fr
---

En 2023 nos projets opensources et associatifs se structurent.  Une nouvelle association partenaire d'InterHop est créée : [Toobib.org](https://toobib.org).

Nous sommes heureux·euses de partager nos avancées avec vous !

<!-- more -->

# Pourquoi l'opensource ? 

Nous avons récemment publié  [un article](https://interhop.org/2023/11/03/la-recherche-sera-ethique) qui questionne sur la place de l'éthique et le partage des connaissances dans le domaine du numérique en santé.

Voici quelques passages : 

> Le "Serment d’Hippocrate et la Déclaration de Genève contiennent des engagements clairs de partage des connaissances: “JE PARTAGERAI mes connaissances médicales au bénéfice du·de la patient·e et pour les progrès des soins de santé”.

> Cependant le modèle économique de développement des algorithmes impose souvent leur propriétarisation. Le code source de ces programmes est majoritairement caché et la protection de la propriété intellectuelle se fait au détriment des patient·es. 

> Un·e professionnel·le de santé impliqué·e dans l’innovation en soins de santé est donc souvent en conflit de valeur.

InterHop se propose d'aider à résoudre ce conflit en promouvant le [logiciel libre en santé](https://interhop.org/2021/01/18/opensource-libre-difference).

# Création de l'association Toobib

InterHop est ravie d'avoir participé à la création de l'association Toobib aux côtés de [plusieurs professionnel.le.s de santé](https://toobib.org/us){:target="_blank"}, de la [SAS Dokos](https://dokos.io/){:target="_blank"}, de l'association [P4pillon](https://p4pillon.org/){:target="_blank"} et du [Syndicat de Médecine Générale](https://syndicat-smg.fr/){:target="_blank"}.

Côté patient·es, Toobib.org est une plateforme web de recherche des professionnel·les de santé enregistrés sur l'une des instances du réseau Toobib (en fonction de leur nom, de leur ville...).

Côté des soignant·es, [ToobibPro](https://toobib.org/toobibpro){:target="_blank"} sera une plateforme permettant aux professionnel·les de santé d'utiliser des logiciels libres (FLOSS) tels que le gestionnaire de mots de passe (VaultWarden), la messagerie instantanée (Element/Matrix), la vidéoconférence (BigBlueButton) et le lecteur chiffré de bout en bout (Cryptpad). L'outil central de ToobibPro est le dossier électronique du patient basé sur l'ERP Dokos. Tous ces services seront également décentralisés, open source et chiffrés de bout en bout (dans la mesure du possible).


<div class="responsive-video-container">
<iframe title="ToobibPro - Bref aperçu du logiciel" width="560" height="315" src="https://peertube.interhop.org/videos/embed/d7811615-120f-45bb-ae7e-9d7213862934" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
</div>


Longue vie à l'association Toobib !!!

# Développement de la recherche

InterHop propose toujours un encadrement réglementaire (RPGD), notamment avec l'aide de sa Déléguée à la Protection des Données.

InterHop poursuit le développement de son formulaire de recueil de données en ligne et opensource [Goupile](https://interhop.org/2021/05/18/goupile-concevoir-formulaires). Il est parfait pour les études scientifiques notamment en santé.
Goupile est maintenant référencé sur le site ["Socle Interministériel de Logiciels Libres"](https://code.gouv.fr/sill/detail?name=Goupilereferencement){:target="_blank"}.


<div class="responsive-video-container">
<iframe title="Goupile ou comment concevoir des formulaires" width="560" height="315" src="https://peertube.interhop.org/videos/embed/155e9773-212c-43c3-a275-0b473afbecab" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
</div>

Goupile est installé sur des serveurs certifiés pour les données de santé (HDS) et peut donc être délivré via une url dédiée et sécurisée. C'est d'ailleurs ce que nous faisons pour plusieurs projets nationaux de recherche.

A côté de Goupile nous avons mis en ligne le premier drive opensource certifié pour les données de santé. Il s’agit du drive CryptPad également installé chez [GPLExpert](https://gplexpert.com/hebergement-donnees-sante-hds/){:target="_blank"} notre Hébergeur de Données de Santé (HDS). 


# Des données pour quoi faire ?

InterHop est très fière de présenter son tout nouvel outil de recherche. Il s'agit de [LinkR](https://interhop.org/2023/11/15/linkr-logiciel-datasciences). 

### LinkR à quoi ça sert ?

LinkR est une plateforme collaborative et opensource de data science en santé. Elle facilite la formation et le partage décentralisé des algorithmes utilisés durant la recherche (statistiques, machine learning, deep learning).
- pour le ou la *clinicien·ne*, elle permet d’accéder aux données de santé avec une interface graphique, ne nécessitant pas de connaissances en programmation
- pour le *data scientist*, elle permet de manipuler les données via une interface de programmation R et Python
- pour *l'étudiant·e*, elle permet d’apprendre la manipulation des données médicales

<p align="center">
  <img src="https://pad.interhop.org/uploads/99d98f74-0b2a-4118-b42c-2aae450dd5b3.png" alt="Données agrégées" style="border:solid 1px; color:#CECECE; padding:5px;">
  <em>Onglets de données agrégées avec un plugin "Figure (ggplot2)"</em>
</p>

Avec Goupile et LinkR, InterHop apporte sa pierre à l'édifice pour une recherche plus ouverte et partagée.
D'ailleurs InterHop anime aussi un groupe d'entraide : [InterCHU](https://interhop.org/2023/10/23/reunion-interchu-anesth-rea). Ces journées sont un temps d’échange entre soignant·es et ingénieur·es en santé. Nous promouvons l’interopérabilité qui est un moteur du partage des connaissances. Ce temps permet de préparer la recherche de demain. Une recherche adossée aux logiciels libres, décentralisée par défaut.


# Dons

Pour qu’InterHop continue, nous faisons un appel à votre générosité. InterHop ne fonctionne pour l’instant qu’avec des stagiaires et des bénévoles. A côté de la location des serveurs HDS, nous souhaitons salarier une personne pour améliorer nos outils. 
Nous avons aussi l'idée de proposer une plateforme opensource d'analyse des données...

Aidez-nous financièrement avec un don (défiscalisé à hauteur de 66 % du montant de votre don) sur [interhop.org/dons](https://interhop.org/dons). Un petit don mensuel est mieux, pour nous, qu’un don ponctuel.

Vous êtes chercheurs.euses ou soignant·es ? Aidez nous en utilisant nos services open sources associatifs installés sur des serveurs certifiés pour le soin.

Nous vous souhaitons de belles fêtes de fin d’année. Et à bientôt en 2024 !
