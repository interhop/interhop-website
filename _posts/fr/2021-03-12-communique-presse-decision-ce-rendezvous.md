---
layout: post
title: "Décision du Conseil d’Etat du 12 mars 2021"
categories:
  - Conseil Etat
  - Doctolib
  - Amazon
ref: conseil-etat-decision-ce-rendez
lang: fr
---


Après une audience de plus de 2h et une instruction de 5 jours, les parties entendent faire valoir leur indignation suite à la décision intervenue ce jour et limitée à un unique paragraphe en ce qui concerne l’instruction.

*Ces dernières restent fermes sur leur argumentation initiale.*

<!-- more -->

# Données de santé

Dans son ordonnance du 12 mars[^ce_docto], le juge considère que les données en cause ne sont pas des données de santé :

> “Les données litigieuses comprennent les données d’identification des personnes et les données relatives aux rendez-vous mais pas de données de santé sur les éventuels motifs médicaux d’éligibilité à la vaccination”

Les parties rejettent fermement cette interprétation qui contrevient à la définition juridique telle que prévue par le RGPD, rappelé avec constance par la CNIL et l’Ordre des Médecins[^cnil_cnom]:

> « Ces informations peuvent renseigner sur l’état de santé des patients, de même que la simple connaissance d’une consultation d’un spécialiste peut donner une indication sur l’état de santé (ex. consulter un cardiologue régulièrement). »

Le secret médical est un principe pluri-centenaire, co-substantiel à la relation de soin.

> « Il n’y a pas de soins sans confidences, de confidences sans confiance, de confiance sans secret »[^conf].

Les requérants ne remettent pas en cause l’intérêt du service proposé par Doctolib.

Le combat se situe sur le terrain des libertés fondamentales. Comme la CNIL ou la Cour de Justice de l’Union Européenne, ils veulent pérenniser ce contrat de confiance tacite entre les soignant.e.s et les soigné.e/s.

# Extraterritorialité du droit américain

Si le juge reconnait les ingérences des lois américaines et leurs effets sur le droit à la protection des données à caractère personnel, il fait valoir que la procédure déployée par Amazon est suffisante.

Cette procédure conclue par addendum complémentaire permettrait à Amazon de s’opposer directement aux demandes d’accès par des autorités américaines ou de les notifier à la société Doctolib.

Les parties récusent cette affirmation. Elle rappellent que les demandes fondées sur la section 702 du FISA et sur l’EO 12333 sont des demandes qui peuvent contraindre Amazon à passer sous silence cette “collecte en vrac” des données de santé. Cette garantie juridique additionnelle serait insuffisante et ne garantirait nullement la préservation des données de santé.

# Mesures techniques additionnelles

Les parties rappellent l’analyse technique développée par InterHop lors de l’audience, et disponible sur son blog : [https://interhop.org/2021/03/10/reponse-franceinter-doctolib](https://interhop.org/2021/03/10/reponse-franceinter-doctolib)

Leurs conclusions sont les mêmes que celles déployés par plusieurs spécialistes de sécurité informatique[^ludo] [^remy] ou journalistes dont France Inter[^franceinter]. Les mesures techniques seraient insuffisantes pour garantir le droit à la protection des données de santé.

Hier, le journaliste Olivier Tesquet a révélé un document d’importance daté de septembre 2019, par laquelle Doctolib elle-même admettait un accès possible aux données :

<img src="https://i.ibb.co/4N9Mkyv/Ew-Rh-Wh-XEAQ0-Ge-C.jpg" alt="Ew-Rh-Wh-XEAQ0-Ge-C" border="0">

Les parties constatent que l’ensemble de leurs arguments techniques ne figurent pas dans l’ordonnance du juge alors qu’ils ont donné lieu à près de 45 minutes de débat.

Elles regrettent que la CNIL n’ait pas été saisie malgré les maintes demandes réalisées par leur avocate Me Juliette Alibert.

Vous êtes expert.e.s en sécurité ? Vous êtes développeur.euse.s web ? Aidez-nous ! Donnez votre avis !

<iframe id="haWidget" allowtransparency="true" src="https://www.helloasso.com/associations/interhop/collectes/cagnotte-de-soutien-recours-devant-le-conseil-d-etat/widget-compteur" style="width:100%;height:450px;border:none;"></iframe>

[^ce_docto]: [Le juge des référés ne suspend pas le partenariat entre le ministère de la santé et Doctolib pour la gestion des rendez-vous de vaccination contre la covid-19](https://www.conseil-etat.fr/actualites/actualites/le-juge-des-referes-ne-suspend-pas-le-partenariat-entre-le-ministere-de-la-sante-et-doctolib-pour-la-gestion-des-rendez-vous-de-vaccination-contre)

[^conf]: [Secret médical : respect, partage, dérogation et violation](https://www.france-assos-sante.org/66-millions-dimpatients/patients-vous-avez-des-droits/le-secret-medical/)

[^remy]: [Doctolib est trop bavard](https://remy.grunblatt.org/doctolib-est-trop-bavard.html)

[^ludo]: [https://twitter.com/ldubost/status/1369347379724496896](https://twitter.com/ldubost/status/1369347379724496896)

[^franceinter]: [Doctolib : le chiffrement des données incomplet ?](https://www.franceinter.fr/justice/doctolib-le-chiffrement-des-donnees-incomplet)

[^cnil_cnom]: [Guide Pratique sur la protection des données personnelles](https://www.cnil.fr/sites/default/files/atoms/files/guide-cnom-cnil.pdf)

    
Pour rappel, voici les 13 requérants:
- Association InterHop 
- Syndicat National Jeunes Médecins Généralistes (SNJMG) 
- Syndicat de la Médecine Générale (SMG) 
- Union française pour une médecine libre (UFML) 
- Fédération des Médecins de France (FMF) 
- Didier Sicard, ancien président du Comité Consultatif National d’Ethique CCNE 
- Association Constances 
- Marie Citrini, personne qualifiée au Conseil de l’APHP, représentante des usagers 
- Les Actupiennes 
- Actions Traitements 
- Act-Up Sud Ouest 
- Fédération SUD Santé Sociaux 
- La Ligue des Droits de l’Homme 


Contact : j.alibert@alibert-avocat.fr
