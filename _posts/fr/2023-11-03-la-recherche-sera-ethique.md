---
layout: post
title: "L'informatique médicale sera-t-elle éthique?"
categories:
  - Recherche
  - Ethique
  - OpenSource
ref: informatique-medicale-ethique
lang: fr
---


Historiquement, les logiciels cliniques étaient des systèmes simples. Ils permettaient la gestion des patient.e.s (identité, adresse, facturation) et colligeaient l’information clinique remplie par les praticiens. Ces systèmes basiques n'étaient pas considérés comme faisant partie de la médecine et n'étaient donc pas soumis aux processus scientifiques et aux obligations morales de l’innovation médicale.

<!-- more -->

Dans un futur proche (maintenant ?), nous disposeront d'une gamme de programmes interactifs et complexes qui, il est probable, auront une influence sur les résultats cliniques (positive ou négative). Nous auront des [algorithmes](https://www.lebigdata.fr/big-data-soins-de-sante){:target="_blank"} de prédiction clinique, des aides à la décision clinique, des applications mobiles, des moteurs de triage (aux urgences par exemple), de l’aide automatique au codage... Même si cela n’est pas encore clairement démontré, nous parlons ici de diminuer les erreurs médicales, d’améliorer la présentation de l'information clinique, de réduire la souffrance et de sauver des vies.

Le modèle économique de développement de ces algorithmes impose leur propriétarisation. Le code source de ces programmes est majoritairement caché et la protection de la propriété intellectuelle se fait au détriment des patient.e.s. Ceci est contraire à l'éthique médicale qui préconise le partage des connaissances au reste de l’humanité. 

# L’éthique

Lors du développement de programmes en santé, il est important de reconnaître la dimension éthique que la médecine apporte par rapport aux autres industries.

Dans d'autres domaines d'activité humaine, le consommateur a souvent le choix d'utiliser ou non une technologie. Dans le cadre de la médecine et en urgence surtout, les patients ne pourront pas "choisir" de ne pas utiliser une technologie. De plus les logiciels et l’algorithmique feront sûrement partie de la médecine quotidienne de demain. Ils feront partie de l’[innovation clinique](https://www.pwc.fr/fr/vos-enjeux/data-intelligence.html){:target="_blank"}. 

# Argent public = Code public

La recherche médicale est en partie financée par l'État, donc par les contribuables. Lorsque cette recherche médicale publique aboutit au développement de logiciels ou d'algorithmes, il est logique que le code source soit ouvert, afin que l'investissement soit le plus avantageux possible pour les contribuables, éventuel.le.s patient.e.s. Pratiques qui sont loin d'être généralisées...

# L’opensource pour la validation scientifique

Lorsqu'un programme est propriétaire, il n'y a aucun moyen de le tester indépendamment par des pairs. Cette validation externe des algorithmes est sacrifiée au nom du profit.

La publication ouverte des innovations médicales est le seul moyen d'obtenir un examen indépendant. Sans cela, nous devons simplement nous fier aux affirmations des propriétaires de la technologie. Ceci diminue drastiquement la sécurité pour les patient.e.s.

La publication ouverte permet également le développement itératif et continu d'une technologie existante. Lorsque la technologie est propriétaire, ce développement partagé ne peut tout simplement pas se produire. Pire, les propriétaires ont le contrôle exclusif de l'orientation du développement et même de la question de savoir si le développement se fera.

# L’opensource pour l’efficience et la mise en commun des efforts

Partager les connaissances signifie mettre en commun les efforts - si les connaissances sont librement disponibles, alors je n'ai pas besoin d'utiliser mes ressources pour les créer à nouveau. Je peux donc utiliser mon temps pour créer de nouvelles connaissances que je peux ensuite partager.  

A minima si nous décidons d’adopter le modèle de l'industrie pharmaceutique qui limite l'accès à ses médicaments les plus récents, il faut alors imposer une durée maximale avant que l’algorithme entre dans le domaine publique.

Cependant les industries des algorithmes et du médicaments sont différentes. Les coûts de recherche et de développement des médicaments et de la recherche clinique dépasse largement le coût de développement d’algorithmes, surtout si nous mettons en commun nos efforts. Avec les logiciels, la réplication des coûts d'origine reste marginale. Il n'y a peu de raisons de ne pas partager ces développements avec tous ceux qui en ont besoin.

# La médecine comme profession "open-source" par excellence

Historiquement, la médecine était une profession "open-source". Les médecins ont eu amplement l'occasion au cours des millénaires d'expérimenter les dangers du charlatanisme, de l'alchimie et de la sorcellerie qui sont les conséquences inévitables d'un manque de partage ouvert et de révision ouverte par les pairs des développements en médecine.

La première promesse médicale, le [Serment d'Hippocrate](https://www.conseil-national.medecin.fr/medecin/devoirs-droits/serment-dhippocrate){:target="_blank"}, contient un engagement clair de partage des connaissances : enseigner à la prochaine génération de médecins "sans salaire ni contrat". 

Ensuite, la [Déclaration de Genève](https://www.conseil-national.medecin.fr/medecin/devoirs-droits/serment-dhippocrate){:target="_blank"} renforce cette responsabilité en matière de partage des connaissances et énonce : "JE PARTAGERAI mes connaissances médicales au bénéfice du. de la patient.e et pour les progrès des soins de santé".

Il est difficile d'imaginer comment un.e professionnel.le de santé impliqué.e dans l'innovation en soins de santé pourraient s'opposer à l'ouverture de son travail sans être en conflit avec son éthique médicale. 

Il faut au contraire proposer que les cliniciens et la communauté médicale prennent des mesures actives pour empêcher la propriétarisation de la médecine. Le libre doit devenir la norme en médecine. Et ceci est vrai pour tous les domaines - gouvernance, infrastructures, ontologies, algorithmes et logiciels...

> Cet article est librement inspiré de [Open source is the only way for Medicine](https://marcus-baw.medium.com/open-source-is-the-only-way-for-medicine-9e698de0447e){:target="_blank"} par [Dr Marcus Baw](https://medium.com/@marcus_baw){:target="_blank"}
