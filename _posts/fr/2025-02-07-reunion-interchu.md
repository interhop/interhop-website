---
layout: post
title: "Lancement de la saison 2025 des rencontres InterCHU"
categories:
  - OMOP
  - France
  - InterCHU
ref: journee-interchu-anesth-rea-5
lang: fr
---

InterHop est heureux d'annoncer le retour des **rencontres InterCHU** pour l'année 2025 ! 

<!-- more -->

Depuis maintenant plusieurs années, le réseau interCHU rassemble des ingénieur·es et professionnel·les de santé engagés dans la structuration et l'interopérabilité de données de santé. En partageant nos expériences, nos codes et nos outils, nous progressons collectivement vers des solutions ouvertes et interopérables pour la recherche. Aujourd’hui, notre réseau se renforce grâce à la participation de professionnels issus des CHU d'Amiens, Brest, Lille, Rennes, Rouen, Toulouse, Marseille mais aussi des hôpitaux Foch et de Saint-Malo.

Les rencontres de cette année s'annoncent riches en thématiques et en collaborations : 
- Visualisation et dashboarding sur données de santé
- Présentation des outils développés par la communauté InterHop : [Goupile]({{ site.baseurl }}/projets/goupile), [Libre Data Hub](https://libredatahub.org/), [Linkr]({{ site.baseurl }}/projets/linkr)
- Formats d'interopérabilité : FHIR et OMOP
- Outils de sélection de cohorte : [Cohort360](https://docs.cohort360.org/)
- Présentation du groupe numérique de la [SFAR (Société Française d'Anesthésie et de Réanimation)](https://sfar.org/){:target="_blank"}
- Deuxième édition du [Datathon InterHop]({{ site.baseurl }}/projets/datathon)

Chaque rencontre sera l'occasion d'échanger sur les avancées, d'explorer de nouvelles perspectives et de renforcer notre dynamique collective autour des données de santé.

Vous êtes intéressé·e ? Rejoignez le réseau InterCHU et participez à nos échanges !

La prochaine édition aura lieu le **Jeudi 20 février à 13h** en visio : [https://visio.octopuce.fr/b/int-a1t-rpf-huo](https://visio.octopuce.fr/b/int-a1t-rpf-huo). Nous aborderons la thématique de la visualisation des données et du dashboarding.

Pour plus d’informations sur InterHop et le réseau InterCHU : <a href="mailto:{{ site.data.company.contact_email_address }}">{{ site.data.company.contact_email_address }}</a>

Pour être au courant des prochaines réunions vous pouvez nous rejoindre sur nos fils de discussion associatifs en vous inscrivant sur [Element]({{ site.baseurl }}/projets/element).
