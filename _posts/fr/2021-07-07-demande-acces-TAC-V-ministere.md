---
layout: post
title: "Demande de documents administratifs : Demande d’accès au code source de Tous-Anti-Covid Verif"
categories:
  - Courrier
  - TAC-V
  - Ministere
ref: demande-acces-tac-v-ministere
lang: fr
---


<p style="text-align:right;">
Olivier Véran  <br/>
Ministre des Solidarités et de la Santé<br/>
14 Avenue Duquesne, 75350 Paris
</p>

<!-- more -->

Monsieur,

Le dispositif intitulé « Passe sanitaire », consistant en la présentation numérique ou papier d’une « preuve sanitaire » a été mis en place récemment par le Ministre des Solidarités et de la santé et le secrétaire d'État chargé de la transition numériques et des communications électroniques[^1]. 

Il est prévu que la lecture des justificatifs (vaccination, résultats de tests virologiques) soit réalisée au moyen d'une application mobile dénommée “ TousAntiCovid Vérif ” mise en œuvre par le Ministre chargé de la santé (Direction Générale de la Santé). Cette application permet de lire les noms, prénoms et date de naissance de la personne concernée par le justificatif, ainsi qu'un résultat positif ou négatif de détention d'un justificatif conforme.

La CNIL dans sa délibération n° 2021-067 du 7 juin 2021[^2] relève "le code source de l’application «TousAntiCovid Vérif»,  déjà disponible sur les magasins d’applications mobiles(«AppStore» et «Playstore»), n’a pas  été  rendu  public.  La  Commission  regrette  cette  non-publication et  appelle  le Gouvernement à  rendre  public  ce  code  source expurgé, le  cas  échéant, des  secrets permettant de sécuriser les transmissions de données avec les serveurs centraux.".

Afin d’éclairer ses positions et de jouer son rôle d’expertise et de vigie de la démocratie,  l'association InterHop demande le document administratif suivant, en application des articles L311-1 à R311-15 relatifs au Code des Relations entre le Public et l'Administration [^3]  : 
- code source de l'application “ TousAntiCovid Vérif ”

Nous souhaitons recevoir ce document :
- dans un format numérique, ouvert et réutilisable. 

Nous vous remercions de nous indiquer, après publication, son adresse de téléchargement et de nous l'envoyer  en pièce jointe, par mail.

Comme le livre III du code des relations entre le public et l’administration le prévoit, lorsque le demandeur a mal identifié celui qui est susceptible de répondre à sa requête, je vous prie de bien vouloir transmettre cette demande au service qui détient le document demandé si tel est le cas.

Veuillez agréer, Madame, Monsieur, l'expression de nos sentiments distingués.

InterHop

[^1]: [Décret n° 2021-724 du 7 juin 2021 modifiant le décret n° 2021-699 du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000043618403)

[^2]: [Délibération  n° 2021-067du 7juin  2021  portant  avis  sur  le projet de décret portant application duII de l’article 1erde la loi n°2021-689 du 31 mai 2021 relative à la gestion de la sortie de crise sanitaire](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_2021-067_du_7_juin_2021_portant_avis_sur_le_projet_de_decret_portant_application_du_ii_de_larticle_1er_de_la_loi_du_31_mai_2021.pdf)

[^3]: https://www.cada.fr/particulier/mes-droits

