---
layout: post
title: "Décret exécutif sur le renforcement des garanties pour les activités de renseignement électromagnétique des États-Unis"
categories:
  - RGPD
  - USA
  - FISA
ref: executive-order-enhancing
lang: fr
---

Ce billet de blog propose une traduction du décret présidentiel sur le renforcement des garanties pour les activités de renseignement électromagnétique des États-Unisi.
Voici [un lien vers le texte originel](https://www.whitehouse.gov/briefing-room/presidential-actions/2022/10/07/executive-order-on-enhancing-safeguards-for-united-states-signals-intelligence-activities/) ainsi qu'[un autre vers un pdf mis en forme par l'association NYOB](https://noyb.eu/sites/default/files/2022-10/Biden EO on Surveillance%2C Structured.pdf).
Voici la traduction ci-dessous.

<!-- more -->
Par l'autorité qui m'est conférée en tant que Président par la Constitution et les lois des États-Unis d'Amérique, il est par la présente ordonné ce qui suit :

## Section 1. Objectif.
Les États-Unis collectent des renseignements d'origine électromagnétique afin que leurs décideurs en matière de sécurité nationale aient accès à des informations opportunes, précises et perspicaces nécessaires pour promouvoir les intérêts de sécurité nationale des États-Unis et protéger leurs citoyens et ceux de leurs alliés et partenaires. Les capacités de renseignement d'origine électromagnétique sont l'une des principales raisons pour lesquelles nous avons été en mesure de nous adapter à un environnement de sécurité dynamique et difficile, et les États-Unis doivent préserver et continuer à développer des capacités de renseignement d'origine électromagnétique robustes et technologiquement avancées pour protéger notre sécurité et celle de nos alliés et partenaires. Dans le même temps, les États-Unis reconnaissent que les activités de renseignement électromagnétique doivent tenir compte du fait que toutes les personnes doivent être traitées avec dignité et respect, indépendamment de leur nationalité ou de leur lieu de résidence, et que toutes les personnes ont des intérêts légitimes dans le traitement de leurs informations personnelles. **Par conséquent, cette ordonnance établit des garanties pour de telles activités de renseignement électromagnétique.**

## Sec. 2. Activités de renseignement sur les transmissions.

### (a) Principes. Les activités de renseignement d'origine électromagnétique doivent être autorisées et menées conformément aux principes suivants :

- (i) Les activités de renseignement d'origine électromagnétique sont autorisées par la loi ou par un décret, une proclamation ou une autre directive présidentielle et sont entreprises conformément à la Constitution et aux lois, décrets, proclamations et autres directives présidentielles applicables.

- (ii) Les activités de renseignement d'origine électromagnétique sont soumises à des garanties appropriées, qui font en sorte que la vie privée et les libertés civiles fassent partie intégrante de la planification et de la mise en œuvre de ces activités, afin que :

  - (A) les activités de renseignement électromagnétique ne soient menées qu'après avoir déterminé, sur la base d'une évaluation raisonnable de tous les facteurs pertinents, que les activités sont nécessaires pour faire progresser une priorité validée en matière de renseignement, bien que le renseignement électromagnétique ne doive pas être le seul moyen disponible ou utilisé pour faire progresser certains aspects de la priorité validée en matière de renseignement ; et

  - (B) les activités de renseignement électromagnétique ne sont menées que dans la mesure et d'une manière proportionnées à la priorité validée en matière de renseignement pour laquelle elles ont été autorisées, dans le but de parvenir à **un juste équilibre entre l'importance de la priorité validée en matière de renseignement à faire progresser et l'impact sur la vie privée et les libertés civiles de toutes les personnes, indépendamment de leur nationalité ou de leur lieu de résidence**.

- (iii) Les activités de renseignement sur les transmissions font l'objet d'un contrôle rigoureux afin de s'assurer qu'elles sont conformes aux principes énoncés ci-dessus.

### (b) Objectifs. Les activités de collecte de renseignements sur les transmissions sont menées en vue d'atteindre des objectifs légitimes.

- (i) Objectifs légitimes.

  - (A) Les activités de collecte de renseignements d'origine électromagnétique ne doivent être menées qu'en vue d'atteindre un ou plusieurs des objectifs suivants :

    - (1) comprendre ou évaluer les capacités, les intentions ou les activités d'un gouvernement étranger, d'une armée étrangère, d'une faction d'une nation étrangère, d'une organisation politique basée à l'étranger, ou d'une entité agissant au nom ou sous le contrôle d'un tel gouvernement étranger, d'une telle armée, faction ou organisation politique, afin de protéger la sécurité nationale des États-Unis et de leurs alliés et partenaires ;

     - (2) comprendre ou évaluer les capacités, les intentions ou les activités des organisations étrangères, y compris les organisations terroristes internationales, qui représentent une menace actuelle ou potentielle pour la sécurité nationale des États-Unis, de leurs alliés ou de leurs partenaires ;

    - (3) comprendre ou évaluer les menaces transnationales qui ont un impact sur la sécurité mondiale, notamment les changements climatiques et autres changements écologiques, les risques pour la santé publique, les menaces humanitaires, l'instabilité politique et les rivalités géographiques ;

     - (4) la protection contre les capacités et les activités militaires étrangères ;

     - (5) la protection contre le terrorisme, la prise d'otages et la détention d'individus en captivité (y compris l'identification, la localisation et le sauvetage des otages et des captifs) menés par ou au nom d'un gouvernement étranger, d'une organisation étrangère ou d'une personne étrangère ;

     - (6) la protection contre l'espionnage, le sabotage, l'assassinat ou d'autres activités de renseignement menées par ou au nom d'un gouvernement étranger, d'une organisation étrangère ou d'une personne étrangère, ou avec leur aide ;

     - (7) la protection contre les menaces liées à la mise au point, à la possession ou à la prolifération d'armes de destruction massive ou de technologies connexes, ainsi que contre les menaces menées par un gouvernement étranger, une organisation étrangère ou une personne étrangère, ou pour leur compte ou avec leur aide ;

    - (8) la protection contre les menaces à la cybersécurité créées ou exploitées par un gouvernement étranger, une organisation étrangère ou une personne étrangère, ou contre les cyberactivités malveillantes menées par ces derniers ou en leur nom ;

     - (9) la protection contre les menaces pesant sur le personnel des États-Unis, de leurs alliés ou de leurs partenaires ;

     - (10) la protection contre les menaces criminelles transnationales, y compris le financement illicite et l'évasion des sanctions liées à un ou plusieurs des autres objectifs identifiés au paragraphe (b)(i) de cette section ;

     - (11) protéger l'intégrité des élections et des processus politiques, des biens publics et de l'infrastructure des États-Unis (tant physique qu'électronique) contre les activités menées par, au nom ou avec l'aide d'un gouvernement étranger, d'une organisation étrangère ou d'une personne étrangère ; et

    - (12) faire progresser les capacités ou les activités de collecte ou d'exploitation afin de poursuivre un objectif légitime identifié au paragraphe (b)(i) de cette section.

  - (B) Le Président peut autoriser des mises à jour de la liste des objectifs à la lumière de nouveaux impératifs de sécurité nationale, tels que des menaces nouvelles ou accrues pour la sécurité nationale des États-Unis, pour lesquels le Président détermine que les activités de collecte de renseignements d'origine électromagnétique peuvent être utilisées. **Le directeur du renseignement national (directeur) rendra publique toute mise à jour de la liste des objectifs autorisée par le président, à moins que le président ne détermine que cela poserait un risque pour la sécurité nationale des États-Unis.**


- (ii) Objectifs interdits.

  - (A) Les activités de collecte de renseignements sur les transmissions ne doivent pas être menées dans le but de :

    - (1) supprimer ou alourdir la critique, la dissidence ou la libre expression d'idées ou d'opinions politiques par des individus ou la presse ;
    - (2) supprimer ou restreindre les intérêts légitimes de la vie privée ;
    - (3) supprimer ou restreindre le droit à un avocat ; ou
    - (4) désavantager des personnes en raison de leur origine ethnique, leur race, leur sexe, leur identité sexuelle, leur orientation sexuelle ou leur religion.

  - (B) La collecte d'informations commerciales privées étrangères ou de secrets commerciaux dans le but de conférer un avantage concurrentiel aux entreprises américaines et aux secteurs d'activité américains sur le plan commercial ne constitue pas un objectif légitime. **La collecte de telles informations n'est autorisée que pour protéger la sécurité nationale des États-Unis ou de leurs alliés ou partenaires**



- (iii) Validation des priorités de collecte du renseignement électromagnétique.

  - (A) En vertu de la section 102A de la loi sur la sécurité nationale de 1947, telle que modifiée (50 U.S.C. 3024), le directeur doit établir des priorités pour la communauté du renseignement afin d'assurer la collecte opportune et efficace du renseignement national, y compris le renseignement national collecté par le biais du renseignement d'origine électromagnétique. Pour ce faire, le directeur utilise le National Intelligence Priorities Framework (NIPF), qu'il tient à jour et présente régulièrement au président, par l'intermédiaire de l'assistant du président pour les affaires de sécurité nationale. Afin de s'assurer que les activités de collecte de renseignements d'origine électromagnétique sont entreprises pour promouvoir des objectifs légitimes, avant de présenter le NIPF ou tout autre cadre qui lui succède et qui identifie les priorités en matière de renseignement au président, **le directeur doit obtenir de l'agent de protection des libertés civiles du bureau du directeur du renseignement national (CLPO)** une évaluation permettant de déterminer si, en ce qui concerne les activités de collecte de renseignements d'origine électromagnétique prévues, chacune des priorités en matière de renseignement identifiées dans le NIPF ou le cadre qui lui succède :

    - (1) fait progresser un ou plusieurs des objectifs légitimes énoncés au paragraphe (b)(i) de cette section ;

    - (2) n'a pas été conçue et ne devrait pas donner lieu à la collecte de renseignements électromagnétiques en violation des objectifs interdits énoncés au paragraphe (b)(ii) de la présente section ; et

     - (3) a été établi après une prise en compte appropriée de la vie privée et des libertés civiles de toutes les personnes, indépendamment de leur nationalité ou de leur lieu de résidence.

  - (B) **Si le directeur n'est pas d'accord avec un aspect quelconque de l'évaluation du CLPO en ce qui concerne l'une des priorités en matière de renseignement identifiées dans le NIPF ou le cadre qui lui succède, le directeur inclut l'évaluation du CLPO et l'avis du directeur lorsqu'il présente le NIPF au président**.

### (c) Mesures de protection de la vie privée et des libertés civiles. Les garanties suivantes doivent répondre aux principes contenus dans les paragraphes (a)(ii) et (a)(iii) de cette section.

- (i) Collecte de renseignements d'origine électromagnétique.

  - (A) Les États-Unis ne mèneront des activités de collecte de renseignement d'origine électromagnétique qu'après avoir déterminé qu'une activité spécifique de collecte de renseignement d'origine électromagnétique, **sur la base d'une évaluation raisonnable de tous les facteurs pertinents**, est nécessaire pour faire progresser une priorité validée en matière de renseignement, bien que le renseignement d'origine électromagnétique ne doive pas être le seul moyen disponible ou utilisé pour faire progresser des aspects de la priorité validée en matière de renseignement ; il pourrait être utilisé, par exemple, pour assurer des voies alternatives de validation ou pour maintenir un accès fiable aux mêmes informations. Pour déterminer s'il y a lieu de collecter des renseignements d'origine électromagnétique conformément à ce principe, les **États-Unis - par l'intermédiaire d'un élément de la communauté du renseignement ou d'un comité interinstitutions composé en tout ou en partie des chefs des éléments de la communauté du renseignement, des chefs des départements où se trouvent ces éléments, ou des personnes qu'ils désignent - examinent la disponibilité, la faisabilité et le caractère approprié d'autres sources et méthodes moins intrusives** de collecte des informations nécessaires pour faire progresser une priorité validée en matière de renseignement, y compris de sources diplomatiques et publiques, et donnent la priorité à ces alternatives disponibles, faisables et appropriées au renseignement d'origine électromagnétique.

  - (B) Les activités de collecte de renseignements d'origine électromagnétique doivent être aussi adaptées que possible pour faire avancer une priorité validée en matière de renseignement et, compte tenu des facteurs pertinents, ne pas avoir **d'incidence disproportionnée** sur la vie privée et les libertés civiles. **Ces facteurs peuvent comprendre, selon les circonstances, la nature de l'objectif poursuivi**, les mesures réalisables prises pour limiter la portée de la collecte à l'objectif autorisé, le caractère intrusif de l'activité de collecte, y compris sa durée, la contribution probable de la collecte à l'objectif poursuivi, les conséquences raisonnablement prévisibles pour les personnes, y compris les tiers involontaires, la nature et la sensibilité des données à collecter et les garanties accordées aux informations collectées.

  - (C) Aux fins de la sous-section (c)(i) de la présente section, la portée d'une activité spécifique de collecte de renseignements électromagnétiques peut inclure, par exemple, une ligne d'effort ou une cible spécifique, selon le cas.



- (ii) Collecte en masse de renseignements d'origine électromagnétique.

  - (A) La collecte ciblée doit être hiérarchisée. **La collecte en vrac de renseignements d'origine électromagnétique n'est autorisée que si** un élément de la communauté du renseignement ou un comité interinstitutions composé en tout ou en partie des chefs des éléments de la communauté du **renseignement**, des chefs des départements où se trouvent ces éléments ou des personnes qu'ils désignent, **détermine que les informations nécessaires pour faire progresser une priorité validée en matière de renseignement ne peuvent être raisonnablement obtenues par une collecte ciblée**. Lorsqu'il est déterminé qu'il est nécessaire de procéder à une collecte massive afin de faire progresser une priorité validée en matière de renseignement, l'élément de la Communauté du renseignement applique des méthodes et des mesures techniques raisonnables afin de limiter les données collectées à ce qui est nécessaire pour faire progresser une priorité validée en matière de renseignement, tout en minimisant la collecte d'informations non pertinentes.

  - (B) Chaque élément de la Communauté du renseignement qui recueille des renseignements d'origine électromagnétique par le biais d'une collecte en vrac n'utilise ces informations que dans la poursuite d'un ou de plusieurs des objectifs suivants :

    - (1) protection contre le terrorisme, la prise d'otages et la détention d'individus en captivité (y compris l'identification, la localisation et le sauvetage des otages et des captifs) menés par ou au nom d'un gouvernement étranger, d'une organisation étrangère ou d'une personne étrangère ;

    - (2) la protection contre l'espionnage, le sabotage, l'assassinat ou d'autres activités de renseignement menées par ou au nom d'un gouvernement étranger, d'une organisation étrangère ou d'une personne étrangère, ou avec leur aide ;

    - (3) la protection contre les menaces liées à la mise au point, à la possession ou à la prolifération d'armes de destruction massive ou de technologies connexes, ainsi que contre les menaces menées par un gouvernement étranger, une organisation étrangère ou une personne étrangère, ou pour leur compte ou avec leur aide ;

    - (4) la protection contre les menaces à la cybersécurité créées ou exploitées par un gouvernement étranger, une organisation étrangère ou une personne étrangère, ou contre les cyberactivités malveillantes menées par ces derniers ou en leur nom ;

    - (5) la protection contre les menaces pesant sur le personnel des États-Unis ou de leurs alliés ou partenaires ; et

    - (6) la protection contre les menaces criminelles transnationales, y compris le financement illicite et l'évasion des sanctions liées à un ou plusieurs des autres objectifs identifiés dans la sous-section (c)(ii) de cette section.

  - (C) Le président peut autoriser des mises à jour de la liste des objectifs à la lumière de nouveaux impératifs de sécurité nationale, tels que des menaces nouvelles ou accrues pour la sécurité nationale des États-Unis, pour lesquels le président détermine que la collecte en vrac peut être utilisée. **Le directeur rend publique toute mise à jour de la liste d'objectifs autorisée par le président, à moins que le président ne détermine que cela constituerait un risque pour la sécurité nationale des États-Unis.**

  - (D) Afin de minimiser tout impact sur la vie privée et les libertés civiles, une activité de collecte ciblée de renseignement électromagnétique qui utilise temporairement des données acquises sans discriminants (par exemple, sans identifiants ou termes de sélection spécifiques) est soumise aux garanties décrites dans ce paragraphe, sauf si ces données sont :

    - (1) utilisées uniquement pour soutenir la phase technique initiale de l'activité de collecte de renseignements par signaux ciblés ;

    - (2) conservées uniquement pendant la courte période de temps nécessaire à la réalisation de cette phase ; et

    - (3) supprimées par la suite.


- (iii) Traitement des informations personnelles recueillies par le biais du renseignement électromagnétique.

  - (A) Minimisation. Chaque élément de la Communauté du renseignement qui traite des informations personnelles recueillies par le biais du renseignement d'origine électromagnétique doit établir et appliquer des politiques et des procédures conçues pour minimiser la diffusion et la conservation des informations personnelles recueillies par le biais du renseignement d'origine électromagnétique.

    - (1) Diffusion. Chaque élément de la Communauté du renseignement qui traite des informations personnelles collectées par le biais du renseignement électromagnétique :

      - (a) ne diffusera les informations personnelles concernant des personnes autres que des ressortissants des États-Unis collectées par le biais du renseignement électromagnétique que si elles concernent un ou plusieurs des types d'informations comparables qui, selon la section 2.3 de l'Executive Order 12333 du 4 décembre 1981 (United States Intelligence Activities), tel que modifié, peuvent être diffusées dans le cas d'informations concernant des ressortissants des États-Unis ;

      - (b) ne doit pas diffuser les informations personnelles recueillies par le biais du renseignement électromagnétique uniquement en raison de la nationalité ou du pays de résidence d'une personne ;

      - (c) ne diffuse au sein du gouvernement des États-Unis les informations personnelles recueillies par le biais du renseignement d'origine électromagnétique que si une personne autorisée et formée de manière appropriée a la conviction raisonnable que les informations personnelles seront protégées de manière appropriée et que le destinataire a besoin de connaître ces informations ;

      - (d) doit tenir dûment compte de l'objectif de la diffusion, de la nature et de l'étendue des informations personnelles diffusées, ainsi que du risque d'impact négatif sur la ou les personnes concernées avant de diffuser des informations personnelles collectées par le biais du renseignement électromagnétique à des destinataires extérieurs au gouvernement des États-Unis, y compris à un gouvernement étranger ou à une organisation internationale ; et

      - (e) ne doit pas diffuser les informations personnelles collectées par le biais du renseignement d'origine électromagnétique dans le but de contourner les dispositions de la présente ordonnance.

    - (2) Conservation. Chaque élément de la Communauté du renseignement qui traite des informations personnelles collectées par le biais du renseignement électromagnétique :

      - (a) ne doit conserver les informations personnelles concernant des personnes autres que des ressortissants des États-Unis collectées par le biais du renseignement électromagnétique que si la conservation d'informations comparables concernant des ressortissants des États-Unis est autorisée par la loi applicable et doit soumettre ces informations aux mêmes périodes de conservation que celles qui s'appliquent aux informations comparables concernant des ressortissants des États-Unis ;

      - (b) soumet les informations personnelles concernant des non-américains collectées par le biais du renseignement d'origine électromagnétique pour lesquelles aucune décision finale de conservation n'a été prise aux mêmes périodes de conservation temporaire que celles qui s'appliqueraient à des informations comparables concernant des Américains ; et

      - (c) **supprime les informations personnelles de non-américains collectées par le biais du renseignement d'origine électromagnétique qui ne peuvent plus être conservées, de la même manière que les informations comparables concernant des Américains seraient supprimées.**

  - (B) Sécurité des données et accès. Chaque élément de la Communauté du renseignement qui traite des informations personnelles collectées par le biais du renseignement électromagnétique :

    - (1) doit traiter et stocker les informations personnelles collectées par le biais du renseignement électromagnétique dans des conditions qui assurent une protection appropriée et empêchent l'accès par des personnes non autorisées, conformément aux mesures de protection applicables aux informations sensibles contenues dans les décrets, proclamations, autres directives présidentielles, directives de la Communauté du renseignement et politiques associées ;

    - (2) limitera l'accès à ces informations personnelles au personnel autorisé qui a besoin de connaître ces informations pour accomplir sa mission et qui a reçu une formation appropriée sur les exigences de la loi américaine applicable, comme décrit dans les politiques et procédures émises en vertu de la sous-section (c)(iv) de cette section ; et

    - (3) doit s'assurer que les informations personnelles collectées par le biais du renseignement d'origine électromagnétique pour lesquelles aucune **décision finale de conservation n'a été prise ne sont accessibles que dans le but de prendre ou de soutenir une telle décision ou d'effectuer des fonctions administratives, d'essai, de développement, de sécurité ou de surveillance autorisées.**

  - (C) Qualité des données. Chaque élément de la communauté du renseignement qui traite des informations personnelles collectées par le biais du renseignement d'origine électromagnétique n'inclut ces informations personnelles dans les produits du renseignement que si elles sont conformes aux normes applicables de la communauté du renseignement en matière d'exactitude et d'objectivité, en mettant l'accent sur l'application de normes relatives à la qualité et à la fiabilité de l'information, à la prise en compte d'autres sources d'information et d'autres interprétations des données, et à l'objectivité de l'analyse.




  - (D) Interrogations sur la collecte en vrac. Chaque élément de la **communauté du renseignement qui effectue des interrogations sur des renseignements d'origine électromagnétique non minimisés obtenus par collecte en vrac doit le faire conformément aux utilisations autorisées des renseignements** d'origine électromagnétique obtenus par collecte en vrac définies au paragraphe (c)(ii)(B) de la présente section et conformément aux politiques et procédures publiées au titre du paragraphe (c)(iv) de la présente section, qui doivent tenir compte de manière appropriée de l'impact sur la vie privée et les libertés civiles de toutes les personnes, indépendamment de leur nationalité ou de leur lieu de résidence.

  - (E) Documentation. Afin de faciliter les processus de surveillance énoncés au paragraphe (d) de la présente section et le mécanisme de recours énoncé à la section 3 de la présente ordonnance, chaque élément de la Communauté du renseignement qui s'engage dans des activités de collecte de renseignements sur les transmissions doit conserver une documentation dans la mesure où cela est raisonnable compte tenu de la nature et du type de collecte en question et du contexte dans lequel elle est effectuée. Le contenu de cette documentation peut varier en fonction des circonstances, mais doit, dans la mesure du raisonnable, fournir la base factuelle en vertu de laquelle l'élément de la communauté du renseignement, sur la base d'une évaluation raisonnable de tous les facteurs pertinents, estime que l'activité de collecte de renseignements d'origine électromagnétique est nécessaire pour faire progresser une priorité validée en matière de renseignement.


- (iv) Mise à jour et publication des politiques et procédures. Le chef de chaque élément de la Communauté du renseignement :

  - (A) continue à utiliser les politiques et procédures publiées conformément à la Presidential Policy Directive 28 du 17 janvier 2014 (Activités de renseignement sur les transmissions) (PPD-28), jusqu'à ce qu'elles soient mises à jour conformément à la sous-section (c)(iv)(B) de cette section ;

  - (B) doit, dans un délai d'un an à compter de la date de la présente ordonnance, en consultation avec le procureur général, le CLPO et la Commission de surveillance de la vie privée et des libertés civiles (PCLOB), mettre à jour ces politiques et procédures si nécessaire afin de mettre en œuvre les garanties en matière de vie privée et de libertés civiles prévues par la présente ordonnance ; et

  - (C) devra, dans un délai d'un an à compter de la date du présent décret, rendre publiques ces politiques et procédures dans toute la mesure du possible, dans le respect de la protection des sources et des méthodes de renseignement, afin d'améliorer la compréhension du public et de promouvoir sa confiance dans les garanties en vertu desquelles les États-Unis mènent des activités de renseignement électromagnétique.



- (v) Révision par le PCLOB.

  - (A) Nature de la révision. Conformément à la législation en vigueur, le PCLOB est encouragé à procéder à un examen des politiques et procédures mises à jour décrites au paragraphe (c)(iv)(B) de cette section, une fois qu'elles ont été publiées, afin de s'assurer qu'elles sont conformes aux garanties renforcées contenues dans cette ordonnance.

  - (B) Prise en compte de la révision. Dans les 180 jours suivant l'achèvement de tout examen par le PCLOB décrit dans la sous-section (c)(v)(A) de cette section, le chef de chaque élément de la Communauté du renseignement doit examiner attentivement et mettre en œuvre ou traiter d'une autre manière toutes les recommandations contenues dans cet examen, conformément à la loi applicable.


### (d) Soumettre les activités de renseignement électromagnétique à une surveillance rigoureuse. **Les actions visées dans cette sous-section sont conçues pour s'appuyer sur les mécanismes de surveillance que les éléments de la communauté du renseignement ont déjà mis en place**, afin de garantir davantage que les activités de renseignement électromagnétique sont soumises à une surveillance rigoureuse.

- (i) Responsables juridiques, de la surveillance et de la conformité. Chaque élément de la Communauté du renseignement qui collecte des renseignements d'origine électromagnétique :

  - (A) doit avoir en place des fonctionnaires de haut niveau chargés des questions juridiques, de la surveillance et de la conformité qui effectuent une surveillance périodique des activités de renseignement électromagnétique, y compris un inspecteur général, **un responsable de la protection de la vie privée et des libertés civiles, et un ou plusieurs officiers jouant un rôle désigné de conformité et ayant le pouvoir de surveiller et d'assurer la conformité à la loi américaine applicable** ;

  - (B) doit permettre à ces responsables juridiques, de surveillance et de conformité d'accéder à toutes les informations pertinentes pour assumer leurs responsabilités de surveillance en vertu de la présente sous-section, conformément à la protection des sources ou des méthodes de renseignement, y compris leurs responsabilités de surveillance pour s'assurer que toutes les mesures appropriées sont prises pour remédier à un incident de non-conformité avec le droit américain applicable ; et

  - (C) **ne doit prendre aucune mesure visant à empêcher ou à influencer de manière inappropriée ces responsables juridiques, de surveillance et de conformité dans l'exercice de leurs responsabilités de surveillance** en vertu de la présente sous-section.

- (ii) Formation. Chaque élément de la Communauté du renseignement maintiendra des exigences de formation appropriées pour s'assurer que tous les employés ayant accès au renseignement électromagnétique connaissent et comprennent les exigences de cette ordonnance et les politiques et procédures de signalement et de correction des incidents de non-conformité avec le droit américain applicable.

- (iii) Incidents significatifs de non-conformité.

  - (A) Chaque élément de la Communauté du renseignement doit s'assurer que, si un responsable juridique, de surveillance ou de conformité, tel que décrit dans le paragraphe (d)(i) de cette section, ou tout autre employé, identifie un incident significatif de non-conformité avec le droit américain applicable, l'incident est signalé rapidement au chef de l'élément de la Communauté du renseignement, au chef du département ou de l'agence exécutive (agence) dont dépend l'élément de la **Communauté du renseignement** (dans la mesure où cela est pertinent), et au Directeur.

  - (B) Dès réception de ce rapport, le chef de l'élément de la Communauté du renseignement, le chef de l'agence contenant l'élément de la Communauté du renseignement (dans la mesure où cela est pertinent), et le Directeur s'assurent que toutes les actions nécessaires sont prises pour remédier à l'incident significatif de non-conformité et empêcher qu'il ne se reproduise.

### (e) Savings clause. 

**Sous réserve que la collecte de renseignements d'origine électromagnétique soit effectuée conformément et de la manière prescrite par la présente section de cet ordre, cet ordre ne limite aucune technique de collecte de renseignements d'origine électromagnétique autorisée en vertu de la loi sur la sécurité nationale de 1947, telle que modifiée (50 U.S.C. 3001 et seq.), de la loi sur la surveillance des renseignements étrangers de 1978, telle que modifiée (50 U.S.C. 1801 et seq.) (FISA), de l'ordre exécutif 12333, ou de toute autre loi ou directive présidentielle applicable.**


## Sec. 3. Mécanisme de recours en matière de renseignement sur les transmissions.

### (a) Purpose. 

Cette section établit un mécanisme de recours pour examiner les plaintes qualifiées transmises par l'autorité publique appropriée d'un État qualifié concernant les activités de renseignement d'origine électromagnétique des États-Unis pour toute violation couverte de la loi des États-Unis et, si nécessaire, pour prendre les mesures correctives appropriées.

### (b) Processus de soumission des plaintes qualifiées. 

Dans un délai de 60 jours à compter de la date de cette ordonnance, le directeur, en consultation avec le procureur général et les chefs des éléments de la communauté du renseignement qui recueillent ou traitent les informations personnelles collectées par le biais du renseignement d'origine électromagnétique, établit un processus de soumission des plaintes admissibles transmises par l'autorité publique appropriée dans un État admissible.

### (c) Enquête initiale des plaintes qualifiées par le CLPO.

> Role central du CLPO : Civil Liberties Protection Officer of the Office of the Director of National Intelligence

- (i) Mise en place. Le Directeur, en consultation avec l'Attorney General, établit une procédure autorisant le CLPO à enquêter, à examiner et, le cas échéant, à ordonner des mesures correctives appropriées pour les plaintes qualifiées. Ce processus doit régir la manière dont le CLPO examinera les plaintes admissibles d'une manière qui protège les informations classifiées ou autrement privilégiées ou protégées et doit garantir, au minimum, que pour chaque plainte admissible, le **CLPO** : 

  - (A) examine les informations nécessaires pour enquêter sur la plainte ;

  - (B) exerce son autorité statutaire et déléguée pour déterminer s'il y a eu une violation couverte en :

    - (i) en tenant compte à la fois des intérêts de sécurité nationale pertinents et des protections de la vie privée applicables ;

    - (ii) en faisant preuve de retenue à l'égard de toute décision pertinente prise par les responsables de la sécurité nationale ; et

    - (iii) en appliquant la loi de manière impartiale ;

  - (C) déterminer les mesures correctives appropriées pour toute violation couverte ;

  - (D) fournir un **rapport classifié** sur **les informations indiquant une violation de toute autorité soumise au contrôle de la Foreign Intelligence Surveillance Court (FISC) au procureur général adjoint pour la sécurité nationale**, qui signalera les violations à la FISC conformément à ses règles de procédure ;

  - (E) une fois l'examen terminé, informer le plaignant, par l'intermédiaire de l'autorité publique appropriée dans un État admissible et sans confirmer ou infirmer que le plaignant a fait l'objet d'activités de renseignement électromagnétique des États-Unis, que :

     - (1) "l'examen n'a pas identifié de violations couvertes ou l'officier de protection des libertés civiles du bureau du directeur du renseignement national a émis une détermination exigeant une remédiation appropriée" ;

      -  (2) le plaignant ou un élément de la communauté du renseignement peut, comme le prévoient les règlements publiés par l'Attorney General en vertu de la section 3(d)(i) de la présente ordonnance, demander le réexamen des déterminations du CLPO par la Cour de révision de la protection des données décrite à la sous-section (d) de la présente section ; et

      - (3) si le plaignant ou un élément de la communauté du renseignement demande le réexamen par la Cour de révision de la protection des données, un avocat spécial sera choisi par la Cour de révision de la protection des données pour défendre l'intérêt du plaignant dans cette affaire ;

  - (F) conserver une documentation appropriée de son examen de la plainte qualifiée et produire une décision classifiée expliquant la base de ses constatations factuelles, sa détermination quant à l'existence d'une violation couverte, et sa détermination de la réparation appropriée dans le cas où il y aurait eu une telle violation, conformément à son autorité statutaire et déléguée ;

   - (G) **préparer un compte rendu d'examen ex parte classifié, qui consistera en une documentation appropriée de son examen de la plainte qualifiée et de la décision classifiée décrite au paragraphe (c)(i)(F) de cette section** ; et

  - (H) fournir tout soutien nécessaire à la Cour de révision de la protection des données.


- (ii) Effet contraignant. Chaque élément de la Communauté du renseignement, et chaque agence contenant un élément de la Communauté du renseignement, doit se conformer à toute décision du CLPO de prendre des mesures correctives appropriées conformément au paragraphe (c)(i)(C) de cette section, sous réserve de toute décision contraire de la Cour de révision de la protection des données.

- (iii) Assistance. Chaque élément de la Communauté du Renseignement doit fournir à la CLPO l'accès aux informations nécessaires à la conduite des examens décrits dans le paragraphe (c)(i) de cette section, dans le respect de la protection des sources et méthodes de renseignement, et ne doit prendre aucune mesure visant à entraver ou à influencer de manière inappropriée les examens du CLPO. Les responsables de la protection de la vie privée et des libertés civiles au sein des éléments de la communauté du renseignement soutiendront également la CLPO dans le cadre des examens décrits au paragraphe (c)(i) de la présente section.

- (iv) Indépendance. **Le directeur n'interviendra pas dans l'examen par le CLPO d'une plainte qualifiée au titre de la sous-section (c)(i) de la présente section ; il ne pourra pas non plus révoquer le CLPO pour toute action entreprise en vertu de la présente ordonnance, sauf en cas de faute professionnelle, de malversation, d'atteinte à la sécurité, de manquement au devoir ou d'incapacité**.




### (d) Cour de révision de la protection des données.

- (i) Establishment. Le Procureur général (Attorney General) est autorisé à et doit établir une procédure de révision des décisions prises par le CLPO en vertu de la sous-section (c)(i) de cette section. Dans l'exercice de ce pouvoir, le Procureur général doit, dans un délai de 60 jours à compter de la date de cette ordonnance, **promulguer des règlements établissant une Cour de révision de la protection des données pour exercer le pouvoir du Procureur général de réviser ces décisions**. Ces règlements doivent, au minimum, prévoir que : 


  - (A) L'Attorney General (Le procureur général est nommé par le président des États-Unis après ratification par le Sénat : https://fr.wikipedia.org/wiki/Procureur_g%C3%A9n%C3%A9ral_des_%C3%89tats-Unis), en consultation avec le Secrétaire au Commerce, le Directeur et le PCLOB, **nommera des personnes pour servir en tant que juges au sein de la Cour d'examen de la protection des données**, qui seront des praticiens du droit ayant une expérience appropriée dans les domaines de la confidentialité des données et du droit de la sécurité nationale, en donnant du poids aux personnes ayant une expérience judiciaire antérieure, et qui ne seront pas, au moment de leur nomination initiale, des employés du gouvernement des États-Unis. Pendant la durée de leur nomination à la Cour de révision de la protection des données, ces juges n'auront aucune fonction officielle ni aucun emploi au sein du gouvernement des États-Unis autres que leurs fonctions officielles et leur emploi en tant que juges à la Cour de révision de la protection des données.

  - (B) Dès réception d'une demande de révision déposée par le plaignant ou un élément de la communauté du renseignement d'une détermination faite par le CLPO en vertu de la sous-section (c) de cette section, un panel de trois juges de la Cour de révision de la protection des données est convoqué pour examiner la demande. Pour faire partie du panel de la Cour de révision de la protection des données, le juge doit détenir les habilitations de sécurité requises pour accéder à des informations de sécurité nationale classifiées.

  - (C) Dès qu'il est convoqué, **le panel du Tribunal de révision de la protection des données choisit un avocat spécial** selon les procédures prescrites dans les règlements de l'Attorney General. L'avocat spécial assiste la commission dans l'examen de la demande de révision, notamment en défendant les intérêts du plaignant dans l'affaire et en veillant à ce que la commission du tribunal de révision de la protection des données soit bien informée des questions et du droit relatifs à l'affaire. Le service en tant qu'avocat spécial exige que l'avocat spécial détienne les habilitations de sécurité requises pour accéder aux informations classifiées relatives à la sécurité nationale et qu'il respecte les restrictions prescrites dans les règlements de l'Attorney General concernant les communications avec le plaignant afin de garantir la protection des informations classifiées ou autrement privilégiées ou protégées.

  - (D) La commission de la Cour de révision de la protection des données examine de manière impartiale les décisions prises par la CLPO concernant l'existence d'une violation couverte et les mesures correctives appropriées dans le cas où une telle violation a été commise. L'examen se fonde au minimum sur le dossier d'examen ex parte classifié décrit au paragraphe (c)(i)(F) de la présente section et sur les informations ou soumissions fournies par le plaignant, l'avocat spécial ou un élément de la communauté du renseignement. Lors de l'examen des décisions prises par le CLPO, le panel de la Cour de révision de la protection des données sera guidé par les décisions pertinentes de la Cour suprême des États-Unis, de la même manière que les tribunaux établis en vertu de l'article III de la Constitution des États-Unis, y compris les décisions concernant le respect approprié des décisions pertinentes des responsables de la sécurité nationale.

  - (E) Si la commission de la Cour de révision de la protection des données n'est pas d'accord avec l'une des décisions de la CLPO concernant l'existence d'une violation couverte ou la réparation appropriée dans le cas d'une telle violation, la commission rendra ses propres décisions.

  - (F) **Le panel de la Cour de révision de la protection des données fournit un rapport classifié sur les informations indiquant une violation de toute autorité soumise au contrôle du FISC** au procureur général adjoint pour la sécurité nationale, qui signale les violations au FISC conformément à ses règles de procédure.

  - (G) Une fois l'examen terminé, le CLPO est informé des décisions de la commission de la Cour de révision de la protection des données par le biais des procédures prescrites par les règlements de l'Attorney General.

  - (H) Une fois la révision achevée en réponse à la demande de révision d'un plaignant, la Cour de révision de la protection des données, par le biais des procédures prescrites par les règlements de l'Attorney General, informe le plaignant, par l'intermédiaire de l'autorité publique appropriée dans un État admissible et sans confirmer ou nier que le plaignant a fait l'objet d'activités de renseignement électromagnétique des États-Unis, que "la révision n'a pas identifié de violations couvertes ou que la Cour de révision de la protection des données a rendu une décision exigeant des mesures correctives appropriées".


- (ii) Effet contraignant. Chaque élément de la communauté du renseignement, et chaque agence contenant un élément de la communauté du renseignement, se conforme à toute décision d'un panel de la Cour de révision de la protection des données d'entreprendre des mesures correctives appropriées.

- (iii) Assistance. Chaque élément de la communauté du renseignement donne au CLPO l'accès aux informations nécessaires à la conduite de l'examen décrit au paragraphe (d)(i) de la présente section, dans le respect de la protection des sources et méthodes de renseignement, qu'une commission de la Cour de révision de la protection des données demande au CLPO, et ne prend aucune mesure visant à entraver ou à influencer indûment l'examen d'une commission.

- (iv) Indépendance. Le Procureur général n'interviendra pas dans l'examen par une commission du tribunal de révision de la protection des données d'une décision prise par le CLPO concernant une plainte admissible en vertu du paragraphe (c)(i) de la présente section ; Le Procureur général ne peut pas non plus révoquer les juges nommés conformément à la sous-section (d)(i)(A) de la présente section, **ni révoquer un juge d'une commission de la Cour de révision de la protection des données, sauf en cas d'inconduite, de malfaisance, de manquement à la sécurité, de négligence du devoir ou d'incapacité**, après avoir dûment pris en compte les normes des Règles de conduite judiciaire et des procédures d'incapacité judiciaire promulguées par la Conférence judiciaire des États-Unis conformément à la Loi sur la conduite judiciaire et l'incapacité (28 U. S.C. 351 et seq.).

- (v) Enregistrement des déterminations. Pour chaque plainte qualifiée transmise par l'autorité publique appropriée dans un État qualifié, le Secrétaire au commerce doit :

  - (A) conserver un dossier sur le plaignant qui a soumis cette plainte ;

  - (B) au plus tard 5 ans après la date de cet ordre et au moins tous les 5 ans par la suite, **contacter l'élément ou les éléments concernés de la Communauté du Renseignement pour savoir si les informations relatives à l'examen de cette plainte par le CLPO ont été déclassifiées et si les informations relatives à l'examen de toute demande de révision soumise à la Cour de Révision de la Protection des Données ont été déclassifiées**, y compris si un élément de la Communauté du Renseignement a déposé une demande de révision auprès de la Cour de Révision de la Protection des Données ; et

  - (C) **s'il est informé que ces informations ont été déclassifiées, il informe le plaignant, par l'intermédiaire de l'autorité publique appropriée dans un État admissible, que les informations relatives à l'examen de sa plainte par le CLPO ou à l'examen de toute demande de révision soumise à la Cour de révision de la protection des données peuvent être disponibles en vertu du droit applicable**.


### (e) Examen annuel par le PCLOB de la procédure de recours.

- (i) Nature de l'examen. Conformément à la législation applicable, la Commission de surveillance de la vie privée et des libertés civiles PCLOB est encouragé à procéder à un examen annuel du traitement des plaintes admissibles par le mécanisme de recours établi par la section 3 de la présente ordonnance, notamment pour déterminer si le CLPO et la Cour de révision de la protection des données ont traité les plaintes admissibles en temps opportun ; si le CLPO et la Cour de révision de la protection des données obtiennent un accès complet aux informations nécessaires ; si la CLPO et la Cour de révision de la protection des données agissent conformément à cet ordre ; si les garanties établies par la section 2 de cet ordre sont correctement prises en compte dans les processus de la CLPO et de la Cour de révision de la protection des données ; et si les éléments de la communauté du renseignement se sont pleinement conformés aux décisions prises par la CLPO et la Cour de révision de la protection des données.

- (ii) Assistance. Le Procureur général, le CLPO et les éléments de la Communauté du Renseignement fourniront au PCLOB l'accès aux informations nécessaires à la conduite de l'examen décrit au paragraphe (e)(i) de cette section, dans le respect de la protection des sources et méthodes de renseignement.

- (iii) Rapport et certification. Dans les 30 jours suivant la fin de l'examen décrit au paragraphe (e)(i) du présent article, le (e)(i) de cette section, le PCLOB est encouragé à :

  - (A) **fournir au président, au procureur général, au directeur, aux chefs des éléments de la communauté du renseignement, au CLPO et aux comités du renseignement du Congrès un rapport classifié détaillant les résultats de son examen** ;

  - (B) **diffuser au public une version non classifiée du rapport** ; et

  - (C) certifier publiquement chaque année que le mécanisme de recours établi conformément à la section 3 du présent décret traite les plaintes conformément au présent décret.

- (iv) Examen de la révision. Dans les 180 jours suivant la réception de tout rapport du PCLOB décrit au paragraphe (e)(iii)(A) de la présente section, le procureur général, le directeur, les chefs des éléments de la communauté du renseignement et le CLPO examineront attentivement et mettront en œuvre ou traiteront autrement toutes les recommandations contenues dans ce rapport, conformément à la loi applicable.



### (f) Désignation d'un Etat qualifié.

- (i) Pour mettre en œuvre le mécanisme de recours établi par la section 3 de cette ordonnance, le Procureur général est autorisé à désigner un pays ou une organisation d'intégration économique régionale en tant qu'État admissible aux fins du mécanisme de recours. (i) Pour mettre en œuvre le mécanisme de recours établi par la section 3 de la présente ordonnance, l'Attorney General est autorisé à désigner un pays ou une organisation d'intégration économique régionale comme État admissible aux fins du mécanisme de recours établi conformément à la section 3 de la présente ordonnance, avec effet immédiat ou à une date précisée par l'Attorney General, si ce dernier détermine, en consultation avec le Secrétaire d'État, le Secrétaire au Commerce et le Directeur, que :

  - (A) les lois du pays, de l'organisation régionale d'intégration économique ou des pays membres de l'organisation régionale d'intégration économique exigent des garanties appropriées dans la conduite des activités de renseignement électromagnétique pour les informations personnelles des ressortissants des États-Unis qui sont transférées des États-Unis vers le territoire du pays ou d'un pays membre de l'organisation régionale d'intégration économique ;

  - (B) **le pays, l'organisation d'intégration économique régionale ou les pays membres de l'organisation d'intégration économique régionale permettent, ou sont censés permettre, le transfert de renseignements personnels à des fins commerciales entre le territoire de ce pays ou de ces pays membres et le territoire des États-Unis** ; et

  - (C) **cette désignation servirait les intérêts nationaux des États-Unis**.

- (ii) Le Procureur général peut révoquer ou modifier une telle désignation, avec effet immédiat ou à une date spécifiée par le Procureur général, si le Procureur général détermine, en consultation avec le Secrétaire d'État, le Secrétaire au Commerce et le Directeur, que :

  - (A) le pays, l'organisation d'intégration économique régionale ou les pays membres de l'organisation d'intégration économique régionale ne fournissent pas de garanties appropriées dans la conduite des activités de renseignement électromagnétique pour les informations personnelles des ressortissants des États-Unis qui sont transférées des États-Unis vers le territoire du pays ou vers un pays membre de l'organisation d'intégration économique régionale ;

  - (B) le pays, l'organisation régionale d'intégration économique ou les pays membres de l'organisation régionale d'intégration économique ne permettent pas le transfert de renseignements personnels à des fins commerciales entre le territoire de ce pays ou de ces pays membres et le territoire des États-Unis ; ou

  - (C) cette désignation n'est pas dans l'intérêt national des États-Unis.




## Sec. 4. Définitions. Aux fins de la présente ordonnance :

### (a) "Mesures correctives appropriées" 
signifie des mesures légales conçues pour remédier pleinement à une violation couverte identifiée concernant un plaignant spécifique et limitées aux mesures conçues pour traiter la plainte de ce plaignant spécifique, en tenant compte des façons dont une violation du type identifié a été habituellement traitée. Ces mesures peuvent inclure, en fonction de la violation couverte spécifique en question, la correction, par des mesures administratives, des violations qui se sont avérées être des erreurs de procédure ou des erreurs techniques relatives à l'accès ou au traitement des données par ailleurs légaux, la cessation de l'acquisition de données lorsque la collecte n'est pas légalement autorisée, la suppression des données qui ont été acquises sans autorisation légale, supprimer les résultats d'interrogations inappropriées de données collectées par ailleurs légalement, restreindre l'accès aux données collectées légalement aux personnes ayant reçu une formation appropriée, ou rappeler les rapports de renseignement contenant des données acquises sans autorisation légale ou qui ont été diffusées d'une manière incompatible avec le droit des États-Unis. Les mesures correctives appropriées doivent être étroitement adaptées pour remédier à la violation couverte et pour minimiser les impacts négatifs sur les opérations de la Communauté du renseignement et la sécurité nationale des États-Unis.

### (b) " Collecte en vrac " 
signifie **la collecte autorisée de grandes quantités de données de renseignement électromagnétique qui, en raison de considérations techniques ou opérationnelles, sont acquises sans l'utilisation de discriminants** (par exemple, sans l'utilisation d'identifiants ou de termes de sélection spécifiques).

### (c) "Contre-espionnage" 
a le même sens que celui qui lui est donné dans l'Executive Order 12333.

### (d) "Violation couverte" 
signifie une violation qui :

- (i) découle d'activités de renseignement électromagnétique menées après la date du présent décret concernant des données transférées aux États-Unis depuis un État admissible après la date d'entrée en vigueur de la désignation de cet État par le procureur général, comme le prévoit la section 3(f)(i) du présent décret ;

- (ii) porte atteinte à la vie privée individuelle et aux libertés civiles du plaignant ; et

- (iii) viole un ou plusieurs des éléments suivants :

  - (A) la Constitution des États-Unis ;

  - (B) les sections applicables de la FISA ou toute procédure applicable approuvée par la FISC ;

  - (C) l'Executive Order 12333 ou toute procédure applicable de l'agence conformément à l'Executive Order 12333 ;

  - (D) le présent décret ou les politiques et procédures applicables de l'agence publiées ou mises à jour conformément au présent décret (ou les politiques et procédures identifiées à la section 2(c)(iv)(A) du présent décret avant qu'elles ne soient mises à jour conformément à la section 2(c)(iv)(B) du présent décret) ;

  - (E) toute loi, tout ordre, toute politique ou toute procédure qui succède à ceux identifiés à la section 4(d)(iii)(B)-(D) de la présente ordonnance ; ou

  - (F) toute autre loi, ordonnance, politique ou procédure adoptée après la date de la présente ordonnance qui fournit des garanties en matière de vie privée et de libertés civiles en ce qui concerne les activités de renseignement électromagnétique des États-Unis relevant de la présente ordonnance, telles qu'identifiées dans une liste publiée et mise à jour par l'Attorney General, en consultation avec le directeur du renseignement national.

### (e) "Renseignement étranger" 
a le même sens que celui qui lui est donné dans le décret 12333.

### (f) "Intelligence" 
a le même sens que celui qui lui est attribué dans l'Executive Order 12333.

### (g) Les termes "Intelligence Community" et "éléments de l'Intelligence Community" 
ont la même signification que dans l'Executive Order 12333.

### (h) "Sécurité nationale" 
a le même sens que celui qui lui est attribué dans l'Executive Order 13526 du 29 décembre 2009 (Classified National Security Information).

### (i) "Personne non américaine" 
désigne une personne qui n'est pas une personne américaine.

### (j) " Personnel des États-Unis ou de leurs alliés ou partenaires " : 
tout membre actuel ou ancien des forces armées des États-Unis, tout fonctionnaire actuel ou ancien du gouvernement des États-Unis, et toute autre personne actuellement ou anciennement employée par le gouvernement des États-Unis ou travaillant pour son compte, ainsi que tout membre actuel ou ancien des forces armées, tout fonctionnaire actuel ou ancien, ou toute autre personne actuellement ou anciennement employée par un allié ou un partenaire ou travaillant pour son compte.




### (k) "Plainte qualifiée" 
signifie une plainte, soumise par écrit, qui :

- (i) allègue qu'une violation couverte a eu lieu et qu'elle concerne des informations personnelles du plaignant, une personne physique, dont on peut raisonnablement penser qu'elles ont été transférées aux États-Unis depuis un État admissible après la date d'entrée en vigueur de la désignation du procureur général pour cet État, comme prévu à l'article 3(f)(i) de la présente ordonnance ;

- (ii) comprend les informations de base suivantes pour permettre un examen : les informations qui constituent la base de l'allégation selon laquelle une violation couverte a eu lieu, qui n'ont pas besoin de démontrer que les données du plaignant ont en fait été soumises à des activités de renseignement électromagnétique des États-Unis ; la nature de la réparation demandée ; les moyens spécifiques par lesquels les informations personnelles du plaignant ou concernant le plaignant étaient censées avoir été transmises aux États-Unis ; l'identité des entités du gouvernement des États-Unis censées être impliquées dans la violation présumée (si elle est connue) ; et toute autre mesure que le plaignant a prise pour obtenir la réparation demandée et la réponse reçue par le biais de ces autres mesures ;

- (iii) n'est pas frivole, vexatoire ou faite de mauvaise foi ;

- (iv) est présentée au nom du plaignant, agissant pour son propre compte, et non en tant que représentant d'une organisation gouvernementale, non gouvernementale ou intergouvernementale ; et

- (v) est transmise par l'autorité publique compétente d'un État admissible, après qu'elle a vérifié l'identité du plaignant et que la plainte satisfait aux conditions de la section 5(k)(i)-(iv) de la présente ordonnance.

### (l) "Incident significatif de non-conformité" :
 un manquement systémique ou intentionnel à un principe, une politique ou une procédure du droit américain applicable, qui pourrait porter atteinte à la réputation ou à l'intégrité d'un élément de la Communauté du renseignement ou remettre en question le bien-fondé d'une activité de la Communauté du renseignement, y compris à la lumière de tout impact significatif sur les intérêts de la ou des personnes concernées en matière de vie privée et de libertés civiles.

### (m) "Personne des États-Unis" 
a le même sens que celui qui lui est donné dans l'Executive Order 12333. 

### (n) "Priorité validée en matière de renseignement" : 
pour la plupart des activités de collecte de renseignements d'origine électromagnétique des États-Unis, une priorité validée selon le processus décrit à la section 2(b)(iii) du présent décret ; ou, dans des circonstances étroites (par exemple, lorsqu'un tel processus ne peut être mis en œuvre en raison de la nécessité de répondre à un besoin de renseignement nouveau ou en évolution), une priorité fixée par le président ou le chef d'un élément de la communauté du renseignement conformément aux critères décrits à la section 2(b)(iii)(A)(1)-(3) du présent décret, dans la mesure du possible. (o) "Armes de destruction massive" a le même sens que celui qu'il a dans l'Executive Order 13526.




## Sec. 5. Dispositions générales.

- (a) Aucune disposition du présent arrêté ne doit être interprétée comme portant atteinte ou affectant d'une autre manière :

  - (i) l'autorité accordée par la loi à un département exécutif, à une agence, ou à son chef ; ou

  - (ii) les fonctions du directeur de l'Office of Management and Budget relatives aux propositions budgétaires, administratives ou législatives.

- (b) La présente ordonnance est mise en œuvre conformément au droit applicable, y compris les ordonnances et les procédures approuvées par le FISC, et sous réserve de la disponibilité des crédits.

- (c) Rien dans cette ordonnance n'empêche l'application de garanties plus protectrices de la vie privée pour les activités de renseignement électromagnétique des États-Unis qui s'appliqueraient en l'absence de cette ordonnance. En cas de conflit entre le présent décret et une autre loi applicable, les mesures de protection de la vie privée les plus strictes régiront la conduite des activités de renseignement électromagnétique, dans toute la mesure permise par la loi.

- (d) Aucune disposition de la présente ordonnance n'interdit aux éléments de la Communauté du renseignement de diffuser des informations relatives à un crime à des fins d'application de la loi ; de diffuser des avertissements concernant des menaces de meurtre, de blessure corporelle grave ou d'enlèvement ; de diffuser des informations sur les cybermenaces, les incidents ou les intrusions ; d'informer les victimes ou d'avertir les victimes potentielles d'un crime ; ou de se conformer aux obligations de diffusion requises par la loi, un traité ou une ordonnance judiciaire, y compris les ordonnances et les procédures approuvées par le FISC ou d'autres ordonnances judiciaires.

- (e) **La collecte, la conservation et la diffusion d'informations concernant des personnes des États-Unis sont régies par de multiples exigences juridiques et politiques, telles que celles requises par la FISA et l'Executive Order 12333. Cette ordonnance ne vise pas à modifier les règles applicables aux personnes des États-Unis adoptées en vertu de la FISA, de l'Executive Order 12333 ou de toute autre loi applicable**.

- (f) Cet ordre s'applique aux activités de renseignement électromagnétique conformément à la portée de l'application de la PPD-28 à ces activités avant la révocation partielle de la PPD-28 par le mémorandum de sécurité nationale publié en même temps que cet ordre. Pour mettre en œuvre cette sous-section, le chef de chaque agence contenant un élément de la communauté du renseignement, en consultation avec le procureur général et le directeur, se voit par la présente déléguer le pouvoir d'émettre des directives, qui peuvent être classifiées, le cas échéant, quant au champ d'application de cet ordre en ce qui concerne l'élément ou les éléments de la communauté du renseignement au sein de leur agence. Le CLPO et la Cour de révision de la protection des données, dans l'exercice des fonctions qui leur sont attribuées en vertu de la présente ordonnance, considèrent ces orientations comme faisant autorité et ayant force obligatoire.

- (g) Aucune disposition du présent ordre ne confère le pouvoir de déclassifier ou de divulguer des informations classifiées relatives à la sécurité nationale, sauf si cela est autorisé en vertu de l'Executive Order 13526 ou de tout autre ordre qui lui succéderait. Conformément aux exigences de l'Executive Order 13526, le CLPO, la Cour de révision de la protection des données et les avocats spéciaux ne sont pas habilités à déclassifier des informations classifiées relatives à la sécurité nationale, ni à divulguer des informations classifiées ou autrement privilégiées ou protégées, sauf aux personnes autorisées et dûment habilitées qui ont besoin de connaître ces informations.

- (h) Cette ordonnance crée le droit de soumettre des plaintes qualifiées à la CLPO et d'obtenir le réexamen des décisions de la CLPO par la Cour de révision de la protection des données conformément au mécanisme de recours établi dans la section 3 de cette ordonnance. Cette ordonnance n'est pas destinée à, et ne crée pas, d'autres droits ou avantages, substantiels ou procéduraux, applicables en droit ou en équité par toute partie contre les États-Unis, ses départements, agences ou entités, ses dirigeants, employés ou agents, ou toute autre personne. Cette ordonnance n'est pas destinée à, et ne modifie pas, la disponibilité ou la portée de tout contrôle judiciaire des décisions rendues par le mécanisme de recours, qui est régi par le droit existant.
