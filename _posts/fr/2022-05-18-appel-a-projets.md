---
layout: post
title: "Le HealthDataHub fait son appel à donner"
categories:
  - HealthDataHub
  - Microsoft
  - AAP
ref: eds-appel-a-projets
lang: fr
---

Faute de donnée le Health Data Hub change de stratégie : il lance un appel à projets AAP "à la constitution d’entrepôts de données de santé hospitaliers". C'est un appel à données provenant des "établissements de santé publics et privés".

<!-- more -->

## Une centralisation à marche forcée !

Bloqué par son absence d'autorisation CNIL, la Plateforme Nationale des Données de Santé va ainsi débloquer 50 millions d'euros[^aap_50] via la Banque Publique d'Investissement (BPI) pour la constitution d'entrepôts locaux ayant plus tard l'obligation de lui envoyer les données collectés.

[^aap_50]: [50M d’euros pour la constitution d’entrepôts de données de santé hospitaliers](https://www.health-data-hub.fr/actualites/50m-deuros-pour-la-constitution-dentrepots-de-donnees-de-sante-hospitaliers)

Ainsi lit-on dans l'appel à projets que InterHop s'est procuré :
> "Le premier versement des financements est conditionné à la signature de la convention de collaboration au réseau des EDS et de partage de données avec le Health Data Hub, et le dernier versement à l'exécution de cette convention."

Le candidat doit aussi s'engager, dès sa sélection, à : 
> "Identification d’un périmètre de données ayant vocation à être partagé au niveau national par le biais du HDH et justification de sa pertinence (qualité, potentiel de réutilisation et croisement avec les données de la base principale du SNDS) ;"

En janvier 2022[^twitter] l'association InterHop a révélé que la demande d'autorisation CNIL, pourtant obligatoire pour faire fonctionner tout "Entrepôt de Données de Santé"[^eds], avait finalement été retirée à la demande du Ministère de la Santé lui-même. Cette autorisatiion est nécessaire pour ajouter toute nouvelle base de données au catalogue du HDH (fixé par arrêté).

[^twitter]: [https://twitter.com/interchu/status/1479455213270585352](https://twitter.com/interchu/status/1479455213270585352)

[^eds]: [Traitements de données de santé : comment faire la distinction entre un entrepôt et une recherche et quelles conséquences ?](https://www.cnil.fr/fr/traitements-de-donnees-de-sante-comment-faire-la-distinction-entre-un-entrepot-et-une-recherche-et)

[^ref_eds_cnil]: [La CNIL adopte un référentiel sur les entrepôts de données de santé](https://www.cnil.fr/fr/la-cnil-adopte-un-referentiel-sur-les-entrepots-de-donnees-de-sante)

En fin tacticien le gouvernement a jugé que la protection des données de santé était un sujet à risque politique. La demande d’autorisation a donc été enlevée, et cela sûrement jusqu’aux prochaines élections législatives.
Cette autorisation est pourtant obligatoire au lancement du HDH[^validation_cnil_eds]. Attendons-nous les résultats des législatives pour voir reprendre le fonctionnement du HDH ? Est-il légitime de bloquer la recherche pour des problématiques électorales ?

[^catalogue_hdh]: [https://www.health-data-hub.fr/catalogue-de-donnees](https://www.health-data-hub.fr/catalogue-de-donnees)

[^validation_cnil_eds]: [La Plateforme des données de santé (Health Data Hub)](https://www.cnil.fr/fr/la-plateforme-des-donnees-de-sante-health-data-hub)

## De quelle interopérabilité parlons-nous ?
L'appel à donner évoque la question de l’interopérabilité. 
> “Etre ou viser à être conforme aux standards d'interopérabilité de la recherche permettant la mise en commun de ces données avec d'autres EDS ou bases en fonction des opportunités d’ici la fin de l’accompagnement de l’AAP, pour passage à l’échelle vers la donnée de santé massive ; “

InterHop promeut ce sujet depuis plusieurs années. Nous ne pouvons que rejoindre le HDH qui lui réserve une place centrale. 
Mais le diable se cache dans les détails ! Dernièrement la Délégation du Numérique en Santé s’est prononcée en faveur de  l'achat d’une terminologie nommée snomed[^snomed]. Sa licence d’exploration est excessivement contraignante. Surtout sa gouvernance ne laisse pas voix au chapitre pour la France. Être prisonnier dans l'utilisation des termes médeciaux est un risque qu'il ne faut pas prendre. D’autre terminologies opensources doivent donc être promues. L'OMS (et donc la France) maintient l'ICD10 et l'ICD11 ; celles-ci  doivent être priviligiées. Quels types d'interopérabilité seront donc mises en avant ?

[^snomed]: [Informatique de santé: la France adopte la terminologie Snomed CT (DNS)](https://www.ticsante.com/story?ID=6039)

## On oublie la confiance dans le cloud

Enfin et surtout, dans son appel à projets, le HDH préconise de :
> "Privilégier l’utilisation de solutions Cloud de confiance[^circulaire] (nationales/européennes) lorsque disponibles et, à défaut, d’autres solutions Cloud"

[^circulaire]: [Circulaire n° 6282-SG du 5 juillet 2021 relative à la doctrine d’utilisation de l’informatique en nuage par l’État](https://www.legifrance.gouv.fr/circulaire/id/45205)

[^cloud_confiance_interhop]: [Vers un Cloud de Confiance ?](https://interhop.org/2021/05/20/analyse-cloud-confiance)

Le cloud de confiance devait réunir le meilleur de deux mondes[^cloud_confiance_interhop]. Il permettrait d'utiliser les technologies propriétaires américaines (étant faussement admisses comme toujours meilleures) mais sans les risques liés à l’hébergement des données de santé chez des acteurs dépendant des lois extra-territoriales américaines. Nous rappelons que ce risque est pointé conjointement pas la Cour de justice de l’Union européenne[^cjue] (CJUE), le Conseil d’Etat[^ce], la CNIL[^memoire_cnil] et la CNAM[^cnam].
Problème : le cloud de confiance n’a pour le moment aucun existence réelle, il reste du domaine de l'idée. La société Bleue devant naitre de l’union de Microsoft, Orange Business Services et Capgemini et devant permettre sa concrétisation, n’existe pas (pas même  un simple site internet). Elle ne devrait pas voir le jour avant 2023.
Pourtant le ministère de la santé s’est engagé devant la présidence de la CNIL a éliminer le risque extra-terriorial avant novembre 2022[^cnil_veran] !

[^cnil_veran]: [La Plateforme des données de santé (Health Data Hub)](https://www.cnil.fr/en/node/120008)
[^cjue]: [La Cour invalide la décision 2016/1250 relative à l'adéquation de la protection assurée par le bouclier de protection des données UE-États-Unis](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

[^memoire_cnil]: [https://cnll.fr/documents/35/OBSERVATIONS_DE_LA_CNIL_8_OCTOBRE_2020.pdf](https://cnll.fr/documents/35/OBSERVATIONS_DE_LA_CNIL_8_OCTOBRE_2020.pdf)

[^ce]: [Saisi en référé par le collectif SantéNathon, le Conseil d’Etat reconnait que le gouvernement américain peut accéder sans contrôle aux données de santé des Français hébergées par le Health Data Hub chez Microsoft](https://interhop.org/2020/10/14/communique-presse-conseil-detat-reconnait-acces-donnes)

[^cnam]: [Avis du Conseil de la CNAM sur les travaux menés par la Commission mixte (COR et CSITN) sur les données de santé](https://interhop.org/2022/02/04/avis-cnam-commission-mixte)

La pression est d’autant plus forte qu’après la décision de la plus haute juridiction de l’union européenne (CJUE) d’annuler les transferts de données transatlantiques plusieurs états membres de l’union commencent l'application in concreto de cette jurisprudence. Sur cette base ont donc été condamné l'utilisation de Google Analytics[^GA_cnil], Google Fonts[^GF], Cloudflare[^cloudflare] et Mailchimp[^mailchimp]. 

[^GA_cnil]: [Utilisation de Google Analytics et transferts de données vers les États-Unis : la CNIL met en demeure un gestionnaire de site web](https://www.cnil.fr/fr/utilisation-de-google-analytics-et-transferts-de-donnees-vers-les-etats-unis-la-cnil-met-en-demeure)

[^cloudflare]: [Le sort des flux de données européennes vers les Etats-Unis est toujours en suspens ](https://www.usine-digitale.fr/editorial/le-sort-des-flux-de-donnees-europeennes-vers-les-etats-unis-est-toujours-en-suspens.N1172817)

[^GF]: [Un tribunal allemand condamne l’intégration de Google Fonts dans un site](https://www.zindex.fr/un-tribunal-allemand-condamne-lintegration-de-google-fonts-dans-un-site/)

[^mailchimp]: [ RGPD : Mailchimp retoqué par une CNIL allemande ](https://www.zdnet.fr/actualites/rgpd-mailchimp-retoque-par-une-cnil-allemande-39920021.htm)

Adjoindre à cet appel à projets, l’obligation de centralisation de l’ensemble des données de santé chez Microsoft Azure est donc particulièrement intentatoire au libertés fondamentales et contraire à la récente jurisprudence de l'Union Européenne.

Interhop alerte :
- Tant que le HDH est hébergé chez Microsoft Azure, il est impossible de conditionner la validation des candidatures à la centralisation des données de santé au HDH (et de quelque façon que ce soit). 
- Il est impossible que les nouveaux entrepôts ainsi créés soient hébergés par des entités assujetties aux lois américaines. D’ailleurs ceci ne serait pas conforme aux dernières recommandations de la CNIL sur les entrepôts de donnée[^ref_eds_cnil].
