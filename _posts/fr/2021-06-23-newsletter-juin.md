---
layout: post
title: "L'ouverture des codes source et plainte CNIL"
categories:
  - Newsletter
  - Juin
ref: newsletter-juin-2021
lang: fr
---

Dans cette nouvelle newsletter, nous abordons les point suivants : [Goupile](https://goupile.fr/), l'ouverture des codes source et de notre plainte contre Iqvia.

<!-- more -->

# Goupile, le premier e-CRF libre et opensource 

[InterHop](https://interhop.org/projets/) lance [Goupile](https://goupile.fr/), éditeur de formulaires facile d’utilisation sur ordinateur, sur mobile et hors ligne pour le recueil de données. Des structures (Groupement d'intérêt Public, Etablissement Public de Santé mentale, CHU et groupement d'anesthésistes) ainsi que des professionnel.le.s de santé s'appuient sur Goupile pour réaliser leurs travaux de recherche. InterHop est également en capacité de fournir un hébergement HDS aux utilisateur.rice.s.
Un tutoriel concernant l'utilisation de Goupile verra prochainement le jour.
[Pour en savoir plus...](https://pad.interhop.org/47hV-ZTDSBKvhv0QRjNPRQ)

# Pour la publication du code source en santé

Le Conseil National du Logiciel Libre (CNLL) le 4 janvier dernier, a demandé la transmission de “l’intégralité des codes sources de la plateforme Health Data Hub.” La demande a été refusée, la raison avancée étant : l'exception : “sécurité des systèmes d’information des administrations”.

Rappelons que le Health Data Hub, qui vise à centraliser l’ensemble des données de santé des français, est hébergé par une entreprise américaine soumise au Cloud Act. Le Cloud Act, loi fédérale américaine promulguée le 23 mars 2018, permet aux forces de l'ordre ou aux agences de renseignement américaines d’obtenir des informations stockées sur leurs serveurs quel que soit l'endroit où ces données dites sensibles sont situées (États-Unis ou étranger).
 
Pourtant, dans un récent rapport, le député Éric Bothorel rappelle que : “De nombreux acteurs publics mettent en avant l’argument de la sécurité informatique pour ne pas publier les données. [...] En réalité, les acteurs faisant valoir la sécurité des systèmes d’information semblent méconnaître la possibilité de renforcer leur résilience offerte par la démarche d’ouverture des codes sources”. Dans ce rapport, "la mauvaise foi" des administrations était pointée. 

Le partage libre des connaissances informatiques améliore la santé : on sait que l’open source et l’intelligence collective  augmentent la sécurité les projets informatiques.

En fait “[La sécurité ne réside pas dans le fait de cacher la porte d’entrée, mais dans la robustesse de la clé](https://interhop.org/2021/05/10/codes-sources-entre-ouverts)”.

Le code source du Health Data Hub comme celui des algorithmes de soins doit être libre!

Par ailleurs, un nouveau label voit le jour  : le cloud de confiance qui vise à proposer des services plus respectueux des exigences techniques (sécurité) tout en se protégeant des risques juridiques liés à l’application de lois extra-européennes, de l’impact d’une crise géopolitique...

Le [Health Data Hub](https://interhop.org/2021/05/20/analyse-cloud-confiance) devrait quitter Microsoft pour rejoindre un des hébergeurs labellisés "Cloud de confiance".

# Conservation des métadonnées

Tout d’abord, pour comprendre...
Les métadonnées sont des données qui renseignent sur la nature de certaines autres données et qui permettent notamment de comprendre comment les données sont structurées, de savoir comment y avoir accès et comment les interpréter.
Elles sont utilisées dans presque tous les domaines. Elles permettent à votre Fournisseurs d’Accès Internet de définir votre portrait, aux annonceurs et aux gouvernements de créer votre plus fidèle profil à des fins publicitaires et de surveillance.
Le Conseil d'Etat saisi par plusieurs organisations (La Quadrature du Net, French Data Network, Igwan.net, la Fédération des fournisseurs d'accès à Internet associatifs et l’opérateur Free) a rendu sa décision le 21 avril dernier pour stopper le stockage des métadonnées de connexion. Les opérateurs télécoms continueront de conserver les données  pour la préservation de la sécurité nationale :  "L'état des menaces pesant sur la sécurité nationale (…) justifie légalement que soit imposée aux opérateurs la conservation générale et indifférenciée des données de connexion". Moins de 48h plus tard, [la Cour constitutionnelle belge](https://www.const-court.be/public/f/2021/2021-057f-info.pdf
) annule l’obligation de conservation généralisée et indifférenciée des données relatives aux communications électroniques

# Tout ce qu'on a oublié de vous dire quand vous entrez dans votre pharmacie 

Le 20 mai dernier, [Cash investigation](https://peertube.interhop.org/w/efFi5JvzWh5jMsk7jttGbD), magazine d'enquête et d'information qui se penche sur un sujet proche des préoccupations de Français, révélait comment IQVIA, société américaine, siphonne une partie de nos données de santé.
Suite à ce reportage, la Fédération SUD Santé Sociaux, l’association AIDES, l’association Nothing2Hide, l’association Actions Traitement, la Ligue des Droits de l’Homme, le Syndicat de la Médecine Générale, l'Union Française pour une Médecine Libre et l'association InterHop ont déposé un [signalement auprès l'autorité française de régulation](https://interhop.org/2021/05/27/signalement-cnil-iqvia) (CNIL) pour qu'elle éclaircisse certains points :
- évaluation du respect du droit à l'information du patient;
- évaluation du respect du recueil de consentement du patient sur le traitement de ses données quand il achète des produits en pharmacie;
- manquement potentiel dans la distinction entre « les traitements destinés à des fins d’intérêt public » dans le cadre de l’entrepôt de données de santé LRX et entre « les traitements à des fins commerciales » dans le cadre de Pharmastat;
- évaluation du respect à la portabilité des données
- évaluation des risques liés à l’hébergement des données de santé auprès d’un entrepôt soumis au droit américain.
