---
layout: post
title: "LibreDataHub.org :  Plateforme libre de sciences de la données"
categories:
  - DataThon2024
  - LibreDataHub
ref: libredatahub-blog
lang: fr
---

> InterHop est fière d'annoncer le lancement de [LibreDataHub.org](https://libredatahub.org){:target="_blank"} <br>
> LibreDataHub est une plateforme ubiquitaire, opensource et sécurisée d'analyse des données.

<!-- more -->


LibreDataHub est un méta-projet qui intègre plusieurs projets libres dans une solution pratique, sécurisée, et clé en main.

LibreDataHub s'installe facilement sur un serveur Linux.


# Présentation

Avec l'organisation du [datathon](https://interhop.org/projets/datathon), InterHop est fière d'annoncer le lancement de son tout nouveau projet : [LibreDataHub.org](https://libredatahub.org){:target="_blank"} 

LibreDataHub fournit des outils libres pour le stockage de données, l'IA décentralisée, les statistiques, l'apprentissage machine (ML) et le Deep Learning.

LibreDataHub est un méta-projet informatique, scalable, modulaire, opensource et collaboratif. LibreDataHub est assemblée par les experts en traitement des données de santé de l'association InterHop.

LibreDataHub est librement installable sur une machine Linux (Debian par exemple).

LibreDataHub est un projet dévoppé et maintenu par l'association InterHop.



# Technologies utilisées

### Schéma d'architecture



![](https://pad.interhop.org/uploads/e4b99716-5528-4555-9821-278d232936a7.png)



### Shiny Proxy


![](https://pad.interhop.org/uploads/e7a1b1e3-d2dd-4cfc-8c69-485a68c8f145.png)


[Shiny Proxy](https://www.shinyproxy.io/){:target="_blank"} est une application java permmetant de déployer des applications. Les ressources (CPU et RAM) sont partagées entre les utilisateur·trices.

Le principe : l'utilisateur se connecte à Shinyproxy, il clique sur l'application qu'il souhaite exécuter, un container docker de l'application se lance et celle-ci s'affiche dans le browser de l'utilisateur. 
Les applications suivantes sont installées au sein du Shiny Proxy de LibreDataHub :
- Metabase
- LinkR
- Jupyter Notebook
- RStudio
- CloudBeaver
- SchemaSpy

Nous remercions particulièrement les contributeurs du projet Shiny Proxy (et donc l'équipe de [openanalytics](https://www.openanalytics.eu/){:target="_blank"}) qui ont donc rendus possible la création de LibreDataHub. 


### Metabase

[Metabase](https://www.metabase.com/){:target="_blank"} est un outil polyvalent de Business Intelligence (BI) pour la visualisation des données, adapté à un large éventail d’utilisateurs, pour l’exploration, l’analyse de données et l’aide à la décision.

![](https://pad.interhop.org/uploads/fbbeb470-6c85-4ebf-9a2a-53148093a70a.png)


### LinkR

Au cœur de LibreDataHub se trouve [LinkR](https://linkr.interhop.org){:target="_blank"}, une application web open-source développée par InterHop. 

LinkR permet aux utilisateurs d'accéder, de manipuler et d'analyser des données de santé avec des outils low-code, c'est-à-dire sans avoir besoin de connaissances approfondies en programmation. LinkR utilise le modèle commun de données [OMOP](https://ohdsi.github.io/TheBookOfOhdsi/){:target="_blank"} pour faciliter l'échange de code entre plusieurs centres.

Il fournit à la fois une interface graphique pour les cliniciens et un environnement de programmation pour les scientifiques des données, ce qui en fait un outil idéal pour les projets collaboratifs dans le domaine de la santé.

![](https://pad.interhop.org/uploads/64b64fb7-556c-4f6e-9d02-b42cedd2c788.png)



### Jupyter


[Jupyter](https://jupyter.org/){:target="_blank"} est une application web utilisée pour programmer dans plus de 40 langages de programmation, dont Python, Julia, Ruby, R, ou encore Scala. Jupyter permet de réaliser des calepins ou notebooks, c'est-à-dire des programmes contenant à la fois du texte en markdown et du code en Julia, Python, R... 
Ces calepins de code sont utilisés en science des données pour explorer et analyser des données.

![](https://pad.interhop.org/uploads/0db2ec40-7ebb-4c53-ab97-0b32ce3ec91a.png)


###  CloudBeaver

[CloudBeaver](https://dbeaver.com/){:target="_blank"} est une application web légère permettant de travailler avec différents types de bases de données, le tout à travers une solution cloud unique et sécurisée accessible via un navigateur.

![](https://pad.interhop.org/uploads/a1e16eed-729b-4238-9fb1-c1b48c677fec.png)


### Schema Spy

[SchemaSpy](https://schemaspy.org/){:target="_blank"} est conçu pour simplifier la compréhension et la documentation des schémas de base de données. Il génère des rapports détaillés et des diagrammes intéractifs de la structure de la base de données, aidant les analystes et les chercheur·euses à naviguer facilement dans des bases de données complexes. Cet outil est particulièrement utile pour comprendre les relations entre les différentes tables de données et garantir l'intégrité des données.

![](https://pad.interhop.org/uploads/a5c4dd38-c22c-4032-987a-4225edb63fa7.png)

### Grafana

Pour la visualisation des données et la surveillance en temps réel, LibreDataHub intègre Grafana, une application web open-source  qui permet aux utilisateurs de créer des tableaux de bord dynamiques et personnalisables pour visualiser les données. 
LibreDataHub utilise Graphana pour monitorer son infrastructure technique.

Voici le résultat d'un dashboard recueilli durant le datathon.
![](https://pad.interhop.org/uploads/0d971430-8f05-472d-a29a-1b07dc1b5fa7.png)

### DuckDB

LibreDataHub intègre également [DuckDB](https://duckdb.org/), un système de gestion de base de données opensource conçu pour les analyses de données. 

Sa prise en charge des formats de stockage en colonnes tels que Parquet permet une intégration transparente avec d'autres composants de LibreDataHub, tels que les Jupyter Notebooks ou LinkR, permettant des requêtes de données de haute performance directement dans l'environnement de recherche. 

# Développement

[LibreDataHub.org](https://libredatahub.org){:target="_blank"} est un projet porté par l'association InterHop.

Le code source de la plateforme LibreDataHub est disponible en ligne : [https://framagit.org/interhop/libre-data-hub](https://framagit.org/interhop/libre-data-hub/datathon)

Toutes propositions d'amélioration ou signalement de problèmes sont les bienvenues : [https://framagit.org/groups/interhop/libre-data-hub/-/issues](https://framagit.org/groups/interhop/libre-data-hub/-/issues)

L'équipe d'InterHop est en support pour héberger LibreDataHub sur des serveurs HDS, pour les retours utilisateurs au sein de réseau de l'association ainsi que pour une aide en lien avec le RGPD.
