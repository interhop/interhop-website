---
layout: post
title: "Pétition adressée au Sénat"
categories:
  - HDH
  - Microsoft
  - Santé
  - Europe
  - Privacy Shield
  - RGPD
redirect_from:
  - /petition-senat
ref: petition-senat
lang: fr
---

L'affaire Snowden a révélé au monde entier l'utilisation massive de nos données informatiques au travers de programmes de surveillance globalisés[^snowden_europ].

De façon tout aussi brutale, le confinement a fait vivre à chacun le poids de privations de liberté imposées en raison d'un évènement sanitaire mondial.

Si, selon un principe éthique fondamental qui veut que “les technologies doivent être au service de la personne et de la société” plutôt qu’“asservies par les géants technologiques”[^CNOM_IA], la confiance aveugle dans la technologie comporte des risques déterminants.

En effet, l'utilisation sauvage et sans moyen de contrôle effectif de ces nouveaux outils statistiques pourrait conduire à légitimer des systèmes anti-démocratiques et réducteurs de libertés.


<!-- more -->

Le gouvernement français déploie depuis novembre 2019 la Plateforme de Données de Santé[^theconversation_Fallery] (ou Health Data Hub) pour développer l’intelligence artificielle appliquée à la santé. C'est un guichet unique d’accès à l’ensemble des données de santé.
Les données concernées sont celles des centres hospitaliers, des pharmacies, du dossier médical partagé et les données de recherche issues de divers registres. La quantité de données hébergées est amenée à exploser, notamment avec l’émergence de la génomique, de l’imagerie et des objets connectés. Il est prévu que l'ensemble des données de santé des Français soient stockées chez Microsoft Azure[^HDH_microsoft], cloud public du géant américain Microsoft. 

Le refus citoyen d'abdiquer ce choix d'avoir recours à Microsoft motive [cette pétition au Sénat](https://petitions.senat.fr/initiatives/i-455). 

En tant que citoyen.ne.s, nous voulons réaffirmer notre autonomie numérique et créer des Communs pour le futur de notre santé.

Le problème est que le droit américain s’applique au monde entier !

Ainsi le CLOUDAct[^cloudact] (Clarifying Lawful Overseas Use of Data Act) permet à la justice américaine de récupérer les données stockées sur des serveurs appartenant à des sociétés américaines, même s’ils sont situés en Europe[^CCBE_cloud_act]. Microsoft est soumis à ce texte qui est en conflit avec notre règlement européen sur la protection des données (RGPD)[^CNIL_denis].
Pire encore, s’agissant des programmes de surveillance américains, les textes internationaux “ne [font] ressortir d’aucune manière l’existence de limitations à l’habilitation qu’[ils] comportent pour la mise en œuvre de ces programmes, pas plus que l’existence de garanties pour des personnes non américaines potentiellement visées”. La Cour de Justice de l'Union Européenne a ainsi ouvert la brèche en bloquant légalement l'échange des données entre l'Union Européenne et les États-Unis par l'invalidation cet été d'un accord nommé "Privacy Shield".

Comment soutenir le choix de l'entreprise Microsoft alors que le Président de l’Agence Nationale de Sécurité des Systèmes d’Information lui-même s’oppose publiquement aux géants du numérique qui représenteraient une attaque pour nos systèmes de “santé mutualiste”[^ANSSI] ?

Comment soutenir ce choix alors que la CNIL, autorité de contrôle gardienne des libertés numériques mentionne dans le contrat liant la Plateforme des Données de Santé à Microsoft “l’existence de transferts de données en dehors de l’Union Européenne dans le cadre du fonctionnement courant de la plateforme”[^CNIL_microsoft] ?

Comment soutenir ce choix alors que la CNIL précise que les clés de chiffrement de ces données seront confiées à Microsoft, rendant ainsi les données stockées vulnérables à d'éventuelles ingérences[^CNIL_microsoft] ?

Comment soutenir ce choix alors qu’existent des dizaines d’alternatives  françaises et européennes, industrielles et institutionnelles[^HDS] ?

Cette Plateforme centralisée chez un acteur non européen n’est ni nécessaire, ni proportionnée, ni adaptée. 
Elle porte une atteinte grave et sûrement irréversible aux droits de 67 millions d’habitants de disposer de la protection de leur vie privée, notamment celle de leurs données parmi les plus intimes, protégées de façon absolue par le secret médical : leurs données de santé.

[En signant cette pétition](https://petitions.senat.fr/initiatives/i-455), vous demandez au Sénat la création d'une commission d'enquête[^proposition_com_enq] sur la protection des données de santé. 
Celle-ci devra examiner les conditions de passation d'un accord confiant la gestion des données de santé française à la société Microsoft. 
Elle devra élaborer des préconisations pour renforcer l'autonomie numérique et pour assurer une gestion plus sûre des données de santé de notre système de santé et de nos concitoyen.ne.s.

Voici le lien pour signer : [https://petitions.senat.fr/initiatives/i-455](https://petitions.senat.fr/initiatives/i-455)

[^theconversation_Fallery]: [Données de santé : l’arbre StopCovid qui cache la forêt Health Data Hub](https://theconversation.com/donnees-de-sante-larbre-stopcovid-qui-cache-la-foret-health-data-hub-138852)

[^HDH_microsoft]: [Modalités de stockage du « health data hub »](https://www.senat.fr/basile/visio.do?id=qSEQ200114130&idtable=q371194%7Cq370883%7Cq370105%7Cq369641%7Cq368627%7Cq371617%7Cq371538%7Cq371754%7Cq371099%7Cq371232&_c=recherche&rch=qs&de=20170205&au=20200205&dp=3+ans&radio=dp&aff=sep&tri=dd&off=0&afd=ppr&afd=ppl&afd=pjl&afd=cvn)

[^snowden_europ]: [Trente-cinq chefs d'État étaient sous écoute de la NSA](https://www.lepoint.fr/monde/trente-cinq-chefs-d-etat-etaient-sous-ecoute-de-la-nsa-24-10-2013-1747689_24.php)

[^CNOM_IA]: [Médecins et Patients dans le monde des data, des algorithmes et de l'intelligence artificielle](https://www.conseil-national.medecin.fr/sites/default/files/external-package/edition/od6gnt/cnomdata_algorithmes_ia_0.pdf)

[^cloudact]: [Rapport Gauvain : Rétablir la souveraineté de la France et de l’Europe et protéger nos entreprises des lois et mesures à portée extraterritoriale](https://www.vie-publique.fr/sites/default/files/rapport/pdf/194000532.pdf)

[^CCBE_cloud_act]: [Évaluation du CCBE de la loi CLOUD Act des États-Unis](https://www.ccbe.eu/fileadmin/speciality_distribution/public/documents/SURVEILLANCE/SVL_Position_papers/FR_SVL_20190228_CCBE-Assessment-of-the-U-S-CLOUD-Act.pdf)


[^CNIL_denis]: [Commission spéciale Bioéthique : Auditions diverses, Mme DENIS](https://videos.assemblee-nationale.fr/video.8070927_5d6f64b41f5fe.commission-speciale-bioethique--auditions-diverses-4-septembre-2019?timecode=15077543)

[^ANSSI]: [Audition de M. Guillaume Poupard, directeur général de l'Agence nationale de la sécurité des systèmes d'information (ANSSI)](https://videos.senat.fr/video.1608918_5ebaa04d6b8cd.audition-de-m-guillaume-poupard-directeur-general-de-l-agence-nationale-de-la-securite-des-systeme?timecode=4319000)

[^CNIL_microsoft]: [Délibération n° 2020-044 du 20 avril 2020 portant avis sur un projet d'arrêté complétant l’arrêté du 23 mars 2020 prescrivant les mesures d’organisation et de fonctionnement du système de santé nécessaires pour faire face à l’épidémie de covid-19 dans le cadre de l’état d’urgence sanitaire](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf)

[^HDS]: [Annuaire des hébergeurs aggréés AFHADS](https://www.afhads.fr/wp-content/uploads/2018/05/6-Annuaire-des-membres.pdf)

[^proposition_com_enq]: [Proposition de résolution tendant à la création d'une commission d'enquête sur la protection des données de santé](https://www.senat.fr/leg/exposes-des-motifs/ppr19-576-expose.html)

