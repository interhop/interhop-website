---
layout: post
title: "Datathon 2024 - Fracture fémur"
categories:
  - DataThon2024
  - Sujets
ref: datathon-2024
lang: fr
---

> Avec ce sujet les participant⸱es ambitionnent d'implémenter un module   de statistiques  prêt à l'emploi au sein de l'outil opensource logiciel LinkR.<br>
> Plus d’informations et inscription : [interhop.org/projets/datathon](https://interhop.org/projets/datathon)
<!-- more -->

# Introduction

La FESF est un pathologie fréquente (> 75 000 cas par an en France), touchant une population âgée et est à l’origine d’une mortalité, d’une morbidité et d’une perte d’autonomie importante entrainant un cout important. Son incidence est en augmentation du fait d’un vieillissement de la population. Tout ceci en fait un problème majeur de santé publique. 

La prévalence des patients sous traitement anticoagulant est importante dans cette population gériatrique. On constate depuis 2012 et la mise sur le marché des « nouveaux » anticoagulants oraux, une forte augmentation des patients traités par ces médicaments qui remplacent les classiques AVK. 

De nombreuse études ont maintenant prouvé l’importance d’une chirurgie précoce (< 48h) pour réduire la morbi-mortalité de ces fractures. Cependant la présence d’un traitement anticoagulant pourrait allonger les délais de prise en charge et augmenter la morbi-mortalité. 

Afin d’uniformiser la prise en charge de ces patients et de réduire les délais de prise en charge un protocole de service a été mis en place au CH de Saint Malo en Janvier 2023. 

L’objectif principal est l’évaluation de la mise en place de ce protocole. L’hypothèse formulée est que la mise en place d’un protocole, permet d’opérer les patients plus rapidement (dans un délai inférieur à 48h).

# Matériel et méthode

Nous réalisons une étude de cohorte rétrospective, observationnelle, monocentrique. 

Le **critère de jugement principal** est le délai de prise en charge au bloc opératoire après l’admission des patients (en heures). 

Les critères de jugements secondaire sont la mortalité à 30 jours et 1an et les complications hémorragiques (transfusion, saignement peropératoire, variation du taux hémoglobine préopératoire). 

Les patients seront séparés en 3 groupes : 


| A | B | C |
| -------- | -------- | -------- |
| Patients traité par AOD opéré avant la mise en place du protocole (avant 2023)     | Patients traité par AOD opéré après la mise en place du protocole (à partir de 2023)      | Patient contrôle non anticoagulé opéré sur la même période     |

	
Une première analyse statistique descriptive sera réalisée. Les données quantitatives sont exprimées par une moyenne et un écart-type. Les données qualitatives sont exprimées en nombre et pourcentage.
Pour le critère de jugement principale nous comparerons le délais de prise en charge au bloc (exprimé en heures) entre les 3 groupes. Nous pouvons également comparer la proportion de patients opérés dans les 48 premières heures suivant leurs admissions.

S’agissant d’une **variable quantitative** nous utiliserons un test t de student  ou un test de *Kruskal-Wallis*. 

Pour les critères de jugements secondaire, les variable quantitative seront comparées avec les même test et les variables qualitatives à l’aide d’un Test du Chi2. 

Une **analyse de survie** selon Kaplan-Mayer sera réalisé pour la mortalité à 30 jours et à 1 an. Une comparaison des courbes de survies à l’aide d’un test du Log Rank sera ensuite réalisée.

### Ressources
Jeu de données fictif. Mis a disposition aux participant.es

# Résultats

Le datathon vise à implémenter un module de statistique au sein de l'outil opensource [LinkR](https://interhop.org/projets/linkr) (survie, tests paramétriques et non paramétriques).


Les résultats seront présentés sous forme de graphiques.
Voici un exemple de survie : 
![](https://pad.interhop.org/uploads/fe51494e-5c1b-4656-86c6-6ff002b531f5.png)


Ce module de statistique sera opensource et pourra donc être réutilisé par l'ensemble des utilisateurs.trices de LinkR
Le code source produits lors de ce datathon sera colligé sur le dépot de code de l'association InterHop. Il sera disponible à cette adresse : [https://framagit.org/interhop/challenges/datathon-2024](https://framagit.org/interhop/challenges/datathon-2024){:target="_blank"}.


# Conclusion

Ce projet s'inscrit dans une démarche de science ouverte, où la production de code, les méthodes et les outils développés seront rendus accessibles à tous et toutes.

Avec ce sujet les participant.es ambitionnent de développer des outils et des méthodologies réutilisables par d'autres acteurs du système de santé.
