---
layout: post
title: "Communiqué de Presse : courrier à la CNIL"
categories:
  - Courrier
  - CNIL
  - Pseudonymisation
  - Chiffrement
  - Information
  - RGPD
redirect_from:
  - /communique-presse-cnil-pseudonymisation-chiffrement
ref: communique-presse-cnil-pseudonymisation-chiffrement
lang: fr
---

Envoi à la Commission Nationale de l'Informatique et des Libertés CNIL d'un courrier faisant suite à l'ordonnance du Conseil d'État du 19 juin.

<!-- more -->

# Objet

Demande de réponse quant à la qualité des processus de chiffrement et de pseudonymisation au sein de la “Plateforme des Données de Santé” ou "HealthDataHub”.

# Contenu de la lettre

Madame La Présidente,

Le déploiement d’une “Plateforme des Données de Santé” par la création d’un Groupement d’Intérêt Public, a été proclamé par la loi n° 2019-774 du 24 juillet 2019 relative à l’organisation et à la transformation du système de santé[^loisante].
Cette “Plateforme des Données de Santé” vise à développer l’intelligence artificielle dans le secteur de la santé et à devenir le guichet unique d’accès à l’ensemble des données de santé du territoire national.
Les données concernées sont celles des centres hospitaliers, des pharmacies, du dossier médical partagé et des données de recherche issues de divers registres. La quantité des données hébergées est amenée à exploser avec l’émergence de la génomique, de l’imagerie et des objets connectés.

Actuellement ces données sont stockées chez Microsoft Azure[^senat_microsoft], cloud public du géant américain Microsoft. Ce choix a été critiqué par de nombreux acteurs publics[^lemonde] [^lenouvelobs] [^theconversation] et privés[^lepoint].

L'avis n° 2020-044 du 20 avril 2020 de votre Commission[^CNILdel] évoque les risques de transferts de données vers des pays tiers et les divulgations non autorisées par le droit de l’Union dans le cadre du contrat de sous-traitance de la solution technique de la "plateforme" à Microsoft Azure. 

Suivant cet avis, le 19 juin 2020, le Conseil d'État a enjoint la "Plateforme des Données de Santé" d'informer les citoyens du "possible transfert de données hors de l'Union Européenne, compte tenu du contrat passé avec son sous-traitant"[^conseil_etat]. **Nous remarquons que cette information est très difficilement accessible sur le site internet health-data-hub.fr puisqu'elle n'est visible  dans la rubrique "Projets",  que dans le projet numéro 3173 "Exploitation des données de passages aux urgences pour l’analyse du recours aux soins et le suivi de la crise sanitaire du Covid-19".**

La "Plateforme des Données de Santé" devait aussi fournir "tous éléments relatifs aux procédés de pseudonymisation utilisés, propres à permettre à [votre Commission] de vérifier que les mesures prises assurent une protection suffisante des données de santé"[^conseil_etat]. La position de la CNIL avait été sollicitée  d'autant plus que l'**obsolescence du système de chiffrement** (nommé FOIN) avait déjà été critiquée par la Cour des Comptes et l'Agence National de Sécurité des Systèmes d'Information ANSSI en 2016[^ccomptes].

Nous alertons également sur la qualité du chiffrement puisque pour   "bénéficier  de  toutes  les  capacités  de  la  solution technique  de  l’hébergeur  ces  clés  devront  lui  être  confiées"[^CNILdel].

Enfin la CNIL et le Conseil d'État ont rappelé qu'au terme de l'état d'urgence sanitaire l'**ensemble des données collectées devait être supprimé et que le traitement ne disposait plus de base légale**. Cependant l'arrêté du 10 juillet 2020 "prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans les territoires sortis de l'état d'urgence sanitaire et dans ceux où il a été prorogé" **prolonge l'utilisation de ces données jusqu'au 30 octobre 2020**[^arrete_sortie_etat_urgence].

Compte tenu du nombre particulièrement important de personnes concernées (plus de 67 millions d’utilisateurs) et du caractère sensible des données personnelles contenus dans la "Plateforme des Données de santé", nous avons décidé de rendre publique ce courrier.
Cette publicité contribue  à l’objectif de transparence défendu par votre Commission[^stopcovid_cnil].

Dans l'attente de votre réponse, nous vous prions de croire, Madame la Présidente, en l’expression de notre très haute considération.

Association interhop.org

[^stopcovid_cnil]: [Application « StopCovid » : la CNIL tire les conséquences de ses contrôles](https://www.cnil.fr/fr/application-stopcovid-la-cnil-tire-les-consequences-de-ses-controles)

[^arrete_sortie_etat_urgence]: [ Arrêté du 10 juillet 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans les territoires sortis de l'état d'urgence sanitaire et dans ceux où il a été prorogé](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000042106233&dateTexte=&categorieLien=id)

[^ccomptes]: [LES DONNÉES PERSONNELLES DE SANTÉ GÉRÉES PAR L'ASSURANCE MALADIE](https://www.ccomptes.fr/sites/default/files/EzPublish/20160503-donnees-personnelles-sante-gerees-assurance-maladie.pdf)

[^senat_appeloffre]: [Protection des données de santé : MORIN-DESAILLY Catherine](https://videos.senat.fr/video.1710660_5f10400c7efbf.seance-publique-du-16-juillet-2020-apres-midi?timecode=16471000)


[^reunin_stopcovid]: [Conférence de presse sur l'application StopCovid, le 23 juin](https://www.economie.gouv.fr/direct-video-conference-presse-sur-application-stopcovid-23-juin#)


[^senat_microsoft]: [Modalités de stockage du « health data hub »](https://www.senat.fr/basile/visio.do?id=qSEQ200114130&idtable=q371194%7Cq370883%7Cq370105%7Cq369641%7Cq368627%7Cq371617%7Cq371538%7Cq371754%7Cq371099%7Cq371232&_c=recherche&rch=qs&de=20170205&au=20200205&dp=3+ans&radio=dp&aff=sep&tri=dd&off=0&afd=ppr&afd=ppl&afd=pjl&afd=cvn)

[^CNILdel]: [Mesures d’organisation et de fonctionnement du système de santé nécessaires pour faire face à l’épidémie de covid-19 dans le cadre de l’état d’urgence sanitaire ](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf)

[^loisante]: [LOI n° 2019-774 du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038821260&categorieLien=id)

[^theconversation]: [The Conversation - Données de santé : l’arbre StopCovid qui cache la forêt Health Data Hub ](https://theconversation.com/donnees-de-sante-larbre-stopcovid-qui-cache-la-foret-health-data-hub-138852)

[^lenouvelobs]: [Le Nouvel Obs - Nos données de santé à Microsoft ? « On offre aux Américains une richesse nationale unique au monde »](https://www.nouvelobs.com/sante/20200623.OBS30391/nos-donnees-de-sante-a-microsoft-on-offre-aux-americains-une-richesse-nationale-unique-au-monde.html)

[^lemonde]: [Le Monde - « La politique publique des données de santé est à réinventer »](https://www.lemonde.fr/idees/article/2020/06/04/la-politique-publique-des-donnees-de-sante-est-a-reinventer_6041706_3232.html)

[^lepoint]: [Le Point - Health Data Hub : « Le choix de Microsoft, un contresens industriel ! »](https://amp.lepoint.fr/2379394?utm_term=Autofeed&utm_medium=Social&utm_source=Twitter&Echobox=1591814194&__twitter_impression=true)

[^conseil_etat]: [Conseil d'État, 19 juin 2020, Plateforme Health Data Hub](https://www.conseil-etat.fr/ressources/decisions-contentieuses/dernieres-decisions-importantes/conseil-d-etat-19-juin-2020-plateforme-health-data-hub)
