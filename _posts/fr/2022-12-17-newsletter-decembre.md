---
layout: post
title: "Bilan de l'année 2022"
categories:
  - Newsletter
  - Décembre
  - "2022"
ref: newsletter-decembre-2022
lang: fr
---

L’année 2022 fut une année associative joyeuse et extrêmement riche ! Nous sommes heureux de partager cela avec vous !

<!-- more -->

# On se soignera avec Toobib

[Toobib.org](https://toobib.org) sera une plateforme web permettant aux professionnel.le.s de santé d’avoir accès à des services numériques éthiques et libres : visioconférence, messagerie instantanée chiffrée, partage des documents de façon sécurisée et chiffrée…

Depuis cet été nous accueillons des étudiant.e.s au sein d’un partenariat avec l’école informatique ENI. En plus de dynamiser l’association ils et elles permettent l’adaptation progressive du logiciel opensource EasyAppointments.org pour les prises de rendez-vous en santé. Merci au [Syndicat de Médecine Générale](https://syndicat-smg.fr/) pour le temps passé à prioriser les développements à effectuer.
Dès que possible nous aimerions salarier un développeur. Voici notre feuille de route : [toobib.org/roadmap](https://toobib.org/roadmap). Tout soutien financier est le bienvenu.

Pour 2023 nous avons besoin de vous. Si vous voulez tester l’adaptation de EasyAppointments pour la prise rendez-vous médicaux, contactez-nous.
Pour 2023 nous avons besoin de vous. Si vous voulez soutenir les projets numériques que l’on porte merci de faire un don sur [interhop.org/dons](https://interhop.org/dons).

Nous remercions chaleureusement CopiePublique.fr qui croit en InterHop et spécifiquement en Toobib. Merci  pour leur généreux financement.

En 2023 nous mettrons également en ligne le premier drive opensource certifié pour les données de santé. Il s’agit du drive CryptPad que nous installerons chez GPLExpert notre Hébergeur de Données de Santé (HDS).
Tout le contenu stocké dans CryptPad est chiffré par le navigateur avant d’être envoyé au serveur, ce qui signifie que seule la personne qui a les clés  peut accéder aux données. L'ensemble des données est hébergé en France.

# On fait (déjà) de la recherche avec Goupile
InterHop accompagne également des projets de recherche. En plus des projets parisiens et lillois nous sommes maintenant présents en Bretagne. 

Nous sommes terriblement fiers de l’expertise que nous pouvons apporter aux centres hospitaliers autour des projets de recherche clinique. En effet, InterHop développe le formulaire en ligne et opensource [Goupile.fr](https://goupile.fr). Il est parfait pour les études scientifiques notamment en santé. Il permet de réaliser des Cahiers d’observation électronique (ou eCRF).
Pour chaque projet, InterHop ouvre des accès à Goupile et développe de nouvelles fonctionnalités adaptées à aux besoins spécifiques.

InterHop propose également des services de gestion de projets ainsi que d’encadrement réglementaire (RPGD).

A coté de cela InterHop anime toujours InterCHU. Ses journées sont un temps d’échange entre soignant.e.s et ingénieur.e.s en santé. Nous promouvons l’interopérabilité qui est un moteur du partage des connaissances. Ce temps permet de préparer la recherche de demain. Une recherche adossée aux logiciels libres, décentralisée par défaut.

# Nos recours

A côté de ce pôle technique qui prend de plus en plus d’ampleur, nous poursuivons nos recours pour défendre encore vos libertés publiques.

Plusieurs recours sont toujours en cours : concernant le databroker [IQVIA](https://interhop.org/2021/05/27/signalement-cnil-iqvia) ou par exemple l’utilisation des [Google Analytics](https://interhop.org/2022/09/06/saisine-cnil-google-analytics-suite-1). D’autres sont pour le moment confidentiels.

Inscrivez-vous à notre lettre d’information pour être tenu au courant des dernières avancées.


# Comment nous aider ?

Pour qu'InterHop continue, nous faisons un appel à votre générosité. InterHop ne fonctionne pour l’instant qu’avec des stagiaires et des bénévoles. Nos frais fixes vont augmenter puisqu’à coté de la location des serveurs certifiés pour la santé (900 euros par mois), nous voulons embaucher une personne sur le projet Toobib.

Aidez-nous financièrement avec un don (défiscalisé à hateur de 66 % du montant de votre don) sur [interhop.org/dons](https://interhop.org/dons). Un petit don mensuel est mieux, pour nous, qu’un don ponctuel.

Vous êtes chercheurs.euses ou soignant.e.s ? Aidez nous en utilisant nos services opensources associatifs installés sur des serveurs certifiés pour le soin.

Nous vous souhaitons de belles fêtes de fin d’année. Et à bientôt en 2023 !
