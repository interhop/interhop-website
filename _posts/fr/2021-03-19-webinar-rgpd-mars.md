---
layout: post
title: "Webinar RGPD : Tout sur la CNIL !"
categories:
  - Webinar
  - RPGD
ref: webinar-rgpd-fevrier
lang: fr
---

Le prochain Webinar RGPD aura lieu le jeudi 25 mars à 18h pour une durée de 20 à 30 minutes.
Il est intitulé : "Tout sur la CNIL".

<!-- more -->

## Webinar RGPD
Les Webinar RPGD ont lieux en ligne tous les quatrièmes jeudi du mois à 18h, et s'adressent préférentiellement aux professionnel.le.s de santé. La Déléguée à la Protection des données (DPD/DPO) d'InterHop veut accompagner la mise en oeuvre et pérenniser la démarche de mise en conformité au RGPD au sein des organisations de santé (cabinets médicaux, paramédicaux...) et sensibiliser les professionnels de santé salariés aux enjeux des textes réglementaires européens. 

## Le prochain Webinar RGPD d'InterHop : Mise en conformité au RGPD

## Quoi ?
Pour se mettre en jambes, le premier traitera de la conformité au RGPD, et d'autres suivront.
Tu peux nous communiquer tes questions, nous y répondrons:)

## A qui s'adresse ce webinar?
Ce webinar s'adresse plus particulièrement aux **professionnels de santé**.

## Quand ?

Le jeudi 25 mars à 18h00 !

## Points traités
- Historique et contexte,
- Grand rôle de la Commission,
- Modalités d'exercice,
- Missions de la Commission.

# Pour nous rejoindre

[https://visio.octopuce.fr/b/int-isp-0fz-iyb](https://visio.octopuce.fr/b/int-isp-0fz-iyb){:target="_blank"}

Nous utilisons la solution de vidéo conférence BigBlueButton, opensource et protectrice des données personnelles car hébergée chez Octopuce, société de droit français (avec des serveurs en France) : [https://www.octopuce.fr/mentions-legales/](https://www.octopuce.fr/mentions-legales/){:target="_blank"}

Pour s'inscrire c'est ici [https://framaforms.org/webinar-rgpd-tout-savoir-sur-la-cnil-1616594944](https://framaforms.org/webinar-rgpd-tout-savoir-sur-la-cnil-1616594944){:target="_blank"}


Nous t'enverrons le code d'accès au salon de discussion 24h avant par courriel. Cela nous servira à avoir une idée du nombre de participants (pour que le serveur tienne le coup :-)). 

