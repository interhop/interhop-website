---
layout: post
title: "Les preuves de l'efficacité des outils numériques anti-covid doivent être publiées"
categories:
  - Courrier
  - Covid
  - Ministere
ref: demande-efficacite-covid
lang: fr
---


L’association InterHop par la voix de son avocate Juliette ALIBERT demande la production des évaluations relatifs aux outils numériques « SI-DEP », « CONTACT COVID », « TOUSANTICOVID », « VACCIN COVID » auprès du ministère de la santé.

<!-- more -->

> Le **fichier SI-DEP** centralise les résultats des tests de dépistage de la COVID-19.

> **Contact COVID** recueille des informations sur les cas contact et les chaînes de contamination

> **TousAntiCovid** est une application mobile de suivi de contacts, basée sur le volontariat des personnes et utilisant la technologie Bluetooth. Elle permet d’alerter les utilisateurs d’un risque de contamination lorsqu’ils ont été à proximité d’un autre utilisateur ayant été diagnostiqué ou dépisté positif à la COVID-19. 

> Le fichier **VACCIN COVID** a pour objectif la mise en œuvre, le suivi et le pilotage des campagnes vaccinales contre la COVID-19. Il comprend des informations sur les personnes invitées à être vaccinées ou déjà vaccinées afin notamment d’organiser la campagne de vaccination, le suivi et l’approvisionnement en vaccins et consommables (seringues, etc.), et la réalisation de recherches et du suivi de pharmacovigilance.[^cnil_explain]

[^cnil_explain]: [La CNIL publie son quatrième avis adressé au Parlement sur les conditions de mise en œuvre des dispositifs contre la COVID-19](https://www.cnil.fr/fr/la-cnil-publie-son-quatrieme-avis-adresse-au-parlement-covid-19)

La CNIL a récemment[^cnil_deliberation]  regretté l’absence de production d’une quelconque évaluation sur l’efficacité des dispositifs numériques de lutte contre la covid19, permettant d’en apprécier la nécessité et la proportionnalité relativement aux atteintes aux libertés individuelles en cause, dont le droit au respect de la vie privée et à la protection des données personnelles.

[^cnil_deliberation]: [Délibération n° 2021-139 du 21 octobre 2021 portant avis public sur les conditions de mise en œuvre des systèmes d'information développés aux fins de lutter contre la propagation de l'épidémie de COVID-19 (mai à septembre 2021)](https://www.cnil.fr/sites/default/files/atoms/files/avis_ndeg_2021-139_du_21_octobre_2021_sur_la_mise_en_oeuvre_des_systemes_dinformation_contre_la_covid-19.pdf)

Cette dernière aurait pourtant réitéré vainement ses demandes à plusieurs reprises et estime que plus de dix-huit mois après le début de la crise sanitaire, la production de tels éléments d’évaluation des outils est « primordial », rappelant avec force la nature particulièrement sensible des données recueillies.

Plus généralement, et dans le cadre de ses précédents avis cette dernière a alerté :
> « à plusieurs reprises sur le risque d’accoutumance et de banalisation de tels dispositifs attentatoires à la vie privée, craignant le glissement vers une société où de tels contrôles deviendraient la norme et non l’exception. Elle a ainsi rappelé que ces mesures ne peuvent être justifiées que si leur efficacité est prouvée, leur application limitée en termes de durée, de personne ou de lieux où elles s'appliquent, et qu’elles sont assorties de garanties de nature à prévenir efficacement les abus, notamment compte tenu de l’extension importante du dispositif consacrée par la loi n° 2021-1040 du 5 août 2021 relative à la gestion de la crise sanitaire[^cnil_deliberation]

Selon des propos rapportés par BFMTV, le porte parole du gouvernement Monsieur Gabriel Attal a en effet déclaré :

> "Les preuves concrètes, nous les avons: l'augmentation très forte de la couverture vaccinale à la suite de la mise en place du pass est une démonstration de l'efficacité du pass sanitaire", affirme le porte-parole du gouvernement (…)

> S'il faut que tout cela soit formalisé dans un document transmis à la Cnil, je n'ai aucun doute que mes collègues concernés du gouvernement le feront"[^preuve_attal].


[^preuve_attal]: ["Nous avons des preuves concrètes": Gabriel Attal répond à la Cnil sur les preuves d'efficacité du pass sanitaire](https://www.bfmtv.com/tech/nous-avons-des-preuves-concretes-gabriel-attal-repond-a-la-cnil-sur-les-preuves-d-efficacite-du-pass-sanitaire_AN-202112010307.html)

Nous demandons donc la réalisation et la communication de tout élément d’évaluation permettant de justifier de la proportionnalité et de la nécessité des traitements au regard des finalités poursuivies concernant « SI-DEP », « CONTACT COVID », « TOUSANTICOVID », « VACCIN COVID ».
