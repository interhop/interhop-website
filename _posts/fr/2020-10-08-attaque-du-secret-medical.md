---
layout: post
title: "Attaque du secret médical : le Health Data Hub centralise les données de santé chez Microsoft"
categories:
  - HDH
  - Microsoft
  - Conseil d'Etat
  - Europe
  - Privacy Shield
  - RGPD
redirect_from:
  - /attaque-du-secret-medical-point-santenathon
ref: attaque-du-secret-medical-point-santenathon
lang: fr
---

Le collectif SantéNathon issu du monde informatique et de la santé s'oppose à la centralisation chez Microsoft Azure des données de santé de plus de 67 millions de personnes. 

Un recours déposé au Conseil d'Etat demande l'application de la décision de la plus haute juridiction européenne qui s'alarmait de l'accès sans limitation des services de renseignements américains aux données hébergées par les GAFAM - Google, Amazon, Facebook, Apple et Microsoft.

<div class="responsive-video-container">
	<iframe title="Attaque du secret médical : le Health Data Hub centralise les données de santé chez Microsoft" width="560" height="315" src="https://peertube.interhop.org/videos/embed/56150be2-195f-42a5-8d8e-a68b0c810dc8" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
</div>



<!-- more -->


## La genèse du projet Health Data Hub

Tout commence en janvier 2016 avec la loi de modernisation du système de santé. Le Système national des données de santé (SNDS) est créé. Initialement il est limité aux données médico-administratives et met à disposition des données individuelles de santé issues de 3 bases de données :
- Les données de la carte vitale,
- Les données de la tarification des établissements de santé,
- Les données statistiques relatives aux causes médicales de décès.

En juillet 2019, la loi élargit considérablement le SNDS et crée le Health Data Hub ou Plateforme des Données de Santé PDS. La CNIL s'alarme du changement de paradigme engendré : “Au-delà d’un simple élargissement, cette évolution change la dimension même du SNDS qui vise à contenir ainsi l’ensemble des données médicales donnant lieu à remboursement”.
En plus du SNDS historique voici à titre d'exemples les bases qui doivent être hébergées au sein de la PDS :
- la base OSCOUR : base de données relative aux passages aux urgences en France. Pour chaque patient admis aux urgences, elle recueille les éléments suivants : code postal de résidence, date de naissance, sexe, date et heure d’entrée et de sortie, durée de passage, mode d’entrée, provenance, mode de transport, classification de gravité, diagnostics (principal et associés), mode de sortie, destination pour les patients mutés ou transférés.
- le Registre France Greffe de Moëlle :  données permettant d'étudier le parcours de soin entre l'annonce du don et le don de Moëlle,
- Base des 500 000 angioplasties :  données permettant d'étudier l'impact des stents dans la vie réelle,
- la cohorte I-Share :  données permettant d'analyser la santé des étudiants,
- SIVIC : données nationales de suivi de la prise en charge de patients hospitalisés COVID19 (depuis mars 2020),
- STOIC : bases de données de scanners thoraciques issus de plusieurs centres ainsi que des données cliniques, 
- COVID TELE : formulaires d'orientation concernant la COVID19 issues d'application en santé et outils de télémedecin,
- ...

Cette liste est loin d'être exhaustive ; les données de cabinet de médecins généralistes, des hôpitaux, des laboratoires, d'imagerie doivent aussi remplir cette Plateforme. Toutes ces bases seront centralisées et liées (on parle d'appariement) pour former le catalogue de données de la PDS.

## Vive inquiétude quant au choix de Microsoft Azure

Le 10 décembre 2019 l'alerte est lancée. Dans [une tribune](https://interhop.org/2019/12/10/donnees-de-sante-au-service-des-patients) dans Le Monde, un collectif initié par des professionnels du secteur de santé et de l’informatique médicale s’inquiète. En effet, la PDS qui regroupe l’ensemble des données de santé de plus de 67 millions de personnes sera hébergée chez [Microsoft Azure](https://www.mediapart.fr/journal/france/221119/health-data-hub-le-mega-fichier-qui-veut-rentabiliser-nos-donnees-de-sante?onglet=full), le cloud du géant américain. Ces données constituent les plus sensibles des données à caractère personnel car protégées par le secret médical.

Progressivemnt ce collectif évolue. Il s'appelle maintenant SanteNathon.org. C'est une rencontre inédite de 18 personnes physiques et morales issues : 
- de l'industrie du logiciel libre : Conseil National du Logiciel Libre (CNLL), Ploss Auvergne-Rhône-Alpes, SoLibre, NEXEDI, 
- de la santé avec des associations de patients et de volontaires : Association Constances, Association les "Actupiennes", Association Française des Hémophiles, Marie Citrini représentantes des Usagers des Hôpitaux de PARIS ainsi que des associations de professionnels de santé comme le Syndicat National des Jeunes Médecins Généralistes (SNJMG), le Syndicat de la Médecine Générale (SMG), l’Union Française pour une Médecine Libre (UFML) et Monsieur Didier Sicard, médecin et professeur de médecine à l’Université Paris Descartes.
- d'ingénieurs : l'Union Fédérale Médecins, Ingénieurs, Cadres, Techniciens (UFMICT-CGT), l'Union Générale des Ingénieurs, Cadres et Techniciens (UGICT-CGT), l'Association InterHop ainsi que Monsieur Bernard Fallery, professeur émérite en systèmes d’information,
- d'organisations de la société civile : l'Observatoire de la transparence dans les politiques du médicament, et Le Syndicat National des Journalistes (SNJ)

Ce collectif dénonce le choix de Microsoft essentiellement à cause de l'absence [d'appel d'offre](https://www.franceinter.fr/donnees-de-sante-des-francais-faut-il-avoir-peur-du-geant-microsoft) et des effets de l'extraterritorialité du droit américain. En effet Microsoft est soumis au CLOUDAct qui permet aux autorités américaines de mettre la main sur des données détenues par des entreprises américaines même si les données sont hébergées dans l'Union Européenne.

Pire encore la Cour de Justice de l’Union Européenne a récemment révélé que les renseignements américains n’ont aucune limitation quant à l’utilisation des données des Européen.ne.s. En ce sens, elle a invalidé l'accord facilitant le transfert des données entre les USA et l'Union Européenne, le "Privacy Shield" ou "Bouclier de Protection des Données".

Facebook, réseau social menacé directement par les conséquences de cette décision qui rend illicite tous les transferts de données en dehors des frontières de l'Union fait pression sur l'Union Européenne. Il menace de stopper ses services d'ici la fin de l'année.

Le bras de fer entre les État-Unis et l'Union Européenne est engagé. Nos données de santé ne doivent pas être prises en otage. Le collectif SantéNathon se bat pour une recherche garante du secret médical et protectrice de nos données.

## Les actions initiées par le collectif SantéNathon

Un premier recours a été déposé le 28 mai 2020 devant le Conseil d'État. La juge concluait à plusieurs irrégularités dans le traitement des données sur la plateforme et des risques majeurs pour les droits et libertés fondamentaux. Cependant, le Conseil d’État considérait, qu’au moment du jugement, la société Microsoft intégrait la liste des organisations ayant adhéré au « Bouclier de protection des données ». Le transfert des données était donc licite. Ce n’est plus cas aujourd’hui ! 

Les requérant.e.s ont donc déposé un nouveau référé liberté. Elles demandent par conséquent au Conseil d’État de prendre la mesure de l'invalidation du "Privacy Shield" et des risques en matière de respect de la vie privée. En ce sens, ils sollicitent la suspension du traitement et la centralisation des données au sein du Health Data Hub.
Ils font également valoir que les engagements contractuels conclus entre la société Microsoft et le Health Data Hub sont insuffisants.

Une audience au Conseil d'Etat est donc prévue ce 8 octobre ...
