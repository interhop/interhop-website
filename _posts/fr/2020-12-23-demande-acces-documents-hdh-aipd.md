---
layout: post
title: "Demande de documents administratifs : Analyse d'Impact (PIA) du Health Data Hub"
categories:
  - Courrier
  - Analyse d'Impact
  - AIPD
  - Health Data Hub
ref: demande-acces-doc-adm-pia-hdh
lang: fr
---

<p style="text-align:right;">
Bruno MAQUART  <br/>
Président de la Plateforme des données de santé ou « Health Data Hub » <br/>
9 rue Georges Pitard, 75015 Paris
</p>

Paris, le 23 décembre 2020

<!-- more -->

Monsieur,

Le déploiement d'un groupement d'intérêt public GIP, dénommé “Plateforme des données de santé” a été proclamé par la loi n° 2019-774 du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé[^1]. 

Cette plateforme vise à développer l’intelligence artificielle appliquée à la santé et veut devenir un guichet unique d’accès à l’ensemble des données de santé.
Les données concernées sont celles des centres hospitaliers, des pharmacies, du dossier médical partagé et les données de recherche issues de divers registres. La quantité des données hébergées est amenée à exploser, notamment avec l’émergence de la génomique, de l’imagerie et des objets connectés. Ces données sont stockées chez Microsoft Azure, cloud public du géant américain Microsoft.

Par ailleurs, la CNIL dans sa délibération  n° 2020-044 du 20 avril 2020[^2] relève "que la Plateforme des données de santé a réalisé une analyse d’impact relative à la protection des données".

Afin d’éclairer nos positions et de pouvoir jouer son rôle d’expertise et de vigie de la démocratie,  l'association InterHop demande de communiquer le document administratif suivant, en application des articles L311-1 à R311-15 relatifs du Code des Relations entre le Public et l'Administration à la communication des documents administratifs [^3]  : 
- analyse globale des risques et des impacts sur la vie privée du Health Data Hub : Privacy Assessment Impact

Nous souhaitons recevoir ces documents :
- dans un format numérique, ouvert et réutilisable. 
- dans un format papier
Nous vous remercions de nous indiquer, après publication, leur adresse de téléchargement et nous les envoyer en pièce jointe.

Comme le livre III du code des relations entre le public et l’administration le prévoit lorsque le demandeur a mal identifié celui qui est susceptible de répondre à sa requête, je vous prie de bien vouloir transmettre cette demande au service qui détient les documents demandés si tel est le cas.

Veuillez agréer, Madame, Monsieur, l'expression de nos sentiments distingués.

Adrien PARROT 
Président InterHop

[^1]: https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000038821260/

[^2]: https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf

[^3]: https://www.cada.fr/particulier/mes-droits
