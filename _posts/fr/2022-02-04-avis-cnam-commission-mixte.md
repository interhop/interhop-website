---
layout: post
title: "Avis du Conseil de la CNAM sur les travaux menés par la Commission mixte (COR et CSITN) sur les données de santé"
categories:
  - HealthDataHub
  - Microsoft
  - CNAM
  - AFP
ref: avis-cnam-commission-mixte
lang: fr
---

> InterHop a eu accès à l'avis définitif du Conseil de la CNAM sur les données de santé. Ce document est le fruit d'un travail documenté  de la commission de la réglementation (COR) et des systèmes d'information et de la transition numérique (CSI-TN). Ce travail s'est étalé sur plus d'un an.
> 
> Interhop publie ci-dessous l'intégralité de l'avis.

<!-- more -->


**Gouvernance, sécurité, éthique et déontologie des usages** des données de santé, et **souverainetés** juridique et technique au service de la confiance des acteurs, telle était la feuille de route définie par le Conseil de la CNAM pour analyser l’état d’avancement de la mise en place de la **Plateforme des Données de Santé** (PDS) dite Health Data Hub (HDH), dans le cadre de la saisine reçue le 25 Août 2020, pour avis du projet de décret en **Conseil d’Etat** relatif au traitement des données à caractère personnel dénommé « **système national des données de santé** » (SNDS). 

Cette plateforme, dont la vocation est de faciliter le partage et l’usage des données de santé, prévoit notamment d’héberger le SNDS, considéré comme le **trésor national des données de santé** ou la mémoire de la santé des assurés, avec une profondeur historique pouvant aller à l’avenir jusqu’à 20 ans. Si la mission de cette plateforme est saluée et partagée par tous, le choix de l’hébergeur et des services associés, le cloud d’une société américaine, non soumise exclusivement au **règlement général sur la protection des données** (RGPD), interroge de nombreux membres du Conseil à bien des égards, voire suscite de fortes oppositions ; d’autant plus que ce choix a été effectué par la PDS sans appel d’offres.

Les Conseillers de la CNAM se sont émus que ce projet, qui est aux confins des enjeux clefs de la **santé et du numérique**, priorités des français, n’ait pas été pensé de manière globale au bénéfice d’une **solution industrielle française**. 

Ainsi, après plus d’une dizaine d’auditions menées tout au long de l’année 2021, soit plus de quinze heures de débats riches qui ont permis d’échanger de manière tout à fait accessible sur des questions complexes liées à l’**hébergement et à l’exploitation** des données de santé, avec les acteurs en charge de la stratégie du numérique en santé, des autorités de régulation, de la sécurité juridique, logicielle et physique des données de santé, des industriels et des start-ups développant des applications en santé, basées sur l’intelligence artificielle, les conseillers de la Caisse Nationale d’Assurance Maladie (CNAM), ainsi mieux éclairés sur les enjeux définis préalablement, souhaitent dans une démarche constructive porter à la connaissance des pouvoirs publics leurs constats et leurs propositions pour garantir la confiance dans une plateforme des données de santé aujourd’hui dépréciée, et contribuer à son évolution en conformité avec la nouvelle doctrine définie par l’Etat en matière de cloud. 

### Pour une gouvernance des données de santé mieux partagée entre experts et non experts eu égard aux usages à venir en terme de santé publique 

**Sur l’enjeu de la gouvernance**, le Conseil de la CNAM avait, dans sa première expression, regretté l’absence d’un représentant des assurés sociaux au sein du Conseil d’administration de la PDS, alors que les assurés du régime général sont les premiers fournisseurs de données de santé, et les principaux financeurs de leur exploitation. Le Président de la CNAM s’en était ouvert dans un courrier en date du 6 mars 2020, adressé à la direction de la plateforme et au Ministre des Solidarités et de la Santé. 

Les auditions ont mis en évidence le fait que la gouvernance exécutive de la PDS, réservée majoritairement à des collèges d’experts, s’inscrivait dans un contexte plus global de réforme de la gouvernance du numérique en santé, avec un repositionnement et un renforcement du rôle de l’Etat comme pilote légitime **d’une stratégie nationale du numérique en santé**, mais définie et mise en œuvre exclusivement par des experts issus des sphères publique et privée. 

Cette démarche n’a pas permis d’associer, dès l’origine, les assurés sociaux à travers leurs représentants et les a privés de porter leurs recommandations et avis, afin de partager d’une part, leur vision sur les enjeux autour du numérique en santé, notamment sur un **plan éthique et déontologique**, c’est-à-dire le cadre de **valeurs** de l’usage des données, et d’autre part, sur la centralisation et l’exploitation des données au sein d’une plateforme telle que la PDS, et enfin sur la structuration et donc de la gouvernance exécutive de la plateforme des données de santé. 

Les Français sont méfiants à l’égard de l’utilisation, par les pouvoirs publics, de leurs données, dans la mesure où ce sont des **données sensibles** qui touchent à leur vie privée. En revanche, ils ont davantage confiance dans les instances de Sécurité sociale, au sein desquelles les partenaires sociaux participent à la gouvernance. 

Ainsi, dans le cadre de l’utilisation des données de santé des assurés du régime général, il apparait légitime d’associer les représentants des assurés, afin de recueillir leur confiance en garantissant **une totale transparence** à leur égard sur les valeurs d’usages de ces données, dans le cadre de leur traitement massif, en particulier par **l’intelligence artificielle** dont tout le monde reconnaît le potentiel en terme de santé publique, mais qui peut poser des questions éthiques et déontologiques.

Ceux-ci doivent, en effet, jouer **ce rôle de confiance** au sein de la PDS pour garantir sur les valeurs d’usage éthiques et déontologiques de l’utilisation de leurs données, et assurer l’exercice de leurs droits, mais aussi d’être les relais des usages innovants et concrets pour leur santé, et plus globalement pour la santé publique, rendus possibles grâce à l’utilisation de leurs données.

Si la mise à disposition de données anonymisées dans le cadre de la politique d’open data recueille l’assentiment des assurés et des utilisateurs, puisqu’elle suscite même des initiatives très intéressantes comme nous avons pu le voir durant la crise sanitaire dans le suivi de l’épidémie, la mise à disposition de données **pseudonymisées** appelle à une plus grande **vigilance** ; celles-ci pouvant être associées à des données tierces, rendant toutefois possible l’identification de l’intéressé. 

Ainsi, afin de lever les résistances liées à l’exploitation des données, de porter la volonté des assurés au bon niveau et d’assurer la confiance dans une plateforme éthique et déontologique, le Conseil propose :
- de renforcer  la représentation des assurés sociaux par **l’intégration du Président du Conseil de la CNAM, ou son représentant, au sein du conseil d’administration de la PDS** ;
- de renforcer le rôle de **l’assemblée générale de la plateforme**, réduit actuellement à une chambre d’enregistrement, en lui octroyant d**es pouvoirs de contrôle et des moyens de préparer les grandes orientations stratégiques et politiques à venir** ;
- de contribuer au côté de la PDS et de l’Etat, à la constitution **d’une charte du numérique en santé** autour des valeurs éthiques, déontologiques, humaines et environnementales ;
- de s’appuyer sur **des réseaux d’acteurs de proximité**, comme les conseils locaux de caisses d’assurance maladie, en organisant des **conseils publics chargés d’informer et de former des assurés aux divers usages**. 

### Pour un haut niveau de sécurité basé sur des expertises et des certifications les plus avancées

**Sur l’enjeu de la sécurité**, le Conseil de la CNAM avait indiqué que les **conditions** notamment **juridiques nécessaires à la protection de ces données ne lui semblaient pas réunies** pour que le SNDS soit mis à disposition d’une entreprise non soumise exclusivement au droit européen (RGPD), et ce, indépendamment de garanties contractuelles qui auraient pu être apportées.

Les auditions menées confortent cette position. En effet, les risques d’accès non autorisés ont bien été identifiés, sans possibilité de recours, et sont reconnus par le Conseil d’Etat, la CNIL et l’ANSSI, et la CJUE qui a annulé le « Privacy Shield » par l’arrêt SCHREMS II le 16 juillet 2020.

Le stockage des données sur le cloud, d’une entreprise américaine, ne garantit plus un niveau de protection des données équivalent à celui de l’Union Européenne. Cela contrevient donc au RGPD et crée, par conséquent, une **insécurité juridique de la plateforme des données de santé**.  La Commission souligne qu’il appartenait aux préfigurateurs de mesurer les risques pris et poursuivis par ce choix. 

Le Patriotact puis Freedomact, la section 702 de la Loi FISA, **Executive Order 12333** et le Cloud act consacrant le caractère extraterritorial du droit américain sur leurs entreprises, rendent caduques toutes dispositions nationales de protection des données qu’elles relèvent du domaine législatif, réglementaire ou contractuel. Aussi, ni l’arrêté du 10 octobre 2020, pris dans l’urgence interdisant le transfert de données à caractère personnel hors de l’Union européenne, ni les clauses contractuelles ne permettent de lutter efficacement contre le risque d’accès non autorisé aux données de santé.

Par ailleurs, concernant **les circuits de pseudonymisation** des identifiants des personnes, les mesures de sécurité et les responsabilités respectives de la CNAM et de la PDS n’ont pas encore été validées. La CNIL n’a pas été encore saisie de l’évolution du référentiel sécurité du SNDS et la demande d’autorisation concernant l’hébergement et l’exploitation des données de santé par la plateforme a été retirée.

Comme l’a rappelé le Directeur général de l’ANSSI, la **cybersécurité doit être une préoccupation portée à un niveau stratégique**. Le système de sécurité doit être robuste et permettre d’anticiper les attaques pour gagner la confiance d’utilisateurs des applications numériques. 

Au regard de leur vulnérabilité, l’hébergement et l’exploitation de données de santé, lorsqu’elles sont centralisées et interconnectées dans un cloud, doivent être soumis **au plus haut niveau de certifications délivrées par l’ANSSI**, c’est-à-dire HDS (Hébergeur de Données de Santé) et SecNumCloud, et d’expertise par la CNIL. 

Les conseillers de la CNAM souhaitent être consultés au préalable et être informés des résultats de ces certifications et expertises de la CNIL, avant toute mise à disposition du SNDS. 

Pour le Conseil, la sécurité, qui est au cœur de la confiance, n’est pas pleinement garantie par la plateforme et ne permet donc pas en l’état d’héberger le SNDS. 

Il appelle à : 
- sanctuariser dans la Loi l’hébergement du SNDS sur le territoire national, par une entreprise soumise exclusivement au RGDP ;
- opter pour un hébergement disposant du niveau de certification le plus avancé actuellement HDS et SecNumCloud ;
- être régulièrement consulté afin de disposer d’informations  claires et compréhensibles sur la sécurité des données, et que chacun puisse exercer pleinement ses droits (refus de transmission, modification, effacement).  

### Pour une maitrise des données de santé sur une plateforme technologique souveraine

Sur les enjeux liés à la souveraineté, le Conseil de la CNAM avait considéré également, dans sa dernière expression, que compte tenu du caractère spécifique des données de santé au regard de leur enjeu **stratégique**, seul un **dispositif souverain** et uniquement soumis au RGPD permettra de **gagner la confiance** des assurés, dans l’utilisation de leurs données.

Les auditions ont mis en lumière le fait que la question de la souveraineté n’a pas été une priorité lors de la réflexion autour de la stratégie du numérique en santé, et n’a donc pas été un objectif dans la création de la PDS. Celle-ci n’a été perçue que sous l’angle de la **réversibilité**, mais sans réel engagement précis sur une éventuelle migration sur un hébergeur souverain.

Le choix d’un cloud américain, comme hébergeur des données de santé plus couteux, s’est fait dans une relative urgence sur la base d’une **étude de marché** qui n’a vraisemblablement pas permis aux industriels français ou **européens** d’identifier précisément les besoins de la plateforme, et ainsi de proposer une alternative toute aussi performante. Or la France, mais aussi l’Europe, bénéfice d’un **écosystème favorable** grâce à des entreprises technologiques reconnues. Ils existent désormais des fournisseurs de moyens et de solutions, qui proposent des offres de stockage, de services d’exploitation et de sécurité des données que la Commission mixte a pu auditionner.

Compte tenu des nombreux retards observés et annoncés, l’urgence souvent avancée ne semble donc plus une raison suffisante, il aurait donc été plus opportun de les **réunir et de bâtir avec eux une solution répondant**, dans les délais, aux exigences techniques attendues, voire de proposer une autre architecture correspondant davantage au fonctionnement d’un Hub.

En outre, si la solution « **clé en main** », hébergement et services, retenue pouvait constituer un avantage pour la plateforme, elle peut être un frein à changer d’opérateur, à moyen ou long terme, dans la mesure où la **dépendance** est de plus en plus forte.

Par ailleurs, sur demande du Ministère des Solidarités et de la Santé, et en conformité avec la nouvelle doctrine de l’Etat en matière de cloud, la plateforme devra changer d’opérateur dans un **délai compris entre 12 et 18 mois** et, en tout état de cause, **ne dépassant pas les deux ans** (délai précisé sur le site internet de la CNIL). 

Les membres de l’assemblée générale de la plateforme, réunie le 19 janvier 2022, ont pris connaissance et acte du plan d’actions prévu pour cette année. Celui-ci prévoit la **finalisation de la qualification des offres souveraines** (9 solutions d’hébergement souverain éligibles dans le cadre du benchmark réversibilité) et **la préparation de la migration** et la mise en œuvre de celle-ci, ainsi que la poursuite de la **réduction de l’adhérence** à la solution actuelle.

Cet infléchissement, répondant aux préoccupations du Conseil de la CNAM, invite néanmoins, afin d’éviter les écueils connus dans le passé, à mettre en place quelques garde-fous pour regagner la confiance : 
- **Rendre public le benchmark sur la réversibilité** ; 
- S’engager sur un calendrier très précis de migration ;
- S’assurer de la destruction des données de santé déposées sur la plateforme actuelle ;
- **Procéder à un appel d’offre à la suite d’un audit indépendant** ; 
- Publier les documents exprimant le cahier des charges exhaustifs de la PDS.

Pour conclure, le Conseil de la CNAM rappelle **son plein soutien à l’exploitation des données de santé** dans le but d’améliorer la qualité et l’efficience des prises en charges, de participer et d’améliorer la sécurité sanitaire, et de favoriser l’innovation et la recherche en santé publique. 

Ainsi **il salue le dispositif exceptionnel mis en place par la CNAM, pour accélérer les mises à disposition de données de santé** pour les projets autorisés par la CNIL, qui va mobiliser des moyens supplémentaires humains et techniques, via notamment un renfort d’agents de la plateforme des données de santé (PDS) permettant ainsi d’accroitre les capacités d’appariements, de ciblages et d‘extractions des données. 

Le Conseil de la CNAM soutient donc **ce partenariat renforcé entre la CNAM et la PDS**, le temps de la mise en conformité de la plateforme, avec la nouvelle doctrine de l’Etat en matière de cloud. 



### ANNEXE 1 : liste des propositions 

- En matière de gouvernance : 
  - renforcer  la représentation des assurés sociaux par **l’intégration du Président du Conseil de la CNAM, ou son représentant, au sein du conseil d’administration de la PDS** ;
  - renforcer le rôle de **l’assemblée générale de la plateforme**, réduite actuellement à une chambre d’enregistrement, en lui octroyant **des pouvoirs de contrôle et des moyens de préparer les grandes orientations stratégiques et politiques à venir** ;
  - contribuer au côté de la PDS et de l’Etat, à la constitution d’une **charte du numérique en santé** autour des valeurs éthiques, déontologiques, humaines et environnementales ;
  - s’appuyer sur des réseaux **d’acteurs de proximité** comme les **conseils locaux de caisses d’assurance maladie en organisant des conseils publics chargés  d’informer et de former des assurés aux divers usages.**
- En matière de sécurité : 
    - **sanctuariser dans la Loi l’hébergement du SNDS sur le territoire national par une entreprise soumise exclusivement au RGDP** ;
    - opter pour un hébergement disposant du niveau de certification le plus avancé actuellement **HDS et SecNumCloud** ;
    - être régulièrement consulté afin de disposer d’informations claires et compréhensibles sur la sécurité des données afin que chacun puisse exercer pleinement ses droits (refus de transmission, modification, effacement).  
- En matière de souveraineté : 
    - **Rendre public le benchmark sur la réversibilité** ; 
    - S’engager sur un calendrier très précis de migration ;
    - S’assurer de la destruction des données de santé déposées sur la plateforme actuelle ;
    - **Procéder à un appel d’offre avec la mise en place d’une commission indépendante** ; 
    - Publier les documents exprimant le cahier des charges exhaustifs de la PDS.


### ANNEXE 2 : auditions de la Commission mixte

Introduction le  mercredi 10 mars 2021 sur la stratégie numérique de l’Etat par Mme Laura Létourneau, **délégation ministérielle du numérique en santé**.

- Session I : vendredi 19 mars 2021
Le contexte juridique national et européen par Mme Valérie Peugeot, Commissaire en charge du secteur santé, et ses collaborateurs - **CNIL**
- Session II : jeudis 15 avril et 22 avril 2021
Les aspects éthiques ont été évoqués au cours de deux séminaires : 
    1. l’un sur les aspects éthiques médical et numérique, invité M. le P. Didier Sicard, Président d'honneur du **Comité Consultatif National d’Ethique** ;
    2. L’autre, sur les activités du CESRESS, invité M. Bernard Nordlinger, Président du **CESREES**, comité placé auprès du HDH. 
- Session III : mercredi 19 mai 2021
Les enjeux de la sécurité des données, du stockage, etc. avec pour intervenant M. Guillaume Poupard, Directeur général de l’**ANSSI**. 
- Session IV : vendredi 11 juin 2021
Mieux comprendre l’écosystème français du numérique par ses acteurs pour cela trois représentants du secteur industriel du numérique en santé sont intervenus : 
    1. Monsieur Pascal Gayat - Directeur de Digital influence entreprise de consulting dans le numérique
    2. Monsieur Sylvain Rouri – **OVH cloud**
    3. Madame Servane Augier – **Outscale** (Dassault système) et M. Stéphane Messika – **Kynapse**
- Session V : vendredi 24 septembre et lundi 8 novembre 2021
Les algorithmes, l’utilisation et le partage des données ont été abordés lors de deux séminaires : 
1. l’un par des représentants de deux start-up, M. Olivier de Fresnoye, Directeur général d’**Echopen factory**, et M.  Gaëtan Dissiez, Machine Learning Engineer chez behold.ai. 
2. l’autre par M. Frédéric Rimattéi, Directeur général adjoint du **CHU de Rennes**.
- Session VI : mercredi 1er décembre 2021
Sur la stratégie et la gouvernance des outils informatiques pour l’amélioration des processus de soins au sein de l’**Hôpital Universitaire de Genève** (HUG) par M. le P. Antoine Geissbuhler. 


