---
layout: post
title: "Journées InterCHU - Réutilisation des données d'anesthésie / réanimation"
categories:
  - OMOP
  - France
  - InterCHU
ref: journee-interchu-anesth-rea-4
lang: fr
---

Nous parlerons d'interopérabilité en anesthésie et réanimation !

<!-- more -->

## Contexte

Les logiciels hospitaliers enregistrent quotidiennent et automatiquement des volumes importants de données pour le soin ou la facturation. Il est possible de réutiliser ces données pour un autre usage, comme la recherche ou le décisionnel.

Pour cela, plusieurs centres hospitaliers ont développé un entrepôt de données, qui après nettoyage des données et homogénéisation de la structure, permet de croiser des données issues au départ de logiciels différents [^Degoul]. 

Certains ont ensuite transformé les données dans un modèle de données commun (OMOP), qui permet de s'affranchir des vocabulaires et logiciels utilisés localement, en standardisant la structure de données et le vocabulaire [^Lamer].

Vous voulez nous rejoindre ? Discutons lors de notre prochain webinar distanciel.

## Quand et où ?

Mardi 26 juin à 13h00.

Voici le lien Big Blue Button de la visio-conférence : [https://visio.octopuce.fr/b/int-a1t-rpf-huo](https://visio.octopuce.fr/b/int-a1t-rpf-huo). 

N'hésitez pas à transférer cette information aux personnes potentiellement intéressées.
N'hésitez pas à faire remonter des points que vous voudriez faire partager à la communauté !
 
Au plaisir

[^Degoul]: Degoul S, Chazard E, Lamer A, Lebuffe G, Duhamel A, Tavernier B. lntraoperative administration of 6% hydroxyethyl starch 130/0.4 is not associated with acute kidney injury in elective non-cardiac surgery: A sequential and propensity-matched analysis. Anaesth Crit Care Pain Med. 2020 Apr;39(2):199-206. doi: 10.1016/j.accpm.2019.08.002. Epub 2020 Feb 14. PMID: 32068135

[^Lamer]: Lamer A, Abou-Arab O, Bourgeois A, Parrot A, Popoff B, Beuscart JB, Tavernier B, Moussa MD. Transforming Anesthesia Data Into the Observational Medical Outcomes Partnership Common Data Model: Development and Usability Study. J Med Internet Res. 2021 Oct 29;23(10):e29259. doi: 10.2196/29259. PMID: 34714250; PMCID: PMC8590192.
