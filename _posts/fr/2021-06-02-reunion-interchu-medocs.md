---
layout: post
title: "Journées InterCHU - médicaments"
categories:
  - OMOP
  - France
  - InterCHU
ref: journee-interchu-medocs
lang: fr
---

Nous parlerons d'interopérabilité des médicaments !

<!-- more -->

# Prochaine réunion

### Quand ?

Le 15 juin entre 9h et 12h30.

### Intéressons-nous aux médicaments

**Ce wébinaire abordera les terminologies de médicaments utilisées en France.**

Voici le programme :
- 9h00-9h30 - Antoine LAMER (CHU Lille, InterHop) : Présentation des journées InterCHU
- 9h30-10h00 - Adrien PARROT, Nicolas PARIS (InterHop) : Comment collaborer sur les terminologies avec Susana ? 
- 10h00 - 10h30 - Florent Desgrippes (APHP, InterHop) : **L'interopérabilité et le médicament**
- 10h30 - 11h00 - Julien Dubiel (APHP) : **profiling des ressources FHIR et médicament**
- 11h00 - 11h30 - Sébastien Cossin (CHU Bordeaux) : **mapping sémantique et médicament**
- 11h30 - 12h - Jean-François Laurent (CNAM, Thesorimed) : **présentation du référentiel Thésorimed**

Voici un lien Framadate d'inscription : [https://framadate.org/8GVgjZw8667Cgrc6](https://framadate.org/8GVgjZw8667Cgrc6){:target="_blank"}
Laissez-nous votre courriel, nous vous enverrons le code d'accès pour la réunion BigBlueButton

### Où ?

Nous utilisons la solution de vidéo conférence BigBlueButton, opensource et protectrice des données personnelles car hébergée chez Octopuce, société de droit français (avec des serveurs en France) : [https://www.octopuce.fr/mentions-legales/](https://www.octopuce.fr/mentions-legales/){:target="_blank"}

Voici le lien BBB de la visio : [https://visio.octopuce.fr/b/int-a1t-rpf-huo](https://visio.octopuce.fr/b/int-a1t-rpf-huo)
Le code d'accès arrivera par courriel.

# [Les "journées InterCHU"](https://interhop.org/2021/01/08/reunion-interchu) c'est quoi ?

InterCHU est un réseau d’ingénieurs souhaitant promouvoir l’interopérabilité en santé et utilisant le modèle OMOP ou voulant l'utiliser dans les prochains mois.
InterCHU s’adresse d’abord aux hôpitaux français.

Pour rappel voici [le compte-rendu de la dernière réunion de janvier](https://interhop.org/2021/01/25/reunion-interchu-21-janvier).


