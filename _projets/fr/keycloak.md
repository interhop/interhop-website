---
title: Keycloak
description: "Partage de fichiers"
image: /assets/images/projets/sso.png
logiciels: true
order_logiciels: 8
sso_url: https://keycloak.interhop.org/
external_name: "En savoir plus sur Keycloak"
external_url: https://keycloak.org/
ref: projets-keycloak
lang: fr
---


Keycloak est une solution open source de gestion des identités et des accès, offrant des fonctionnalités telles que l'authentification unique (SSO), la gestion des utilisateurs et la prise en charge de protocoles comme OAuth 2.0 et SAML. Il permet de sécuriser facilement des applications tout en centralisant la gestion des utilisateurs.

Son principal atout réside dans la simplification de la sécurité des applications, avec une gestion centralisée des identités et un accès unifié. Keycloak réduit les risques liés à la gestion décentralisée des mots de passe tout en offrant une intégration flexible et extensible pour différents environnements.
:w
# L'instance d'InterHop

L'instance Keycloak d'InterHop est disponible ici : <a href="https://keycloak.interhop.org" target="_blank">keycloak.interhop.org</a>.
