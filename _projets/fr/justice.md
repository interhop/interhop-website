---
title: Justice
description: Plaidoyer, contentieux
image: /assets/images/projets/justice.png
une: true
order_une: 4
ref: projets-actions-en-justice
lang: fr
---

Pour faire changer la loi et protéger tes droits, InterHop développe ses activités de plaidoyer et de contentieux stratégique.

<div class="responsive-video-container">
	<iframe title="Attaque du secret médical : le Health Data Hub centralise les données de santé chez Microsoft" width="560" height="315" src="https://peertube.interhop.org/videos/embed/56150be2-195f-42a5-8d8e-a68b0c810dc8" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
</div>

En 2020 et au sein du collectif SantéNathon, nous avons porté [un recours devant le Conseil d'Etat contre la Plateforme nationale des Données de Santé PDS (ou Health Data Hub) hébergée chez la société Microsoft]({{ site.baseurl }}/2020/09/16/communique-presse-refere-privacy-shield). Les risques liés à l'utilisation des plateformes extra-territoriales ont été reconnus par le [Conseil d'Etat]({{ site.baseurl }}/2020/10/14/communique-presse-conseil-detat-reconnait-acces-donnes).

Le ministre de la santé s'était engagé le [19 novembre 2020 devant la CNIL]({{site.baseurl}}/2020/11/23/la-reversibilite-est-acquise) a adopter "une nouvelle solution technique permettant de ne pas exposer les données hébergées par la PDS à d'éventuelles divulgations illégales aux autorités américaines, dans un délai qui soit autant que possible compris en 12 et 18 mois et en tout état de cause, ne dépasse pas deux ans". 
Ce délai ayant été dépassé sans effet nous nous réservons le droit de continuer les actions juridiques pour défendre vos droits.

Nous poursuivons à alerter à chaque fois que nous le jugeons nécessaire :
- concernant le [Data Broker américain Iqvia]({{ site.baseurl }}/2021/05/27/signalement-cnil-iqvia)
- concernant les cookies [Google Analytics]({{ site.baseurl }}/2022/09/06/saisine-cnil-google-analytics-suite-1) utilisés par plusieurs acteurs de la e-santé
- concernant les [prises de rendez-vous en ligne]({{ site.baseurl }}/2021/03/12/communique-presse-decision-ce-rendezvous)

Nos actions de plaidoyer sont nombreuses et concernent notamment:
- [Mon Espace Santé]({{ site.baseurl }}/2022/01/31/mon-espace-sante)
- [La dernière décision d'adéquation prise par la Commission Européenne]({{ site.baseurl }}/2022/10/31/new-executive-order)
- [Auditions devant l’Assemblée Nationale]({{ site.baseurl }}/2021/02/22/commission-souverainete)
