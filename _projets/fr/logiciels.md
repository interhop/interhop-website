---
title: "Nos logiciels"
description: "Opensources et libres"
layout: default
image: /assets/images/projets/software.svg
show_image: false
projets_outils: true
order_outils: 1
projets_collab: false
redirect_from:
  - /apps
ref: projets-logiciels
lang: fr
---

<section class="hero diagonal">
	<div class="container" {% if page.full_width %}style="max-width: 100%"{% endif %}>
		<h2>{{ page.title }}</h2>
		<p class="subtext">{{ page.description }}</p>
	</div>
</section>


<section class="diagonal alternate">
	<div class="container halves">
		<div>
			<h3 class="editable">Au sein de notre SSO</h3>

		</div>
		<div>
			<ul class="image-grid">
				{% assign _projets = site.projets | where: "lang", "fr" | where: "sso", "true" | sort: 'order_sso' %}
				{% for projet in _projets limit: 10 %}
					<li><a href="{{ site.baseurl }}{{ projet.url_sso }}" target="_blank"><img src="{% include relative-src.html src=projet.image  %}" alt="Logo - {{ projet.title }}" class="screenshot">
					<div class="details">
							<div class="position">{{ projet.title }}</div>
					</div>
					</a></li>
				{% endfor %}
			</ul>
		</div>
	</div>


</section>

<section class="diagonal patterned">
	<div class="container halves">
		<div>
			<h3 class="editable">Notes explicatives</h3>
		<!--	<p class="editable">Fiches explicatives</p> -->

		</div>
		<div>
			<ul class="image-grid">
				{% assign _projets = site.projets | where: "lang", "fr" | where: "logiciels", "true" | sort: 'order_logiciels' %}
				{% for projet in _projets limit: 10 %}
					<li><a href="{{ site.baseurl }}{{ projet.url }}"><img src="{% include relative-src.html src=projet.image  %}" alt="Logo - {{ projet.title }}" class="screenshot">
					<div class="details">
							<div class="position">{{ projet.title }}</div>
					</div>
					</a></li>
				{% endfor %}
			</ul>
		</div>
	</div>

</section>






<section class="diagonal alternate">
	<div class="container">
           <h2>L'ensemble des logiciels d'InterHop !</h2>

	   <p class="subtext editable">
	   	InterHop.org administre des services nécessitant un <a href="{{ site.baseurl}}/projets/hds">Hébergeur de Données de Santé (HDS)</a> et des services qui ne le nécessitent pas.
	   </p>

           <h4>En ce qui concerne les services HDS, il s’agit de :</h4>
           <ul>
           	<li><a href="https://goupile.hds.interhop.org/" target="_blank">goupile.hds.interhop.org</a></li>
           	<li><a href="https://cryptpad.hds.interhop.org/" target="_blank">cryptpad.hds.interhop.org</a></li>
           </ul>

           <h4>En ce qui concerne les services qui ne sont pas HDS, il s’agit de :</h4>
           <ul>
           	<li><a href="https://goupile.interhop.org/" target="_blank">goupile.interhop.org</a></li>
           	<li><a href="https://app.linkr.interhop.org/login" target="_blank">app.linkr.interhop.org</a></li>
           	<li><a href="https://element.interhop.org" target="_blank">element.interhop.org</a></li>
           	<li><a href="https://plik.interhop.org" target="_blank">plik.interhop.org</a></li>
           	<li><a href="https://pad.interhop.org" target="_blank">pad.interhop.org</a></li>
           	<li><a href="https://cpad.interhop.org" target="_blank">cpad.interhop.org</a></li>
           	<li><a href="https://password.interhop.org" target="_blank">password.interhop.org</a></li>
           	<li><a href="https://keycloak.interhop.org" target="_blank">keycloak.interhop.org</a></li>
           </ul>


	<p class="subtext editable">
	Les <a href="{{ site.baseurl}}/cgu">conditions générales d’utilisation</a> (dites « CGU ») du site InterHop.org ont pour objet l’encadrement juridique des modalités de mise à disposition de nos services <strong>non HDS</strong>. 
	</p>
	<p class="subtext editable">
	Les CGU ne concernent pas les services HDS pour lesquels un contrat est établi.
	</p>

		<p><a style="text-align: center;" class="button alt" type="blue" href="{{ site.baseurl }}/2023/04/06/brochure-recherche">Voir notre brochure "Recherche" HDS</a></p>
<!--
    	<ul>
 		<li>les sites web <a href="https://interhop.org">InterHop.org</a> et  <a href="https://linkr.interhop.org" target="_blank">linkr.interhop.org</a> sont  hébergés en France chez Framagit</a></li>
		<li>Les services <a href="https://pad.interhop.org" target="_blank">pad.interhop.org</a>, <a href="https://cpad.interhop.org" target="_blank">cpad.interhop.org</a>, <a href="https://password.interhop.org" target="_blank">password.interhop.org</a>, matrix.interhop.org et <a href="https://element.interhop.org" target="_blank">element.interhop.org</a> sont autohébergés par le secrétaire de l'association InterHop (en France)</li>
		<li>Les sites web de <a href="https://goupile.fr/" target="_blank">goupile.fr</a>, <a href="https://goupile.interhop.org/" target="_blank">goupile.interhop.org</a> et <a href="https://linkr.interhop.org" target="_blank">linkr.interhop.org</a> sont hébergés en France chez OVH (VPS)</li>

           i/ul>
-->


   	</div>



</section>
