---
title: Element
description: "Messagerie instantanée"
image: /assets/images/projets/communication.svg
show_image: false
une: false
projets_outils: false
projets_collab: true
order_collab: 3
sso: true
order_sso: 1
url_sso: https://element.interhop.org/#/start_sso
logiciels: true
order_logiciels: 3
external_name: "En savoir plus sur Element"
external_url: https://element.io/
ref: projets-element
lang: fr
---



Element est une application opensource de messagerie instantanée conçue pour améliorer la communication et la collaboration entre individus et équipes. 

Avec des fonctionnalités telles que les messages texte, les appels vocaux et vidéo, et le partage de fichiers, Element offre une solution complète pour rester connecté et organisé. 

# Matrix et la décentralisation

Element repose sur le protocole Matrix[^matrix], qui permet la fédération, c'est-à-dire la capacité de communiquer avec des utilisateurs sur différents serveurs Matrix. Cette architecture décentralisée et interopérable renforce la résilience et l'indépendance de la plateforme par rapport aux solutions centralisées. 
![](https://framagit.org/interhop/wiki/-/wikis/application-type.png)

Synapse[^synapse], le serveur de référence pour le protocole Matrix, joue un rôle crucial dans cette infrastructure. Il permet aux organisations de déployer et gérer leurs propres serveurs Matrix, assurant ainsi un contrôle total sur les données et la possibilité de fédérer avec d'autres serveurs. 

Avec Element comme client et Synapse comme serveur, les utilisateurs bénéficient d'une messagerie sécurisée et décentralisée, adaptée à des environnements variés allant des petites équipes aux grandes entreprises.



# Inscription

Pour tirer pleinement parti d'Element, il est possible rejoindre divers fils de discussion. Par exemple, le fil de discussion public d'[InterHop](https://matrix.to/#/#public:matrix.interhop.org){:target="_blank"} ou ceux dédiés a [Goupile](https://matrix.to/#/#goupile:matrix.interhop.org){:target="_blank"} ou [LinkR](https://matrix.to/#/#linkr:matrix.interhop.org).

Element est disponible sur mobile, ordinateur ou via un [simple navigateur](https://element.interhop.org){:target="_blank"}.

Voici un tutoriel de connexion.<br/>
Contactez-nous (par <a href="mailto:interhop@riseup.net">courriel</a>) pour obtenir un jeton pour finaliser votre inscription.

 <div class="responsive-video-container">
<iframe width="100%" height="500" src="https://pad.interhop.org/p/rynEJduy_#/" frameborder="0"></iframe>
</div>


[^matrix]: [What is Matrix?](https://matrix.org/docs/older/introduction/){:target="_blank"}

[^synapse]: [Synapse: Matrix homeserver written in Python/Twisted](https://github.com/matrix-org/synapse){:target="_blank"}

[^element]: [https://element.io](https://element.io/){:target="_blank"}
