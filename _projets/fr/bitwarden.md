---
title: Bitwarden
description: "Gestionnaire de mots de passe sécurisé"
image: /assets/images/projets/secure_communication.svg
logiciels: true
order_logiciels: 7
external_name: "En savoir plus sur Bitwarden"
external_url: https://bitwarden.com/fr-fr/
pdf_local: "/assets/pdf/bitwarden_tutorial_compressed.pdf"
ref: projets-bitwarden
lang: fr
---

# Gestionnaires de mots de passe


Un gestionnaire de mots de passe est coffre-fort virtuel et permett de stocker et de gérer en toute sécurité les mots de passe et autres informations sensibles. Les gestionnaires de mots de passe offrent une solution pratique pour générer des mots de passe complexes et uniques pour chaque service utilisé, réduisant ainsi le risque de violation de données. L'utilisation d'un gestionnaire de mots de passe élimine le besoin de se souvenir de nombreux mots de passe différents, simplifiant la vie des utilisateurs tout en augmentant la sécurité.

# Côté client, Bitwarden

Bitwarden offre des applications pour diverses plateformes, y compris Windows, macOS, Linux, iOS, et Android, ainsi que des extensions de navigateur pour une intégration transparente avec les navigateurs web. Sa nature open source permet une transparence totale, permettant aux experts en sécurité de vérifier le code pour des vulnérabilités potentielles. En outre, Bitwarden propose des fonctionnalités telles que l'authentification à deux facteurs (2FA), le partage sécurisé d'éléments, et des outils de gestion pour les équipes et les entreprises.

# Côté serveur, Vaultwarden

Côté serveur InterHop administre son instance Vaultwarden[^vaultwarden] une version auto-hébergée de Bitwarden, écrite en Rust. Elle est idéale pour avoir un contrôle total sur des données.

# Notre tutoriel de connexion


{% pdf {{ page.pdf_local }} no_link width=100% height=1000px %}

Votre navigateur ne supporte pas les PDF. Vous pouvez <a href="{{ page.pdf_local }}">télécharger le fichier PDF</a>.

[^vaultwarden]: [https://github.com/dani-garcia/vaultwarden](https://github.com/dani-garcia/vaultwarden){:target="_blank"}
