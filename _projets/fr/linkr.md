---
title: LinkR
description: "Plateforme collaborative de Data Science en santé"
image: /assets/images/projets/linkr-official.png
show_image: false
une: true
order_une: 2
logiciels: true
order_logiciels: 2
ref: projets-linkr
lang: fr
---


<div style="display: flex; align-items: center; justify-content: space-between;">
    <div style="flex-grow: 1; display: flex; align-items: center;">
        <h1 style="margin: 0;">LinkR pour la Data Science</h1>
    </div>
    <div>
        <a href="https://framagit.org/interhop/linkr/linkr">
            <img src="{{ site.baseurl }}/assets/images/projets/linkr-official-2.png" alt="Logo - {{ page.title }}" width="123" height="140" />
        </a>
    </div>
</div>


Depuis l'**informatisation** des services hospitaliers, de **nombreuses données** sont disponibles pour la recherche dans le milieu de la **santé**.

Ces données sont stockées dans des **entrepôts de données de santé** (EDS), au sein des hôpitaux.

A l'heure actuelle, pour pouvoir utiliser ces données pour la recherche, il faut avoir des **connaissances poussées** en **programmation**. En effet, manipuler ces données nécessite des compétences en SQL, R et Python notamment.

La réalisation d'études sur EDS nécessite une **collaboration étroite** entre **cliniciens**, **data scientists** et **statisticiens**.

Il existe des **plateformes de travail collaboratif** telles que Jupyter Notebook[^jup], où le code est mêlé à l'affichage des résultats de l'exécution de ce code. Néanmoins, cette plateforme nécessite des connaissances en programmation et a des fonctionnalités limitées, n'étant pas créée dans le but précis de la création d'études sur données d'EDS.

D'autre part, il existe des **logiciels d'analyse statistique**, avec des **interfaces graphiques** dites "no-code", permettant de réaliser des analyses statistiques sans nécessiter de connaissances a priori en programmation. Ce type de logiciel ne permet habituellement pas de modifier le code, ce qui est limitant pour le data scientist *et le développement de nouvelles fonctionnalités*.

C'est pourquoi **LinkR** a été créée.

C'est une application web qui permet :

- au ![clinicien](https://img.shields.io/badge/-clinicien-blue) : d'accéder aux données de santé avec une **interface graphique**, ne nécessitant pas de connaissances en programmation
- au ![data scientist](https://img.shields.io/badge/-data_scientist-3EA646) : de manipuler les données via une interface de programmation **R** et **Python**
- à l'![étudiant](https://img.shields.io/badge/-étudiant-FF6347) : d'apprendre la manipulation des données médicales, à la fois en parcourant les dossiers des patients et manipulant les données agrégées

Ainsi, elle permet aux **différents intervenants** impliqués dans la recherche médicale sur EDS de travailler de façon **collaborative**, sur une **plateforme commune**.

Du fait de cette plateforme commune adaptée aux ![cliniciens](https://img.shields.io/badge/-cliniciens-blue) et aux ![data scientists](https://img.shields.io/badge/-data_scientists-3EA646), les études peuvent être réalisées **beaucoup plus vite**, et avec l'apport de la **spécificité de chacun** : l'expertise médicale du clinicien et l'expertise des données du data scientist.

[^jup]: [Jupyter Notebook](https://jupyter.org/){:target="_blank"},

## Vue d'ensemble

LinkR permet l'analyse de données provenant de **différentes sources** :

- des données d'**EDS hospitaliers**
- des données issues de **bases de données partagées**, telles que les bases de données MIMIC ou AmsterdamUMCdb
- des données issues de **recueils de données**, sous forme de fichiers CSV ou Excel

Une fois ces données importées dans LinkR, vous pouvez créer des **études**. Chaque étude comprendra un ou plusieurs subsets de patients.

<p align="center">
  <img src="https://pad.interhop.org/uploads/b2832a77-29aa-488d-bd1e-2c37b19761c2.svg" alt="Structure des données" style="display:block; margin:auto;"><br /><br />
  <em>Organisation des données au sein de LinkR</em>
</p>

Une fois une étude créée, il faut la configurer.

Le principe est simple : une étude comporte une page "**Données individuelles**" et une page "**Données agrégées**".


### Données individuelles

Dans la page des **données individuelles**, je créé, selon **mes besoins**, un template de **dossier médical**.

Par exemple, si mon étude porte sur les pneumopathies bactériennes, je vais créer :

- un onglet où seront affichés les **compte-rendus** des **imageries** médicales
- un onglet où seront affichés les **résultats bactériologiques**
- un onglet où seront affichés les **traitements antibiotiques** reçus par le patient

Avantages :

- le ![clinicien](https://img.shields.io/badge/-clinicien-blue) peut donc visualiser les données des patients comme il a l'habitude de le faire, sous forme de dossier médical.
- le ![data scientist](https://img.shields.io/badge/-data_scientist-3EA646) peut, s'il le souhaite, modifier le code permettant la visualisation de données.
- l'![étudiant](https://img.shields.io/badge/-étudiant-FF6347) peut s'initier à la lecture d'un dossier clinique

<p align="center">
  <img src="https://pad.interhop.org/uploads/8aa2af3b-16d9-4e9a-a7f0-7c5ab126d8ae.png" alt="Données individuelles" style="border:solid 1px; color:#CECECE; padding:5px;">
  <em>Onglets de données individuelles avec un plugin "Console R"</em>
</p>


### Données agrégées

Une fois que cela est fait, nous pouvons **poursuivre notre étude** via l'analyse des **données agrégées**.

De la même façon, je créé des **onglets**, correspondant aux **différentes étapes de mon étude** :

- pour visualiser la distribution des données et éliminer les **données aberrantes**
- pour créer et appliquer des **critères d'exclusion**
- pour créer et visualiser le **flowchart**
- pour réaliser les **statistiques** de mon étude
- pour importer une **bibliographie**
- pour créer le **rapport** de mon étude

Chacun de ces exemples correspond à ce qu'on appelle un **plugin** : c'est un script permettant d'ajouter une fonctionnalité à l'application.<br />Nous aurons donc un plugin "Flowchart", un plugin "Bibliographie" etc.

Avantages :

- le ![clinicien](https://img.shields.io/badge/-clinicien-blue) peut réaliser les statistiques sur les données, sans connaissances en programmation
- le ![data scientist](https://img.shields.io/badge/-data_scientist-3EA646) peut, encore une fois, modifier le code des onglets, afin d'ajouter du code non proposé par les plugins.
- l'![étudiant](https://img.shields.io/badge/-étudiant-FF6347) peut s'initier aux biostatistiques

<p align="center">
  <img src="https://pad.interhop.org/uploads/99d98f74-0b2a-4118-b42c-2aae450dd5b3.png" alt="Données agrégées" style="border:solid 1px; color:#CECECE; padding:5px;">
  <em>Onglets de données agrégées avec un plugin "Figure (ggplot2)"</em>
</p>

Les différents intervenants peuvent **communiquer** via l'onglet "**Messages**".

Ainsi :
- le ![clinicien](https://img.shields.io/badge/-clinicien-blue) peut interroger le ![data scientist](https://img.shields.io/badge/-data_scientist-3EA646) s'il a des questions portant sur la programmation
- le ![data scientist](https://img.shields.io/badge/-data_scientist-3EA646) peut interroger le ![clinicien](https://img.shields.io/badge/-clinicien-blue) s'il a des questions portant sur la médecine
- l'![étudiant](https://img.shields.io/badge/-étudiant-FF6347) peut interroger le ![clinicien](https://img.shields.io/badge/-clinicien-blue) ou le ![data scientist](https://img.shields.io/badge/-data_scientist-3EA646) selon ses besoins

Le code peut être **exécuté** au sein des messages.

<p align="center">
  <img src="https://pad.interhop.org/uploads/35e64252-8e70-462d-9ef0-273d84995404.png" alt="Messages" style="border:solid 1px; color:#CECECE; padding:5px;">
  <em>Echange de messages entre les membres de l'étude "Pneumopathies bactériennes"</em>
</p>

## Interopérabilité

LinkR s'appuie sur le **modèle de données commun OMOP**, qui est un modèle utilisé par de nombreux hôpitaux dans le monde.

Ceci permet l'**interopérabilité des données**, il est ainsi possible de réaliser des **études multicentriques** facilement, en ne codant l'étude qu'une fois.

LinkR intègre des outils d'**alignement des concepts**, permettant de faire correspondre les concepts contenus dans différentes bases de données.<br />Par exemple pour faire correspondre le concept "123456 - Fréquence cardiaque" du centre 1 au concept "456789 - FC (bpm)" du centre 2.

Il est également possible de partager **tout ce que vous développez** au sein de LinkR.

A savoir :

- les **études** : si vous avez terminé votre étude, vous pouvez la partager en un clic à d'autres centres. Ils pourront, depuis l'application, **télécharger votre étude** et la **lancer sur *leurs* données**.
- les **scripts** de data cleaning (quel data scientist n'a jamais fait un script pour filtrer les faux poids et les fausses tailles, et n'aurait pas aimé en avoir un déjà développé à disposition ?)
- les **plugins** que l'on a vus plus haut
- les **sets de données** : les codes permettant de charger les sets sont partagés, et non les données
- les **terminologies** : vous pouvez partagez à la fois le code permettant de charger les concepts, mais aussi les **concepts** des différentes terminologies ainsi que les **alignements** que vous avez réalisés

<p align="center">
  <img src="https://pad.interhop.org/uploads/2a339440-b8aa-44ee-8236-4e4e632faf67.png" alt="Interopérabilité" style="width:100%; max-width:700px; display:block; margin:auto;"><br />
  <em>Interopérabilité algorithmique proposée par LinkR</em>
</p>

En combinant l'**interopérabilité des données** ainsi que l'**interopérabilité algorithmique** (partage des études, scripts et plugins), LinkR permet rééllement de faire de la recherche selon les **principes de la "Science Ouverte" ou open science[^open_science]**.

Ceci permet également une **protection des données de santé** : LinkR suit une **architecture décentralisée**, ce sont les **algorithmes** qui **voyagent** et non les données.

<p align="center">
  <img src="https://pad.interhop.org/uploads/dc0a4f48-82b6-4769-9eec-43bd2ff3af62.png" alt="Catalogue de plugins" style="border:solid 1px; color:#CECECE; padding:5px; height:auto;"><br />
  <em>Catalogue de plugins disponibles sur le git d'InterHop. L'installation d'un plugin se fait en un clic.</em>
</p>

[^open_science]: ["Science Ouverte" ou open science](https://fr.wikipedia.org/wiki/Science_ouverte){:target="_blank"}


## Spécificités techniques

LinkR est une application web créée avec la librairie **Shiny** en **R**.

C'est un logiciel **open source**, sous licence **GPLv3**.

L'application peut être installée via <a target="_blank" href="https://www.shinyproxy.io/">**ShinyProxy**</a> et être protégée par une authentification par SSO.

Pour **installer** l'application, suivez les étapes <a target="_blank" href="https://framagit.org/interhop/linkr/linkr">indiquées ici</a>.

Vous pouvez également <a target="_blank" href="https://linkr.interhop.org">tester l'application ici</a> :

- avec les logs "test" / "test" pour ShinyProxy (cf première image ci-dessous)
- en cliquant sur "LinkR - test - v0.2..."
- puis de nouveau avec les logs "test" / "test" pour LinkR (cf deuxième image ci-dessous)

<img src="https://pad.interhop.org/uploads/70a07834-7945-419a-9308-a89d16039a8e.png" alt="Connexion à LinkR" style="max-width:400px; display:block; margin:auto;" />
<img src="https://pad.interhop.org/uploads/773cf94e-acd7-4949-bbdb-66ed11c0c4be.png" alt="Connexion à LinkR" style="max-width:400px; display:block; margin:auto;" />

## L'aide proposée par InterHop

**InterHop** propose pour ses outils numériques un l'**hébergement de données** sur ses serveurs [**HDS** (hébergeur de données de santé)]({{ site.baseurl }}/projets/hds), loués chez GPLExpert.

L'association propose également d'utiliser <a href="{{ site.baseurl }}/projets/goupile">**Goupile**</a>, un outil d'eCRF, permettant de **recueillir des données** directement sur les serveurs HDS.

Toujours dans le cadre d'InterHop il est possible d'**utiliser LinkR** sur les **serveurs HDS**.

Ainsi, il sera possible de recueillir les données puis de réaliser les études au **même endroit**, de façon sécurisée.

InterHop propose aussi un **support** à l'**utilisation de LinkR**.

<p><a style="text-align: center;" class="button alt" type="blue" href="{{ site.baseurl }}/2023/04/06/brochure-recherche">Voir notre brochure "Recherche"</a></p>

## Collaboration

Le projet étant open source et réalisé par des bénévoles, **toute contribution est la bienvenue** !

## Résumé

<div class="alert alert-blue" markdown="1">
- **Synopsis:** Plateforme collaborative de data Science
- **Site Internet:** [linkr.interhop.org](https://linkr.interhop.org){:target="_blank"}
- **Techno:** R Shiny
- **Gitlab:** [framagit.org/interhop/linkr/linkr](https://framagit.org/interhop/linkr/linkr){:target="_blank"}
- [Tutoriel en vidéo](https://peertube.interhop.org/c/linkr/videos){:target="_blank"}
- Vous pouvez aussi nous contacter via la messagerie instantanée et opensource [Element]({{site.baseurl}}/projets/element)
</div>
