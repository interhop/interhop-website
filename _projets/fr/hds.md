---
title: "Nos serveurs"
description: "Certifiés et sécurisés"
image: /assets/images/projets/hds.svg
show_image: false
projets_outils: true
order_outils: 2
projets_collab: false
ref: projets-hds
lang: fr
---

InterHop installe des logiciels libres pour la santé. Ces logiciels sont hébergés sur des machines particulières que l'on appelle serveurs. 




Pour la santé le niveau de sécurité et de disponibilité est renforcée : la certification des Hébergeurs de Données de Santé (HDS) est obligatoire

# Hébergeur de Données de Santé HDS?

#### Une données personnelles de santé

Le Règlement Général sur la Protection des Données (RGPD), mis en vigueur le 25 mai 2018, est un outil de régulation proposé par l’Union Européenne.

Ce règlement propose les définitions de données personnes et de données de santé.

Les données à caractère personnel concernent :
> “toute information se rapportant à une personne physique identifiée ou identifiable”[^rpgd_def].

Une personne physique peut être identifiée, soit directement par son nom ou prénom par exemple, soit indirectement 
> “par référence à un identifiant, tel qu’un numéro d’identification, des données de localisation, un identifiant en ligne, ou à un ou plusieurs éléments spécifiques propres à son identité physique, physiologique, génétique, psychique, économique, culturelle ou sociale”[^rpgd_def]. La voix et l’image font partie des identifiants indirects.

> Des données personnelles de santé sont des "données à caractère personnel relatives à la santé physique ou mentale d'une personne physique, y compris la prestation de services de soins de santé, qui révèlent des informations sur l'état de santé de cette personne"[^rpgd_def].

[^rpgd_def]: [Article 4 - EU RGPD - "Définitions"](https://www.privacy-regulation.eu/fr/4.htm){:target="_blank"}

#### Quel cadre légal ?
L’article L.1111-8[^l1111] du code de la santé publique stipule : 
> Toute personne qui héberge des données de santé à caractère personnel recueillies à l'occasion d'activités de prévention, de diagnostic, de soins ou de suivi social et médico-social, pour le compte de personnes physiques ou morales à l'origine de la production ou du recueil de ces données ou pour le compte du patient lui-même [doit être] titulaire d'un certificat de conformité.

Cet article est issu de l’Ordonnance n° 2017-27 du 12 janvier 2017. Il différencie[^apssis] : 
> - les données hébergées sur support papier ou sur support numérique dans le cadre d'un service d'archivage électronique, pour lesquelles l’hébergeur doit être agréé par le ministre chargé de la culture[^apssis]
> - les données hébergées sur support numérique (hors cas d’un service d’archivage électronique), pour lesquelles l’hébergeur doit être titulaire d’un certificat de conformité, délivré par des organismes de certification accrédités par l'instance française d'accréditation[^apssis]


En conclusion, toute application qui traite ou stocke des données de santé pour le compte d'un responsable de traitement[^resp_trt_hds] (au sens RPGD) doit donc être dans un environnement HDS.


[^ordo_12012017]: [Ordonnance n° 2017-27 du 12 janvier 2017 relative à l'hébergement de données de santé à caractère personnel](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000033860770/){:target="_blank"}

[^apssis]: [APSSIS - Point d’actualité sur la certification hébergeur de données de santé](https://www.apssis.com/actualite-ssi/335/point-d-actualite-sur-la-certification-hebergeur-de-donnees-de-sante.htm){:target="_blank"}

[^resp_trt_hds]: [Hébergement de données de santé et RGPD](https://www.village-justice.com/articles/hebergement-donnee-sante-rgpd,30355.html){:target="_blank"}

[^l1111]: [Code de Santé Publique](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006072665/LEGISCTA000006185255/2021-02-02/){:target="_blank"}

#### La certification

Depuis le 1er avril 2018, l’agrément Hébergeur de Données de Santé HDS délivré par l’ASIP Santé devenue l'Agence du Numérique en Santé (ANS) a été remplacé par une obligation réglementaire d’obtenir une certification HDS.

Le principe de certification repose sur plusieurs référentiels[^ref_hds], audités par un organisme indépendant, accrédité par le COFRAC[^cofrac] (Comité Français d’Accréditation).

"Le certificat est délivré pour une durée de trois ans, par l’organisme certificateur et chaque année, un audit de surveillance est effectué."[^cert_hds]

Voici la liste des organisations ont déjà obtenu la certification HDS[^cert_hds_list].

[^ref_hds]: [Les référentiels de la procédure de certification](https://esante.gouv.fr/services/hebergeurs-de-donnees-de-sante/les-referentiels-de-la-procedure-de-certification){:target="_blank"}

[^cofrac]: [Comité Français d'Accréditation (COFRAC)](https://www.cofrac.fr/){:target="_blank"}

[^cert_hds]: [Certification des hébergeurs de données de santé](https://esante.gouv.fr/labels-certifications/hds/certification-des-hebergeurs-de-donnees-de-sante){:target="_blank"}

[^cert_hds_list]: [Liste des hébergeurs certifiés pour les données de santé à caractère personnel ](https://esante.gouv.fr/offres-services/hds/liste-des-herbergeurs-certifies){:target="_blank"}

#### Les périmètres

Il en existent deux types[^cert_hds] : 

1. Hébergeur d'infrastructure physique

![](https://esante.gouv.fr/sites/default/files/inline-images/Prestation_h%C3%A9bergeurs_infrastructure_physique.png)

2. Hébergeur infogéreur

![](https://esante.gouv.fr/sites/default/files/inline-images/Prestation_h%C3%A9bergeur_infog%C3%A9reur.png)









# Notre HDS : GPLExpert

#### Présentation
Depuis décembre 2019[^gpl_hds], GPLExpert est officiellement certifié HDS.

L’activité 1 est sous traitée dans un data center prestataire de GPLExpert.

Les données de santé sont donc strictement hébergées en France. Les sièges sociaux des entreprises sous-traitantes sont exclusivement localisés en France.


[^gpl_hds]: [GPLExpert - Hébergement de Données de Santé (HDS)](https://gplexpert.com/hebergement-donnees-sante-hds/){:target="_blank"}

#### Procédure de choix

Nous avons lancer notre procédure de choix en juillet 2020.

Nos critères étaient les suivants : 
- le prix :  impossible pour InterHop de payer des centaines de milliers d'euros, notre budget mensuel étant d'environ 1000 euros par mois
- la qualité des services : disponibilité des machines. Nous proposons des services pour la santé, il faut que les serveurs soient disponibles et fonctionnels 24h/24 7j/7
- l'équipe : nous avons besoin d'avoir un contact priviligié avec notre prestataire HDS. Ce choix engage en effet notre responsabilité et surtout la confiance que les personnes mettent dans notre association
- notre priorité : nous avons besoin d'être convaincu·es de l'indépendance vis à vis de l'extraterritorialité du droit de notre prestataire. Nous ne voulons absolument pas que les logiciels que nous proposons soient sous la dépendance d'intérêts extra-européens. Ce critère est très contraignant. Il ne suffit pas que les serveurs soient localisés en France ou en Europe ; il faut aussi que les sièges sociaux des entreprises soient strictement en Europe.

En plus d'être uniquement soumises aux droits français et Européen, GPLExpert ne travaille d'une aucune façon que ce soit avec les clouds américains comme Microsoft Azure, Amazon Web Service ou Google Cloud Plateform. Enfin aucun outil ou logiciel soumis au droit américain n'est installé par défaut sur leurs serveurs.

#### En cas de résolution d'incident : Bug technique

De base, ni GPLExpert ni InterHop n'a accès aux données de santé stockées.

Parfois :-) des incidents techniques se produisent. Dans ces cas il est nécessaire d'avoir accès à la machine (au serveur).

Deux cas de figure peuvent se produire. Dans le premier cas un accès à la machine simple suffit. Il n'est donc pas nécessaire d'avoir accès à la base de données stockée sur cette même machine. Dans ce cas un·e ingénieur·e de GPLExpert. sous la responsabilité d'InterHop, redémarre le programme ou la machine.

Dans des cas plus rares, il est nécessaire d'avoir accès à la base de données où sont stockées les informations de santé. Dans cette situation un·e professionnel·le de santé soumis·e au secret médical accède  à la base de données. Plusieurs membres de l'association sont soignant·e et ingénieur·e. S'il est nécessaire d'avoir des expertises métiers poussées (réseaux, base de données, langage de programmation spécifique) le·la responsable médical·e peut demander de l'aide. Tous ces processus sont consignés dans le Plan d'Assurance Qualité (PAQ) que InterHop et GPLexpert remplissent conjointement.

# Architecture proposée

#### Reverse proxy
L'architecture technique déployée par InterHop avec GPLExpert pour protéger efficacement les données de santé contient une  Zone DMZ[^dmz] ou une zone démilitarisée (DMZ  demilitarized zone) est un sous-réseau séparé du réseau local et isolé de celui-ci et d'Internet (ou d'un autre réseau).
En clair, lorsque l'utilisatrice ou l'utilisateur demande à avoir accès à une page web l'information passe par un reverse-proxy. Ainsi, au lieu d'accéder directement à la machine stockant les données, les flux transitent via une machine tierce.

On sein de cette DMZ nous avons mis en place ce que l'on appelle un reverse proxy qui peut servir de pont entre un client et les serveurs web HDS, en récupérant les ressources du serveur web (ou des serveurs web) et en les fournissant au client comme si les ressources provenaient directement du reverse proxy.

![](https://geektechstuff.files.wordpress.com/2020/05/reverse_proxy_h2g2bob.svg_.png?w=656)

Le Reverse-Proxy permet de garantir un niveau de sécurité optimal sur nos sites web ainsi que d'être conforme aux certifications HDS.

#### Environnements

Nous avons installé au sein de  l'**environnement de Développement** (machines de test) un clone de l'**environnement de Production** (architecture cible HDS). Pour ceci nous utiliserons :
- Ansible pour automatiser des processus de déploiement
- Nginx en tant que serveur (machine "WEB SERVER") et reverse proxy (machine "PROXY")

Le code source permettant le déploiement des machines est libre. Il est accessible ici : [https://framagit.org/interhop/hds](https://framagit.org/interhop/hds){:target="_blank"}

NGINX est un serveur web qui peut également être utilisé comme reverse proxy et/ou load balancer. Ce sont les options de reverse proxy que nous utilisons pour la machine "PROXY".

Ansible est un projet opensource disponible sous licence GNU GPLv3. Le code source est disponible : [https://github.com/ansible/ansible](https://github.com/ansible/ansible){:target="_blank"}

Nginx est un projet opensource disponible sous licence BSD. Le code source est disponible : [https://github.com/nginx/nginx](https://github.com/nginx/nginx){:target="_blank"}

Ansible et Nginx sont intégrés à la liste des logiciels libres préconisés par l’État français dans le cadre de la modernisation globale de ses systèmes d’informations (SI)[^sill].

[^sill]: [Socle Interministériel de Logiciels Libres (SILL)](https://code.gouv.fr/sill/list){:target="_blank"}

[^dmz]: [Zone démilitarisée (informatique)](https://fr.wikipedia.org/wiki/Zone_d%C3%A9militaris%C3%A9e_(informatique)){:target="_blank"}


# Budget

<div class="alert alert-red" markdown="1">
Plus de 1000 euros / mois.

Nous louons déjà ces serveurs HDS et tu peux nous aider en donnant. :-)
L’argent servira d'abord à financer des serveurs dédiés à la santé (HDS).
</div>

Aide-nous en faisant un don pour InterHop.
<p><a style="text-align: center;" class="button alt" type="blue" href="{{ site.baseurl }}/dons/">Dons !</a></p>
