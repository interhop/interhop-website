---
title: PrivateBin
description: "Partage de fichiers"
image: /assets/images/projets/transfer.png
logiciels: true
sso: true
order_logiciels: 4
order_sso: 2
url_sso: https://privatebin.interhop.org/
external_name: "En savoir plus sur PrivateBin"
external_url: https://privatebin.info/
ref: projets-privatebin
lang: fr
---


PrivateBin est une application simple et sécurisée pour partager des fichiers de manière rapide, même de grande taille. Il suffit de glisser-déposer un fichier pour générer un lien de partage, sans limite de taille, tout en offrant des options de confidentialité, comme une durée de vie limitée des fichiers envoyés.

L'application est disponible sur plusieurs plateformes et garantit la sécurité des transferts grâce à un chiffrement de bout en bout. Elle est idéale pour ceux qui recherchent une solution facile et rapide pour partager des données sans complications.

# L'instance d'InterHop

L'instance PrivateBin d'InterHop est disponible ici : <a href="https://privatebin.interhop.org" target="_blank">privatebin.interhop.org</a>.
