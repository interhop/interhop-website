---
title: HedgeDoc
description: "Editeur de texte en markdown"
image: /assets/images/projets/hedgedoc.png
logiciels: true
sso: true
order_logiciels: 5
order_sso: 3
url_sso: https://pad.interhop.org/auth/oauth2
external_name: "En savoir plus sur HedgeDoc"
external_url: https://hedgedoc.org/
ref: projets-hedgedoc
lang: fr
---

HedgeDoc est une plateforme collaborative en ligne conçue pour la rédaction et le partage de documents en temps réel. 

HedgeDoc utilise le langage de balisage Markdown, qui permet de créer facilement des documents bien formatés. L'interface conviviale de HedgeDoc permet à plusieurs utilisateurs de travailler simultanément sur un même document, facilitant ainsi la collaboration et le brainstorming. Les utilisateurs peuvent également intégrer des diagrammes, du code, des vidéos et bien plus encore, rendant HedgeDoc très versatile. Grâce à sa capacité à sauvegarder automatiquement les modifications et à maintenir un historique des versions, HedgeDoc est un outil précieux pour la gestion de projets, la documentation technique et la rédaction collaborative.

# Markdown

Markdown est un langage de balisage léger conçu pour formater du texte en utilisant une syntaxe simple et facile à lire. 

Markdown se distingue par sa simplicité. Les balises Markdown permettent de formater le texte en ajoutant des éléments comme des titres, des listes, des liens, des images et des blocs de code, sans avoir à apprendre une syntaxe complexe. Cette simplicité et cette facilité d'utilisation ont fait de Markdown un choix populaire pour les rédacteurs de contenu et les développeurs, permettant une rédaction rapide et un rendu clair et structuré.

# L'instance d'InterHop

L'instance d HedgeDoc d'InterHop est disponible ici : <a href="https://pad.interhop.org" target="_blank">pad.interhop.org</a>.
