---
title: Cryptpad
description: "Stockage sécurisé"
image: /assets/images/projets/cryptpad.png
logiciels: true
order_logiciels: 6
sso: true
order_sso: 4
url_sso: https://cpad.interhop.org/login/
external_name: "En savoir plus sur Cryptpad"
external_url: https://cryptpad.fr/
ref: projets-cryptpad
lang: fr
---

# Chiffrement de bout en bout

CryptPad est un outil de collaboration en ligne qui met l'accent sur la confidentialité et la sécurité des données. Contrairement à de nombreux services de collaboration qui stockent les données de manière non chiffrée sur leurs serveurs, CryptPad utilise un chiffrement de bout en bout, garantissant que seul l'utilisateur et les personnes avec qui il partage ses documents peuvent y accéder. Cette plateforme offre une suite d'applications intégrées, telles que des éditeurs de texte, des feuilles de calcul, des présentations et des outils de prise de notes, permettant une collaboration en temps réel sans compromettre la sécurité. CryptPad est idéal pour les équipes et les organisations qui souhaitent conserver un contrôle total sur leurs données tout en bénéficiant des avantages de la collaboration en ligne.

# Open Source et Autohébergement

CryptPad est un projet open source. Cette transparence permet aux utilisateurs de vérifier le code source pour des vulnérabilités potentielles et d'assurer leur confiance dans la sécurité de la plateforme. En outre, CryptPad peut être auto-hébergé, offrant ainsi une flexibilité et un contrôle supplémentaires pour les utilisateurs qui souhaitent gérer leur propre infrastructure.

# Nos instances de Cryptpad


InterHop.org administre des services nécessitant un <a href="{{ site.baseurl}}/projets/hds">Hébergeur de Données de Santé (HDS)</a> et des services qui ne le nécessitent pas.

Deux versions de Cryptpad sont donc disponibles au sein de l'association InterHop
- <a href="https://cryptpad.hds.interhop.org/" target="_blank">cryptpad.hds.interhop.org</a>
- <a href="https://cpad.interhop.org" target="_blank">cpad.interhop.org</a>
