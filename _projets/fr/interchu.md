---
ttle: InterCHU
description: Le réseau
image: /assets/images/projets/collaboration.png
show_image: false
une: false
projets_outils: false
projets_collab: true
order_collab: 1
ref: projets-interchu
lang: fr
---


# L’utopie


Conformément à [notre manifeste]({{ site.baseurl }}/manifeste), l'association InterHop s'engage à garantir un numérique éthique dans le secteur de la recherche en santé.

Dans le cadre du réseau interCHU nous souhaitons particulièrement mettre en avant deux des valeurs promues.

#### Partage

La solidarité, le partage et l’entraide entre les différents acteurs sont les valeurs centrales en santé. Au même titre qu’Internet est un bien commun, le savoir en informatique médicale doit être disponible et accessible à tous. Nous voulons donc promouvoir la dimension éthique particulière qu’engendre l’ouverture de l’innovation dans le domaine médical et nous voulons prendre des mesures actives pour empêcher la privatisation de la médecine.

#### Interopérabilité

L’interopérabilité des systèmes informatisés est le moteur du partage des connaissances et des compétences ainsi que le moyen de lutter contre l’emprisonnement technologique. En santé, l’interopérabilité est le gage de la reproductibilité de la recherche, du partage et de la comparaison des pratiques pour une recherche performante et transparente. Il ne peut pas y avoir d’interopérabilité sans communauté.

## Contexte

Les logiciels hospitaliers enregistrent quotidiennent et automatiquement des volumes importants de données pour le soin ou la facturation. Il est possible de réutiliser ces données pour un autre usage, comme la recherche ou le décisionnel.

Pour cela, plusieurs centres hospitaliers ont développé un entrepôt de données, qui après nettoyage des données et homogénéisation de la structure, permet de croiser des données issues au départ de logiciels différents[^Degoul].

Certains ont ensuite transformé les données dans un modèle de données commun (OMOP), qui permet de s'affranchir des vocabulaires et logiciels utilisés localement, en standardisant la structure de données et le vocabulaire <sup>[^Lamer] [^Paris]</sup>.

Aujourd'hui, les CHU d'Amiens, Caen, Paris, Lille, Rouen, Toulouse, Marseille, Rennes, Foch ... collaborent pour implémenter leurs entrepôts d'anesthésie-réanimation au format OMOP : partage des spécifications, des mappings de vocabulaires, des scripts d'implémentation, et des outils.


# Le réseau InterCHU

Le réseau InterCHU s’adresse préférentiellement aux personnes francophones (ingénieur·es du secteur public, …) utilisant les modèles d'interopérabilité comme OMOP[^omop] ou FHIR.


Au sein des réunions InterCHU, nous voulons :
- Faire des points d’étape des diverses transformations réalisées dans les centres hospitaliers. Nous avons du code et de l’expérience à partager !
- Faire un état des lieux des algorithmes déjà implémentés avec OMOP.
- Parler des designs d’études possibles pour utiliser ces algorithmes dans nos hôpitaux à brève échéance.
- Partager le travail de futurs développements entre les hôpitaux.
- Parler de l’harmonisation des terminologies en France.
- Parler du développement d’architectures BigData opensources pour la santé. Nous développons au sein d’InterHop une plateforme collaborative de Data Science nommée [LinkR]({{ site.baseurl }}/projets/linkr).

# Programme 2025

Les rencontres de cette année s'annoncent riches en thématiques et en collaborations : 
- Visualisation et dashboarding sur données de santé
- Présentation des outils développés par la communauté InterHop : [Goupile]({{ site.baseurl }}/projets/goupile), [Libre Data Hub](https://libredatahub.org/), [Linkr]({{ site.baseurl }}/projets/linkr)
- Formats d'interopérabilité : FHIR et OMOP
- Outils de sélection de cohorte : [Cohort360](https://docs.cohort360.org/)
- Présentation du groupe numérique de la [SFAR (Société Française d'Anesthésie et de Réanimation)](https://sfar.org/){:target="_blank"}
- Deuxième édition du [Datathon InterHop]({{ site.baseurl }}/projets/datathon)

<p><a style="text-align: center;" class="button alt" type="blue" href="{{ site.baseurl }}/2025/02/07/reunion-interchu">Voir le programme</a></p>

L'ensemble du code produit par l'association InterHop est disponible sur notre [FramaGit]({{ site.baseurl }}/projets/framagit).

Pour être au courant des prochaines réunions vous pouvez nous rejoindre sur nos fils de discussion associatifs en vous inscrivant sur [Element]({{ site.baseurl }}/projets/element).

[^omop]: [Standardized Data: The OMOP Common Data Model](https://www.ohdsi.org/data-standardization/){:target="_blank"}

[^Degoul]: [Degoul S, Chazard E, Lamer A, Lebuffe G, Duhamel A, Tavernier B. lntraoperative administration of 6% hydroxyethyl starch 130/0.4 is not associated with acute kidney injury in elective non-cardiac surgery: A sequential and propensity-matched analysis. Anaesth Crit Care Pain Med. 2020 Apr;39(2):199-206. doi: 10.1016/j.accpm.2019.08.002. Epub 2020 Feb 14. PMID: 32068135](https://pubmed.ncbi.nlm.nih.gov/32068135/){:target="_blank"}

[^Lamer]: [Lamer A, Abou-Arab O, Bourgeois A, Parrot A, Popoff B, Beuscart JB, Tavernier B, Moussa MD. Transforming Anesthesia Data Into the Observational Medical Outcomes Partnership Common Data Model: Development and Usability Study. J Med Internet Res. 2021 Oct 29;23(10):e29259. doi: 10.2196/29259. PMID: 34714250; PMCID: PMC8590192](https://pubmed.ncbi.nlm.nih.gov/34714250/)

[^Paris]: [Paris N, Lamer A, Parrot A. Transformation and Evaluation of the MIMIC Database in the OMOP Common Data Model: Development and Usability Study. JMIR Med Inform. 2021 Dec 14;9(12):e30970. doi: 10.2196/30970. PMID: 34904958](https://pubmed.ncbi.nlm.nih.gov/34904958/){:target="_blank"}
