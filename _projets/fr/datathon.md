---
title: Datathon
description: "Hackathon sur données"
image: /assets/images/projets/heart.png
show_image: false
une: true
order_une: 3
projets_outils: false
projets_collab: true
order_collab: 2
ref: projets-datathon
lang: fr
---



L'association InterHop organisera un hackathon sur données (Datathon) en septembre 2024.


# Un « datathon », qu’est-ce que c’est ?

Un datathon est un événement regroupant sur quelques jours des spécialistes du traitement des données, des soignant·es et des patient·es pour travailler de façon collaborative sur de la programmation informatique.

En seulement deux à trois jours, les équipes sont chargées de développer une solution en réponse à une problématique de recherche spécifique.

Le Datathon se destine aux professionnel·les de santé, aux patient·es et aux data scientists. Il n'y a aucun prérequis nécessaire, en particulier il n'est pas nécessaire d'avoir des connaissances en programmation pour participer. Le but du datathon est de créer du lien entre les différents acteurs impliqués dans la recherche autour de ces données.

Voici une vidéo de présentation du datathon des hôpitaux de Paris de 2018 co-organisé avec la Société Française d'Anesthésie et de Réanimation (SFAR) et la Société de Réanimation de Langue Française (SRLF).
<div class="responsive-video-container">
<iframe title="DAT-ICU 2018 : 48h de datathon pour rapprocher réanimateurs et experts en analyse de données" width="560" height="315" src="https://peertube.interhop.org/videos/embed/83f063da-cdd4-45de-8c13-aba5c695ab10" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
</div>


# Modalités pratiques

Le datathon aura lieu à la Maison Des Associations, <a target="_blank" href="https://www.openstreetmap.org/search?query=6%20cours%20des%20alli%C3%A9s%2035000%20Rennes#map=19/48.10478/-1.67646">6 Cours des Alliés à  35000 Rennes</a>.

Il commencera  le jeudi 5 septembre 2024 à 9h et finira le vendredi 6 septembre à 18h.

Voici le programme plus en détails :

### Jeudi 5 septembre


- 08:30 - 09:30 : Accueil des participants, café 
- 09:30 - 12:30 : Présentation des bases de données, de OMOP, de l'infra
  - 09:30 - 09:50 : présentation [OMOP](https://ohdsi.github.io/TheBookOfOhdsi/CommonDataModel.html){:target="_blank"}
  - 09:50 - 10:10 : présentation [LinkR]({{ site.baseurl }}/projets/linkr)
  - 10:10 - 10:30 : présentation Libre Data Hub
  - 10:30 - 11:00 : présentation du [Référentiel national du Bâtiment RNB](https://rnb.beta.gouv.fr/){:target="_blank"}
  - 11:00 - 12:30 : présentation des [sujets](https://interhop.org/2024/06/26/datathon-sujet)
- 14:00 - 17:00 : Datathon
- 17:00 - 18:00 : 10 minutes de feedback par projet

### Vendredi 6 septembre

- 08:00 - 09:00 : Café
- 09:00 - 12:00 : Datathon
- 13:30 - 16:00 : Datathon
- 16:00 - 18:00 : Présentation des projets 20-30 minutes

InterHop met à disposition le logiciel LinkR (disponible sur un simple navigateur) ainsi que des serveurs pour réaliser les traitements de données.
N'oubliez pas d'**apporter votre ordinateur personnel** !

Les frais de location de la salle et des serveurs sont pris en charge par l'association InterHop. 
Nous vous demanderons une participation pour les repas du midi et du soir.
Les frais de transports et de logement restent à votre charge.

Voici le [replay de la réunion d’information sur les modalités pratiques](https://visio.octoconf.com/playback/presentation/2.3/8d7742fb121806737a8c2bc5bd9dbade20963e96-1719486059418){:target="_blank"} de réalisation du datathon.

# Sujets

Chacun·une est libre de venir avec ses propres sujets.

Voici une liste préliminaire disponible dans ce billet de blog : [https://interhop.org/2024/06/26/datathon-sujet]({{ site.baseurl }}/2024/06/26/datathon-sujet)

# Outils

La principale source de données utilisée sera la base de données MIMIC au format OMOP[^medrxiv]. 

Le modèle commun de donnée OMOP[^cdm] et notre plateforme Low Code de Data Science [LinkR]({{ site.baseurll}}/projets/linkr) seront de la partie.


Dans son format **complet** elle contient plus de 45000 patient·es hospitalisé·es en réanimation.
Dans ce format il est nécessaire de passer un diplome s'appelant le CITI : [https://physionet.org/about/citi-course/](https://physionet.org/about/citi-course/){:target="_blank"}<br/>
Il faut donc : 
1. Créer un [compte utilisateur sur physionet](https://physionet.org/login/?next=/settings/credentialing/){:target="_blank"}
2. Suivre une petite formation : [CITI Recherche de données](https://physionet.org/about/citi-course/){:target="_blank"}
3. Soumettre votre formation [ici](https://physionet.org/settings/training/){:target="_blank"}
4. [Signer l'accord d'utilisation](https://physionet.org/login/?next=/sign-dua/mimiciii/1.4/){:target="_blank"} des données pour le projet
5. [Allez sur la page dédiée à  l'évènement sur Physionet](https://physionet.org/events/fyETG69ISUJT/){:target="_blank"}

#### CITI, une obligation pour le datathon ?

Ces étapes sont **obligatoires** pour avoir avoir accès à la version complète de MIMIC-OMOP. 

Si vous ne souhaitez pas prendre du temps pour passer ce diplôme vous pourrez tout de même avoir accès à MIMIC. :-)<br/>
Par contre nous vous mettrons à disposition un jeu de patient·es **restreint** ; contenant une **centaine** de patient·es seulement.

Aussi si vous souhaitez utiliser un autre jeu de données le CITI n'est évidemment pas nécessaire.

[^medrxiv]: [MIMIC in the OMOP Common Data Model](https://www.medrxiv.org/content/10.1101/2020.08.14.20175141v1){:target="_blank"}

[^git_mimic_omop]: [MIMIC OMOP](https://github.com/MIT-LCP/mimic-omop){:target="_blank"}

[^cdm]: [https://ohdsi.github.io/CommonDataModel/](https://ohdsi.github.io/CommonDataModel/){:target="_blank"}

# Contacts

Pour vous inscrire il suffit de remplir ce questionnaire en ligne : *[https://goupile.interhop.org/datathon](https://goupile.interhop.org/datathon)*.

Rejoignez-nous aussi sur messagerie d'équipe [Element]({{ site.baseurl }}/projets/element) !
Voici fil de discussion public dédié au Datathon : [https://matrix.to/#/#datathon:matrix.interhop.org](https://matrix.to/#/#datathon:matrix.interhop.org){:target="_blank"}
