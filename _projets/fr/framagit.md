---
title: Framagit
description: Développeurs, collaborons !
image: /assets/images/projets/framagit.png
show_image: false
une: false
projets_outils: false
projets_collab: true
order_collab: 4
ref: projets-framagit
lang: fr
---

Framagit est une instance de GitLab[^gitlab] maintenu par l'association loi 1901 [Framasoft](https://framasoft.org/fr/){:target="_blank"}.

# FramaGit

GitLab est une plateforme de gestion de dépôts Git[^git] et de développement logiciel qui offre une gamme complète d'outils pour le cycle de vie des application. 
En tant qu'alternative open source à des services comme GitHub, GitLab se distingue par ses multiples instances maintenues par une large communauté comme celle de l'association Framasoft.

Pour chaque dépôt du code il existe une section "Issues" (ou "Problèmes").
> Les issues dans GitLab sont des outils de suivi de tâches et de gestion de projet. Elles permettent aux utilisateurs de signaler des bogues, de proposer des fonctionnalités, de planifier des améliorations ou de suivre tout autre type de tâche. Chaque issue peut être détaillée avec une description, des étiquettes, des assignations à des membres de l'équipe, des dates d'échéance, et peut être liée à des commits, des branches ou des merge requests. Les issues facilitent la collaboration et la communication au sein des équipes en centralisant les discussions et en fournissant une vue d'ensemble des travaux en cours et à venir.




# L'association Framasoft

Framasoft est une association française à but non lucratif, fondée en 2004, qui se consacre à la promotion du logiciel libre, de la culture libre et des services en ligne éthiques. Connue pour ses nombreuses initiatives visant à sensibiliser le public à l'importance de la protection de la vie privée et de la souveraineté numérique, Framasoft propose une multitude de services alternatifs aux grandes plateformes propriétaires. 

Parmi ces initiatives, la campagne « Dégooglisons Internet » a particulièrement marqué les esprits, en offrant des alternatives libres et respectueuses de la vie privée à des services couramment utilisés. Framasoft s'appuie sur une communauté active et engagée pour développer et maintenir ses projets, offrant ainsi des outils accessibles à tous pour une utilisation plus éthique et autonome du web.

# Présentation du dépôt d'InterHop sur Framagit

Le dépôt du code source de l'association InterHop est disponible ici : [framagit.org/interhop](https://framagit.org/interhop){:target="_blank"}.

Framagit, l'instance de GitLab maintenue par Framasoft, fournit à InterHop une plateforme collaborative pour le développement de logiciels libres.

Le code source des principaux projets de l'association est donc entièrement disponible :
- Site Internet Interhop.org : [framagit.org/interhop/interhop-website](https://framagit.org/interhop/interhop-website){:target="_blank"}
- Goupile [framagit.org/interhop/goupile](https://framagit.org/interhop/goupile){:target="_blank"}
- LinkR : [framagit.org/interhop/linkr](https://framagit.org/interhop/linkr){:target="_blank"}

Le déploiement de nos outils sur les [serveurs certifiés et HDS]({{ site.baseurl }}/projets/hds) nécessite aussi de produire du code source qui est disponible sur [framagit.org/interhop/hds](https://framagit.org/interhop/hds){:target="_blank"}.

En cas de problèmes ou de bogues signalez les nous dans la section "Issues" de chaque projet !


[^git]: [https://fr.wikipedia.org/wiki/Git](https://fr.wikipedia.org/wiki/Git){:target="_blank"}

[^gitlab]: [https://fr.wikipedia.org/wiki/GitLab](https://fr.wikipedia.org/wiki/GitLab){:target="_blank"}
