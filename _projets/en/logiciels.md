---
title: "Our softwares"
description: "Opensources and libres"
layout: default
image: /assets/images/projets/software.svg
show_image: false
projets_outils: true
order_outils: 1
projets_collab: false
ref: projets-logiciels
lang: en
---


<section class="hero diagonal">
	<div class="container" {% if page.full_width %}style="max-width: 100%"{% endif %}>
		<h2>{{ page.title }}</h2>
		<p class="subtext">{{ page.description }}</p>
	</div>
</section>



<section class="diagonal patterned">
	<div class="container halves">
		<div>
			<h3 class="editable">Explanatory notes</h3>
		</div>
		<div>
			<ul class="image-grid">
				{% assign _projets = site.projets | where: "lang", "en" | where: "logiciels", "true" | sort: 'order_logiciels' %}
				{% for projet in _projets limit: 10 %}
					<li><a href="{{ site.baseurl }}{{ projet.url }}"><img src="{% include relative-src.html src=projet.image %}" alt="Logo - {{ projet.title }}" class="screenshot">
					<div class="details">
							<div class="position">{{ projet.title }}</div>
					</div>
					</a></li>
				{% endfor %}
			</ul>
		</div>
	</div>

</section>






<section class="diagonal alternate">
	<div class="container">
	<h2>Use our software!</h2>


	<p class="subtext editable">
	   	InterHop.org administers services requiring a <a href="{{ site.baseurl}}/projets/hds">Health Data Host (HDS)</a> and services that do not require it.
	   </p>

           <h4>Regarding HDS services, these are:</h4>
           <ul>
           	<li><a href="https://goupile.hds.interhop.org/" target="_blank">goupile.hds.interhop.org</a></li>
           	<li><a href="https://cryptpad.hds.interhop.org/" target="_blank">cryptpad.hds.interhop.org</a></li>
           </ul>

	   <h4>As for services that are not HDS, these are:</h4>
           <ul>
           	<li><a href="https://goupile.interhop.org/" target="_blank">goupile.interhop.org</a></li>
           	<li><a href="https://app.linkr.interhop.org/login" target="_blank">app.linkr.interhop.org</a></li>
           	<li><a href="https://cpad.interhop.org" target="_blank">cpad.interhop.org</a></li>
           	<li><a href="https://pad.interhop.org" target="_blank">pad.interhop.org</a></li>
           	<li><a href="https://password.interhop.org" target="_blank">password.interhop.org</a></li>
           	<li><a href="https://element.interhop.org" target="_blank">element.interhop.org</a></li>
           </ul>


	<p class="subtext editable">
	The <a href="{{ site.baseurl}}/en/cgu">general terms and conditions of use</a> (hereinafter referred to as the “GCU”) of the InterHop.org site aim to provide a legal framework for the terms and conditions of provision of our <strong>non-HDS</strong> services.
	</p>
	<p class="subtext editable">
	The gCU do not concern HDS services for which a contract is established.
	</p>

		<p><a style="text-align: center;" class="button alt" type="blue" href="{{ site.baseurl }}/2023/04/06/brochure-recherche">See our HDS “Research” brochure</a></p>


   	</div>



</section>
