---
title: LinkR
description: "Collaborative Data Sciences platform"
image: /assets/images/projets/linkr-official.png
show_image: false
une: true
order_une: 2
logiciels: true
order_logiciels: 2
ref: projets-linkr
lang: en
---

<div style="display: flex; align-items: center; justify-content: space-between;">
    <div style="flex-grow: 1; display: flex; align-items: center;">
        <h1 style="margin: 0;">LinkR for Data Science</h1>
    </div>
    <div>
        <a href="https://framagit.org/interhop/linkr/linkr">
            <img src="{{ site.baseurl }}/assets/images/projets/linkr-official-2.png" alt="Logo - {{ page.title }}" width="123" height="140" />
        </a>
    </div>
</div>



<!-- more -->


Since the **informatisation** of hospital services, **many data** are available for research in the **healthcare** sector.

These data are stored in **Clinical Data Warehouses** (CDWs) within hospitals.

At present, to be able to use these data for research, you need to have **advanced knowledge** of **programming**. Indeed, manipulating this data requires skills in SQL, R and Python in particular.

Conducting studies on EDS requires **close collaboration** between **clinicians**, **data scientists** and **statisticians**.

There are **collaborative work platforms** such as Jupyter Notebook[^jup], where code is mixed with the display of the results of executing that code. Nevertheless, this platform requires programming knowledge and has limited functionality, not being created for the specific purpose of creating studies on EDS data.

On the other hand, there is **statistical analysis software**, with so-called "no-code" **graphical interfaces**, enabling statistical analysis to be carried out without any a priori knowledge of programming. This type of software doesn't usually allow you to modify the code, which is a limiting factor for the data scientist *and the development of new functionalities*.

This is why **LinkR** was created.

It's a web application that allows :

- the ![clinician](https://img.shields.io/badge/-clinician-blue): access health data via a **graphical interface**, requiring no programming skills
- the ![data scientist](https://img.shields.io/badge/-data_scientist-3EA646): to manipulate data via a **R** and **Python** programming interface
- the ![student](https://img.shields.io/badge/-student-FF6347): to learn how to manipulate medical data, both by browsing patient records and by manipulating aggregated data.

In this way, it enables the **different stakeholders** involved in medical research on EDS to work **collaboratively**, on a **common platform**.

Thanks to this common platform adapted to both ![clinicians](https://img.shields.io/badge/-clinicians-blue) and ![data scientists](https://img.shields.io/badge/-data_scientists-3EA646), studies can be carried out **much faster**, and with the contribution of the **specificity of each**: the medical expertise of the clinician and the data expertise of the data scientist.

[^jup]: [Jupyter Notebook](https://jupyter.org/){:target="_blank"},

## Overview of LinkR

LinkR enables the analysis of data from **different sources** :

- hospital **EDS** data
- data from **shared databases**, such as the MIMIC or AmsterdamUMCdb databases
- data from **data repositories**, in the form of CSV or Excel files.

Once this data has been imported into LinkR, you can create **studies**.

Each study will include one or more subsets of patients.

<p align="center">
  <img src="https://pad.interhop.org/uploads/b2832a77-29aa-488d-bd1e-2c37b19761c2.svg" alt="Structure des données" style="display:block; margin:auto;"><br /><br />
  <em>Organising data within LinkR</em>
</p>

Once a study has been created, it needs to be configured.

The principle is simple: a study has an "Individual data" page and an "Aggregate data" page.

### Individual data

In the **individual data** page, I create a **medical record** template according to **my needs**.

For example, if my study concerns bacterial pneumonia, I will create :

- a tab where **reports** from medical **imageries** will be displayed
- a tab where bacteriological **results** will be displayed
- a tab displaying the **antibiotic treatments** received by the patient

Benefits :
- the ![clinician](https://img.shields.io/badge/-clinician-blue) can therefore view patient data as he is accustomed to doing, in the form of a medical file.
- the ![data scientist](https://img.shields.io/badge/-data_scientist-3EA646) can, if he wishes, modify the code allowing data visualization.
- the![student](https://img.shields.io/badge/-student-FF6347) can learn to read a clinical file

<p align="center">
  <img src="https://pad.interhop.org/uploads/8aa2af3b-16d9-4e9a-a7f0-7c5ab126d8ae.png" alt="Individual data" style="border:solid 1px; color:#CECECE; padding: 5px;">
  <em>Individual data tabs with a “Console R” plugin</em>
</p>

### Aggregated data

Once this is done, we can **continue our study** by analyzing the **aggregated data**.

In the same way, I create **tabs**, corresponding to the **different stages of my study** :

- to visualize the distribution of data and eliminate **outliers** **data
- to create and apply **exclusion criteria**, and
- to create and view the **flowchart**, and
- to produce the **statistics** of my study
- to import a **bibliography**
- to create the **report** of my study

Each of these examples corresponds to what we call a **plugin**: it is a script allowing you to add functionality to the application.<br />We will therefore have a "Flowchart" plugin, a "plugin" Bibliography" etc.

Advantages:

- the ![clinician](https://img.shields.io/badge/-clinician-blue) can perform statistics on the data, without programming knowledge
- the ![data scientist](https://img.shields.io/badge/-data_scientist-3EA646) can, once again, modify the tab code to add code not proposed by the plug-ins.
- the ![student](https://img.shields.io/badge/-student-FF6347) can learn about biostatistics.

<p align="center">
  <img src="https://pad.interhop.org/uploads/99d98f74-0b2a-4118-b42c-2aae450dd5b3.png" alt="Données agrégées" style="border:solid 1px; color:#CECECE; padding:5px;">
  <em>Aggregated data tabs with a "Figure (ggplot2) plugin"</em>
</p>


The different stakeholders can **communicate** via the “**Messages**” tab.

So :
- the ![clinician](https://img.shields.io/badge/-clinician-blue) can query the ![data scientist](https://img.shields.io/badge/-data_scientist-3EA646) if he has questions about programming
- the ![data scientist](https://img.shields.io/badge/-data_scientist-3EA646) can query the ![clinician](https://img.shields.io/badge/-clinician-blue) if he has questions about medicine
- the![student](https://img.shields.io/badge/-student-FF6347) can question the ![clinician](https://img.shields.io/badge/-clinician-blue) or the ![data scientist](https://img.shields.io/badge/-data_scientist-3EA646) depending on your needs

## Interoperability

LinkR builds on the **OMOP Common Data Model**, which is a model used by many hospitals around the world.

This allows **interoperability of data**, it is thus possible to carry out **multicenter studies** easily, by only coding the study once.

LinkR integrates **concept alignment** tools, allowing you to match concepts contained in different databases.<br />For example to match the concept "123456 - Heart rate" from center 1 to the concept " 456789 - HR (bpm)" from center 2.

It is also possible to share **everything you develop** within LinkR.

To know :

- **studies**: if you have completed your study, you can share it with one click to other centers. From the application, they will be able to **download your study** and **launch it on *their* data**.
- data cleaning **scripts** (what data scientist has never made a script to filter false weights and false sizes, and would not have liked to have one already developed available?)
- the **plugins** that we saw above
- the **data sets**: the codes allowing the sets to be loaded are shared, and not the data
- the **terminologies**: you can share both the code allowing you to load the concepts, but also the **concepts** of the different terminologies as well as the **alignments** that you have made

<p align="center">
  <img src="https://pad.interhop.org/uploads/2a339440-b8aa-44ee-8236-4e4e632faf67.png" alt="Interoperability" style="width:100%; max-width:700px; display: block; margin:auto;"><br />
  <em>Algorithmic interoperability offered by LinkR</em>
</p>

By combining **data interoperability** as well as **algorithmic interoperability** (sharing of studies, scripts and plugins), LinkR really allows research to be carried out according to the **principles of "open science" or open science[^open_science]**.

This also allows **protection of health data**: LinkR follows a **decentralized architecture**, it is the **algorithms** that **travel** and not the data.
  9 ---

<p align="center">
  <img src="https://pad.interhop.org/uploads/dc0a4f48-82b6-4769-9eec-43bd2ff3af62.png" alt="Plugin catalog" style="border:solid 1px; color:#CECECE; padding :5px; height:auto;"><br />
  <em>Catalog of plugins available on the InterHop git. Installing a plugin is done in one click.</em>
</p>

[^open_science]: ["open science" or open science](https://fr.wikipedia.org/wiki/science_ouverte){:target="_blank"}

## Technical specifications

LinkR is a web application created with the **Shiny** library in **R**.

It is **open source** software, under the **GPLv3** license.

The application can be installed via <a target="_blank" href="https://www.shinyproxy.io/">**ShinyProxy**</a> and protected by SSO authentication.

To **install** the app, follow the steps <a target="_blank" href="https://framagit.org/interhop/linkr/linkr">given here</a>.

You can also <a target="_blank" href="https://linkr.interhop.org">test the app here</a>:

- with the "test" / "test" logs for ShinyProxy (see first image below)
- by clicking on "LinkR - test - v0.2..."
- then again with the "test" / "test" logs for LinkR (see second image below)

<img src="https://pad.interhop.org/uploads/70a07834-7945-419a-9308-a89d16039a8e.png" alt="Connecting to LinkR" style="max-width:400px; display:block; margin :auto;" />
<img src="https://pad.interhop.org/uploads/773cf94e-acd7-4949-bbdb-66ed11c0c4be.png" alt="Connecting to LinkR" style="max-width:400px; display:block; margin :auto;" />

## Help from InterHop

**InterHop** offers **data hosting** for its digital tools on its servers [**HDS** (health data host)]({{ site.baseurl }}/en/projets/hds) , rented from GPLExpert.

The association also suggests using <a href="{{ site.baseurl }}/en/projets/goupile">**Goupile**</a>, an eCRF tool, allowing **collect data* *directly on HDS servers.

Still within the framework of InterHop it is possible to **use LinkR** on **HDS servers**.

This will make it possible to collect the data and then carry out the studies in the **same place**, in a secure manner.

InterHop also offers **support** for the **use of LinkR**.

<p><a style="text-align: center;" class="button alt" type="blue" href="{{ site.baseurl }}/2023/04/06/brochure-recherche">See our “Research” brochure</a></p>

##Collaboration

The project being open source and carried out by volunteers, **any contribution is welcome**!

## Summary

<div class="alert alert-blue" markdown="1">
- **Synopsis:** Collaborative data science platform
- **Website:** [linkr.interhop.org](https://linkr.interhop.org){:target="_blank"}
- **Technology:** R Shiny
- **Gitlab:** [framagit.org/interhop/linkr/linkr](https://framagit.org/interhop/linkr/linkr){:target="_blank"}
- [Video tutorial](https://peertube.interhop.org/c/linkr/videos){:target="_blank"}
- You can also contact us via instant messaging and opensource [Element]({{site.baseurl}}/en/projets/element)
</div>
