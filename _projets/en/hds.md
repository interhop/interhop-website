---
title: "Our servers"
description: "Certified and secure"
image: /assets/images/projets/hds.svg
show_image: false
projets_outils: true
order_outils: 2
projets_collab: false
ref: projets-hds
lang: en
---

InterHop installs free software for health. This software is hosted on specific machines called servers.

For health, the level of security and availability is reinforced: certification of Health Data Hosts (HDS) is mandatory

# HDS Health Data Host?

#### Personal health data

The General Data Protection Regulation (GDPR), put into force on May 25, 2018, is a regulatory tool proposed by the European Union.

This regulation proposes definitions of personal data and health data.

Personal data concerns:
> “any information relating to an identified or identifiable natural person”[^rpgd_def].

A natural person can be identified, either directly by their first or last name for example, or exclusively
> “by reference to an identifier, such as an identification number, location data, an online identifier, or to one or more specific elements specific to their physical, physiological, genetic, psychological, economic, cultural or social »[^rpgd_def]. Voice and image are part of indirect identifiers.

> Personal health data is "personal data relating to the physical or mental health of a natural person, including the provision of health care services, which reveals information about the state of health of that natural person "[^rpgd_def].

[^rpgd_def]: [Article 4 - EU GDPR - "Definitions"](https://www.privacy-regulation.eu/fr/4.htm){:target="_blank"}


#### What legal framework?
Article L.1111-8[^l1111] of the public health code stipulates:
> Any person who hosts personal health data collected during prevention, diagnosis, care or social and medico-social monitoring activities, on behalf of natural or legal persons at the origin of the production or collection of this data or on behalf of the patient himself [must be] holder of a certificate of conformity.

This article comes from Ordinance No. 2017-27 of January 12, 2017. It differentiates[^apssis]:
> - data hosted on paper or digital media as part of an electronic archiving service, for which the host must be approved by the Minister responsible for culture[^apssis]
> - data hosted on digital media (except in the case of an electronic archiving service), for which the host must hold a certificate of conformity, issued by certification bodies accredited by the French authority of accreditation[^apssis]


In conclusion, any application which processes or stores health data on behalf of a data controller[^resp_trt_hds] (in the RPGD sense) must therefore be in an HDS environment.


[^ordo_12012017]: [Order No. 2017-27 of January 12, 2017 relating to the hosting of personal health data](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000033860770/){ :target="_blank"}

[^apssis]: [APSSIS - News update on health data host certification](https://www.apssis.com/actualite-ssi/335/point-d-actualite-sur-la-certification-hebergeur-de-donnees-de-sante.htm){:target="_blank"}


[^resp_trt_hds]: [Health data hosting and GDPR](https://www.village-justice.com/articles/hebergement-donnee-sante-rgpd,30355.html){:target="_blank"}

[^l1111]: [Public Health Code](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006072665/LEGISCTA000006185255/2021-02-02/){:target="_blank"}

#### The certification

Since April 1, 2018, the HDS Health Data Host approval issued by ASIP Santé, now the Digital Health Agency (ANS), has been replaced by a regulatory obligation to obtain HDS certification.

The certification principle is based on several standards[^ref_hds], audited by an independent body, accredited by COFRAC[^cofrac] (French Accreditation Committee).

"The certificate is issued for a period of three years, by the certifying body and each year, a surveillance audit is carried out."[^cert_hds]

Here is the list of organizations that have already obtained HDS certification[^cert_hds_list].

[^ref_hds]: [The standards of the certification procedure](https://esante.gouv.fr/services/hebergeurs-de-donnees-de-sante/les-referentiels-de-la-procedure-de-certification ){:target="_blank"}

[^cofrac]: [French Accreditation Committee (COFRAC)](https://www.cofrac.fr/){:target="_blank"}

[^cert_hds]: [Certification of health data hosts](https://esante.gouv.fr/labels-certifications/hds/certification-des-hebergeurs-de-donnees-de-sante){:target=" _blank"}

[^cert_hds_list]: [list of certified hosts for personal health data](https://esante.gouv.fr/offres-services/hds/liste-des-herbergeurs-certifies){:target="_blank"}

#### The perimeters

There are two types[^cert_hds]:

1. Physical infrastructure host

![](https://esante.gouv.fr/sites/default/files/inline-images/Prestation_h%C3%A9bergeurs_infrastructure_physique.png)

2. Managed hosting provider

![](https://esante.gouv.fr/sites/default/files/inline-images/Prestation_h%C3%A9bergeur_infog%C3%A9reur.png)

# Our HDS: GPLExpert

#### Presentation
Since December 2019[^gpl_hds], GPLExpert has been officially HDS certified.

Activity 1 is subprocessed in a data center providing GPLExpert.

Health data is therefore strictly hosted in France. The head offices of subcontracting companies are located exclusively in France.


[^gpl_hds]: [GPLExpert - Health Data Hosting (HDS)](https://gplexpert.com/hebergement-donnees-sante-hds/){:target="_blank"}

#### Selection procedure

We launched our selection procedure in July 2020.

Our criteria were as follows:
- the price: impossible for InterHop to pay hundreds of thousands of euros, our monthly budget being 1000 euros per month maximum
- quality of services: availability of machines. We offer health services, the servers must be available and functional 24/7
- the team: we need to have privileged contact with our HDS service provider. This choice indeed engages our responsibility and above all the trust that people place in our association.
- our priority: we need to be convinced of the independence from the extraterritoriality of our service provider's law. We absolutely do not want the software we offer to be dependent on non-European interests. This criterion is very restrictive. It is not enough for the servers to be located in France or Europe; the head offices of companies must also be strictly in Europe.

In addition to being solely subject to French and European rights, GPLExpert does not work in any way with American clouds such as Microsoft Azure, Amazon Web Service or Google Cloud Platform. Finally, no tool or software subject to American law is installed by default on their servers.

#### In case of incident resolution: Technical bug

Basically, neither GPLExpert nor InterHop has access to stored health data.

Sometimes :-) technical incidents occur. In these cases it is necessary to have access to the machine (to the server).

Two scenarios can occur. In the first case, simple access to the machine is sufficient. It is therefore not necessary to have access to the database stored on this same machine. In this case a GPLExpert engineer. under the responsibility of InterHop, restarts the program or machine.

In rarer cases, it is necessary to have access to the database where health information is stored. In this situation, a healthcare professional subject to medical confidentiality accesses the database. Several members of the association are caregivers and engineers. If it is necessary to have advanced business expertise (networks, database, specific programming language) the medical manager can ask for help. All these processes are recorded in the Quality Assurance Plan (QAP) that InterHop and GPLexpert complete jointly.

# Proposed architecture

#### Reverse proxy
The technical architecture deployed by InterHop with GPLExpert to effectively protect health data contains a DMZ[^dmz] or a demilitarized zone (DMZ demilitarized zone) is a subnetwork separated from the local network and isolated from it and from Internet (or other network).
Clearly, when the user requests access to a web page, the information goes through a reverse proxy. Thus, instead of directly accessing the machine storing the data, the flows pass through a third-party machine.

Within this DMZ we have set up what is called a reverse proxy which can serve as a bridge between a client and the HDS web servers, by retrieving resources from the web server (or web servers) and providing them to the client as if the resources came directly from the reverse proxy.

![](https://geektechstuff.files.wordpress.com/2020/05/reverse_proxy_h2g2bob.svg_.png?w=656)

The Reverse-Proxy allows us to guarantee an optimal level of security on our websites as well as to comply with HDS certifications.


#### Environments

We installed within the **Development environment** (test machines) a clone of the **Production environment** (HDS target architecture). For this we will use:
- Ansible to automate deployment processes
- Nginx as server ("WEB SERVER" machine) and reverse proxy ("PROXY" machine)

The source code allowing the deployment of the machines is free. It is accessible here: [https://framagit.org/interhop/hds](https://framagit.org/interhop/hds){:target="_blank"}

NGINX is a web server that can also be used as a reverse proxy and/or load balancer. These are the reverse proxy options we use for the "PROXY" machine.

Ansible is an opensource project available under the GNU GPLv3 license. The source code is available: [https://github.com/ansible/ansible](https://github.com/ansible/ansible){:target="_blank"}

Nginx is an opensource project available under the BSD license. The source code is available: [https://github.com/nginx/nginx](https://github.com/nginx/nginx){:target="_blank"}

Ansible and Nginx are included in the list of free software recommended by the French State as part of the overall modernization of its information systems (IS)[^sill].

# Budget

<div class="alert red-alert" markdown="1">
More than 1000 euros/month.

We already rent these HDS servers and you can help us donate. :-)
The money will first be used to finance servers dedicated to health (HDS).
</div>

Help us by making a donation to InterHop.
<p><a style="text-align: center;" class="button alt" type="blue" href="{{ site.baseurl }}/en/dons/">Donations!</a></p>

[^sill]: [Interministerial Base of Free Software (SILL)](https://code.gouv.fr/sill/list){:target="_blank"}

[^dmz]: [Demilitarized Zone (IT)](https://en.wikipedia.org/wiki/DMZ_(computing)){:target="_blank"}
