---
tltle: Bitwarden
description: "Secure Password Manager"
image: /assets/images/projets/secure_communication.svg
logiciels: true
order_logiciels: 4
external_name: "Learn more about Bitwarden"
external_url: https://bitwarden.com/
pdf_local: "/assets/pdf/bitwarden_tutorial_compressed.pdf"
ref: projets-bitwarden
lang: en
---

# Password managers


A password manager is a virtual safe and helps securely store and manage passwords and other sensitive information. Password managers offer a convenient solution for generating complex and unique passwords for each service used, including the risk of data breaches. Using a password manager eliminates the need to remember many different passwords, simplifying users' lives while improving security.

# Client side, Bitwraden

Bitwarden offers apps for various platforms, including Windows, macOS, Linux, iOS, and Android, as well as browser extensions for seamless integration with web browsers. Its open source nature allows for full transparency, allowing security experts to check the code for potential vulnerabilities. Additionally, Bitwarden offers features such as two-factor authentication (2FA), secure item sharing, and management tools for teams and businesses.

# Server side, Vaultwarden

On the InterHop server side, administer its Vaultwarden[^vaultwarden] instance, a self-hosted version of Bitwarden, written in Rust. It is ideal for having total control over data.

# Our connection tutorial

{% pdf {{ page.pdf_local }} no_link width=100% height=1000px %}

Your browser does not support PDFs. You can <a href="{{ page.pdf_local }}">download the PDF file</a>.

[^vaultwarden]: [https://github.com/dani-garcia/vaultwarden](https://github.com/dani-garcia/vaultwarden){:target="_blank"}
