---
title: Cryptpad
description: "Secure storage"
image: /assets/images/projets/cryptpad.png
logiciels: true
order_logiciels: 6
external_name: "Learn more about Cryptpad"
external_url: https://cryptpad.fr/
ref: projets-cryptpad
lang: en
---

# End-to-end encryption

CryptPad is an online collaboration tool that focuses on data privacy and security. Unlike many collaboration services that store data unencrypted on their servers, CryptPad uses end-to-end encryption, ensuring that only the user and the people they share their documents with can access it. This platform offers a suite of integrated applications, such as text editors, spreadsheets, presentations and note-taking tools, enabling real-time collaboration without compromising security. CryptPad is ideal for teams and organizations that want to maintain full control over their data while enjoying the benefits of online collaboration.

# Open Source and Self-hosting

CryptPad is an open source project. This transparency allows users to check the source code for potential vulnerabilities and ensures their confidence in the security of the platform. Additionally, CryptPad can be self-hosted, providing additional flexibility and control for users who want to manage their own infrastructure. 

# Our Cryptpad instances


InterHop.org administers services requiring a <a href="{{ site.baseurl}}/en/projets/hds">Health Data Host (HDS)</a> and services that do not.

Two versions of Cryptpad are therefore available within the InterHop association
- <a href="https://cryptpad.hds.interhop.org/" target="_blank">cryptpad.hds.interhop.org</a>
- <a href="https://cpad.interhop.org" target="_blank">cpad.interhop.org</a>
