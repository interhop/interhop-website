---
title: Justice
description: Advocacy, legal actions
image: /assets/images/projets/justice.png
une: true
order_une: 4
ref: projets-actions-en-justice
lang: en
---

To change the law and protect your rights, InterHop is developing its advocacy and strategic litigation activities.

<div class="responsive-video-container">
	<iframe title="Attack on medical confidentiality: the Health Data Hub centralizes health data at Microsoft" width="560" height="315" src="https://peertube.interhop.org/videos/embed/56150be2-195f-42a5-8d8e-a68b0c810dc8" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
</div>

In 2020 and within the SantéNathon collective, we brought [an appeal before the Council of State against the National Health Data Platform PDS (or Health Data Hub) hosted by the company Microsoft]({{ site.baseurl }}/en/2020/09/16/communique-presse-refere-privacy-shield). The risks linked to the use of extra-territorial platforms have been recognized by the [Council of State]({{ site.baseurl }}/en/2020/10/14/communique-presse-conseil-detat-reconnait-acces-donnes).

The Minister of Health committed on [November 19, 2020 before the CNIL]({{site.baseurl}}/2020/11/23/la-reversibilite-est-acquise) to adopt "a new technical solution allowing not expose the data hosted by the PDS to possible illegal disclosures to the American authorities, within a period which, as far as possible, includes 12 and 18 months and in any case, does not exceed two years.
This deadline having passed without effect, we reserve the right to continue legal action to defend your rights.

We continue to alert whenever we deem it necessary:
- concerning the [American Data Broker Iqvia]({{ site.baseurl }}/2021/05/27/signalement-cnil-iqvia)
- concerning cookies [Google Analytics]({{ site.baseurl }}/2022/09/06/saisine-cnil-google-analytics-suite-1) used by several e-health players
- concerning [online appointment booking]({{ site.baseurl }}/2021/03/12/communique-presse-decision-ce-rendezvous)

Our advocacy actions are numerous and concern in particular:
- [Mon Espace Santé]({{ site.baseurl }}/2022/01/31/mon-espace-sante)
- [The latest price adequacy decision by the European Commission]({{ site.baseurl }}/2022/10/31/new-executive-order)
- [Hearings before the National Assembly]({{ site.baseurl }}/2021/02/22/commission-souverainete)
