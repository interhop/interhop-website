---
title: Element
description: "Instant messaging"
image: /assets/images/projets/communication.svg
show_image: false
une: false
projets_outils: false
projets_collab: true
order_collab: 3
logiciels: true
order_logiciels: 3
external_name: "Learn more about Element"
external_url: https://element.io/
ref: projets-element
lang: en
---

Element is an opensource instant messaging application designed to improve communication and collaboration between individuals and teams.

With features like text messages, voice and video calls, and file sharing, Element offers a complete solution for staying connected and organized.

# Matrix and decentralization

Element is based on the Matrix[^matrix] protocol, which allows federation, that is, the ability to communicate with users on different Matrix servers. This decentralized and interoperable architecture reinforces the resilience and independence of the platform compared to centralized solutions.
![](/assets/images/projets/decentralisation.png)

Synapse[^synapse], the reference server for the Matrix protocol, plays a crucial role in this infrastructure. It allows organizations to deploy and manage their own Matrix servers, ensuring full control over data and the ability to federate with other servers.

With Element as the client and Synapse as the server, users benefit from secure, decentralized messaging, suitable for diverse environments ranging from small teams to large enterprises.


# Registration

To take full advantage of Element, you can join various discussion threads. For example, the public thread of [InterHop](https://matrix.to/#/#public:matrix.interhop.org){:target="_blank"} or those dedicated to [Goupile](https://matrix.to/#/#goupile:matrix.interhop.org){:target="_blank"} or [LinkR](https://matrix.to/#/#linkr:matrix.interhop.org) .

Element is available on mobile, desktop or via a [simple browser](https://element.interhop.org){:target="_blank"}.

Here is a connection tutorial.<br/>
Contact us (via <a href="mailto:interhop@riseup.net">email</a>) to obtain a token to complete your registration.

 <div class="responsive-video-container">
<iframe width="100%" height="500" src="https://pad.interhop.org/p/rynEJduy_#/" frameborder="0"></iframe>
</div>


[^matrix]: [What is Matrix?](https://matrix.org/docs/older/introduction/){:target="_blank"}

[^synapse]: [Synapse: Matrix homeserver written in Python/Twisted](https://github.com/matrix-org/synapse){:target="_blank"}

[^element]: [https://element.io](https://element.io/){:target="_blank"}
