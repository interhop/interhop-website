---
title: Goupile
description: "Easy, user-friendly E-CRF"
image: /assets/images/projets/goupile-official.png
show_image: false
une: true
order_une: 1
logiciels: true
order_logiciels: 1
ref: projets-goupile
lang: en
---

<div style="display: flex; align-items: center; justify-content: space-between;">
    <div style="flex-grow: 1; display: flex; align-items: center;">
        <h1 style="margin: 0;">Goupile for data collection</h1>
    </div>
    <div>
        <a href="https://framagit.org/interhop/goupile">
            <img src="{{ site.baseurl }}/assets/images/projets/goupile-official.png" alt="Logo - {{ projet.title }}" width="123" height="140" />
        </a>
    </div>
</div>

Goupile is a form editor for research data collection. It is a web application that can be used on computer, mobile and offline.

<div class="responsive-video-container">
<iframe title="Goupile or how to design forms" width="560" height="315" src="https://peertube.interhop.org/videos/embed/155e9773-212c-43c3-a275-0b473afbecab" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
</div>

The name Goupile has no particular meaning. When Niels Martignène created the project folder, he needed a name. He asked for help from his partner, Apolline, a great lover of foxes... And the name "goupil" came out by itself.

After a few months, the "e" was added to facilitate the search on Google and to have a domain name available :)

Traditionally, the Case Report Form (CRF) is a paper document designed to collect data from people included in a research project.
These documents are increasingly being replaced or supplemented by digital portals, and are referred to as eCRFs (''e'' for *electronic*).

![](https://goupile.fr/static/screenshots/overview.webp)

Goupile was born because data are still often collected in Excel files, and stored on researchers' personal machines. Others use online tools, like "Google Sheets" or "Office 365". The data is then stored at GAFAMs, on servers not adapted to health data.

There is also software specifically developed for health. These tools offer a drag and drop approach and/or a metalanguage, from a palette of input fields (e.g. date, number, text fields). This approach is easy and intuitive, but it limits the development possibilities. Moreover, these tools are often cumbersome to set up, and are generally used in the context of funded studies.

Goupile is a free and opensource eCRF design tool that strives to make form creation and data entry both powerful and easy. Its access is nominative and protected.

All Goupile pages are *responsive*, which means they are suitable for mobile, tablet and desktop browsing.

Goupile has an offline mode in case of network outage. The data is synchronized when the connection is available again.

Today, the "Goupile" project is supported and developed by the association InterHop.org, which is the publisher of the free software Goupile. Niels Martignène is the creator and the main developer.

InterHop's team is supporting Goupile on HDS certified servers (Health Data Hosts), to accompany users in the development of their forms and new features. Finally, InterHop also offers support for questions related to the protection and the General Data Protection Regulation (GDPR).

Goupile is used by researchers for prospective, retrospective, mono-centric or multicentric studies.

Goupile is also used by health students for their thesis and dissertation projects which generally require data collection. These relatively simple technical projects are our immediate target for several reasons: they do not need very complex functionalities, the number of theses (per year) is several hundreds and the proposed alternatives are neither free nor secured.

InterHop members use Goupile daily to develop their own forms, which are tested and used by their close collaborators (and friends).

**Let's talk about Goupile's functionalities...**

Goupile allows you to design an eCRF with an approach that is a little different from the usual tools, since it consists in programming the content of the forms, while automating the other aspects common to all eCRFs:
- Pre-programmed and flexible field types,
- Publication of forms,
- Data storage and synchronization,
- Online and offline collection, on computer, tablet or mobile,
- User management and rights.

In addition to the usual functionalities, we have tried to reduce as much as possible the time between the development of a form and the data entry.

With Goupile, I code on the left, I see the result on the right.

Changes made in the editor are immediately executed and the preview panel on the right displays the resulting form. This is the same page that will be presented to the end user. At this point, it is already possible to enter data to test the form, but also to view and export it.

![](https://goupile.fr/static/screenshots/editor.webp)


Once the form is ready, the tracking panels allow you to observe the progress of the data entry, view the records and export the data.

![](https://goupile.fr/static/screenshots/data.webp)

**Goupile has many advantages.

Goupile is not based on a metalanguage (contrary to the classic tools in this field) but uses the Javascript language. It is a very well known programming language.
The fact that it is programmed gives a lot of possibilities, in particular the realization of complex forms, without sacrificing the simplicity for the usual forms. Thus, there are very few limits to the development of new input fields or features.

We want to reassure you about the complexity. You can start creating your form without any programming knowledge. Indeed, Goupile provides a set of predefined functions that allow you to create input fields (text field, numeric field, dropdown list, visual scale, date, etc.) very simply.

![](https://goupile.fr/static/screenshots/instant.png)

It is possible to test the tool on a [demo instance](https://goupile.fr/demo){:target="_blank"} proposed by the InterHop association.<br>
Be careful ! The demo instance is not certified for health: only fictitious data can be collected.

Finally, as Goupile is a free software, two possibilities are offered to the users:
- Use and install Goupile locally in their center. Indeed, Goupile is delivered under a free license: AGPL 3.0 license. The source code is available for free online [here](https://framagit.org/interhop/goupile){:target="_blank"}. In this case they have to manage all the hosting aspects.
- Use the turnkey service provided by the InterHop association via the servers rented from our [health data host, GPLExpert]({{ site.baseurl }}/en/projets/hds).

In any case, users can ask us to develop new features.


## Help from InterHop

**InterHop** offers **data hosting** for its digital tools on its servers [**HDS** (health data host)]({{ site.baseurl }}/en/projets/hds) , rented from GPLExpert.

The association also suggests using <a href="{{ site.baseurl }}/en/projets/linkr">**LinkR**</a>, a data science tool, allowing **treat data** directly on HDS servers.

Still within the framework of InterHop it is possible to **use Goupile** on **HDS servers**.

This will make it possible to collect the data and then carry out the studies in the **same place**, in a secure manner.

InterHop also offers **support** for the **use of Goupile**.

<p><a style="text-align: center;" class="button alt" type="blue" href="{{ site.baseurl }}/2023/04/06/brochure-recherche">See our “Research” brochure</a></p>

## Collaboration

The project being open source and carried out by volunteers, **any contribution is welcome**!

- You can also contact us via instant messaging and opensource [Element]({{site.baseurl}}/en/projets/element).


## Summary

<div class="alert alert-blue" markdown="1">
- **Synopsis:** e-CRF data collection form
- **Website:** [goupile.org](https://goupile.org/demo){:target="_blank"}
- [Test platform](https://goupile.org/demo){:target="_blank"}
- **Techno:** Lit-html, c++, Postgresql
- **Gitlab:** [framagit.org/interhop/goupile](https://framagit.org/interhop/goupile){:target="_blank"}
- [Video tutorial](https://peertube.interhop.org/c/goupile/videos){:target="_blank"}
- You can also contact us via instant messaging and opensource [Element]({{site.baseurl}}/en/projets/element)
</div>
