---
title: Datathon
description: "Data hackathon"
image: /assets/images/projets/heart.png
show_image: false
une: true
order_une: 3
projets_outils: false
projets_collab: true
order_collab: 2
ref: projets-datathon
lang: en
---

The InterHop association is organizing a data hackathon (Datathon) in September 2024.

# What is a “datathon”?

A datathon is an event bringing together data processing specialists, caregivers and patients over a few days to work collaboratively on computer programming.

In just two to three days, the teams are responsible for developing a solution in response to a specific research problem.


The Datathon is aimed at healthcare professionals, patients and data scientists. There are no prerequisites, and in particular no programming skills are required to take part. The aim of the Datathon is to forge links between the various players involved in data research.

Here is a video presentation of the 2018 Paris hospital datathon co-organized with the French Society of Anesthesia and Intensive Care (SFAR) and the French Language Intensive Care Society (SRLF).
<div class="responsive-video-container">
<iframe title="DAT-ICU 2018 : 48h de datathon pour rapprocher réanimateurs et experts en analyse de données" width="560" height="315" src="https://peertube.interhop.org/videos/embed/83f063da-cdd4-45de-8c13-aba5c695ab10" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
G</div>

# Practical arrangements

The datathon will take place at the Maison Des Associations, <a target="_blank" href="https://www.openstreetmap.org/search?query=6%20cours%20des%20alli%C3%A9s%2035000%20Rennes# map=19/48.10478/-1.67646">6 Allied Courses at 35000 Rennes</a>.

It begins on Thursday, September 5, 2024 at 9am and ends on Friday, September 6 at 6pm.

For information, Rennes is 1h30 from Paris by TGV :-)

Here is the [replay of the information meeting on the practical arrangements](https://visio.octoconf.com/playback/presentation/2.3/8d7742fb121806737a8c2bc5bd9dbade20963e96-1719486059418){:target="_blank"} for carrying out the datathon.

# Topics

Everyone is free to come with their own topics.

Here is a preliminary list available in this blog post: [https://interhop.org/2024/06/26/datathon-sujet](https://interhop.org/2024/06/26/datathon-sujet)

# Tools

The main data source used will be the MIMIC database in OMOP[^medrxiv] format.

The OMOP[^cdm] common data model and our Low Code Data Science platform [LinkR] ({{ site.baseurll}}/en/projets/linkr) will be used.


In its **complete** format, it contains over 45,000 intensive care patients.
In this format, it is necessary to pass a diploma called the CITI: [https://physionet.org/about/citi-course/](https://physionet.org/about/citi-course/)
This requires :
1. Create a [user account on physionet](https://physionet.org/login/?next=/settings/credentialing/){:target="_blank"}
2. Take a short training course: [CITI Data retrieval](https://physionet.org/about/citi-course/){:target="_blank"}
3. Submit your training [here](https://physionet.org/settings/training/){:target="_blank"}
4. [Sign the agreement to use](https://physionet.org/login/?next=/sign-dua/mimiciii/1.4/){:target="_blank"} the data for the project
5. [Go to the event page on Physionet](https://physionet.org/events/fyETG69ISUJT/){:target="_blank"}

#### CITI, an obligation for the datathon?

These steps are **required** to have access to the full version of MIMIC-OMOP.

If you don't want to take the time to complete this diploma, you can still access MIMIC. :-)
On the other hand, we will provide you with a **restricted** patient set, containing around **100** patients only.

So if you wish to use another data set, CITI is obviously not necessary.

[^medrxiv]: [MIMIC in OMOP Common Data Model](https://www.medrxiv.org/content/10.1101/2020.08.14.20175141v1){:target="_blank"}

[^git_mimic_omop]: [MIMIC OMOP](https://github.com/MIT-LCP/mimic-omop){:target="_blank"}

[^cdm]: [https://ohdsi.github.io/CommonDataModel/](https://ohdsi.github.io/CommonDataModel/){:target="_blank"}

# Inscription

To register, simply complete this online questionnaire: *[https://goupile.interhop.org/datathon](https://goupile.interhop.org/datathon)*.


Next point Thursday August 8 at 1 p.m. to talk about the subjects covered during the datathon and the formation of teams.
Here's the connection link: [https://visio.octopuce.fr/b/int-qkc-mco-jqf](https://visio.octopuce.fr/b/int-qkc-mco-jqf){:target="_blank"}

Join us on our team messaging system [Element]({{ site.baseurl }}/en/projets/element)!
Here's a public thread dedicated to the Datathon: [https://matrix.to/#/#datathon:matrix.interhop.org](https://matrix.to/#/#datathon:matrix.interhop.org){:target="_blank"}
