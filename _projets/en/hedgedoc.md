---
title: HedgeDoc
description: "Markdown text editor"
image: /assets/images/projets/hedgedoc.png
logiciels: true
order_logiciels: 5
external_name: "Learn more about HedgeDoc"
external_url: https://hedgedoc.org/
ref: projets-hedgedoc
lang: en
---

HedgeDoc is an online collaborative platform designed for writing and sharing documents in real time.

HedgeDoc uses the Markdown markup language, which makes it easy to create well-formatted documents. HedgeDoc's user-friendly interface allows multiple users to work on the same document simultaneously, making collaboration and brainstorming easier. Users can also embed diagrams, code, videos and more, making HedgeDoc very versatile. With its ability to automatically save changes and maintain a version history, HedgeDoc is a valuable tool for project management, technical documentation, and collaborative writing.

# Markdown

Markdown is a lightweight markup language designed to format text using simple, easy-to-read syntax.

Markdown stands out for its simplicity. Markdown tags allow you to format text by adding elements like titles, lists, links, images, and blocks of code, without having to learn complex syntax. This simplicity and ease of use has made Markdown a popular choice for content writers and developers, enabling rapid writing and clear, structured output.

# The InterHop instance

The InterHop HedgeDoc instance is available here: <a href="https://pad.interhop.org" target="_blank">pad.interhop.org</a>.
