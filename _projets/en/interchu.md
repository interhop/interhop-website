---
title: InterCHU
description: Network
image: /assets/images/projets/collaboration.png
show_image: false
une: false
projets_outils: false
projets_collab: true
order_callab: 1
ref: projets-interchu
lang: en
---


# Utopia


In accordance with [our manifesto]({{ site.baseurl }}/en/manifeste), the InterHop association is committed to guaranteeing digital ethics in the health research sector.

As part of the interCHU network, we particularly wish to highlight two of the values promoted.

#### Sharing

Solidarity, sharing and mutual aid between different stakeholders are the central values in health. Just as the Internet is a common good, medical IT knowledge must be available and accessible to all. We therefore want to promote the special ethical dimension that the openness of innovation in the medical field engenders and we want to take active measures to prevent the privatization of medicine.

#### Interoperability

The interoperability of computerized systems is the driving force for sharing knowledge and skills as well as the means to combat technological imprisonment. In health, interoperability is the guarantee of the reproducibility of research, the sharing and comparison of practices for efficient and transparent research. There cannot be interoperability without community.


## Context

Hospital software automatically records large volumes of data for care or billing on a daily basis. It is possible to reuse this data for another use, such as research or decision-making.

For this, several hospitals have developed a data warehouse, which, after cleaning the data and homogenizing the structure, makes it possible to cross-reference data initially from different software[^Degoul].

Some then transformed the data into a common data model (OMOP), which makes it possible to overcome the vocabularies and software used locally, by standardizing the data structure and vocabulary <sup>[^Lamer] [^Paris]</sup>.

Today, the University Hospitals of Amiens, Caen, Paris, Lille, Rouen, Toulouse, Marseille, Rennes, Foch... are collaborating to implement their anesthesia-resuscitation warehouses in OMOP format: sharing specifications, vocabulary mappings, implementation scripts, and tools.


# The InterCHU network

The InterCHU network is primarily aimed at French-speaking people (public sector engineers, etc.) using the OMOP[^omop] model or wanting to use it in the coming months.

Within InterCHU meetings, we want:
- Take stock of the various transformations carried out in hospital centers. We have code and experience to share!
- Take stock of the algorithms already implemented with OMOP.
- Discuss possible study designs to use these algorithms in our hospitals in the short term.
- Share the work of future developments between hospitals.
- Talk about the harmonization of terminologies in France.
- Talk about the development of opensource BigData architectures for health. We are developing within InterHop a collaborative Data Science platform called [LinkR]({{ site.baseurl }}/en/projets/linkr).


<p><a style="text-align: center;" class="button alt" type="blue" href="{{ site.baseurl }}/en/calendrier/">Events calendar</a></p>

All of the code produced by the InterHop association is available on our [FramaGit]({{ site.baseurl }}/en/projets/framagit).

To be informed of upcoming meetings you can join us on our associative discussion threads by registering on [Element]({{ site.baseurl }}/en/projets/element).

[^omop]: [Standardized Data: The OMOP Common Data Model](https://www.ohdsi.org/data-standardization/){:target="_blank"}

[^Degoul]: [Degoul S, Chazard E, Lamer A, Lebuffe G, Duhamel A, Tavernier B. lntraoperative administration of 6% hydroxyethyl starch 130/0.4 is not associated with acute kidney injury in elective non-cardiac surgery: A sequential and propensity-matched analysis. Anaesth Crit Care Pain Med. 2020 Apr;39(2):199-206. doi: 10.1016/j.accpm.2019.08.002. Epub 2020 Feb 14. PMID: 32068135](https://pubmed.ncbi.nlm.nih.gov/32068135/){:target="_blank"}

[^Lamer]: [Lamer A, Abou-Arab O, Bourgeois A, Parrot A, Popoff B, Beuscart JB, Tavernier B, Moussa MD. Transforming Anesthesia Data Into the Observational Medical Outcomes Partnership Common Data Model: Development and Usability Study. J Med Internet Res. 2021 Oct 29;23(10):e29259. doi: 10.2196/29259. PMID: 34714250; PMCID: PMC8590192](https://pubmed.ncbi.nlm.nih.gov/34714250/)

[^Paris]: [Paris N, Lamer A, Parrot A. Transformation and Evaluation of the MIMIC Database in the OMOP Common Data Model: Development and Usability Study. JMIR Med Inform. 2021 Dec 14;9(12):e30970. doi: 10.2196/30970. PMID: 34904958](https://pubmed.ncbi.nlm.nih.gov/34904958/){:target="_blank"}
