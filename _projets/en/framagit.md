---
title: Framagit
description: Developers, let's work together
image: /assets/images/projets/framagit.png
show_image: false
une: false
projets_outils: false
projets_collab: true
order_collab: 4
ref: projets-framagit
lang: en
---

Framagit is an instance of GitLab[^gitlab] maintained by the association law 1901 [Framasoft](https://framasoft.org/fr/){:target="_blank"}.

#FramaGit

GitLab is a Git[^git] repository management and software development platform that offers a comprehensive range of tools for the application lifecycle.
As an open source alternative to services like GitHub, GitLab stands out for its multiple instances produced by a large community such as that of the Framasoft association.

For each code repository there is an "Issues" (or "Problems") section.
> Issues in GitLab are task tracking and project management tools. They allow users to report bugs, suggest features, plan improvements, or track any other type of task. Each issue can be detailed with a description, labels, team member assignments, due dates, and can be linked to commits, branches or merge requests. Issues facilitate collaboration and communication within teams by centralizing discussions and providing an overview of current and upcoming work.


# The Framasoft association

Framasoft is a French non-profit association, founded in 2004, dedicated to the promotion of free software, free culture and ethical online services. Continuing for its numerous initiatives aimed at raising public awareness of the importance of privacy protection and digital sovereignty, Framasoft offers a multitude of alternative services to large proprietary platforms.

Among these initiatives, the “Let's ungooglize the Internet” campaign particularly made an impression, by offering free and privacy-friendly alternatives to commonly used services. Framasoft relies on an active and engaged community to develop and maintain its projects, thus offering tools accessible to all for more ethical and autonomous use of the web.

# Presentation of the InterHop repository on Framagit

The source code repository of the InterHop association is available here: [framagit.org/interhop](https://framagit.org/interhop){:target="_blank"}.

Framagit, the GitLab instance made by Framasoft, provides InterHop with a collaborative platform for the development of free software.

The source code of the association's main projects is therefore fully available:
- Interhop.org website: [framagit.org/interhop/interhop-website](https://framagit.org/interhop/interhop-website){:target="_blank"}
- Goupile [framagit.org/interhop/goupile](https://framagit.org/interhop/goupile){:target="_blank"}
- LinkR: [framagit.org/interhop/linkr](https://framagit.org/interhop/linkr){:target="_blank"}

The deployment of our tools on [certified and HDS servers]({{ site.baseurl }}/projets/hds) also requires producing source code which is available on [framagit.org/interhop/hds](https://framagit.org/interhop/hds){:target="_blank"}.

In case of problems or bugs, report us in the "Issues" section of each project !

[^gitlab]: [https://en.wikipedia.org/wiki/GitLab](https://en.wikipedia.org/wiki/GitLab){:target="_blank"}

[^git]: [https://en.wikipedia.org/wiki/Git](https://en.wikipedia.org/wiki/Git){:target="_blank"}
