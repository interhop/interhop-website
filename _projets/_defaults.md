---
title: LinkR
description: "Plateforme collaborative de Data Science en santé"
image: /assets/images/projets/linkr-official.png
show_image: false
une: true
order_une: 2
logiciels: true
order_logiciels: 6
external_name: "En savoir plus sur LinkR"
external_url: https://linkr.interhop.org
ref: projets-linkr
---
