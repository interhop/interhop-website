# Jekyll theme 

Urban theme from [CloudCannon Academy](https://learn.cloudcannon.com/).
- github : https://github.com/CloudCannon/urban-jekyll-template 
- Live Demo : https://teal-worm.cloudvent.net/

## Usage

[Jekyll](https://jekyllrb.com/) version 3.3.1

Install the dependencies with [Bundler](https://bundler.io/):

~~~bash
$ bundle install
~~~

Run `jekyll` commands through Bundler to ensure you're using the right versions:

~~~bash
$ bundle exec jekyll serve
~~~

https://css-tricks.com/separate-form-submit-buttons-go-different-urls/
http://www.fizerkhan.com/blog/posts/Working-with-upcoming-posts-in-Jekyll.html


# More 

## Jekyll
- Cheatsheet : https://devhints.io/jekyll
- Mutltilingual : https://www.sylvaindurand.org/making-jekyll-multilingual/
- Scope : https://jekyllrb.com/docs/configuration/front-matter-defaults/
- Permalink and Slug : https://jekyllrb.com/docs/permalinks/
- redirect Module : https://github.com/jekyll/jekyll-redirect-from
- Collections : https://jekyllrb.com/docs/collections/

## SEO-tag
- https://github.com/jekyll/jekyll-seo-tag/blob/master/docs/usage.md

## Pagination Multi Language
- https://github.com/scandio/jekyll-paginate-multiple
- https://webniyom.com/jekyll-dual-language/
- example : https://github.com/bradonomics/jekyll-dual-language
- https://fcbrossard.net/blog/limits-of-jekyll-categories-and-i18n

## Generator in jekyll
- https://jekyllrb.com/docs/plugins/generators


## Pdf viewer
- https://github.com/MihajloNesic/jekyll-pdf-embed
 
## Online svg converter
- https://image.online-convert.com/fr/convertir-en-svg
- https://www.pngtosvg.com

## Online pixel converter
- http://convert-my-image.com/ImageConverter_Fr
- https://www.resizepixel.com/fr/edit?act=blackwhite

## Free icons
- https://www.flaticon.com
- https://www.iconfinder.com/
- https://healthicons.org/

## gitlab pages deploiment
- https://framacolibri.org/t/gitlab-page-explication-derreur-pipeline/5852/5
- https://docs.framasoft.org/fr/gitlab/gitlab-pages.html
- https://blog.cloudflare.com/secure-and-fast-github-pages-with-cloudflare/

## Get all the gems with their versions
- https://rubygems.org/gems

##  Check links
- https://github.com/gjtorikian/html-proofer
- bundle exec htmlproofer --help

### LocalHost
bundle exec jekyll build  && bundle exec htmlproofer ./_site --assume_extension '.html' --disable-external=true 2> log.txt

### GitLab CI
cf ```.gitlab-ci.yml```
