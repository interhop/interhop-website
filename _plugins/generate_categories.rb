module CategoriesGenerator 
  class CategoryPageGenerator < Jekyll::Generator
    safe true

    def generate(site)
      site.categories.each do |category, posts|
        ["en", "fr"].each do |lang|
          generate_category_page(site, category, lang)
        end
      end
    end

    def generate_category_page(site, category, lang)
      dir = site.config['category_dir'] || 'categories'
      lang_dirs = {
        'fr' => "#{dir}/fr",
        'en' => "#{dir}/en"
      }
      site.pages << CategoryPage.new(site, site.source, lang_dirs[lang], category, lang)
    end
  end

  class CategoryPage < Jekyll::Page
    def initialize(site, base, dir, category, lang)
      @site = site
      @base = base
      @dir = dir
      @name = 'index.html'

      # Transform category name by replacing spaces with hyphens
      slugified_category = category.downcase.gsub(' ', '-').gsub("'",'-')

      self.process(@name)
      self.read_yaml(File.join(base, '_layouts'), 'archive.html')
      self.data['ref'] = 'categories-autogenerate' + slugified_category
      self.data['category'] = category
      self.data['title'] = category.capitalize
      self.data['lang'] = lang
      self.data['permalink'] = generate_permalink(lang, slugified_category)
    end

    def generate_permalink(lang, slugified_category)
      if lang == 'fr'
        "/category/#{slugified_category}/"
      else
        "/#{lang}/category/#{slugified_category}/"
      end
    end

  end
end
